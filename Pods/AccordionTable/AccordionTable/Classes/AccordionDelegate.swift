//
//  AccordionDelegate.swift
//  AccordionTable
//
//  Created by Lucas Paim on 15/08/19.
//

import Foundation

@objc public protocol AccordionDelegate {
    @objc optional func onOpenSection(section : Int)
    @objc optional func onCloseSection(section: Int)
    @objc optional func canOpenSection(section: Int) -> Bool
}
