//
//  Openable.swift
//  AccordionTable
//
//  Created by Lucas Paim on 15/08/19.
//

import Foundation

public enum Openable {
    case open, closed
}
