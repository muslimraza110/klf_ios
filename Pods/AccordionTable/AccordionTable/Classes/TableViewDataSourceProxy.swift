//
//  TableViewDataSourceProxy.swift
//  AccordionTable
//
//  Created by Lucas Paim on 15/08/19.
//

import Foundation


class TableViewDataSourceProxy: NSObject, UITableViewDataSource {
    
    var accordionDataSource: AccordionDataSource
    var tableView: AccordionTableView
    
    init(accordionDataSource: AccordionDataSource, tableView: AccordionTableView) {
        self.accordionDataSource = accordionDataSource
        self.tableView = tableView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return accordionDataSource.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = accordionDataSource.tableView(self.tableView, dataForSection: section)
        return data.status == .open ? data.rows.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return accordionDataSource.tableView(tableView, cellForRow: indexPath.row, data: accordionDataSource.tableView(self.tableView, dataForSection: indexPath.section)
        )
    }
    
}
