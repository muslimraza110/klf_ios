//
//  AccordionTable.swift
//  AccordionTable
//
//  Created by Lucas Paim on 15/08/19.
//

import Foundation
import UIKit


open class AccordionTableView : UITableView {
    
    public weak var accordionDelegate: (AccordionDelegate & UITableViewDelegate)? {
        didSet {
            self.delegate = accordionDelegate
        }
    }
    
    public weak var accordionDatasource: AccordionDataSource? {
        didSet {
            guard let _dataSource = accordionDatasource else { return }
            self.proxyDataSource = TableViewDataSourceProxy(accordionDataSource: _dataSource, tableView: self)
        }
    }
    
    fileprivate var proxyDataSource: TableViewDataSourceProxy? {
        didSet {
            self.dataSource = proxyDataSource
            reloadData()
        }
    }
    
    public func openSection (section: Int) {
        
        if !(accordionDelegate?.canOpenSection?(section: section) ?? true) { return }
        
        guard let _dataSource = accordionDatasource else { return }
        let data =  _dataSource.tableView(self, dataForSection: section)
        
        if data.status == .open || data.rows.count == 0 { return }
        
        accordionDelegate?.onOpenSection?(section: section)
        data.status = .open
        self.beginUpdates()
        self.insertRows(at: (0..<data.rows.count).map {
            IndexPath(row: $0, section: section)
        }, with: .fade)
        self.endUpdates()
    }
    
    public func closeSection (section: Int) {
        guard let _dataSource = accordionDatasource else { return }
        let data =  _dataSource.tableView(self, dataForSection: section)
        
        if data.status == .closed || data.rows.count == 0 { return }

        accordionDelegate?.onCloseSection?(section: section)
        data.status = .closed
        self.beginUpdates()
        self.deleteRows(at: (0..<data.rows.count).map {
            IndexPath(row: $0, section: section)
        }, with: .fade)
        self.endUpdates()
    }
    
    public func toogleSection (section: Int) {
        guard let _dataSource = accordionDatasource else { return }
        let data =  _dataSource.tableView(self, dataForSection: section)
        switch data.status {
        case .open:
            self.closeSection(section: section)
        case .closed:
            self.openSection(section: section)
        }
    }
    
}
