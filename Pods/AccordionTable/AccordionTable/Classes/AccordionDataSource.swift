//
//  AccordionDataSource.swift
//  AccordionTable
//
//  Created by Lucas Paim on 15/08/19.
//

import Foundation

public protocol AccordionDataSource : AnyObject {
    func tableView( _ tableView: AccordionTableView,
                    dataForSection section: Int) -> TableData
    
    func tableView(_ tableView: UITableView,
                   cellForRow row: Int,
                   data: TableData) -> UITableViewCell
    
//    func tableView(_ tableView: UITableView,
//                   headerForData data: TableData) -> AccordionTableHeaderFooterView
    
    func numberOfSections(in tableView: UITableView) -> Int
}
