//
//  TableData.swift
//  AccordionTable
//
//  Created by Lucas Paim on 15/08/19.
//

import Foundation

public class TableData {
    
    public var header: Any
    public var rows: [Any]
    public var status: Openable = .closed
    
    public init(header: Any, rows: [Any],
                status: Openable = .closed) {
        self.header = header
        self.rows = rows
        self.status = status
    }
    
}
