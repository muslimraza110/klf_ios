//
//  AccordionTableHeader.swift
//  AccordionTable
//
//  Created by Lucas Paim on 15/08/19.
//

import Foundation

open class AccordionTableHeaderFooterView : UITableViewHeaderFooterView {
    
    public var section: Int = 0
    
    lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onTouch))
        gesture.numberOfTapsRequired = 1
        return gesture
    }()
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func onTouch() {
        (self.superview as? AccordionTableView)?.toogleSection(section: self.section)
    }
    
}
