# AccordionTable

[![CI Status](https://img.shields.io/travis/lucasmpaim/AccordionTable.svg?style=flat)](https://travis-ci.org/lucasmpaim/AccordionTable)
[![Version](https://img.shields.io/cocoapods/v/AccordionTable.svg?style=flat)](https://cocoapods.org/pods/AccordionTable)
[![License](https://img.shields.io/cocoapods/l/AccordionTable.svg?style=flat)](https://cocoapods.org/pods/AccordionTable)
[![Platform](https://img.shields.io/cocoapods/p/AccordionTable.svg?style=flat)](https://cocoapods.org/pods/AccordionTable)

<img src="https://github.com/lucasmpaim/accordiontable/raw/master/images/example.gif" width="256"/>

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AccordionTable is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AccordionTable'
```

## Author

lucasmpaim, lucasmpaim1@gmail.com

## License

AccordionTable is available under the MIT license. See the LICENSE file for more info.
