
import UIKit
import Alamofire

public enum responseType{
    case get
    case post
    
    var rawValue: HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        }
    }
}



class Api: NSObject {
    
    static let sharedInstance = Api()
    var reachAbility : Reachability!
    
    override init() {
        super.init()
        
        do {
           reachAbility = try Reachability.init()

        } catch _ {
            reachAbility = nil
        }
//        reachAbility = Reachability()
        
       startMonitoring()
    }
    
/*-----------------------------------------------------------------------------------------------------*/


    func sendData(url:String,param:[String:Any]?,method:HTTPMethod,header:HTTPHeaders?,completion: @escaping (_ JSON:NSDictionary,_ status:Bool) -> Void) {
         var encodingType : ParameterEncoding!
        if method == .get {
           encodingType = URLEncoding.default
        }else if method == .post{
           encodingType = JSONEncoding.default
        }


        var stringparams = ""
        if let tmp_param = param {
            for (i, j) in tmp_param  {
stringparams += "\(i): \(j)\n"

            }
        }

        print(stringparams)
        let url = url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
        let triggerMode = UserDefaults.standard.value(forKey: "triggerOffline") as? Bool ?? false
        if !triggerMode {
            debugPrint("URL: \(url)")
            AF.request(url, method: method,parameters: param,encoding: encodingType,headers:header).responseJSON { (response) in

                
                switch response.result {
                    
                   case .success(let value):
                    if let JSON = value as? NSDictionary {
                        completion(JSON, true)
                    } else{
                        completion(["status":"Failed"], true)
                    }

                case .failure( _):
                    completion(["status":"Failed"], true)
                }
//                if let JSON = response.result.value as? NSDictionary {
//                    completion(JSON, true)
//                }else {
//                    completion(["status":"Failed"], true)
//                }
           
            }
            
        }else {
            completion(["":""], false)
            print("Check your internet Connection")
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
        switch reachability.connection {
        case .unavailable:
            UserDefaults.standard.set(true, forKey: "triggerOffline")
            debugPrint("Network became unreachable")
            NotificationCenter.default.post(name: NSNotification.Name("networkUnreachableNotification"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name("popToRootNotification"), object: nil)

        case .wifi:
            UserDefaults.standard.set(false, forKey: "triggerOffline")
            debugPrint("Network reachable through WiFi")
            NotificationCenter.default.post(name: NSNotification.Name("networkReachableNotification"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name("popToRootNotification"), object: nil)
            
        case .cellular:
            UserDefaults.standard.set(false, forKey: "triggerOffline")
            debugPrint("Network reachable through Cellular Data")
            NotificationCenter.default.post(name: NSNotification.Name("networkReachableNotification"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name("popToRootNotification"), object: nil)
            
        case .none:
            break
        }
       
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    /// Starts monitoring the network availability status
    func startMonitoring() {

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: reachAbility)
        do{
            try reachAbility?.startNotifier()
        }catch{
            debugPrint("Could not start reachability notifier")
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
    func stopMonitoring(){
        reachAbility.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged,
                                                  object: reachAbility)
    }
/*-----------------------------------------------------------------------------------------------------*/
}




































//    func getAffiliatesList(url:String,param:[String:Any]?,method:HTTPMethod,completion: @escaping (_ status: Bool) -> Void) {
//
//        let url = url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
//        let param = ["searchcatagory":"admin","page_no":1,"pagesize":1000] as [String : Any]
//        if ((reachAbility!.connection) != .unavailable) {
//            Alamofire.request(url, method: method, parameters: param,headers:nil ).responseJSON { (response) in
//                self.status = false
//                if let JSON = response.result.value as? NSDictionary {
//                    self.status = true
//                    if let data = JSON["data"] as? [[String:Any]] {
//                        for i in data {
//                            self.model.append(Data(data: [i]))
//                        }
//                        self.tableView.reloadData()
//                    }
//                }
//
//            }
//            completion(self.status)
//        }
//    }
