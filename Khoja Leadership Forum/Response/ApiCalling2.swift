//
//  ApiDelegate.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/22/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD

protocol ApiDelegate : AnyObject {
    func successApiCall(data:Data, identifier: String, status: Bool)
    func failureApiCall(message: String, identifier: String)
}

enum ApiCallsMessages: String
{
    case SomethingWentWrongWithResult = "Something went wrong."
    case ParsingError = "Parsing Error!"
    case NoResponseFromServer = "No response from Server!"
    case FailedToCallSuccessFunction = "Failed to call success function."
    case Updated = "Updated!"
    case RequestSentSuccessFully = "Meeting Request sent successfully."
    case MeetingNotesSuccessFully = "Meeting Notes updated successfully."
    case NoRecordsFound = "No record found."

}


class ResponseStatus {
    static let SUCCESS = "success"
    static let FAILURE = "failure"
}


class ApiCalling2
{
    weak var delegate: ApiDelegate?
    
    func loadApiCall(identifier:String, url:String,param:[String:Any]?,method:HTTPMethod,header:HTTPHeaders?, view:UIViewController, showProgress: Bool = true) {
        
        var hud: MBProgressHUD?
        if(showProgress){
            hud = MBProgressHUD.showAdded(to: view.view, animated: true)
            hud?.backgroundView.style = .solidColor
            hud?.backgroundView.color = UIColor(hexString: "#F5F5F5")
            hud?.backgroundView.alpha = 0.5
        }
        
        print(param ?? "No params")

        //view.showSpinner(onView: view.view)
        
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            
            //view.removeSpinner()
            hud?.hide(animated: true)
            
            if status {
                
                do {
                    
                    let _data = convert_DICTIONARY_to_DATA(json: JSON)
                    let jsonDecoder = JSONDecoder()
                    let baseModel = try jsonDecoder.decode(BaseModelClass.self, from: _data! )

                    if baseModel.status == ResponseStatus.SUCCESS
                    {
                        self.delegate?.successApiCall(data: _data!, identifier: identifier, status: status)
                    }
                    else
                    {
                        self.delegate?.failureApiCall(message: baseModel.message ?? ApiCallsMessages.SomethingWentWrongWithResult.rawValue, identifier: identifier)
                    }

               } catch let parsingError {
                    print(parsingError.localizedDescription)
                self.delegate?.failureApiCall(message: ApiCallsMessages.ParsingError.rawValue, identifier: identifier)
               }

            }else {
                self.delegate?.failureApiCall(message: ApiCallsMessages.NoResponseFromServer.rawValue, identifier: identifier)
            }
        }
    }
}
