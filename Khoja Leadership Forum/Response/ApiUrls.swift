
import Foundation

//Forum Urls
// let mainUrl = "https://stgapi.khojaleadershipforum.org/"
//let mainUrl = "http://172.16.200.121:85/KLF/API/klf_api/"

//let mainUrl = "http://172.16.202.171:85/KLF/API/klf_api/"

let mainUrl = "https://api.khojaleadershipforum.org/" // "http://172.16.202.123:73/klf_api/"  staging https://stgapi.khojaleadershipforum.org/
//let mainUrl = "https://09167be53afe.ngrok.io/klf_api/"
//let mainUrl = "http://172.16.202.224:85/KLF/API/klf_api/"
// http://172.16.201.179:73/klf_api/
let mainUrl_Portal = "http://stgportal.khojaleadershipforum.org//"
//let mainUrl_Portal = "https://edf17af7688e.eu.ngrok.io/klf_portal/"

//let mainUrl = "https://stgapi.khojaleadershipforum.org/"
//let mainUrl = "http://154.51.158.13/klf_api/index.php/"
let forumAddTopicUrl = "forums/addsubtopics.json"
let forumMainScreenUrl = "\(mainUrl)forums/gettopics.json"
let deleteMainScreenPost = "\(mainUrl)forums/deletesubforum.json"
let SubTopic = "\(mainUrl)forums/getsubtopicsdetailsv2.json"
let AddSubTopicUrl = "\(mainUrl)forums/addsubtopicsposts.json"
let deleteSubPostUrl = "\(mainUrl)forums/deleteforumpost.json"
let editSubPostUrl = "\(mainUrl)forums/editsubtopicsposts.json"
let eblastUrl = "\(mainUrl)forums/sendeblast.json"
let recipientListUrl = "\(mainUrl)forums/eblastuserlist.json"
let whiteListContact = "\(mainUrl)forums/iswhitelistusers.json"
let whiteListGroup = "\(mainUrl)forums/iswhitelistgroups.json"
let addInContact = "\(mainUrl)forums/addcontacttowhitelist.json"
let addInGroup = "\(mainUrl)forums/addgrouptowhitelist.json"
let removeContact = "\(mainUrl)forums/removecontacttowhitelist.json"
let removeGroup = "\(mainUrl)forums/removegrouptowhitelist.json"
let moderatorListUrl = "\(mainUrl)forums/postmoderator.json?post_id="
let addModerator = "\(mainUrl)forums/addpostmoderator.json"
let removeModerator = "\(mainUrl)forums/removepostmoderator.json"
let forumPost = "\(mainUrl)forums/subtopicspostlistv2.json"
let forumPostLike = "\(mainUrl)forums/forumpostslikesdislikes.json"
let forumPostAddQoute = "\(mainUrl)forums/addquote.json"
let forumPostDelete = "\(mainUrl)forums/deleteforumpost.json"
let forumPostAddComment = "\(mainUrl)forums/addforumpostreply.json"
let forumPostEdit = "\(mainUrl)forums/editforumpost.json"
let forumPostApprove = "\(mainUrl)forums/approvereply.json"
let forumPostFrequency = "\(mainUrl)forums/getfrequency.json"
let forumPostUnsubcribe = "\(mainUrl)forums/unsubscribeforumpost.json"
let forumPostSubcribe = "\(mainUrl)forums/subscribeforumpost.json"

//Login Urls
let loginUrl = "\(mainUrl)users/login.json"
let forgotUrl = "\(mainUrl)users/forgotpassword.json"

//Gorup Urls
let groupListUrl = "\(mainUrl)groups/grouplistbytype.json"
let deleteGroupUrl = "\(mainUrl)groups/deletegroup.json"
let AddGroupUrl = "\(mainUrl)groups/addgroup.json"
let editGroupUrl = "\(mainUrl)groups/editgroup.json"
let listOfMemberUrl = "\(mainUrl)groups/listgroupsmembers.json"
let removeMemberUrl = "\(mainUrl)groups/removeuserfromgroup.json"
let pendingRequestAccessUrl = "\(mainUrl)groups/groupjoiningrequests.json"
let approveRequestUrl = "\(mainUrl)groups/approvegrouprequest.json"
let rejectRequestUrl = "\(mainUrl)groups/removependingrequests.json"
let joinGroupRequestUrl = "\(mainUrl)groups/joingroup.json"
let groupPorjectListUrl = "\(mainUrl)groups/grouprojectlist.json"
let relatedGroupUrl = "\(mainUrl)groups/relatedgroups.json"
let groupProjectListUrl = "\(mainUrl)groups/grouprojectlist.json"
let groupEventUrl = "\(mainUrl)events/listevents.json"
let groupforumPostUrl = "\(mainUrl)groups/listforumspostsgroups.json"
let groupEventsUrl = "\(mainUrl)events/listevents.json"
let allListOfMemberUrl = "\(mainUrl)groups/isgroupmembers.json"
let addMemberInGroupUrl = "\(mainUrl)groups/addmememberinagroup.json"
let removeMemberFromGroupUrl = "\(mainUrl)groups/removeuserfromgroup.json"

//Contact List Urls
let contactListUrl = "\(mainUrl)contact/listcontacts.json"

//Project Urls
let projectListUrl = "\(mainUrl)projects/listallprojects.json"
let deleteProjectUrl = "\(mainUrl)projects/deleteproject.json"
let addProjectUrl = "\(mainUrl)projects/addproject.json"
let editProjectUrl = "\(mainUrl)projects/updateprojectinfo.json"
let charityDropDownUrl = "\(mainUrl)khojacare/charitytype.json"
let audianceDropDownUrl = "\(mainUrl)khojacare/targetaudience.json"
let annualDropDownUrl = "\(mainUrl)khojacare/targetanualspent.json"
let listOfGroupProjectUrl = "\(mainUrl)projects/listgroups.json?project_id="
let removeGroupFromProjectUrl = "\(mainUrl)projects/removegroupfromproject.json"
let addGroupInProjectUrl = "\(mainUrl)projects/addgrouptoproject.json"
let addPercentageUrl = "\(mainUrl)projects/addpercentage.json"
let detailViewProjectUrl = "\(mainUrl)projects/detailproject.json"
let addMemberInProjectUrl = "\(mainUrl)projects/addmembertoproject.json"
let removeMemberFromProjectUrl = "\(mainUrl)projects/removememberfromproject.json"
let projectMemberListUrl = "\(mainUrl)projects/projectmemberlist.json?project_id="
let projectPendingListUrl = "\(mainUrl)projects/projectaccessrequest.json?project_id="
let acceptProjectPendingRequestUrl = "\(mainUrl)projects/acceptprojectrequest.json"
let submitProjectRequestUrl = "\(mainUrl)projects/submitprojectaccessrequest.json"
let cancelProjectRequestUrl = "\(mainUrl)projects/cancelprojectaccessrequest.json"

//News list Urls
let newsListUrl = "\(mainUrl)resources/newslist.json"
let archiveNewsListUrl = "\(mainUrl)resources/addtoarchivenews.json"
let activeNewsListUrl = "\(mainUrl)resources/backtoactiveafterarchived.json"
let addNewsUrl = "\(mainUrl)resources/addposts.json"
let deleteNewsUrl = "\(mainUrl)resources/deleteposts.json"
let editNewsUrl = "\(mainUrl)resources/editposts.json"
let publishNewsUrl = "\(mainUrl)resources/publishedpost.json"
let unPublishNewsUrl = "\(mainUrl)resources/unpublishedpost.json"

//KhojaSummit Urls
let resourceListUrl = "\(mainUrl)resources/viewlistsummit.json"

//Feedback Urls
let feedBackUrl = "\(mainUrl)feedback/addfeedback.json"

//Event Urls
let eventListUrl = "\(mainUrl)events/listevents.json"
let addEventUrl = "\(mainUrl)events/addevents.json"
let editEventUrl = "\(mainUrl)events/editevent.json"
let deleteEventUrl = "\(mainUrl)events/deletevent.json"

//Mission Urls
let missionListUrl = "\(mainUrl)mission/missionstatement.json"

//Faq list Urls
let faqsListUrl = "\(mainUrl)faq/listfaq.json"

//Change Password Urls
let changePasswordUrl = "\(mainUrl)contact/changepassword.json"

//KhojaCare List Urls
let khojaCareListUrl = "\(mainUrl)Khojacare/khojacarelist.json"

//Profile Urls
let completeBioUrl = "\(mainUrl)users/completeuserbio.json"
let viewProfileUrl = "\(mainUrl)users/completeuserbio.json"
let editProfileUrl = "\(mainUrl)users/editprofile.json"

//Dashboard Urls
let dashBoardUrl = "\(mainUrl)home/homecontent.json"
let cancelGroupRequestUrl = "\(mainUrl)groups/cancelgroupjoinrequest.json"
let forumSearchUrl = "\(mainUrl)forums/forumsearch.json?q="

//Initiative Urls
let initiativeListUrl = "\(mainUrl)initiatives/listinitives.json"
let initiativeDetailListUrl = "\(mainUrl)initiatives/initiativedetails.json"
let initiativeGroupListUrl = "\(mainUrl)initiatives/initiativegroups.json"
let addPledgeUrl = "\(mainUrl)initiatives/initiateplege.json"
let listOfAllGroupsUrl = "\(mainUrl)groups/listofallgroups.json"
let makePledgeForEncryptionUrl = "\(mainUrl)users/encrypData.json"
let webSideUrl = "\(mainUrl)pledgenow?ref="
let makePledge = "\(mainUrl)initiatives/submit_initiative_fundraising.json"



//Summit Event Urls

//var newUrl = "https://stgapi.khojaleadershipforum.org/"
//var newURL = "https://5f7ded36834b5c0016b06cd2.mockapi.io/KLF/API/klf_api/"

// ORIGINAL ENDPOINTS
let setSummitEventsRegistration = "\(mainUrl)summitEvents/set_summit_events_registration.json"

let eventSectionUrl = "\(mainUrl)summitEvents/get_summit_events_sections.json"
let currentEventsUrl = "\(mainUrl)summitEvents/get_current_summit_events.json"
let previousEventsUrl = "\(mainUrl)summitEvents/get_previous_summit_events.json"
let eventProgramUrl = "\(mainUrl)summitEvents/get_summit_events_programs.json"
let profileDetailPageUrl = "\(mainUrl)summitEvents/facultyprofilepage.json"
let registrationPageUrl = "\(mainUrl)summitEvents/registration_page.json"
let setSummitEventProgramDetailCheckMyAvailabilityUrl = "\(mainUrl)summitEvents/set_summit_event_program_detail_check_my_availability.json"
let setSummitEventMeetingRequestStatusUrl = "\(mainUrl)summitEvents/set_summit_events_meeting_request.json"
let summitEventsSet_summit_event_breakout_session_registrationURL = "\(mainUrl)summitEvents/set_summit_event_breakout_session_registration.json"
let getSummitEventMeetingRequestToDelegates = "\(mainUrl)summitEvents/get_summit_events_meeting_request_to_delegates.json"

let setSummitEventMeetingRequestToDelegatesUrl = "\(mainUrl)summitEvents/set_summit_events_meeting_request_to_delegates.json"
let deleteMeetingUrl = "\(mainUrl)summitEvents/cancel_summit_events_meeting_request_to_delegates.json"
let updateStatusUrl = "\(mainUrl)summitEvents/update_summit_events_meeting_request_to_delegates_status.json"
let updateNotesUrl = "\(mainUrl)summitEvents/update_summit_events_meeting_request_to_delegates_notes.json"
let updateMeetingEvent = "\(mainUrl)summitEvents/update_summit_events_meeting_delegates.json"
let logoutUser = "\(mainUrl)Users/logout.json"
let initiativePledgeLink = "\(mainUrl_Portal)"
let initiativeLink = "/initiatives/view/"

///summit_events_id:64 (Required)
///user_id (Required)
///summit_events_program_details_id:701 (Optional)
///search:MUSLIM (Optional)
///check_attendee_availibility:true (Optional)
///is_speaker:1 (Optional)
///is_delegate:1 (Optional)
let getSummitEventSpeakerDelegates = "\(mainUrl)summitEvents/get_summit_event_faculty_users.json"


// MOCK API's
//let eventSection = "\(newURL)get_summit_events_sections"
//let currentEvents = "\(newURL)get_current_summit_events"
//let eventProgram = "\(newURL)get_summit_events_programs"
//let profileDetailPage = "\(newURL)facultyprofilepage"

let summitEventNotificationsUrl = "https://5f7ded36834b5c0016b06cd2.mockapi.io/KLF/API/klf_api/summit_event_notifications.json"




//let requestMeetingPeoples = "https://demo2961060.mockable.io/requestMeetingPeoples"
//let peopleDetailPage = "https://demo2961060.mockable.io/people/detail/1"
////let delegateListing = "https://demo2961060.mockable.io/delegateList"
//let delegateListing = "https://5f7ded36834b5c0016b06cd2.mockapi.io/KLF/API/klf_api/delegates"

