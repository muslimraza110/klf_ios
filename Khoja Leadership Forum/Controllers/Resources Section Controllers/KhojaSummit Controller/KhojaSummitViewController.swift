
import UIKit
import Alamofire
import SwiftGifOrigin

class KhojaSummitViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    //MARK:- Btn Outlets
    @IBOutlet weak var summitTableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var imgAdd: UIImageView!

    //MARK:- Custom Function
    var userRole = ""
    var Status = false
    var resourceListModel = [NewsListModel]()
    
    //MARK:- Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()

        registerTableViewCell()
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        UserDefaults.standard.set("4", forKey: "resourceCategory")
       
        self.navigationController?.isNavigationBarHidden = true
        self.summitTableView.delegate = self
        self.summitTableView.dataSource = self
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {

        resourceListApi(url: resourceListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                print("Resource list Appeared")
            }else {
                print("Resource list didn't appear")
            }
            self.loaderView.isHidden = true
            self.summitTableView.reloadData()
        }
        
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        
        if userRole != "A" {
            btnAdd.isHidden = true
            imgAdd.isHidden = true
        }else {
            btnAdd.isHidden = false
            imgAdd.isHidden = false
        }
    }
    
    //MARK:- Button Actions
    @IBAction func addSummitBtn(_ sender: Any) {
        print("Add news button Pressed")
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewsViewController") as? AddNewsViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- CustomFunctions
    func registerTableViewCell ()
    {
        self.summitTableView.register(UINib(nibName: "KhojaSummitTableViewCell", bundle: nil), forCellReuseIdentifier: "SummitCell")
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func resourceListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        resourceListModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.resourceListModel.append(NewsListModel(data: [i]))
                        }
                        print("resourceList data fetched")
                    }else {
                        print("resourceList didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.summitTableView.dequeueReusableCell(withIdentifier: "SummitCell", for: indexPath) as! KhojaSummitTableViewCell
        cell.lblSummitName.text = resourceListModel[indexPath.row].postsName
        if resourceListModel[indexPath.row].postsPublished != "" {
            cell.lblCreatedBy.text = resourceListModel[indexPath.row].postsModified
        }else {
            cell.lblCreatedBy.text = "UnPublished"
        }
        cell.lblCategory.text = ""
        
        return cell
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.summitTableView.deselectRow(at: indexPath, animated: true)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewNewsViewController") as? ViewNewsViewController
        vc?.screenTitle = "Khoja Summit Detail"
        vc?.newListModel = resourceListModel[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 80
        }else{
            return 180
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    //MARK - numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return resourceListModel.count
    }
    
}

