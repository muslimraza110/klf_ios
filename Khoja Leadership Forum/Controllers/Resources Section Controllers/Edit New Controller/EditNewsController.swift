
import UIKit
import Alamofire
import FSCalendar
import SwiftGifOrigin
import RichEditorView

class EditNewsController: UIViewController,UITextFieldDelegate,FSCalendarDelegate,FSCalendarDataSource {

    
    //MARK:- Btn Outlets
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet weak var txtFieldNewsName: TextFieldPadding!
    @IBOutlet weak var txtFieldNewsTopic: TextFieldPadding!
    @IBOutlet weak var txtFieldDOPublication: TextFieldPadding!
    @IBOutlet weak var txtFieldCategory: TextFieldPadding!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var dropDown: UIView!
    @IBOutlet weak var calanderView: UIView!
    @IBOutlet weak var titleScreen: NavigationLabel!
    
    //MARK:- Custom Variables
    var isPublished = ""
    var userId = 0
    var nameFieldFlag = false
    var categoryId = "1"
    var Status = false
    var newsListModel = NewsListModel()
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey: "resourceCategory") as? String == "1" {
           titleScreen.text = "Edit News"
        }else if UserDefaults.standard.value(forKey: "resourceCategory") as? String == "4"{
           titleScreen.text = "Edit Khoja Summit"
        }

        calanderView.isHidden = true
        dropDown.isHidden = true
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        onScreenLoadData()
        
        self.txtFieldNewsName.delegate = self
        self.txtFieldNewsTopic.delegate = self
        self.txtFieldDOPublication.delegate = self
        self.txtFieldCategory.delegate = self
        
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Description"
        
        toolbar.delegate = self
        toolbar.editor = editorView
        hideKeyboard()
        
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let itemDone = RichEditorOptionItem(image: nil, title: "Done") { (toolBar) in
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(itemDone)
        toolbar.options = options
        
        initialLayout ()
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        

    }
     //MARK:- UI Touch Function
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.containerView.endEditing(true)
        let touch = touches.first
        if touch?.view?.tag == 10 {
            calanderView.isHidden = true
        }
        if touch?.view == self.view {
            dropDown.isHidden = true
        }
    }
    
    //MARK:- TextField Delegate Function
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.becomeFirstResponder() {
            dropDown.isHidden = true
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    // TextFiled Delegate Functions when change characters in textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.txtFieldNewsName {
            self.txtFieldNewsName.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        if textField == self.txtFieldNewsTopic {
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else{
            return true
        }
    }
    //MARK:- Button Action
    @IBAction func saveNewsButtonPressed(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if textFieldFormatCheck() && txtFieldNewsTopic.text != "" && editorView.html != "" && txtFieldDOPublication.text != "" {
            print("all mendatory fields are defined")
            let param = ["user_id":userId,"category_id":categoryId,"topic":txtFieldNewsTopic.text!,"name":txtFieldNewsName.text!,"content":editorView.html,"post_id":newsListModel.postsID!,"post_theme_id":"1","published":txtFieldDOPublication.text!] as [String:Any]
            addNewsApi(url: editNewsUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print("News has been edited")
                    for controller in self.navigationController!.viewControllers as Array {
                        if UserDefaults.standard.value(forKey: "resourceCategory") as? String == "1" {
                            if controller is NewsViewController  {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }else if UserDefaults.standard.value(forKey: "resourceCategory") as? String == "4"{
                            if controller is KhojaSummitViewController {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }else {
                    self.showToast(message: "There is problem try again")
                    print("News couldn't edit")
                }
                self.loaderView.isHidden = true
            }
        }
        else{
            showToast(message: "All fields are mandatory")
            print("one or more mendatory fields are empty.")
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func clanderShowBtn(_ sender: Any) {
        calanderView.isHidden = false
        dropDown.isHidden = true
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func showDropDownBtn(_ sender: Any) {
        dropDown.isHidden = false
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func categorySelectionBtn(_ sender: UIButton) {
        if sender.tag == 1 {
            categoryId = "1"
            txtFieldCategory.text = "News"
            dropDown.isHidden = true
        }else if sender.tag == 2 {
            categoryId = "4"
            txtFieldCategory.text = "Khoja"
            dropDown.isHidden = true
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
            
    //MARK:- Custom Functions
    func onScreenLoadData () {
        editorView.html = newsListModel.postsContent ?? ""
        txtFieldDOPublication.text = newsListModel.postsPublished
        txtFieldNewsName.text = newsListModel.postsName
        txtFieldNewsTopic.text = newsListModel.postsTopic
        categoryId = newsListModel.postsCategoryID ?? ""
        if categoryId == "1" {
            txtFieldCategory.text = "News"
        }else if categoryId == "4" {
            txtFieldCategory.text = "Khoja"
        }
     
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func getTodayTimeString() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func addNewsApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func initialLayout () {
        headerHeight()
        self.txtFieldCategory.rightViewMode = .always
        let imageViewForCategory = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldCategory.contentMode = .scaleAspectFit
        imageViewForCategory.image = UIImage(named: "dropdown.png")
        let rightViewForCategory = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForCategory.addSubview(imageViewForCategory)
        self.txtFieldCategory.rightView = rightViewForCategory
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    // textfield format Check function
    func textFieldFormatCheck() -> Bool {
        
        if self.txtFieldNewsName.text?.count == 0 {
            self.txtFieldNewsName.borderColor = .red
            //            self.txtFieldNewsName.becomeFirstResponder()
            nameFieldFlag = false
        }
        else{nameFieldFlag = true}
        
        
        if nameFieldFlag == true {return true}
        else{return false}
    }
        
    //MARK:- FSCalandar Function

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        txtFieldDOPublication.text = "\(self.formatter.string(from: date)) \(getTodayTimeString())"
        print("did select date \(self.formatter.string(from: date))")
        calanderView.isHidden = true
        
    }
    
}


//MARK:- Extensions
extension EditNewsController: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
    
    
}

extension EditNewsController: RichEditorToolbarDelegate {
        
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
}
