
import UIKit
import Alamofire
import SwiftGifOrigin
import SDWebImage

class NewsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var activeNewsButtonOutlet: UIButton!
    @IBOutlet weak var archiveNewsButtonOutlet: UIButton!
    @IBOutlet weak var newsTableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var imgAdd: UIImageView!
    @IBOutlet weak var isRecordfound: UILabel!
    
    //MARK:- Custom Variables
    var userRole = ""
    var Status = false
    var isActive = true
    var newsListModel = [NewsListModel]()
    var activeNewsListModel = [NewsListModel]()
    var archiveNewsListModel = [NewsListModel]()

    //MARK:- Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        headerHeight()
        registerTableViewCell()
        isRecordfound.isHidden = true

        UserDefaults.standard.set("1", forKey: "resourceCategory")
        
        self.loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.newsTableView.delegate = self
        self.newsTableView.dataSource = self
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        
        if userRole != "A" {
            btnAdd.isHidden = true
            imgAdd.isHidden = true
        }else {
             btnAdd.isHidden = false
             imgAdd.isHidden = false
        }
        
    }

/*--------------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {

        newsListApi(url: newsListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                print("news list data updated")
            }else {
                print("news list data coudln't update")
            }
            self.loaderView.isHidden = true
            self.newsTableView.reloadData()
        }
                
    }
    
    //MARK:- Btn Actions
    @IBAction func addNewsBtn(_ sender: Any) {
        print("Add news button Pressed")
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewsViewController") as? AddNewsViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
/*--------------------------------------------------------------------------------------------------*/

    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
        
    }
    
/*--------------------------------------------------------------------------------------------------*/

    @IBAction func activeNewsButtonPressed(_ sender: UIButton) {
        
        self.activeNewsButtonOutlet.isSelected = true
        if self.activeNewsButtonOutlet.isSelected{
            self.activeNewsButtonOutlet.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
            self.archiveNewsButtonOutlet.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
            self.activeNewsButtonOutlet.setTitleColor(.white, for: .normal)
            self.archiveNewsButtonOutlet.setTitleColor(UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0), for: .normal)
            self.archiveNewsButtonOutlet.isSelected = false
        }
        else{}
        
        isActive = true
        newsTableView.reloadData()
        if activeNewsListModel.isEmpty {
            isRecordfound.isHidden = false
        }else{
            isRecordfound.isHidden = true
        }
    }
    
/*--------------------------------------------------------------------------------------------------*/

    @IBAction func archiveNewsButtonPressed(_ sender: UIButton) {
        
        self.archiveNewsButtonOutlet.isSelected = true
        if self.archiveNewsButtonOutlet.isSelected{
            self.archiveNewsButtonOutlet.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
            self.activeNewsButtonOutlet.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
            self.archiveNewsButtonOutlet.setTitleColor(.white, for: .normal)
            self.activeNewsButtonOutlet.setTitleColor(UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0), for: .normal)
            self.activeNewsButtonOutlet.isSelected = false
        }
        else{}
        
        isActive = false
        newsTableView.reloadData()
        if archiveNewsListModel.isEmpty {
            isRecordfound.isHidden = false
        }else{
            isRecordfound.isHidden = true
        }
    }
    
/*--------------------------------------------------------------------------------------------------*/

    @objc func archiveList(sender : UIButton) {
        let url:String?
        let postId:String?
        if isActive{
            postId = activeNewsListModel[sender.tag].postsID
            url = archiveNewsListUrl
        }else {
             postId = archiveNewsListModel[sender.tag].postsID
             url = activeNewsListUrl
        }
        
        let param = ["post_id":postId!] as [String:Any]
        activeOrArchiveList(url: url!, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Post archive")
                self.newsListApi(url: newsListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
                    if status {
                        print("news list data updated")
                    }else {
                        print("news list data coudln't update")
                    }
                    self.loaderView.isHidden = true
                    self.newsTableView.reloadData()
                }
            }else {
                self.loaderView.isHidden = true
                print("Post couldn't archive")
            }
        }
    }
        
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*--------------------------------------------------------------------------------------------------*/

    func registerTableViewCell ()
    {
        self.newsTableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
    }
    
/*--------------------------------------------------------------------------------------------------*/

    func newsListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        newsListModel.removeAll()
        activeNewsListModel.removeAll()
        archiveNewsListModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.newsListModel.append(NewsListModel(data: [i]))
                        }
                     
                        if self.userRole != "A" {
                            for i in self.newsListModel{
                                if i.isPublished == "1" {
                                    if i.postsArchived == "0" {
                                        self.activeNewsListModel.append(i)
                                    }else {
                                        self.archiveNewsListModel.append(i)
                                    }
                                }
                            }
                        }else {
                            for i in self.newsListModel{
                                if i.postsArchived == "0" {
                                    self.activeNewsListModel.append(i)
                                }else {
                                    self.archiveNewsListModel.append(i)
                                }
                            }
                        }
                        
                        
                        
            
                        print("newsListdata fetched")
                    }else {
                        print("newsList didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------*/

    func activeOrArchiveList(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }

    //MARK:- TableView Function
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.newsTableView.deselectRow(at: indexPath, animated: true)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewNewsViewController") as! ViewNewsViewController
        if isActive {
           vc.newListModel = activeNewsListModel[indexPath.row]
            vc.imageURLReceived = activeNewsListModel[indexPath.row].profileImage ?? ""
        }else {
            vc.newListModel = archiveNewsListModel[indexPath.row]
            vc.imageURLReceived = archiveNewsListModel[indexPath.row].profileImage ?? ""
        }
        vc.screenTitle = "News Detail"
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
/*--------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 90
        }else {
            return 200
        }
    }

/*--------------------------------------------------------------------------------------------------*/

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
/*--------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isActive {
            return activeNewsListModel.count
        }
        return archiveNewsListModel.count
    }
    
/*--------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.newsTableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsTableViewCell
        if userRole != "A" {
            cell.activeArchiveButton.isHidden = true
        }else {
            cell.activeArchiveButton.isHidden = false
        }
        
        if isActive {
            cell.lblNewsName.text = activeNewsListModel[indexPath.row].postsTopic
            cell.lblNewsDetail.text = activeNewsListModel[indexPath.row].postsName
//            cell.activeArchiveButton.setImage(UIImage(named: "ic_archived"), for: .normal)
            cell.activeArchiveButton.setBackgroundImage(UIImage(named: "ic_archived_green"), for: .normal)
            cell.activeArchiveButton.tag = indexPath.row
            cell.activeArchiveButton.addTarget(self, action: #selector(self.archiveList), for: .touchUpInside)
            cell.imageViewNews.layer.cornerRadius = cell.imageViewNews.frame.size.width/2
            let remoteImageUrlString = activeNewsListModel[indexPath.row].profileImage ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            
            cell.imageViewNews.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
               print("image \(indexPath.row) loaded")
            })
        }else {
            cell.lblNewsName.text = archiveNewsListModel[indexPath.row].postsTopic
            cell.lblNewsDetail.text = archiveNewsListModel[indexPath.row].postsName
//            cell.activeArchiveButton.setImage(UIImage(named: "ic_active"), for: .normal)
            cell.activeArchiveButton.setBackgroundImage(UIImage(named: "ic_archive"), for: .normal)
            cell.activeArchiveButton.tag = indexPath.row
            cell.activeArchiveButton.addTarget(self, action: #selector(self.archiveList), for: .touchUpInside)
            cell.imageViewNews.layer.cornerRadius = cell.imageViewNews.frame.size.width/2
            let remoteImageUrlString = archiveNewsListModel[indexPath.row].profileImage ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.imageViewNews.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
               print("image \(indexPath.row) loaded")
            })
        }
        return cell
    }
        
/*--------------------------------------------------------------------------------------------------*/

    
}
