
import UIKit
import WebKit
import Alamofire
import SwiftGifOrigin
import SDWebImage

class ViewNewsViewController: UIViewController, WKNavigationDelegate {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var newsWebView: WKWebView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImage: UIImageView!
    @IBOutlet weak var deleteAlertBox: UIView!
    @IBOutlet weak var lblDeleteBox: UILabel!
    @IBOutlet weak var lblDescriptionDeleteBox: UILabel!
    @IBOutlet weak var viewTitle: NavigationLabel!
    @IBOutlet weak var btnPublish: AdaptiveButton!
    @IBOutlet weak var btnEdit: AdaptiveButton!
    @IBOutlet weak var btnDelete: AdaptiveButton!
    @IBOutlet weak var lblTitle: BlueViewLabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var detailViewTopConstraintForPublishBtnActive: NSLayoutConstraint!
    @IBOutlet weak var detailViewTopConstraintForPublishBtnDisable: NSLayoutConstraint!
    
    //MARK:- Custom Variables
    var imageURLReceived = ""
    var isPublished = false
    var Status = false
    var userRole = ""
    var newListModel = NewsListModel()
    var deleteMessage = ""
    var screenTitle = ""
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if imageURLReceived != "" {
            self.profileImageView.sd_setImage(with: URL(string: self.imageURLReceived), completed: nil)

        }
        
        viewTitle.text = screenTitle
        deleteAlertBox.isHidden = true
        headerHeight()
        
        loaderView.isHidden = true
        loaderImage.image = UIImage.gif(name: "loader_gif")
        
        if newListModel.isPublished == "1" {
            isPublished = true
            btnPublish.setTitle("Unpublish Now", for: .normal)
        }else {
            isPublished = false
            btnPublish.setTitle("Publish Now", for: .normal)
        }
        
        onScreenLoadData()
        
        
        if UserDefaults.standard.value(forKey: "resourceCategory") as? String == "1" {
            lblDeleteBox.text = "Delete News"
            lblDescriptionDeleteBox.text = "Are you sure you want to delete news"
            deleteMessage = "News"
        }else if UserDefaults.standard.value(forKey: "resourceCategory") as? String == "4"{
            lblDeleteBox.text = "Delete Khoja Summit Post"
            lblDescriptionDeleteBox.text = "Are you sure you want to delete khoja summit post"
            deleteMessage = "Khoja summit post"
        }
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        
        if userRole != "A" {
            btnPublish.isHidden = true
            btnDelete.isHidden = true
            btnEdit.isHidden = true
            detailViewTopConstraintForPublishBtnActive.isActive = false
            detailViewTopConstraintForPublishBtnDisable.isActive = true
        }else {
            detailViewTopConstraintForPublishBtnActive.isActive = true
            detailViewTopConstraintForPublishBtnDisable.isActive = false
            btnPublish.isHidden = false
            btnDelete.isHidden = false
            btnEdit.isHidden = false
        }
        
    }

    //MARK:- Button Actions
    @IBAction func hideDeleteBoxBtn(_ sender: Any) {
        deleteAlertBox.isHidden = true
    }
    
/*-------------------------------------------------------------------------------------------------------*/

    @IBAction func deletePostBtn(_ sender: Any) {
        print("delete button pressed")
        self.deleteAlertBox.isHidden = true
        let param = ["post_id":newListModel.postsID!] as [String:Any]
        deleteNewsApi(url: deleteNewsUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("news deleted")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.7) {
                     self.navigationController?.popViewController(animated: true)
                }
                self.showToast(message: self.deleteMessage + " has been deleted")
            }else {
                self.showToast(message: self.deleteMessage + " has been deleted")
                print("news couldn't delete")
            }
            self.loaderView.isHidden = true
        }
    }
    
/*-------------------------------------------------------------------------------------------------------*/

    @IBAction func btnPublishUnPublish(_ sender: Any) {
        let id = newListModel.postsID
        let param = ["post_id":id ?? ""] as [String:Any]
        if isPublished {
            deleteNewsApi(url: unPublishNewsUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status{
                    self.isPublished = false
                    self.btnPublish.setTitle("Publish Now", for: .normal)
                    print("unpublished")
                }else {
                    print("couldn't unpublish")
                }
                self.loaderView.isHidden = true
            }
        }else {
            deleteNewsApi(url: publishNewsUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status{
                    self.isPublished = true
                    self.btnPublish.setTitle("Unpublish Now", for: .normal)
                    print("published")
                }else {
                    print("couldn't publish")
                }
                self.loaderView.isHidden = true
            }
        }
       
    }
    
/*-------------------------------------------------------------------------------------------------------*/

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-------------------------------------------------------------------------------------------------------*/

    @IBAction func editButtonPressed(_ sender: UIButton) {
        print("edit button pressed")
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditNewsController") as! EditNewsController
        vc.newsListModel = newListModel
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
/*-------------------------------------------------------------------------------------------------------*/

    @IBAction func deleteButtonPressed(_ sender: UIButton) {
       deleteAlertBox.isHidden = false
        
    }
    
/*-------------------------------------------------------------------------------------------------------*/

    @IBAction func pubUnpubButtonPressed(_ sender: UIButton) {
    }

    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-------------------------------------------------------------------------------------------------------*/

    func onScreenLoadData () {
        
        self.newsWebView.navigationDelegate = self
        var fontSize : CGFloat = 0.0
        if UIDevice().userInterfaceIdiom == .phone {
            fontSize = (32 * (self.view.frame.width / 320))
        }else {
            fontSize = 40
        }
        let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
        self.newsWebView.loadHTMLString(fontSetting + (newListModel.postsContent ?? ""), baseURL: nil)
        
        lblTitle.text = newListModel.postsName
    
    }
    
/*-------------------------------------------------------------------------------------------------------*/

    func deleteNewsApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-------------------------------------------------------------------------------------------------------*/
    
}
