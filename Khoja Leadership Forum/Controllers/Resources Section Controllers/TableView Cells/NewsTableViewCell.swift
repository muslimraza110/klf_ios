
import UIKit

class NewsTableViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var activeArchiveButton: UIButton!
    @IBOutlet weak var lblNewsName: UILabel!
    @IBOutlet weak var lblNewsDetail: UILabel!
    @IBOutlet weak var imageViewNews: UIImageView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
