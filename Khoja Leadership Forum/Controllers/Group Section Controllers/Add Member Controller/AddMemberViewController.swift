
import UIKit
import Alamofire
import SwiftGifOrigin
import SDWebImage

class AddMemberViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    //MARK:- Btn Outlets
    @IBOutlet weak var leftInnerViewSearchBar: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var addMemberTableView: UITableView!
    @IBOutlet weak var doneButtonOutlet: UIButton!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var isRecordFound: UILabel!
    
    //MARK:- Custom Variables
    let groupIndexTitles = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    var Status = false
    var isSearching = false
    var groupId = ""
    var listOfMember = [listOfAllMemberModel]()
    var filtered = [listOfAllMemberModel]()
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loaderView.isHidden = true
        isRecordFound.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        registerTableViewCell()
        headerHeight()
        self.searchView.layer.cornerRadius = 20
        self.leftInnerViewSearchBar.layer.cornerRadius = 20
        loadApi()
        self.addMemberTableView.delegate = self
        self.addMemberTableView.dataSource = self
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        hideKeyboard()
        searchTextField.returnKeyType = UIReturnKeyType.done
        // Do any additional setup after loading the view.
    }
/*--------------------------------------------------------------------------------------------*/
    //MARK - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
       

    }
    
    //MARK:- Touch Delegate Method
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK:- TextField Delegate Methods
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil  {
            
            isSearching = false
            isRecordFound.isHidden = true
            searchTextField.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            addMemberTableView.reloadData()
        }else {
            isSearching = true
            let searchText  = textField.text
            filtered = listOfMember.filter{
                $0.firstName!.localizedCaseInsensitiveContains(String(searchText!))
            }
            for i in filtered {
                print(i.firstName)
            }
            if filtered.count == 0 {
                isRecordFound.isHidden = false
            }else {
                isRecordFound.isHidden = true
            }
            addMemberTableView.reloadData()
        }
        
    }
/*--------------------------------------------------------------------------------------------*/
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK:- Btn Actions
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------*/

    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
    }
    
/*--------------------------------------------------------------------------------------------*/

    @objc func addRemoveMemberBtn(sender : UIButton) {
      
       if isSearching {
           searchTextField.text = ""
           isSearching = false
       }
       if sender.isSelected{
           var userId = ""
           if isSearching{
               userId = filtered[sender.tag].id ?? ""
           }else {
               userId = listOfMember[sender.tag].id ?? ""
           }
           let param = ["group_id":groupId,"user_id":userId] as [String : Any]
           addRemoveMember(url: removeMemberFromGroupUrl, param: param, method: responseType.post.rawValue, headers: headerToken) { (status) in
               if status{
                   self.listOfMember.removeAll()
                   self.getAllMemberList(url: allListOfMemberUrl, method: responseType.get.rawValue, headers: headerToken) { (status) in
                       if status {
                           print("member list has been reloaded")
                       }else {
                           print("member list couldn't load")
                       }
                       self.addMemberTableView.reloadData()
                       self.loaderView.isHidden = true
                   }
                   print("member has been removed in list")
               }else {
                   print("member couldn't remove from list")
                   self.loaderView.isHidden = true
               }
           }
           sender.isSelected = false
       }else{
           var id = ""
           if isSearching{
               id = filtered[sender.tag].id ?? ""
           }else {
               id = listOfMember[sender.tag].id ?? ""
           }
           let param = ["group_id":groupId,"user_id":id] as [String : Any]
           addRemoveMember(url: addMemberInGroupUrl, param: param, method: responseType.post.rawValue, headers: headerToken) { (status) in
               if status{
                   self.listOfMember.removeAll()
                   self.getAllMemberList(url: allListOfMemberUrl, method: responseType.get.rawValue, headers: headerToken) { (status) in
                       if status {
                           print("member list has been reloaded")
                       }else {
                           print("member list couldn't load")
                       }
                       self.addMemberTableView.reloadData()
                       self.loaderView.isHidden = true
                   }
                   print("member has been added in list")
               }else {
                   print("member couldn't add in list")
                   self.loaderView.isHidden = true
               }
           }
           sender.isSelected = true
        }
    }
        
/*--------------------------------------------------------------------------------------------*/

    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func loadApi(){
        getAllMemberList(url: allListOfMemberUrl, method: responseType.get.rawValue, headers: headerToken) { (status) in
            if status {
                print("Moderator list has been come")
                self.addMemberTableView.reloadData()
            }else {
                print("Moderator list couldn't load")
            }
            self.loaderView.isHidden = true
        }
        
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func getAllMemberList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        listOfMember.removeAll()
        let param = ["group_id":groupId] as [String:Any]
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headers) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "ok" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.listOfMember.append(listOfAllMemberModel(data: [i]))
                        }
                        self.listOfMember = self.listOfMember.sorted(by: { ($0.firstName as String?) ?? "" < ($1.firstName as String?) ?? ""})
                    }else {
                        print("data didn't load")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func addRemoveMember(url:String,param:[String:Any],method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headers) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func registerTableViewCell ()
    {
        self.addMemberTableView.register(UINib(nibName: "AddMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "AddMemberTableViewCell")
    }
    
    //MARK:- TableView Delegate and DataSource Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.addMemberTableView.deselectRow(at: indexPath, animated: true)
    }
    
/*--------------------------------------------------------------------------------------------*/

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

/*--------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
/*--------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filtered.count
        }
        return listOfMember.count
    }
    
/*--------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.addMemberTableView.dequeueReusableCell(withIdentifier: "AddMemberTableViewCell", for: indexPath) as! AddMemberTableViewCell
        if isSearching{
            
            let firstname = filtered[indexPath.row].firstName
            let lastname = filtered[indexPath.row].lastName
            cell.lblMemberName.text = "\(firstname ?? "") \(lastname ?? "")"

            let isModerator = filtered[indexPath.row].ismember
            if isModerator == 1 {
                cell.checkBoxButtonOutlet.isSelected = true
            }else {
                cell.checkBoxButtonOutlet.isSelected = false
            }
            cell.checkBoxButtonOutlet.tag = indexPath.row
            cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveMemberBtn), for: .touchUpInside)
            
            cell.memberImageView.layer.cornerRadius = cell.memberImageView.frame.width/2
            let remoteImageUrlString = filtered[indexPath.row].profileImage ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.memberImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
            
        }else {
            
            let firstname = listOfMember[indexPath.row].firstName
            let lastname = listOfMember[indexPath.row].lastName
            cell.lblMemberName.text = "\(firstname ?? "") \(lastname ?? "")"

            let isModerator = listOfMember[indexPath.row].ismember
            if isModerator == 1 {
                cell.checkBoxButtonOutlet.isSelected = true
            }else {
                cell.checkBoxButtonOutlet.isSelected = false
            }
            cell.checkBoxButtonOutlet.tag = indexPath.row
            cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveMemberBtn), for: .touchUpInside)
            
            cell.memberImageView.layer.cornerRadius = cell.memberImageView.frame.width/2
            let remoteImageUrlString = listOfMember[indexPath.row].profileImage ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.memberImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
        }
        return cell
    }
    
/*--------------------------------------------------------------------------------------------*/

}
