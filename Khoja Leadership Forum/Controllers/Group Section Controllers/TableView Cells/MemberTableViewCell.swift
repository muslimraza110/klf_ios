
import UIKit

class MemberTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblMemberName: UILabel!
    @IBOutlet weak var removerMember: UIButton!
    @IBOutlet weak var imageViewUser: UIImageView!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
