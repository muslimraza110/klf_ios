
import UIKit

//protocol MainGroupScreenTableViewCellDelegate{
//    func didTapEditGroup()
//    func didTapDeleteGroup()
//
//}
/*-----------------------------------------------------------------------------------------------------------------------------------*/
class MainGroupScreenTableViewCell: UITableViewCell {

    //Outlet

    @IBOutlet weak var groupIconImageView: UIImageView!
    @IBOutlet weak var groupTitleLable: UILabel!
    @IBOutlet weak var groupDescriptionLable: UILabel!
    @IBOutlet weak var editGroupButton: UIButton!
    @IBOutlet weak var deleteGroupButton: UIButton!
    @IBOutlet weak var editGroupImg: UIImageView!
    @IBOutlet weak var deleteGroupImg: UIImageView!
    
//    var groupItem: Group!
//    var delegate: MainGroupScreenTableViewCellDelegate?
/*-----------------------------------------------------------------------------------------------------------------------------------*/
//    func setGroup(group: Group){
//        groupItem = group
//        groupTitleLable.text = group.groupName
//        groupDescriptionLable.text = group.groupDescription
//    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func groupEditButtonPressed(_ sender: UIButton) {
//        print("group edit button pressed")
//        delegate?.didTapEditGroup()
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func groupDeleteButtonPressed(_ sender: UIButton) {
//        print("group delete button pressed")
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
}

