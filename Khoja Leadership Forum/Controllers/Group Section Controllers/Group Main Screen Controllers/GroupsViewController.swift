
import UIKit
import Alamofire
import SwiftGifOrigin
import SDWebImage

class GroupsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIScrollViewDelegate {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var leftInnerViewSearchBar: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var businessGroupButtonOutlet: UIButton!
    @IBOutlet weak var charityGroupButtonOutlet: UIButton!
    @IBOutlet weak var groupsTableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var deleteAlert: UIView!
    @IBOutlet weak var isRecordFound: UILabel!
    
    //MARK:- Custom Variables
    var sectionGroupModel = [String: [GroupModel]]()
    var groupIndexTitles = [String]()
    var groupModel = [GroupModel]()
    var filtered = [GroupModel]()
    var filteredIndexTitle = [String]()
    var filteredGroupModel = [String: [GroupModel]]()
    var groupId = 0
    var Status = false
    var createdByUserId = ""
    var isSearching = false
    var groupType = "B"
    var userRole = ""
    var tableSectionName = 0
    
    //MARK:- Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        groupsTableView.estimatedRowHeight = 100.0
        groupsTableView.rowHeight = UITableView.automaticDimension
        groupsTableView.sectionIndexBackgroundColor = .white
        searchTextField.delegate = self
        hideKeyboard()
        headerHeight()
        isRecordFound.isHidden = true
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.navigationController?.isNavigationBarHidden = true
        registerTableViewCell()
        
        self.searchView.layer.cornerRadius = 20
        self.leftInnerViewSearchBar.layer.cornerRadius = 20
        
        self.groupsTableView.delegate = self
        self.groupsTableView.dataSource = self
        searchTextField.returnKeyType = UIReturnKeyType.done
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)),for: UIControl.Event.editingChanged)

    }
    
/*----------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {

        deleteAlert.isHidden = true
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        createdByUserId = String(userData["id"] as! Int)
        let param = ["type":groupType,"page_no":"1","pagesize":"1000"] as [String:Any]
        groupData(url: groupListUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("gruop data updated")
            }else {
                print("gruop data didn't update")
            }
            self.loaderView.isHidden = true
            self.groupsTableView.reloadData()
        }
        
    }
    
    //MARK:- ScrollView Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == groupsTableView {
            businessGroupButtonOutlet.isUserInteractionEnabled = false
            charityGroupButtonOutlet.isUserInteractionEnabled = false
            print("disable")
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == groupsTableView {
            businessGroupButtonOutlet.isUserInteractionEnabled = true
            charityGroupButtonOutlet.isUserInteractionEnabled = true
            print("enable")
        }
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        print("enable")
    }
   
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("enable")
    }
   
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        print("enable")
    }
    
    //MARK:- Textfield delegate Methods
    @objc func textFieldDidChange(_ textField: UITextField) {

         if textField.text == "" || textField.text == nil  {
            isSearching = false
            isRecordFound.isHidden = true
            textField.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            groupsTableView.reloadData()
         }else {
            filteredIndexTitle.removeAll()
            filteredGroupModel.removeAll()
            isSearching = true
            let searchText  = textField.text
            filtered = groupModel.filter{
                $0.groupsName!.localizedCaseInsensitiveContains(String(searchText!))
            }
                //Using for alphabet seraches
            for i in self.filtered {
                let firstIndexOfTitle = String(i.groupsName!.prefix(1).uppercased())
                if var carValues = self.filteredGroupModel[firstIndexOfTitle] {
                    carValues.append(i)
                    self.filteredGroupModel[firstIndexOfTitle] = carValues
                } else {
                    self.filteredGroupModel[firstIndexOfTitle] = [i]
                }
                }
                self.filteredIndexTitle = [String](self.filteredGroupModel.keys)
                self.filteredIndexTitle = self.filteredIndexTitle.sorted(by: { $0 < $1 })
            
            if filteredIndexTitle.count == 0 {
                isRecordFound.isHidden = false
            }else {
                isRecordFound.isHidden = true
            }
            groupsTableView.reloadData()
        }

    }

/*----------------------------------------------------------------------------------*/

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK:- Btn Actions
    @IBAction func addgroupBtn(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddGroupViewController") as? AddGroupViewController
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
/*-------------------------------------------------------------------------------------------*/
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)

    }
    
/*-------------------------------------------------------------------------------------------*/
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
    }
    
/*-------------------------------------------------------------------------------------------*/

    //MARK - businessGroupButtonPressed
    @IBAction func businessGroupButtonPressed(_ sender: UIButton) {
        groupType = "B"
        self.businessGroupButtonOutlet.isSelected = true
        if self.businessGroupButtonOutlet.isSelected{
            self.businessGroupButtonOutlet.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
            self.charityGroupButtonOutlet.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
            self.businessGroupButtonOutlet.setTitleColor(.white, for: .normal)
            self.charityGroupButtonOutlet.setTitleColor(UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0), for: .normal)
            self.charityGroupButtonOutlet.isSelected = false
            
            //API call to fetch the data and load the tableview
            let param = ["type":groupType,"page_no":"1","pagesize":"1000"] as [String:Any]
            groupData(url: groupListUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print(" Bussiness gruop data updated")
                }else {
                    print("Bussiness gruop data didn't update")
                }
                self.loaderView.isHidden = true
                self.groupsTableView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.businessGroupButtonOutlet.isUserInteractionEnabled = true
                    self.charityGroupButtonOutlet.isUserInteractionEnabled = true
                }
            }
            
        }
        else{}
    }

/*-----------------------------------------------------------------------------------------*/
    
    @IBAction func charityGroupButtonPressed(_ sender: UIButton) {
        groupType = "C"
        self.charityGroupButtonOutlet.isSelected = true
        if self.charityGroupButtonOutlet.isSelected{
            self.charityGroupButtonOutlet.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
            self.businessGroupButtonOutlet.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
            self.charityGroupButtonOutlet.setTitleColor(.white, for: .normal)
            self.businessGroupButtonOutlet.setTitleColor(UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0), for: .normal)
            self.businessGroupButtonOutlet.isSelected = false
            
            //API call to fetch the data and load the tableview
            let param = ["type":groupType,"page_no":"1","pagesize":"1000"] as [String:Any]
            groupData(url: groupListUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print("Charity gruop data updated")
                }else {
                    print("Charity gruop data didn't update")
                }
                self.loaderView.isHidden = true
                self.groupsTableView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.businessGroupButtonOutlet.isUserInteractionEnabled = true
                    self.charityGroupButtonOutlet.isUserInteractionEnabled = true
                }
            }
        }
        else{}
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func hideDeleteBtn(_ sender: Any) {
        deleteAlert.isHidden = true
    }
    
/*-------------------------------------------------------------------------------------------*/
    
    @IBAction func deleteGroupBtn(_ sender: Any) {
        if isSearching {
            searchTextField.text = ""
            isSearching = false
        }
        deleteAlert.isHidden = true
        let param = ["group_id":groupId] as [String:Any]
        deleteGroup(url: deleteGroupUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Group is deleting")
                let param = ["type":self.groupType,"page_no":"1","pagesize":"1000"] as [String:Any]
                self.groupData(url: groupListUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                    if status {
                        print("New gruop data updated")
                    }else {
                        print("New gruop data didn't update")
                    }
                    self.loaderView.isHidden = true
                    self.groupsTableView.reloadData()
                }
            }else {
                print("Group couldn't delete")
            }
        }
    }
    
/*------------------------------------------------------------------------------------------------*/

    @objc func deleteGroupData(sender : UIButton) {
        deleteAlert.isHidden = false
        groupId = sender.tag
        
    }
        
/*-------------------------------------------------------------------------------------------------*/

    @objc func editGroupData(sender : UIButton) {
        for i in groupModel{
            if sender.tag == Int(i.groupsID!) {
                print ("sender is \(sender.tag)  group id is \(i.groupsID!)")
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditGroupViewController") as! EditGroupViewController
                vc.editGroupModel = i
                self.navigationController?.pushViewController(vc, animated: true)
                
                break
            }
        }
    }
        
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func groupData(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        groupModel.removeAll()
        groupIndexTitles.removeAll()
        sectionGroupModel.removeAll()
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.groupModel.append(GroupModel(data: [i]))
                        }
                        /*---------------------------------------------------------*/
                        //Using for alphabet seraches
                        
                        if self.userRole == "A" {
                            
                            for i in self.groupModel {
                                let firstIndexOfTitle = String(i.groupsName!.prefix(1).uppercased())
                                if var carValues = self.sectionGroupModel[firstIndexOfTitle] {
                                    carValues.append(i)
                                    self.sectionGroupModel[firstIndexOfTitle] = carValues
                                } else {
                                    self.sectionGroupModel[firstIndexOfTitle] = [i]
                                }
                            }
                            
                            self.groupIndexTitles = [String](self.sectionGroupModel.keys)
                            self.groupIndexTitles = self.groupIndexTitles.sorted(by: { $0 < $1 })
                            
                        }else{
                            for i in self.groupModel {
                                if i.groupsHidden != "1" || i.groupsPassive != "1" || i.groupsPending != "1" {
                                    let firstIndexOfTitle = String(i.groupsName!.prefix(1).uppercased())
                                    if var carValues = self.sectionGroupModel[firstIndexOfTitle] {
                                        carValues.append(i)
                                        self.sectionGroupModel[firstIndexOfTitle] = carValues
                                    } else {
                                        self.sectionGroupModel[firstIndexOfTitle] = [i]
                                    }
                                }
                                
                                self.groupIndexTitles = [String](self.sectionGroupModel.keys)
                                self.groupIndexTitles = self.groupIndexTitles.sorted(by: { $0 < $1 })
                            }
                            
                        }
                      /*---------------------------------------------------------*/
                        print("group data fetched")
                    }else {
                         print("group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------*/
    
    func deleteGroup(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                print("internet is unavailable")
            }
        }
    }
  
/*---------------------------------------------------------------------------------------*/
    
    func registerTableViewCell ()
    {
        self.groupsTableView.register(UINib(nibName: "MainGroupScreenTableViewCell", bundle: nil), forCellReuseIdentifier: "groupCell")
    }
        
    //MARK:- Touch Delegates
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.groupsTableView.deselectRow(at: indexPath, animated: true)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewGroupViewController") as! ViewGroupViewController
        if isSearching{
            let character = filteredIndexTitle[indexPath.section]
            if let groupObject = filteredGroupModel[character] {
                if createdByUserId == groupObject[indexPath.row].createdByUserID {
                    vc.isGroupOwnwe = true
                }else {
                    vc.isGroupOwnwe = false
                }
                vc.groupsId = groupObject[indexPath.row].groupsID ?? ""
                vc.aboutUs = groupObject[indexPath.row].groupsBio ?? ""
                vc.address = "\(groupObject[indexPath.row].groupsAddress ?? "") \(groupObject[indexPath.row].groupsAddress2 ?? ""), \(groupObject[indexPath.row].groupsCity ?? "") \(groupObject[indexPath.row].groupsRegion ?? ""), \(groupObject[indexPath.row].groupsCountry ?? "")"
                vc.email = groupObject[indexPath.row].groupsEmail ?? ""
                vc.phoneNumber = groupObject[indexPath.row].groupsPhone ?? ""
                vc.groupName = groupObject[indexPath.row].groupsName ?? ""
                vc.groupDescriptionName = groupObject[indexPath.row].groupsDescription ?? ""
                vc.profileImage = groupObject[indexPath.row].groupImage ?? ""
            }
        }else {
            let character = groupIndexTitles[indexPath.section]
            if let groupObject = sectionGroupModel[character] {
                if createdByUserId == groupObject[indexPath.row].createdByUserID {
                    vc.isGroupOwnwe = true
                }else {
                    vc.isGroupOwnwe = false
                }
                vc.groupsId = groupObject[indexPath.row].groupsID ?? ""
                vc.aboutUs = groupObject[indexPath.row].groupsBio ?? ""
                vc.address = "\(groupObject[indexPath.row].groupsAddress ?? "") \(groupObject[indexPath.row].groupsAddress2 ?? ""), \(groupObject[indexPath.row].groupsCity ?? "") \(groupObject[indexPath.row].groupsRegion ?? ""), \(groupObject[indexPath.row].groupsCountry ?? "")"
                vc.email = groupObject[indexPath.row].groupsEmail ?? ""
                vc.phoneNumber = groupObject[indexPath.row].groupsPhone ?? ""
                vc.groupName = groupObject[indexPath.row].groupsName ?? ""
                vc.groupDescriptionName = groupObject[indexPath.row].groupsDescription ?? ""
                vc.profileImage = groupObject[indexPath.row].groupImage ?? ""
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------*/

    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearching {
            return filteredIndexTitle.count
//            return 1
        }
        return groupIndexTitles.count
    }

/*-------------------------------------------------------------------------------------------------*/
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isSearching {
            return filteredIndexTitle
//            return [""]
        }
        return groupIndexTitles
    }
/*-----------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isSearching {
            return filteredIndexTitle[section]
//            return ""
        }
        return groupIndexTitles[section]
    }
    
/*------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            let character = filteredIndexTitle[section]
            if let carValues = filteredGroupModel[character] {
                return carValues.count
            }
//            return filtered.count
        }
        let character = groupIndexTitles[section]
        if let carValues = sectionGroupModel[character] {
            return carValues.count
        }
        
        return 0
    }
    
/*-----------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupCell", for: indexPath) as! MainGroupScreenTableViewCell
        if isSearching{
            let character = filteredIndexTitle[indexPath.section]
            if let groupObject = filteredGroupModel[character] {
                cell.groupTitleLable.text = groupObject[indexPath.row].groupsName
                cell.groupDescriptionLable.text = groupObject[indexPath.row].groupsDescription
                cell.groupIconImageView.layer.cornerRadius = cell.groupIconImageView.frame.width/2
                cell.groupIconImageView.sd_setImage(with: URL(string: groupObject[indexPath.row].groupImage ?? ""), completed: nil)
                if userRole == "A" {
                    cell.deleteGroupButton.tag = Int(groupObject[indexPath.row].groupsID!)!
                    cell.editGroupButton.tag = Int(groupObject[indexPath.row].groupsID!)!
                    cell.editGroupButton.addTarget(self, action: #selector(self.editGroupData), for: .touchUpInside)
                    cell.deleteGroupButton.addTarget(self, action: #selector(self.deleteGroupData), for: .touchUpInside)
                }else {
                    if createdByUserId == groupObject[indexPath.row].createdByUserID {
                        cell.editGroupButton.isHidden = false
                        cell.editGroupImg.isHidden = false
                        cell.deleteGroupButton.isHidden = false
                        cell.deleteGroupImg.isHidden = false
                        cell.deleteGroupButton.tag = Int(groupObject[indexPath.row].groupsID!)!
                        cell.editGroupButton.tag = Int(groupObject[indexPath.row].groupsID!)!
                        cell.editGroupButton.addTarget(self, action: #selector(self.editGroupData), for: .touchUpInside)
                        cell.deleteGroupButton.addTarget(self, action: #selector(self.deleteGroupData), for: .touchUpInside)
                    }else {
                        cell.editGroupButton.isHidden = true
                        cell.deleteGroupButton.isHidden = true
                        cell.deleteGroupImg.isHidden = true
                        cell.editGroupImg.isHidden = true
                    }
                }
            }
        }else {
            
            let character = groupIndexTitles[indexPath.section]
            if let groupObject = sectionGroupModel[character] {
                cell.groupTitleLable.text = groupObject[indexPath.row].groupsName
                cell.groupDescriptionLable.text = groupObject[indexPath.row].groupsDescription
                cell.groupIconImageView.layer.cornerRadius = cell.groupIconImageView.frame.width/2
                cell.groupIconImageView.sd_setImage(with: URL(string: groupObject[indexPath.row].groupImage ?? ""), completed: nil)
                if userRole == "A" {
                    cell.deleteGroupButton.tag = Int(groupObject[indexPath.row].groupsID!)!
                    cell.editGroupButton.tag = Int(groupObject[indexPath.row].groupsID!)!
                    cell.editGroupButton.addTarget(self, action: #selector(self.editGroupData), for: .touchUpInside)
                    cell.deleteGroupButton.addTarget(self, action: #selector(self.deleteGroupData), for: .touchUpInside)
                }else {
                    if createdByUserId == groupObject[indexPath.row].createdByUserID {
                         cell.editGroupButton.isHidden = false
                       cell.editGroupImg.isHidden = false
                       cell.deleteGroupButton.isHidden = false
                       cell.deleteGroupImg.isHidden = false
                        cell.deleteGroupButton.tag = Int(groupObject[indexPath.row].groupsID!)!
                        cell.editGroupButton.tag = Int(groupObject[indexPath.row].groupsID!)!
                        cell.editGroupButton.addTarget(self, action: #selector(self.editGroupData), for: .touchUpInside)
                        cell.deleteGroupButton.addTarget(self, action: #selector(self.deleteGroupData), for: .touchUpInside)
                    }else {
                         cell.editGroupButton.isHidden = true
                           cell.deleteGroupButton.isHidden = true
                           cell.deleteGroupImg.isHidden = true
                           cell.editGroupImg.isHidden = true
                    }
                }
               
            }
        }
        return cell
    }
    

    
}

