
import UIKit
import Alamofire
import SwiftGifOrigin
import WebKit
import SDWebImage
import MessageUI

class ViewGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var groupMemberListTableView: UITableView!
    @IBOutlet weak var groupProjectTableView: UITableView!
    @IBOutlet weak var groupEventsTableView: UITableView!
    @IBOutlet weak var groupRelatedTableView: UITableView!
    @IBOutlet weak var groupForumTableView: UITableView!
    @IBOutlet weak var pendingAccessRequestTableView: UITableView!
    @IBOutlet weak var btnEmail: AdaptiveButton!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAboutUs: UILabel!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblGroupDescription: UILabel!
    @IBOutlet weak var pendingHeaderView: UIView!
    @IBOutlet weak var groupProfileImageView: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var about: WKWebView!
    @IBOutlet weak var isPendingDataFound: UILabel!
    @IBOutlet weak var isMemberFound: UILabel!
    @IBOutlet weak var isGroupProjectFound: UILabel!
    @IBOutlet weak var isGroupEventFound: UILabel!
    @IBOutlet weak var isRelatedGroupFound: UILabel!
    @IBOutlet weak var isForumPostFound: UILabel!
    @IBOutlet weak var btnAddProject: UIButton!
    @IBOutlet weak var btnJoin: UIButton!
    
    //MARK:- Custom Variables
    var profileImage = ""
    var cancelRequest = false
    var leaveRequest = false
    var isGroupOwnwe = false
    var email = ""
    var phoneNumber = ""
    var address = ""
    var aboutUs = ""
    var groupName = ""
    var groupDescriptionName = ""
    var groupsId = ""
    var userId = 0
    var userRole = ""
    var Status = false
    var dropImage = UIImage.init(named: "dropdown.png")
    var sectionDataArray = [ExpandableGroupProjectMember]()
    var headersTitleArray = [GroupProjectListModel]()
    var listOfMember = [ListOfGroupMemberModel]()
    var pendingRequestModel = [PendingRequestAccessModel]()
    var relatedGroupModel = [RelatedGroupModel]()
    var groupEventsModel = [EventListModel]()
    var groupForumPostModel = [GroupForumPostModel]()
    
     //MARK:- viewDidLoad
     override func viewDidLoad() {
         super.viewDidLoad()

         self.groupProfileImageView.layer.cornerRadius = groupProfileImageView.frame.width/2
         let url = URL(string: profileImage)!
         let imgData = try? Data(contentsOf: url)
         
         if let imageData = imgData {
             self.groupProfileImageView.image = UIImage(data: imageData)
         }
         
         loaderView.isHidden = true
         loaderImg.image = UIImage.gif(name: "loader_gif")
         registerTableViewCell()
         headerHeight()
         
         isMemberFound.isHidden = true
         isForumPostFound.isHidden = true
         isGroupEventFound.isHidden = true
         isPendingDataFound.isHidden = true
         isGroupProjectFound.isHidden = true
         isRelatedGroupFound.isHidden = true
         
         self.groupMemberListTableView.delegate = self
         self.groupProjectTableView.delegate = self
         self.groupEventsTableView.delegate = self
         self.groupRelatedTableView.delegate = self
         self.groupForumTableView.delegate = self
         self.pendingAccessRequestTableView.delegate = self
         
         self.groupMemberListTableView.dataSource = self
         self.groupProjectTableView.dataSource = self
         self.groupEventsTableView.dataSource = self
         self.groupRelatedTableView.dataSource = self
         self.groupForumTableView.dataSource = self
         self.pendingAccessRequestTableView.dataSource = self
         
         groupInfoOnScreenLoad()

     }

/*--------------------------------------------------------------------------------------------*/
     override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.isNavigationBarHidden = true
         
         let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
         let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
         userRole = userData["role"] as! String
         userId = userData["id"] as! Int
         
         if userRole != "A" {
             if isGroupOwnwe {
                 btnAddProject.isHidden = false
             }else {
                 pendingHeaderView.isHidden = true
                 pendingAccessRequestTableView.isHidden = true
                 btnAddProject.isHidden = true
             }
             isPendingDataFound.isHidden = true
         }else {
             pendingHeaderView.isHidden = false
             pendingAccessRequestTableView.isHidden = false
         }
         
         membersAndPending()
         
     }
    
    
    //MARK:- Mail Composer Delegate Method
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([email])
        return mailComposerVC
    }
    
/*--------------------------------------------------------------------------------------------*/

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Btn Actions
    @IBAction func btnJoinGroupPressed(_ sender: UIButton) {
        
        if cancelRequest {
            //Reject Pending Request
            for i in pendingRequestModel {
                if userId == Int(i.usersID ?? "") {
                    let param = ["group_id":groupsId,"user_id":userId] as [String:Any]
                    acceptRejectPendingRequest(url: rejectRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                        if status {
                            print("Request has been rejected")
                            let param = ["group_id":self.groupsId] as [String:Any]
                            self.pendingRequestApi(url: pendingRequestAccessUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                                if status {
                                    self.showToast(message: "Request cancelled.")
                                    self.cancelRequest = false
                                    self.leaveRequest = false
                                    self.btnJoin.setImage(UIImage(named: "ic_key"), for: .normal)
                                    self.pendingAccessRequestTableView.reloadData()
                                    print("Pending request has been updated")
                                }else {
                                    print("Pending request couldn't update")
                                }
                                if self.userRole == "A" || self.isGroupOwnwe {
                                    if self.pendingRequestModel.count == 0 {
                                        self.pendingHeaderView.isHidden = true
                                        self.pendingAccessRequestTableView.isHidden = true
                                        self.isPendingDataFound.isHidden = true
                                    }else {
                                        self.pendingHeaderView.isHidden = false
                                        self.pendingAccessRequestTableView.isHidden = false
                                        self.isPendingDataFound.isHidden = true
                                    }
                                }
                                self.loaderView.isHidden = true
                            }
                        }else {
                            print("Request couldn't reject")
                            self.loaderView.isHidden = true
                        }
                    }
                }
            }
            
        }else if leaveRequest {
            //Remove member from Group
            for i in listOfMember {
                if userId == Int(i.usersID ?? "") {
                    
                    let param = ["group_id":groupsId,"user_id":userId] as [String:Any]
                    removeMemberFromGroup(url: removeMemberUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                        if status {
                            print("Member has been removed")
                            let param = ["group_id":self.groupsId] as [String:Any]
                            self.listOfGroupMembersApi(url: listOfMemberUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                                if status {
                                    self.showToast(message: "Group left successfully.")
                                    self.cancelRequest = false
                                    self.leaveRequest = false
                                    self.btnJoin.setImage(UIImage(named: "ic_key"), for: .normal)
                                    self.groupMemberListTableView.reloadData()
                                    print("Updated List of member group has been loaded")
                                }else {
                                    print("Updated List of member group couldn't load")
                                }
                                if self.listOfMember.count == 0 {
                                    self.isMemberFound.isHidden = false
                                }else {
                                    self.isMemberFound.isHidden = true
                                }
                                self.loaderView.isHidden = true
                            }
                        }else {
                            print("Member couldn't remove")
                            self.loaderView.isHidden = true
                        }
                    }
                }
            }
            
        }else {
            // Join Gorup Api
            let param = ["group_id":groupsId,"user_id":userId] as [String : Any]
            joinGroupApi(url: joinGroupRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    self.showToast(message: "Group request has been sent.")
                    let param1 = ["group_id":self.groupsId] as [String:Any]
                    self.pendingRequestApi(url: pendingRequestAccessUrl, method: responseType.get.rawValue, param: param1, header: headerToken) { (status) in
                        if status {
                            for i in self.pendingRequestModel {
                                if self.userId == Int(i.usersID ?? "") {
                                    self.cancelRequest = true
                                    self.leaveRequest = false
                                    self.btnJoin.setImage(UIImage(named: "ic_exit"), for: .normal)
                                }
                            }
                            self.pendingAccessRequestTableView.reloadData()
                            print("Pending request has been updated")
                        }else {
                            print("Pending request couldn't update")
                        }
                        if self.userRole == "A" || self.isGroupOwnwe {
                            if self.pendingRequestModel.count == 0 {
                                self.pendingHeaderView.isHidden = true
                                self.pendingAccessRequestTableView.isHidden = true
                                self.isPendingDataFound.isHidden = true
                            }else {
                                self.pendingHeaderView.isHidden = false
                                self.pendingAccessRequestTableView.isHidden = false
                                self.isPendingDataFound.isHidden = true
                            }
                        }
                        self.loaderView.isHidden = true
                    }
                    print("group join request has been sent")
                }else {
                    self.loaderView.isHidden = true
                    self.showToast(message: "There is problem in sending group request")
                    print("group join request couldn't send")
                }
            }
            
        }
                
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    @IBAction func addGroupPojectButtonPressed(_ sender: UIButton) {
    }
    
/*--------------------------------------------------------------------------------------------*/

    @IBAction func btnEmailPressed(_ sender: Any) {
        let mailComposeViewController = self.configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
           self.showSendMailErrorAlert()
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------*/

    @IBAction func homeBtn(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------*/

    @objc func removeMember(sender : UIButton) {
        let user_id = listOfMember[sender.tag].groupsUsersUserID
        let param = ["group_id":groupsId,"user_id":user_id!] as [String:Any]
        removeMemberFromGroup(url: removeMemberUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Member has been removed")
                let param = ["group_id":self.groupsId] as [String:Any]
                self.listOfGroupMembersApi(url: listOfMemberUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                    if status {
                        
                        var isMatched = true
                        for i in self.listOfMember {
                            if self.userId == Int(i.usersID ?? "") {
                                isMatched = false
                            }
                        }
                        
                        if isMatched {
                            self.leaveRequest = false
                            self.cancelRequest = false
                            self.btnJoin.setImage(UIImage(named: "ic_key"), for: .normal)
                        }
                       
                        self.showToast(message: "Member has removed from group")
                        self.groupMemberListTableView.reloadData()
                        print("Updated List of member group has been loaded")
                    }else {
                        print("Updated List of member group couldn't load")
                    }
                    if self.listOfMember.count == 0 {
                        self.isMemberFound.isHidden = false
                    }else {
                        self.isMemberFound.isHidden = true
                    }
                    self.loaderView.isHidden = true
                }
            }else {
                print("Member couldn't remove")
                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    @objc func approveBtn(sender : UIButton) {
        
        let user_id = pendingRequestModel[sender.tag].groupRequestsUserID
        let pending_request_id = pendingRequestModel[sender.tag].groupRequestsID
        let param = ["group_id":groupsId,"user_id":user_id!,"pending_request_id":pending_request_id!] as [String:Any]
        acceptRejectPendingRequest(url: approveRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Request has been accepted")
                let param = ["group_id":self.groupsId] as [String:Any]
                self.listOfGroupMembersApi(url: listOfMemberUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                    if status {
                        self.groupMemberListTableView.reloadData()
                        self.pendingRequestApi(url: pendingRequestAccessUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                            if status {
                                self.leaveRequest = true
                                self.cancelRequest = false
                                self.btnJoin.setImage(UIImage(named: "leave"), for: .normal)
                                self.pendingAccessRequestTableView.reloadData()
                                print("Pending request has been updated")
                            }else {
                                print("Pending request couldn't update")
                            }
                            if self.userRole == "A" || self.isGroupOwnwe {
                                if self.pendingRequestModel.count == 0 {
                                    self.pendingHeaderView.isHidden = true
                                    self.pendingAccessRequestTableView.isHidden = true
                                    self.isPendingDataFound.isHidden = true
                                }else {
                                    self.pendingHeaderView.isHidden = false
                                    self.pendingAccessRequestTableView.isHidden = false
                                    self.isPendingDataFound.isHidden = true
                                }
                            }
                            self.loaderView.isHidden = true
                        }
                        print("List of member group has been loaded")
                    }else {
                        print("List of member group couldn't load")
                    }
                    if self.listOfMember.count == 0 {
                        self.isMemberFound.isHidden = false
                    }else {
                        self.isMemberFound.isHidden = true
                    }
                }
            }else {
                print("Request couldn't accept")
                self.loaderView.isHidden = true
            }
        }
        
        
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    @objc func rejectBtn(sender : UIButton) {
        let user_id = pendingRequestModel[sender.tag].groupRequestsUserID
        let param = ["group_id":groupsId,"user_id":user_id!] as [String:Any]
        acceptRejectPendingRequest(url: rejectRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Request has been rejected")
                let param = ["group_id":self.groupsId] as [String:Any]
                self.pendingRequestApi(url: pendingRequestAccessUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                    if status {
                        self.leaveRequest = false
                        self.cancelRequest = false
                        self.btnJoin.setImage(UIImage(named: "ic_key"), for: .normal)
                        self.pendingAccessRequestTableView.reloadData()
                        print("Pending request has been updated")
                    }else {
                        print("Pending request couldn't update")
                    }
                    if self.userRole == "A" || self.isGroupOwnwe {
                        if self.pendingRequestModel.count == 0 {
                            self.pendingHeaderView.isHidden = true
                            self.pendingAccessRequestTableView.isHidden = true
                            self.isPendingDataFound.isHidden = true
                        }else {
                            self.pendingHeaderView.isHidden = false
                            self.pendingAccessRequestTableView.isHidden = false
                            self.isPendingDataFound.isHidden = true
                        }
                    }
                    self.loaderView.isHidden = true
                    
                }
            }else {
                print("Request couldn't reject")
                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    @IBAction func addProjectBtn(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddProjectViewController") as! AddProjectViewController
        vc.isGroupAddProject = true
        vc.groupId = groupsId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*--------------------------------------------------------------------------------------------*/

    @objc func detectSectionHeaderClick(_ sender:UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewProjectViewController") as! ViewProjectViewController
        let section = sender.tag
        vc.projectId = headersTitleArray[section].projectsID ?? "0"
        vc.projectReadOnlyStatus =  headersTitleArray[section].projectsStatus ?? "r"
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
/*--------------------------------------------------------------------------------------------*/

    @objc func ExpandClose (button: UIButton) {
        
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in sectionDataArray[section].names.indices{
            let indexpath = IndexPath(row: row, section: section)
            indexPaths.append(indexpath)
        }
        
        let isExpanded = sectionDataArray[section].isExpanded
        sectionDataArray[section].isExpanded = !isExpanded
        if isExpanded {
            button.isSelected = false
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
            
            button.setImage(dropImage, for: .normal)
            groupProjectTableView.deleteRows(at: indexPaths, with: .top)
        }
        else{
            button.isSelected = true
            dropImage = UIImage.init(named: "of-admin-dropup.png")
            
            button.setImage(dropImage, for: .normal)
            groupProjectTableView.insertRows(at: indexPaths, with: .bottom)
        }
        
    }
    
    //MARK:- Custom Functions
    func groupInfoOnScreenLoad() {
       btnEmail.setTitle(email, for: .normal)
        var fontSize : CGFloat = 0
        if UIDevice().userInterfaceIdiom == .phone {
            fontSize = (32 * (self.view.frame.width/320))
        }else{
            fontSize = 35
        }
       let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
       about.loadHTMLString((fontSetting + aboutUs) , baseURL: nil)
       lblAddress.text = address
       lblGroupName.text = groupName
       lblPhoneNumber.text = phoneNumber
       lblGroupDescription.text = groupDescriptionName
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func pending () {
        let param = ["group_id":groupsId] as [String:Any]
        self.pendingRequestApi(url: pendingRequestAccessUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                for i in self.pendingRequestModel {
                    if self.userId == Int(i.usersID ?? "") {
                        self.cancelRequest = true
                        self.leaveRequest = false
                        self.btnJoin.setImage(UIImage(named: "ic_exit"), for: .normal)
                    }
                }
                
                for i in self.listOfMember {
                    if self.userId == Int(i.usersID ?? "") {
                        self.cancelRequest = false
                        self.leaveRequest = true
                        self.btnJoin.setImage(UIImage(named: "leave"), for: .normal)
                    }
                }
                
                if !self.cancelRequest && !self.leaveRequest {
                    self.cancelRequest = false
                    self.leaveRequest = false
                    self.btnJoin.setImage(UIImage(named: "ic_key"), for: .normal)
                }
                self.relatedGroup()
                self.pendingAccessRequestTableView.reloadData()
                print("Pending request has been updated")
            }else {
                self.relatedGroup()
                print("Pending request couldn't update")
            }
            if self.userRole == "A" || self.isGroupOwnwe {
                if self.pendingRequestModel.count == 0 {
                    self.isPendingDataFound.isHidden = true
                    self.pendingHeaderView.isHidden = true
                    self.pendingAccessRequestTableView.isHidden = true
                }else {
                    self.pendingHeaderView.isHidden = false
                    self.pendingAccessRequestTableView.isHidden = false
                    self.isPendingDataFound.isHidden = true
                }
            }
            
        }
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func membersAndPending () {
        let param = ["group_id":groupsId] as [String:Any]
        listOfGroupMembersApi(url: listOfMemberUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.pending()
                self.groupMemberListTableView.reloadData()
                print("List of member group has been loaded")
            }else {
                self.pending()
                print("List of member group couldn't load")
            }
            if self.listOfMember.count == 0 {
                self.isMemberFound.isHidden = false
            }else {
                self.isMemberFound.isHidden = true
            }
        }
        
        
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func relatedGroup() {
        let relatedGroupParam = ["group_id":groupsId,"user_id":userId] as [String:Any]
        self.relatedGroupApi(url: relatedGroupUrl, method: responseType.get.rawValue, param: relatedGroupParam, header: headerToken) { (status) in
            if status {
                self.groupEvent()
                self.groupRelatedTableView.reloadData()
                print("groupRelated request has been updated")
            }else {
                self.groupEvent()
                print("groupRelated request couldn't update")
            }
            if self.relatedGroupModel.count == 0 {
                self.isRelatedGroupFound.isHidden = false
            }else {
                self.isRelatedGroupFound.isHidden = true
            }
            
        }
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func groupEvent () {
        self.groupsEventApi(url: groupEventUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                self.groupForumPost()
                self.groupEventsTableView.reloadData()
                print("groupEvents request has been updated")
            }else {
                self.groupForumPost()
                print("groupEvents request couldn't update")
            }
            if self.groupEventsModel.count == 0 {
                self.isGroupEventFound.isHidden = false
            }else {
                self.isGroupEventFound.isHidden = true
            }
            
        }
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func groupForumPost ()  {
        let param = ["group_id":groupsId] as [String:Any]
        self.groupsForumPostApi(url: groupforumPostUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.groupProjectList()
                self.groupForumTableView.reloadData()
                print("groupForum request has been updated")
            }else {
                self.groupProjectList()
                print("groupForum request couldn't update")
            }
            if self.groupForumPostModel.count == 0 {
                self.isForumPostFound.isHidden = false
            }else {
                self.isForumPostFound.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func groupProjectList () {
        let param = ["group_id":groupsId] as [String:Any]
        self.groupsProjectListApi(url: groupPorjectListUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.groupProjectTableView.reloadData()
                print("groupProject request has been updated")
            }else {
                print("groupProject request couldn't update")
            }
            self.loaderView.isHidden = true
            if self.headersTitleArray.count == 0 {
                self.isGroupProjectFound.isHidden = false
            }else {
                self.isGroupProjectFound.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func removeMemberFromGroup(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func joinGroupApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func listOfGroupMembersApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        listOfMember.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.listOfMember.append(ListOfGroupMemberModel(data: [i]))
                        }
                        print("group data fetched")
                    }else {
                        print("group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func pendingRequestApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        pendingRequestModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.pendingRequestModel.append(PendingRequestAccessModel(data: [i]))
                        }
                        print("Pending group data fetched")
                    }else {
                        print("Pending group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func relatedGroupApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        relatedGroupModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.relatedGroupModel.append(RelatedGroupModel(data: [i]))
                        }
                        print("Pending group data fetched")
                    }else {
                        print("Pending group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func groupsEventApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        groupEventsModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        var eventData = [EventListModel]()
                        for i in data {
                            eventData.append(EventListModel(data: [i]))
                        }
                        
                        for i in eventData {
                            if self.groupsId == i.groupID {
                                self.groupEventsModel.append(i)
                            }
                        }
                        
                        print("Pending group data fetched")
                    }else {
                        print("Pending group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func groupsForumPostApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        groupForumPostModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.groupForumPostModel.append(GroupForumPostModel(data: [i]))
                        }
                        print("Pending group data fetched")
                    }else {
                        print("Pending group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func groupsProjectListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        headersTitleArray.removeAll()
        sectionDataArray.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        var groupProjectData = [CombineGroupProjectMemberModel]()
                        for i in data {
                            groupProjectData.append(CombineGroupProjectMemberModel(data: [i]))
                        }
                        
                        for i in groupProjectData{
                            self.headersTitleArray.append(i.project)
                            self.sectionDataArray.append(ExpandableGroupProjectMember(isExpanded: false, names: i.projectMemberModel))
                        }
                        
                        
                        print("Pending group data fetched")
                    }else {
                        print("Pending group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func acceptRejectPendingRequest(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func registerTableViewCell ()
    {
        self.groupMemberListTableView.register(UINib(nibName: "MemberTableViewCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
        self.pendingAccessRequestTableView.register(UINib(nibName: "PendingAccessTableViewCell", bundle: nil), forCellReuseIdentifier: "PARCell")
        self.groupEventsTableView.register(UINib(nibName: "GroupEventsTableViewCell", bundle: nil), forCellReuseIdentifier: "GEventCell")
        self.groupProjectTableView.register(UINib(nibName: "GroupProjectTableViewCell", bundle: nil), forCellReuseIdentifier: "GProjCell")
        self.groupRelatedTableView.register(UINib(nibName: "RelatedGroupTableViewCell", bundle: nil), forCellReuseIdentifier: "GRelatedCell")
        self.groupForumTableView.register(UINib(nibName: "GroupForumPostTableViewCell", bundle: nil), forCellReuseIdentifier: "GForumCell")
        
    }
    
/*--------------------------------------------------------------------------------------------*/
   
    func showSendMailErrorAlert() {
        self.ShowErrorAlert(message: "Could Not Send Email", AlertTitle: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
    }
    
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == groupProjectTableView {
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProfileViewController") as! ViewProfileViewController
            let id = sectionDataArray[indexPath.section].names[indexPath.row]
            vc.userId = Int(id.memberId ?? "0") ?? 0
            vc.isLoginMember = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if tableView == groupEventsTableView {
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
            vc.eventData = groupEventsModel[indexPath.row]
            vc.isMasterSearchView = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if tableView == groupMemberListTableView {
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProfileViewController") as! ViewProfileViewController
            vc.userId = Int(listOfMember[indexPath.row].usersID ?? "0") ?? 0
            vc.isLoginMember = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if tableView == groupRelatedTableView {
            
            groupsId = relatedGroupModel[indexPath.row].Groups__id ?? ""
            aboutUs = relatedGroupModel[indexPath.row].Groups__bio ?? ""
            address = relatedGroupModel[indexPath.row].Groups__address ?? ""
            email = relatedGroupModel[indexPath.row].Groups__email ?? ""
            phoneNumber = relatedGroupModel[indexPath.row].Groups__phone ?? ""
            groupName = relatedGroupModel[indexPath.row].Groups__name ?? ""
            groupDescriptionName = relatedGroupModel[indexPath.row].Groups__description ?? ""
            profileImage = relatedGroupModel[indexPath.row].Groups__image ?? ""
            self.groupProfileImageView.layer.cornerRadius = groupProfileImageView.frame.width/2
            let url = URL(string: profileImage)!
            let imgData = try? Data(contentsOf: url)
            
            if let imageData = imgData {
                self.groupProfileImageView.image = UIImage(data: imageData)
            }
            groupInfoOnScreenLoad()
            membersAndPending()
            
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            if tableView == groupEventsTableView{
                return 60
            }else if tableView == self.groupMemberListTableView {
                return 50
            }
            else {
                return 40
            }
        }else {
            if tableView == groupEventsTableView{
                return 80
            }else if tableView == self.groupMemberListTableView {
                return 70
            }
            else {
                return 60
            }
        }
        
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == groupProjectTableView {
            return headersTitleArray.count
        }
        return 1
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.groupMemberListTableView{
            return listOfMember.count
        }
        else if tableView == self.groupEventsTableView{
            return groupEventsModel.count
        }
        else if tableView == pendingAccessRequestTableView {
            return pendingRequestModel.count
        }
        else if tableView == groupRelatedTableView  {
            return relatedGroupModel.count
        }else if tableView == groupForumTableView {
            return groupForumPostModel.count
            
        }else if tableView == groupProjectTableView {
            
            if !sectionDataArray[section].isExpanded {
                return 0
            }
            return self.sectionDataArray[section].names.count
            
        }
        else {
            return 0
        }
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.groupMemberListTableView{
            let cell = self.groupMemberListTableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as! MemberTableViewCell
            let firstName = "\(listOfMember[indexPath.row].usersTitle ?? "") \(listOfMember[indexPath.row].usersFirstName ?? "")"
            let lastName = listOfMember[indexPath.row].usersLastName ?? ""
            
            if userRole != "A" {
                if !isGroupOwnwe {
                    cell.removerMember.isHidden = true
                }
            }
            
            cell.lblMemberName.attributedText = NSAttributedString(string: "\(firstName) \(lastName)", attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            cell.removerMember.tag = indexPath.row
            cell.removerMember.addTarget(self, action: #selector(self.removeMember), for: .touchUpInside)
            
            cell.imageViewUser.layer.cornerRadius = cell.imageViewUser.frame.width/2
            let remoteImageUrlString = listOfMember[indexPath.row].profileImgName ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.imageViewUser.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
            
            return cell
        }
        if tableView == self.groupEventsTableView{
            let cell = self.groupEventsTableView.dequeueReusableCell(withIdentifier: "GEventCell", for: indexPath) as! GroupEventsTableViewCell
            cell.eventDate.text = groupEventsModel[indexPath.row].created
            cell.eventName.attributedText = NSAttributedString(string: groupEventsModel[indexPath.row].name ?? "", attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
            return cell
        }
        if tableView == self.pendingAccessRequestTableView{
            let cell = self.pendingAccessRequestTableView.dequeueReusableCell(withIdentifier: "PARCell", for: indexPath) as! PendingAccessTableViewCell
            let firstName = pendingRequestModel[indexPath.row].usersFirstName ?? ""
            let lastName = pendingRequestModel[indexPath.row].usersLastName ?? ""
            
            if userRole != "A" {
                if !isGroupOwnwe {
                    cell.rejectBtn.isHidden = true
                    cell.approveBtn.isHidden = true
                }
            }
            
            cell.memberName.text = "\(firstName) \(lastName)"
            cell.approveBtn.tag = indexPath.row
            cell.rejectBtn.tag = indexPath.row
            cell.approveBtn.addTarget(self, action: #selector(self.approveBtn), for: .touchUpInside)
            cell.rejectBtn.addTarget(self, action: #selector(self.rejectBtn), for: .touchUpInside)
            return cell
        }
        if tableView == self.groupProjectTableView{
            let cell = self.groupProjectTableView.dequeueReusableCell(withIdentifier: "GProjCell", for: indexPath) as! GroupProjectTableViewCell
            let name = sectionDataArray[indexPath.section].names[indexPath.row]
            cell.projectName.attributedText = NSAttributedString(string: name.memberName ?? "", attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
            cell.memberImg.layer.cornerRadius = cell.memberImg.frame.width/2
            let remoteImageUrlString = name.profileImgName ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.memberImg.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
            return cell
        }
        if tableView == groupRelatedTableView {
            let cell = self.groupRelatedTableView.dequeueReusableCell(withIdentifier: "GRelatedCell", for: indexPath) as! RelatedGroupTableViewCell
            cell.groupName.attributedText = NSAttributedString(string: relatedGroupModel[indexPath.row].Groups__name ?? "", attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
            cell.imgRelatedGroup.layer.cornerRadius = cell.imgRelatedGroup.frame.width/2
            let remoteImageUrlString = relatedGroupModel[indexPath.row].Groups__image ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.imgRelatedGroup.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            })
            return cell
        }
        else {
            let cell = self.groupForumTableView.dequeueReusableCell(withIdentifier: "GForumCell", for: indexPath) as! GroupForumPostTableViewCell
            cell.postTitle.text = groupForumPostModel[indexPath.row].forumPostsTopic
            return cell
        }
        
    }
    
/*--------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if tableView == groupProjectTableView{
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 150))
        
        // code for adding centered title
        headerView.backgroundColor = UIColor.white
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 5, width:tableView.frame.width-80, height: 30))
        headerLabel.textColor = UIColor.black
        if headersTitleArray.count != 0 {
            headerLabel.text = headersTitleArray[section].projectsName
        }
        if UIDevice().userInterfaceIdiom == .phone {
            headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
        }else {
            headerLabel.font = UIFont.boldSystemFont(ofSize: 18)
        }
        headerLabel.textAlignment = .left
        headerLabel.contentMode = .scaleAspectFit
        
            
        //Custom Button to detect the Header section tap event
        let buttonWithTag = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width-80, height: 100))
        buttonWithTag.tag = section
        buttonWithTag.addTarget(self, action: #selector(self.detectSectionHeaderClick(_:)), for: .touchUpInside)
        headerView.addSubview(buttonWithTag)
            
        headerView.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
        headerLabel.textColor = UIColor.white
        headerView.addSubview(headerLabel)
        
        let arrowButton = UIButton(frame: CGRect(x:headerView.frame.size.width - 80, y:0, width:60, height:30))
        dropImage = UIImage.init(named: "of-admin-dropdown.png")
        
        arrowButton.setImage(dropImage, for: .normal)
        arrowButton.setTitleColor(.black, for: .normal)
        arrowButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        arrowButton.backgroundColor = UIColor.clear
        arrowButton.addTarget(self, action: #selector(ExpandClose), for: .touchUpInside)
        arrowButton.tag = section
        arrowButton.imageView?.contentMode = .scaleAspectFit
        
//        let countValue = self.sectionDataArray[section].names.count
//        if countValue != 0{
            headerView.addSubview(arrowButton)
//        }
        //constraint for dropdown arrow
        arrowButton.translatesAutoresizingMaskIntoConstraints = false
        arrowButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        arrowButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        arrowButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        arrowButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
        
        arrowButton.setNeedsLayout()
        arrowButton.layoutIfNeeded()
        arrowButton.clipsToBounds = true
        headerView.layer.cornerRadius = 10
        return headerView
        }
        else{
            let emptyView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            return emptyView
            
        }
        
    }
    
/*--------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            if tableView == groupProjectTableView {
                return 40
            }
            else{
                return 0
            }
        }else {
            if tableView == groupProjectTableView {
                return 50
            }
            else{
                return 0
            }
        }
        
    }
    
/*--------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == groupProjectTableView {
            return 5
        }
        else{
            return 0
        }
    }
    
/*--------------------------------------------------------------------------------------------*/

}


extension UIViewController {
    func ShowErrorAlert(message : String , AlertTitle : String = "Error") {
        let alert = UIAlertController(title: AlertTitle , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
        })
        self.present(alert, animated: true, completion: nil)
    }
}
