
import UIKit
import Alamofire
import SwiftGifOrigin
import RichEditorView
import SDWebImage

class EditGroupViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Btn Outlets
    @IBOutlet weak var txtFieldCategory: UITextField!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldDescription: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var txtFieldWebsite: UITextField!
    @IBOutlet weak var txtFieldAddress1: UITextField!
    @IBOutlet weak var txtFieldAddress2: UITextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldPostalCode: UITextField!
    @IBOutlet weak var txtFieldProvince: UITextField!
    @IBOutlet weak var txtFieldCountry: UITextField!
    @IBOutlet weak var pendingAccessRequestTableView: UITableView!
    @IBOutlet weak var groupMemberListTableView: UITableView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var btnSaveGroupOutlet: UIButton!
    @IBOutlet weak var btnStatusVisibleToGroupsOutlet: UIButton!
    @IBOutlet weak var btnStatusPendingOutlet: UIButton!
    @IBOutlet weak var btnStatusAcceptDonationOutlet: UIButton!
    @IBOutlet weak var btnStatusPassiveOutlet: UIButton!
    @IBOutlet weak var webTextFieldOuterView: UIView!
    @IBOutlet weak var webTextFieldInnerView: UIView!
    @IBOutlet weak var groupTitle: UILabel!
    @IBOutlet weak var groupDescription: UILabel!
    @IBOutlet weak var lblAccept: UILabel!
    @IBOutlet weak var lblpending: UILabel!
    @IBOutlet weak var lblPassive: UILabel!
     @IBOutlet weak var isPendingFound: UILabel!
     @IBOutlet weak var isMemberFound: UILabel!
    
    //MARK:- Custom Variables
    var id = 2
    var visible = 0
    var pending = 0
    var donation = 0
    var passive = 0
    var isAdmin = 0
    var userId = 0
    var userRole = ""
    var groupsId = ""
    var Status = false
    var nameFieldFlag = false
    var emailFieldFlag = false
    var phoneFieldFlag = false
    var address1FieldFlag = false
    var address2FieldFlag = false
    var descriptionFieldFlag = false
    var listOfMember = [ListOfGroupMemberModel]()
    var pendingRequest = [PendingRequestAccessModel]()
    var editGroupModel = GroupModel()
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    //MARK:- LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryView.isHidden = true
        loaderView.isHidden = true
        isPendingFound.isHidden = true
        isMemberFound.isHidden = true
        
        loaderImg.image = UIImage.gif(name: "loader_gif")
        initialLayout()
        registerTableViewCell()
        headerHeight()
        self.groupMemberListTableView.delegate = self
        self.pendingAccessRequestTableView.delegate = self
        
        self.groupMemberListTableView.dataSource = self
        self.pendingAccessRequestTableView.dataSource = self
        
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Description"
        
        toolbar.delegate = self
        toolbar.editor = editorView
        
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let done = RichEditorOptionItem(image: nil, title: "Done") { toolbar in
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(done)
        toolbar.options = options
        hideKeyboard()
        
        onScreenLoad()
        
    }
/*----------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        userId = userData["id"] as! Int
        
        if userRole == "A" {
            isAdmin = 1
        }else {
            isAdmin = 0
            lblAccept.isHidden = true
            lblPassive.isHidden = true
            lblpending.isHidden = true
            btnStatusPassiveOutlet.isHidden = true
            btnStatusPendingOutlet.isHidden = true
            btnStatusAcceptDonationOutlet.isHidden = true
        }
       //Getting the list of group members
        let param = ["group_id":groupsId] as [String:Any]
        listOfGroupMembersApi(url: listOfMemberUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.groupMemberListTableView.reloadData()
                if self.listOfMember.count == 0 {
                    self.isMemberFound.isHidden = false
                }else{
                    self.isMemberFound.isHidden = true
                }
                print("List of member group has been loaded")
            }else {
                print("List of member group couldn't load")
            }
            self.loaderView.isHidden = true
        }
        
        pendingRequestApi(url: pendingRequestAccessUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.pendingAccessRequestTableView.reloadData()
                if self.pendingRequest.count == 0 {
                    self.isPendingFound.isHidden = false
                }else{
                    self.isPendingFound.isHidden = true
                }
                  print("Pending request has been loaded")
            }else {
                  print("Pending request couldn't load")
            }
            self.loaderView.isHidden = true

        }
        
        
    }
    
    //MARK:- TextField Delegate Method
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           
           if textField == self.txtFieldName {
               self.txtFieldName.borderColor = .darkGray
               
               let maxLength = 100
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
               
           }
           if textField == self.txtFieldDescription {
               self.txtFieldDescription.borderColor = .darkGray
               
               let maxLength = 250
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
               
           }
           if textField == self.txtFieldEmail {
               self.txtFieldEmail.borderColor = .darkGray
               
               let maxLength = 250
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           if textField == self.txtFieldPhoneNumber {
               self.txtFieldPhoneNumber.borderColor = .darkGray
               
               let maxLength = 20
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           if textField == self.txtFieldWebsite {
               
               let maxLength = 150
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           if textField == self.txtFieldAddress1 {
               self.txtFieldAddress1.borderColor = .darkGray
               
               let maxLength = 300
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           if textField == self.txtFieldAddress2 {
               self.txtFieldAddress2.borderColor = .darkGray
               
               let maxLength = 300
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           if textField == self.txtFieldCity {
               self.txtFieldCity.borderColor = .darkGray
               
               let maxLength = 30
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           if textField == self.txtFieldPostalCode {
               self.txtFieldPostalCode.borderColor = .darkGray
               
               let maxLength = 20
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           if textField == self.txtFieldProvince {
               self.txtFieldProvince.borderColor = .darkGray
               
               let maxLength = 20
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           else {
               self.txtFieldCountry.borderColor = .darkGray
               
               let maxLength = 50
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
       }
/*----------------------------------------------------------------------------------------------------*/

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
        
/*----------------------------------------------------------------------------------------------------*/
        
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.becomeFirstResponder() {
            categoryView.isHidden = true
        }
    }

    //MARK:- Btn Actions
    
    @IBAction func categoryTriggerBtn(_ sender: Any) {
        categoryView.isHidden = false
    }
    
/*------------------------------------------------------------------------------------------*/

    @IBAction func CategoryButton(_ sender: UIButton) {
        if sender.tag == 1{
            id = 2
            txtFieldCategory.text = "Business"
            categoryView.isHidden = true
            
        }else if sender.tag == 2 {
            id = 3
            txtFieldCategory.text = "Charities"
            categoryView.isHidden = true
            
        }
    }
    
/*------------------------------------------------------------------------------------------*/

    @IBAction func btnStatusVisibleToGroupsPressed(_ sender: UIButton) {
        
        if self.btnStatusVisibleToGroupsOutlet.isSelected{
            visible = 0
            self.btnStatusVisibleToGroupsOutlet.isSelected =  false
        }
        else{
            visible = 1
            self.btnStatusVisibleToGroupsOutlet.isSelected =  true
        }
        
    }
    
/*------------------------------------------------------------------------------------------*/
    
    @IBAction func btnStatusAcceptDonationsPressed(_ sender: UIButton) {
        
        if self.btnStatusAcceptDonationOutlet.isSelected{
            donation = 0
            self.btnStatusAcceptDonationOutlet.isSelected =  false
        }
        else{
            donation = 1
            self.btnStatusAcceptDonationOutlet.isSelected =  true
        }
    }
    
/*------------------------------------------------------------------------------------------*/
    
    @IBAction func btnStatusPendingPressed(_ sender: UIButton) {
        
        if self.btnStatusPendingOutlet.isSelected{
            pending = 0
            self.btnStatusPendingOutlet.isSelected =  false
        }
        else{
            pending = 1
            self.btnStatusPendingOutlet.isSelected =  true
        }
    }
    
/*------------------------------------------------------------------------------------------*/
    
    @IBAction func btnStatusPassivePressed(_ sender: UIButton) {
        
        if self.btnStatusPassiveOutlet.isSelected{
            passive = 0
            self.btnStatusPassiveOutlet.isSelected =  false
        }
        else{
            passive = 1
            self.btnStatusPassiveOutlet.isSelected =  true
        }
    }
    
/*------------------------------------------------------------------------------------------*/

    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*------------------------------------------------------------------------------------------*/
    
    @IBAction func btnGroupEditProfileImagePressed(_ sender: UIButton) {
    }
    
/*------------------------------------------------------------------------------------------*/
    
    @IBAction func btnEditMainImagePressed(_ sender: UIButton) {
    }
    
/*------------------------------------------------------------------------------------------*/
    
    @IBAction func btnAddGroupMemberPressed(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddMemberViewController") as!  AddMemberViewController
        vc.groupId = groupsId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*------------------------------------------------------------------------------------------*/
    
    @IBAction func btnSaveGroupButtonPressed(_ sender: UIButton) {
        if textFieldFormatCheck() {
            print("all mendatory fields are defined")
            let param = ["category_id" :id,"name" : txtFieldName.text!,"description" : txtFieldDescription.text!,"email" : txtFieldEmail.text!,"phone" : txtFieldPhoneNumber.text!,"address" : txtFieldAddress1.text!,"address2" : txtFieldAddress2.text!,"website" : txtFieldWebsite.text!,"city" : txtFieldCity.text!,"region" : txtFieldProvince.text!,"country" : txtFieldCountry.text!,"postal_code" : txtFieldPostalCode.text!,"bio" : editorView.html,"hidden" : visible,"pending" : pending,"vote_decision" : "","donations" : donation,"passive":passive,"user_id":userId,"is_admin":isAdmin,"group_id":groupsId] as [String:Any]
            
            groupData(url: editGroupUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print("Group has been edit")
                    self.navigationController?.popViewController(animated: true)
                }else {
                    print("Group couldn't edit")
                }
                self.loaderView.isHidden = true
            }
            //API Call for Edit Group-save needs to be added here with a custom loader
            
        }
        else{
            print("one or more mendatory fields are empty.")
        }
        
    }

/*------------------------------------------------------------------------------------------*/

    @objc func removeMember(sender : UIButton) {
        let user_id = listOfMember[sender.tag].groupsUsersUserID
        let param = ["group_id":groupsId,"user_id":user_id!] as [String:Any]
        removeMemberFromGroup(url: removeMemberUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Member has been removed")
                let param = ["group_id":self.groupsId] as [String:Any]
                self.listOfGroupMembersApi(url: listOfMemberUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                    if status {
                        self.groupMemberListTableView.reloadData()
                        if self.listOfMember.count == 0 {
                            self.isMemberFound.isHidden = false
                        }else{
                            self.isMemberFound.isHidden = true
                        }
                        print("Updated List of member group has been loaded")
                    }else {
                        print("Updated List of member group couldn't load")
                    }
                    self.loaderView.isHidden = true
                }
            }else {
                print("Member couldn't remove")
                self.loaderView.isHidden = true
            }
        }
    }
    
/*------------------------------------------------------------------------------------------*/

    @objc func approveRequest(sender : UIButton) {
        print(sender.tag)
        let user_id = pendingRequest[sender.tag].groupRequestsUserID
        let pending_request_id = pendingRequest[sender.tag].groupRequestsID
        let param = ["group_id":groupsId,"user_id":user_id!,"pending_request_id":pending_request_id!] as [String:Any]
        acceptRejectPendingRequest(url: approveRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Request has been accepted")
                let param = ["group_id":self.groupsId] as [String:Any]
                self.listOfGroupMembersApi(url: listOfMemberUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                    if status {
                        self.groupMemberListTableView.reloadData()
                        self.pendingRequestApi(url: pendingRequestAccessUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                            if status {
                                self.pendingAccessRequestTableView.reloadData()
                                if self.listOfMember.count == 0 {
                                    self.isMemberFound.isHidden = false
                                }else{
                                    self.isMemberFound.isHidden = true
                                }
                                if self.pendingRequest.count == 0 {
                                    self.isPendingFound.isHidden = false
                                }else{
                                    self.isPendingFound.isHidden = true
                                }
                                print("Pending request has been updated")
                            }else {
                                print("Pending request couldn't update")
                            }
                            self.loaderView.isHidden = true
        
                        }
                        print("List of member group has been loaded")
                    }else {
                        print("List of member group couldn't load")
                    }
                }
            }else {
                print("Request couldn't accept")
                self.loaderView.isHidden = true
            }
        }

       
    }
    
/*------------------------------------------------------------------------------------------*/
    
    @objc func rejectRequest(sender : UIButton) {
        let user_id = pendingRequest[sender.tag].groupRequestsUserID
        let param = ["group_id":groupsId,"user_id":user_id!] as [String:Any]
        acceptRejectPendingRequest(url: rejectRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Request has been rejected")
                let param = ["group_id":self.groupsId] as [String:Any]
                self.pendingRequestApi(url: pendingRequestAccessUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                    if status {
                        self.pendingAccessRequestTableView.reloadData()
                        if self.listOfMember.count == 0 {
                            self.isMemberFound.isHidden = false
                        }else{
                            self.isMemberFound.isHidden = true
                        }
                        if self.pendingRequest.count == 0 {
                            self.isPendingFound.isHidden = false
                        }else{
                            self.isPendingFound.isHidden = true
                        }
                        print("Pending request has been updated")
                    }else {
                        print("Pending request couldn't update")
                    }
                    self.loaderView.isHidden = true
                    
                }
            }else {
                print("Request couldn't reject")
                self.loaderView.isHidden = true
            }
        }
    }


    //MARK:- Custom Function
    func registerTableViewCell ()
    {
        self.groupMemberListTableView.register(UINib(nibName: "MemberTableViewCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
        self.pendingAccessRequestTableView.register(UINib(nibName: "PendingAccessTableViewCell", bundle: nil), forCellReuseIdentifier: "PARCell")
        
    }
    
/*-------------------------------------------------------------------------------------------------*/

    func textFieldFormatCheck() -> Bool {
        
        if self.txtFieldAddress1.text?.count == 0 {
            self.txtFieldAddress1.borderColor = .red
            self.txtFieldAddress1.becomeFirstResponder()
            address1FieldFlag = false
        }
        else{address1FieldFlag = true}
        
        if self.txtFieldPhoneNumber.text?.count == 0 {
            self.txtFieldPhoneNumber.borderColor = .red
            self.txtFieldPhoneNumber.becomeFirstResponder()
            phoneFieldFlag = false
        }
        else{phoneFieldFlag = true}
        
        if self.txtFieldEmail.text?.count == 0 || validateEmail(email: self.txtFieldEmail.text!) == false{
            self.txtFieldEmail.borderColor = .red
            self.txtFieldEmail.becomeFirstResponder()
            emailFieldFlag = false
        }
        else{emailFieldFlag = true}
        
        if self.txtFieldDescription.text?.count == 0 {
            self.txtFieldDescription.borderColor = .red
            self.txtFieldDescription.becomeFirstResponder()
            descriptionFieldFlag = false
        }
        else{descriptionFieldFlag = true}
        
        if self.txtFieldName.text?.count == 0 {
            self.txtFieldName.borderColor = .red
            self.txtFieldName.becomeFirstResponder()
            nameFieldFlag = false
        }
        else{nameFieldFlag = true}
        
        if nameFieldFlag && emailFieldFlag && phoneFieldFlag && address1FieldFlag /*&& address2FieldFlag*/ && descriptionFieldFlag == true {return true}
        else{return false}
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func onScreenLoad (){
        groupTitle.text = editGroupModel.groupsName
        groupDescription.text = editGroupModel.groupsDescription
        txtFieldCategory.text = editGroupModel.categoriesName
        txtFieldName.text = editGroupModel.groupsName
        txtFieldDescription.text = editGroupModel.groupsDescription
        txtFieldEmail.text = editGroupModel.groupsEmail
        txtFieldPhoneNumber.text = editGroupModel.groupsPhone
        txtFieldWebsite.text = editGroupModel.groupsWebsite
        txtFieldAddress1.text = editGroupModel.groupsAddress
        txtFieldAddress2.text = editGroupModel.groupsAddress2
        txtFieldCity.text = editGroupModel.groupsCity
        txtFieldPostalCode.text = editGroupModel.groupsPostalCode
        txtFieldProvince.text = editGroupModel.groupsRegion
        txtFieldCountry.text = editGroupModel.groupsCountry
        editorView.html = editGroupModel.groupsBio ?? ""
        groupsId = editGroupModel.groupsID!
        id = Int(editGroupModel.categoriesID!)!
        
        let isVisible = editGroupModel.groupsHidden!
        let isPending = editGroupModel.groupsPending!
        let isDonation = editGroupModel.groupsDonations!
        let isPassive = editGroupModel.groupsPassive!
        
        if Int(isVisible) == 1 {
            visible = Int(isVisible)!
            btnStatusVisibleToGroupsOutlet.isSelected = true
        }
        
        if Int(isPending) == 1 {
            btnStatusPendingOutlet.isSelected = true
            pending = Int(isPending)!
        }
        
        if Int(isDonation) == 1 {
            btnStatusAcceptDonationOutlet.isSelected = true
            donation = Int(isDonation)!
        }
        
        if Int(isPassive) == 1 {
            btnStatusPassiveOutlet.isSelected = true
            passive = Int(isPassive)!
            
        }
        
    }
/*---------------------------------------------------------------------------------------------------*/

    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*------------------------------------------------------------------------------------------------*/

    func groupData(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------------*/

    func listOfGroupMembersApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        listOfMember.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.listOfMember.append(ListOfGroupMemberModel(data: [i]))
                        }
                        print("group data fetched")
                    }else {
                        print("group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*----------------------------------------------------------------------------------------------------------*/
    
    func pendingRequestApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        pendingRequest.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.pendingRequest.append(PendingRequestAccessModel(data: [i]))
                        }
                        print("group data fetched")
                    }else {
                        print("group data didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
   
/*-------------------------------------------------------------------------------------------------------*/

    func removeMemberFromGroup(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*-------------------------------------------------------------------------------------------------*/
    
    func acceptRejectPendingRequest(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
/*-----------------------------------------------------------------------------------*/
    //MARK -  initialLayout
    func initialLayout(){
        
        //Textfields Delegate settings
        self.txtFieldCategory.delegate = self
        self.txtFieldName.delegate = self
        self.txtFieldDescription.delegate = self
        self.txtFieldEmail.delegate = self
        self.txtFieldPhoneNumber.delegate =  self
        self.txtFieldWebsite.delegate = self
        self.txtFieldAddress1.delegate = self
        self.txtFieldAddress2.delegate = self
        self.txtFieldCity.delegate = self
        self.txtFieldPostalCode.delegate = self
        self.txtFieldProvince.delegate = self
        self.txtFieldCountry.delegate = self
        
        self.btnStatusVisibleToGroupsOutlet.isSelected = false
        self.btnStatusPassiveOutlet.isSelected = false
        self.btnStatusPendingOutlet.isSelected = false
        self.btnStatusAcceptDonationOutlet.isSelected = false
        
        self.txtFieldCategory.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        self.txtFieldCategory.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "dropdown.png")
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 23, height: 12))
        rightView.addSubview(imageView)
        self.txtFieldCategory.rightView = rightView
        
        self.txtFieldWebsite.leftViewMode = UITextField.ViewMode.always
        let leftViewForWebsite = UIView()
        leftViewForWebsite.frame = CGRect(x: 0, y: 0, width: 35, height: 15)
        self.txtFieldWebsite.leftView = leftViewForWebsite
        
    }
    
   //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
/*------------------------------------------------------------------------------------------*/
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
/*------------------------------------------------------------------------------------------*/
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.groupMemberListTableView{
            return listOfMember.count
        }
        else {
            return pendingRequest.count
        }
    }
    
/*------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            if tableView == self.groupMemberListTableView {
                return 50
            }
            return 35
        }else {
            if tableView == self.groupMemberListTableView {
                return 70
            }
            return 55
        }
       
    }
    
/*------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        let group = groups[indexPath.row]
        if tableView == self.groupMemberListTableView{
            let cell = self.groupMemberListTableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as! MemberTableViewCell
            let firstName = listOfMember[indexPath.row].usersFirstName ?? ""
            let lastName = listOfMember[indexPath.row].usersLastName ?? ""
            cell.lblMemberName?.text = "\(firstName) \(lastName)"
            cell.removerMember.tag = indexPath.row
            cell.removerMember.addTarget(self, action: #selector(self.removeMember), for: .touchUpInside)
            
            cell.imageViewUser.layer.cornerRadius = cell.imageViewUser.frame.width/2
            let remoteImageUrlString = listOfMember[indexPath.row].profileImgName ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.imageViewUser.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
               print("image \(indexPath.row) loaded")
            })
            return cell
        }
        else {
            //    pendingAccessRequestTableView
            let cell = self.pendingAccessRequestTableView.dequeueReusableCell(withIdentifier: "PARCell", for: indexPath) as! PendingAccessTableViewCell
            let firstName = pendingRequest[indexPath.row].usersFirstName ?? ""
            let lastName = pendingRequest[indexPath.row].usersLastName ?? ""
            cell.memberName.text = "\(firstName) \(lastName)"
            cell.approveBtn.tag = indexPath.row
            cell.rejectBtn.tag = indexPath.row
            cell.approveBtn.addTarget(self, action: #selector(self.approveRequest), for: .touchUpInside)
            cell.rejectBtn.addTarget(self, action: #selector(self.rejectRequest), for: .touchUpInside)

            return cell
        }
    }
    
}


//MARK:- Extensions
extension EditGroupViewController: RichEditorDelegate {
    
    /*-----------------------------------------------------------------------------------------------------*/
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
}

extension EditGroupViewController: RichEditorToolbarDelegate {
    
/*-----------------------------------------------------------------------------------------------------*/
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
}
