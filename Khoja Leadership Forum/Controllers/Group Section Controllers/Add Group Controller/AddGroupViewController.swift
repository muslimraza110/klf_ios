
import UIKit
import Alamofire
import SwiftGifOrigin
import RichEditorView

class AddGroupViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var txtFieldCategory: UITextField!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldDescription: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var txtFieldWebsite: UITextField!
    @IBOutlet weak var txtFieldAddress1: UITextField!
    @IBOutlet weak var txtFieldAddress2: UITextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldPostalCode: UITextField!
    @IBOutlet weak var txtFieldProvince: UITextField!
    @IBOutlet weak var txtFieldCountry: UITextField!
    @IBOutlet weak var btnSaveGroupOutlet: UIButton!
    @IBOutlet weak var btnStatusVisibleToGroupsOutlet: UIButton!
    @IBOutlet weak var btnStatusPendingOutlet: UIButton!
    @IBOutlet weak var btnStatusAcceptDonationOutlet: UIButton!
    @IBOutlet weak var btnStatusPassiveOutlet: UIButton!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var lblAccept: UILabel!
    @IBOutlet weak var lblpending: UILabel!
    @IBOutlet weak var lblPassive: UILabel!
    @IBOutlet weak var webTextFieldOuterView: UIView!
    @IBOutlet weak var webTextFieldInnerView: UIView!
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    
    //MARK:- Custom Variables
    var visible = 0
    var pending = 0
    var donation = 0
    var passive = 0
    var nameFieldFlag = false
    var emailFieldFlag = false
    var phoneFieldFlag = false
    var address1FieldFlag = false
    var address2FieldFlag = false
    var descriptionFieldFlag = false
    var id = 2
    var Status = false
    var categoryReceived = ""
    var userRole = ""
    var isAdmin = 0
    var userId = 0
    var model = [ForumAddModel]()
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerHeight()
        categoryView.isHidden = true
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        
        initialLayout()
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Bio"
        
        toolbar.delegate = self
        toolbar.editor = editorView
        
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let done = RichEditorOptionItem(image: nil, title: "Done") { toolbar in
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(done)
        toolbar.options = options
        hideKeyboard()
    }
    
/*-----------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        userId = userData["id"] as! Int
        if userRole == "A" {
            isAdmin = 1
        }else {
            isAdmin = 0
            lblAccept.isHidden = true
            lblPassive.isHidden = true
            lblpending.isHidden = true
            btnStatusPassiveOutlet.isHidden = true
            btnStatusPendingOutlet.isHidden = true
            btnStatusAcceptDonationOutlet.isHidden = true
        }
        
    }
    
    //MARK:- Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.txtFieldName {
            self.txtFieldName.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        if textField == self.txtFieldDescription {
            self.txtFieldDescription.borderColor = .darkGray
            
            let maxLength = 250
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        if textField == self.txtFieldEmail {
            self.txtFieldEmail.borderColor = .darkGray
            
            let maxLength = 250
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldPhoneNumber {
            self.txtFieldPhoneNumber.borderColor = .darkGray
            
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldWebsite {
            
            let maxLength = 150
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldAddress1 {
            self.txtFieldAddress1.borderColor = .darkGray
            
            let maxLength = 300
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldAddress2 {
            self.txtFieldAddress2.borderColor = .darkGray
            
            let maxLength = 300
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldCity {
            self.txtFieldCity.borderColor = .darkGray
            
            let maxLength = 30
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldPostalCode {
            self.txtFieldPostalCode.borderColor = .darkGray
            
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldProvince {
            self.txtFieldProvince.borderColor = .darkGray
            
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else {
            self.txtFieldCountry.borderColor = .darkGray
            
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
    }
    
/*-----------------------------------------------------------------------------------------------*/
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.becomeFirstResponder() {
            categoryView.isHidden = true
        }
    }
    
    //MARK:- UITouch Delegate Function
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK:- Btn Actions
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------*/

    @IBAction func btnStatusVisibleToGroupsPressed(_ sender: UIButton) {
        
        if !self.btnStatusVisibleToGroupsOutlet.isSelected{
            self.btnStatusVisibleToGroupsOutlet.setBackgroundImage(UIImage(named: "ic_check.png"), for: .normal)
            self.btnStatusVisibleToGroupsOutlet.isSelected =  true
            visible = 1
        }
        else{
            self.btnStatusVisibleToGroupsOutlet.setBackgroundImage(UIImage(named: "ic_uncheck.png"), for: .normal)
            self.btnStatusVisibleToGroupsOutlet.isSelected =  false
            visible = 0
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------*/

    @IBAction func categoryTriggerBtn(_ sender: Any) {
        categoryView.isHidden = false
    }
    
/*------------------------------------------------------------------------------------------------*/
    
    @IBAction func CategoryButton(_ sender: UIButton) {
        if sender.tag == 1{
            id = 2
            txtFieldCategory.text = "Business"
            categoryView.isHidden = true
            
        }else if sender.tag == 2 {
            id = 3
             txtFieldCategory.text = "Charities"
            categoryView.isHidden = true

        }
    }
/*------------------------------------------------------------------------------------------*/
    
    @IBAction func btnStatusAcceptDonationsPressed(_ sender: UIButton) {
        
        if !self.btnStatusAcceptDonationOutlet.isSelected{
            self.btnStatusAcceptDonationOutlet.setBackgroundImage(UIImage(named: "ic_check.png"), for: .normal)
            self.btnStatusAcceptDonationOutlet.isSelected =  true
            donation = 1
        }
        else{
            self.btnStatusAcceptDonationOutlet.setBackgroundImage(UIImage(named: "ic_uncheck.png"), for: .normal)
            self.btnStatusAcceptDonationOutlet.isSelected =  false
            donation = 0
        }
    }
/*--------------------------------------------------------------------------------------------*/
    
    @IBAction func btnStatusPendingPressed(_ sender: UIButton) {
        
        if !self.btnStatusPendingOutlet.isSelected{
            self.btnStatusPendingOutlet.setBackgroundImage(UIImage(named: "ic_check.png"), for: .normal)
            self.btnStatusPendingOutlet.isSelected =  true
            pending = 1
        }
        else{
            self.btnStatusPendingOutlet.setBackgroundImage(UIImage(named: "ic_uncheck.png"), for: .normal)
            self.btnStatusPendingOutlet.isSelected =  false
            pending = 0
        }
    }
    
/*----------------------------------------------------------------------------------------------*/
    
    @IBAction func btnStatusPassivePressed(_ sender: UIButton) {
        
        if !self.btnStatusPassiveOutlet.isSelected{
            self.btnStatusPassiveOutlet.setBackgroundImage(UIImage(named: "ic_check.png"), for: .normal)
            self.btnStatusPassiveOutlet.isSelected =  true
            passive = 1
        }
        else{
            self.btnStatusPassiveOutlet.setBackgroundImage(UIImage(named: "ic_uncheck.png"), for: .normal)
            self.btnStatusPassiveOutlet.isSelected =  false
            passive = 0
        }
    }
    
/*-----------------------------------------------------------------------------------------------*/
    
    @IBAction func btnSaveGroupButtonPressed(_ sender: UIButton) {
        if textFieldFormatCheck() {
            print("all mendatory fields are defined")
            let param = ["category_id" :id,"name" : txtFieldName.text!,"description" : txtFieldDescription.text!,"email" : txtFieldEmail.text!,"phone" : txtFieldPhoneNumber.text!,"address" : txtFieldAddress1.text!,"address2" : txtFieldAddress2.text!,"website" : txtFieldWebsite.text!,"city" : txtFieldCity.text!,"region" : txtFieldProvince.text!,"country" : txtFieldCountry.text!,"postal_code" : txtFieldPostalCode.text!,"bio" : editorView.html,"hidden" : visible,"pending" : pending,"vote_decision" : "","donations" : donation,"passive":passive,"user_id":userId,"is_admin":isAdmin] as [String:Any]
            
            groupData(url: AddGroupUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print("Group has been added")
                    self.navigationController?.popViewController(animated: true)
                }else {
                    print("Group couldn't add")
                }
                self.loaderView.isHidden = true
            }
        }
        else{
            print("one or more mendatory fields are empty.")
        }
        
    }

    //MARK:- Custom Functions
    func groupData(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------*/
    
    func initialLayout(){
        
        self.txtFieldCategory.delegate = self
        self.txtFieldName.delegate = self
        self.txtFieldDescription.delegate = self
        self.txtFieldEmail.delegate = self
        self.txtFieldPhoneNumber.delegate =  self
        self.txtFieldWebsite.delegate = self
        self.txtFieldAddress1.delegate = self
        self.txtFieldAddress2.delegate = self
        self.txtFieldCity.delegate = self
        self.txtFieldPostalCode.delegate = self
        self.txtFieldProvince.delegate = self
        self.txtFieldCountry.delegate = self
        
        self.btnStatusVisibleToGroupsOutlet.isSelected = false
        self.btnStatusPassiveOutlet.isSelected = false
        self.btnStatusPendingOutlet.isSelected = false
        self.btnStatusAcceptDonationOutlet.isSelected = false
        
        self.txtFieldCategory.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        self.txtFieldCategory.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "dropdown.png")
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 23, height: 12))
        rightView.addSubview(imageView)
        self.txtFieldCategory.rightView = rightView
        
        self.txtFieldWebsite.leftViewMode = UITextField.ViewMode.always
        let leftViewForWebsite = UIView()
        leftViewForWebsite.frame = CGRect(x: 0, y: 0, width: 35, height: 15)
        self.txtFieldWebsite.leftView = leftViewForWebsite
        
    }
    
/*-----------------------------------------------------------------------------------------------*/

    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------*/

    func textFieldFormatCheck() -> Bool {
        
        if self.txtFieldAddress2.text?.count == 0 {
            self.txtFieldAddress2.borderColor = .red
            self.txtFieldAddress2.becomeFirstResponder()
            address2FieldFlag = false
        }
        else{address2FieldFlag = true}
        
        if self.txtFieldAddress1.text?.count == 0 {
            self.txtFieldAddress1.borderColor = .red
            self.txtFieldAddress1.becomeFirstResponder()
            address1FieldFlag = false
        }
        else{address1FieldFlag = true}
        
        if self.txtFieldPhoneNumber.text?.count == 0 {
            self.txtFieldPhoneNumber.borderColor = .red
            self.txtFieldPhoneNumber.becomeFirstResponder()
            phoneFieldFlag = false
        }
        else{phoneFieldFlag = true}
        
        if self.txtFieldEmail.text?.count == 0 || validateEmail(email: self.txtFieldEmail.text!) == false{
            self.txtFieldEmail.borderColor = .red
            self.txtFieldEmail.becomeFirstResponder()
            emailFieldFlag = false
        }
        else{emailFieldFlag = true}
        
        if self.txtFieldDescription.text?.count == 0 {
            self.txtFieldDescription.borderColor = .red
            self.txtFieldDescription.becomeFirstResponder()
            descriptionFieldFlag = false
        }
        else{descriptionFieldFlag = true}
        
        if self.txtFieldName.text?.count == 0 {
            self.txtFieldName.borderColor = .red
            self.txtFieldName.becomeFirstResponder()
            nameFieldFlag = false
        }
        else{nameFieldFlag = true}
        
        if nameFieldFlag && emailFieldFlag && phoneFieldFlag && address1FieldFlag && address2FieldFlag && descriptionFieldFlag == true {return true}
        else{return false}
    }
    
  
}
/*-----------------------------------------------------------------------------------------------------*/
//MARK:- Extensions
extension AddGroupViewController: RichEditorDelegate {
    
/*-----------------------------------------------------------------------------------------------------*/
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
}

extension AddGroupViewController: RichEditorToolbarDelegate {
    
/*-----------------------------------------------------------------------------------------------------*/
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
}

