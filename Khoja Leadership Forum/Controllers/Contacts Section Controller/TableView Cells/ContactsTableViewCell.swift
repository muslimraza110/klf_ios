
import UIKit

class ContactsTableViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblContactPhone: UILabel!
    @IBOutlet weak var lblContactEmail: UILabel!
    
    @IBOutlet weak var contactImageView: UIImageView!
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
}
