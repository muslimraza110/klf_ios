
import UIKit
import WebKit
import MessageUI

class ContactDetailViewController: UIViewController, MFMailComposeViewControllerDelegate {

    //Outlets
    
    @IBOutlet weak var lblContactFullName: UILabel!
    @IBOutlet weak var lblContactDesignation: UILabel!
    @IBOutlet weak var webViewContactBio: WKWebView!
//    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblContactPhone: UILabel!
    @IBOutlet weak var lblContactAddress: UILabel!
    @IBOutlet weak var lblContactWebsite: UILabel!
    @IBOutlet weak var lblContactFacebook: UILabel!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblFacebook: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var btnEmail: AdaptiveButton!
    
    var contactModel = ContactModel()
    var imageURLReceived:String = ""
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerHeight()
        contactInformation()
        
        self.profileImage.layer.cornerRadius = self.profileImage.frame.width/2
        self.profileImage.sd_setImage(with: URL(string: self.imageURLReceived), completed: nil)
        
        // Do any additional setup after loading the view.
    }

/*-----------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
     
    func configuredMailComposeViewController() -> MFMailComposeViewController {
         let mailComposerVC = MFMailComposeViewController()
         mailComposerVC.mailComposeDelegate = self
         mailComposerVC.setToRecipients([email])
         return mailComposerVC
     }
     
     func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
         controller.dismiss(animated: true, completion: nil)
     }
     
     func showSendMailErrorAlert() {
         self.ShowErrorAlert(message: "Could Not Send Email", AlertTitle: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
     }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
        @IBAction func btnEmailPressed(_ sender: Any) {
            let mailComposeViewController = self.configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.present(mailComposeViewController, animated: true, completion: nil)
            } else {
               self.showSendMailErrorAlert()
            }
        }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
      
    func contactInformation() {
        let firstName = contactModel.userFirstName
        let lastName = contactModel.userLastName
        let fontSize = (32 * (self.view.frame.width/320))
        let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
        webViewContactBio.loadHTMLString(fontSetting + (contactModel.userDetailBio ?? "") , baseURL: nil)
        lblContactPhone.text = contactModel.userDetailPhone1
        lblContactAddress.text = "\(contactModel.userDetailAddress1 ?? "") \(contactModel.userDetailAddress2 ?? "")"
        lblContactWebsite.text = contactModel.userDetailWebsite
        lblContactFacebook.text = contactModel.userDetailFacebook
        lblContactFullName.text = "\(firstName ?? "")  \(lastName ?? "")"
        lblContactDesignation.text = contactModel.userDetailDescription
        email = contactModel.userEmail ?? ""
//        btnEmail.titleLabel?.text = contactModel.userEmail
        btnEmail.setTitle(email, for: .normal)
//        lblEmail.text = contactModel.userEmail
        
        if contactModel.userDetailAddress1 == "" {
            lblAddress.isHidden = true
        }
        if contactModel.userDetailWebsite == "" {
            lblWebsite.isHidden = true
        }
        if contactModel.userDetailFacebook == "" {
            lblFacebook.isHidden = true
        }
        
    }
/*-----------------------------------------------------------------------------------------------------*/
    //MARK:- Button Actions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
/*-----------------------------------------------------------------------------------------------------*/
}
