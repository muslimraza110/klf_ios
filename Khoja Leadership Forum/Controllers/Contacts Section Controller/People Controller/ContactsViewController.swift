
import UIKit
import Alamofire
import SwiftGifOrigin
import SDWebImage

class ContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{


    //MARK:- Button Outlets
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var isRecordFound: UILabel!
    @IBOutlet weak var recommendView: UIView!
    @IBOutlet weak var tableviewTopConstraint: NSLayoutConstraint!
    
    //MARK:- Variables
    var Status = false
    var userRole = "A"
    var triggerOfflineMode = false
    var isSearching = false
    var contactModel = [ContactModel]()
    var filtered = [ContactModel]()
    var sectionContactModel = [String: [ContactModel]]()
    var contactIndexTitles = [String]()
    var filteredIndexTitle = [String]()
    var filteredContactModel = [String: [ContactModel]]()

    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        
         triggerOfflineMode = UserDefaults.standard.value(forKey: "triggerOffline") as? Bool ?? false
        
        isRecordFound.isHidden = true
        headerHeight()
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib.init(nibName: "ContactsTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactsTableViewCell")
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        searchBar.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        searchBar.returnKeyType = UIReturnKeyType.done
        hideKeyboard()
        
        if !triggerOfflineMode {
            loadApi()
        }else {
            offlineLoadData()
        }

    }

    //MARK:- Custom Functions
    @objc func textFieldDidChange(_ textField: UITextField) {

        if textField.text == "" || textField.text == nil  {
            isSearching = false
            isRecordFound.isHidden = true
            textField.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            tableView.reloadData()
        }else {
            filteredIndexTitle.removeAll()
            filteredContactModel.removeAll()
            isSearching = true
            let searchText  = textField.text
            filtered = contactModel.filter{
                $0.userFirstName!.localizedCaseInsensitiveContains(String(searchText!)) || $0.userLastName!.localizedCaseInsensitiveContains(String(searchText!)) || $0.userDetailCity!.localizedCaseInsensitiveContains(String(searchText!)) || $0.userDetailAddress1!.localizedCaseInsensitiveContains(String(searchText!)) || $0.userDetailPrivateBio!.localizedCaseInsensitiveContains(String(searchText!))
            }
            //Using for alphabet seraches
            for i in self.filtered {
                let firstIndexOfTitle = String(i.userFirstName!.prefix(1).uppercased())
                if var carValues = self.filteredContactModel[firstIndexOfTitle] {
                    carValues.append(i)
                    self.filteredContactModel[firstIndexOfTitle] = carValues
                } else {
                    self.filteredContactModel[firstIndexOfTitle] = [i]
                }
            }
            self.filteredIndexTitle = [String](self.filteredContactModel.keys)
            self.filteredIndexTitle = self.filteredIndexTitle.sorted(by: { $0 < $1 })
            
            if filteredIndexTitle.count == 0 {
                isRecordFound.isHidden = false
            }else {
                isRecordFound.isHidden = true
            }
            tableView.reloadData()
        
        }
    }
/*-----------------------------------------------------------------------------------------------------*/

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchBar {
            textField.resignFirstResponder()
        }
        return true
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
    func loadApi(){
        getContactList(url: contactListUrl, method: responseType.get.rawValue, headers: nil) { (status) in
            if status {
                print("Contact list comes")
            }else {
                print("Contact list couldn't load")
            }
            self.tableView.reloadData()
            self.loaderView.isHidden = true
        }

    }

/*-----------------------------------------------------------------------------------------------------*/

    func offlineLoadData() {
        let archiveContact = UserDefaults.standard.object(forKey: "contact") as? NSData
        if archiveContact != nil {
            let unarchiveContact = NSKeyedUnarchiver.unarchiveObject(with: archiveContact! as Data) as? [[String:Any]]
            if let contact = unarchiveContact {
                var contactData = [ContactModel]()
                for i in contact {
                    contactData.append(ContactModel(data: [i]))
                }
                
                if self.userRole.uppercased() == "A" {
                   self.contactModel = contactData
               }else {
                   for i in contactData {
                       if i.userRole?.uppercased() != "A" {
                           self.contactModel.append(i)
                       }
                   }
               }
                /*---------------------------------------------------------*/
                //Using for alphabet seraches
                for i in self.contactModel {
                    let firstIndexOfTitle = String(i.userFirstName!.prefix(1).uppercased())
                    if var carValues = self.sectionContactModel[firstIndexOfTitle] {
                        carValues.append(i)
                        self.sectionContactModel[firstIndexOfTitle] = carValues
                    } else {
                        self.sectionContactModel[firstIndexOfTitle] = [i]
                    }
                }
                self.contactIndexTitles = [String](self.sectionContactModel.keys)
                self.contactIndexTitles = self.contactIndexTitles.sorted(by: { $0 < $1 })
                
                /*-----------------------------------------------*/
                tableView.reloadData()
            }
        }else {
            isRecordFound.isHidden = false
        }
    }
/*-----------------------------------------------------------------------------------------------------*/

    func getContactList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: nil, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        var contactData = [ContactModel]()
                        for i in data {
                            contactData.append(ContactModel(data: [i]))
                        }
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: data)
                        UserDefaults.standard.set(encodedData, forKey: "contact")
                        
                        if self.userRole.uppercased() == "A" {
                            self.contactModel = contactData
                        }else {
                            for i in contactData {
                                if i.userRole?.uppercased() != "A" {
                                    self.contactModel.append(i)
                                }
                            }
                        }
                        
                        /*---------------------------------------------------------*/
                        //Using for alphabet seraches
                        for i in self.contactModel {
                            let firstIndexOfTitle = String(i.userFirstName!.prefix(1).uppercased())
                            if var carValues = self.sectionContactModel[firstIndexOfTitle] {
                                carValues.append(i)
                                self.sectionContactModel[firstIndexOfTitle] = carValues
                            } else {
                                self.sectionContactModel[firstIndexOfTitle] = [i]
                            }
                        }
                        self.contactIndexTitles = [String](self.sectionContactModel.keys)
                        self.contactIndexTitles = self.contactIndexTitles.sorted(by: { $0 < $1 })

                            /*-----------------------------------------------*/
                        print("Data Fetched")
                    }else {
                        print("Data couldn't Fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true

            }
        }
    }

/*-----------------------------------------------------------------------------------------------------*/
    //MARK:- Button Actions
    @IBAction func searchBtn(_ sender: Any) {
    }
/*-----------------------------------------------------------------------------------------------------*/

    @IBAction func back(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- TableView Delegates
/*-----------------------------------------------------------------------------------------------------*/
    
    
    // return number of section in a tableview
    //MARK - numberOfSections
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearching {
            return filteredIndexTitle.count
        }
        return contactIndexTitles.count
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    //MARK - sectionIndexTitles
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isSearching {
            return filteredIndexTitle
        }
        return contactIndexTitles
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    //MARK:- numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            let character = filteredIndexTitle[section]
            if let data = filteredContactModel[character] {
                return data.count
            }
        }
        let character = contactIndexTitles[section]
        if let data = sectionContactModel[character] {
            return data.count
        }
        
        return 0
    }

/*-----------------------------------------------------------------------------------------------------*/

    //MARK - sectionForSectionIndexTitle
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isSearching {
            return filteredIndexTitle[section]
        }
        return contactIndexTitles[section]
    }
/*-----------------------------------------------------------------------------------------------------*/
    //MARK - cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTableViewCell") as! ContactsTableViewCell
        if isSearching {
            let character = filteredIndexTitle[indexPath.section]
            if let groupObject = filteredContactModel[character] {
                cell.lblContactName.text = "\(groupObject[indexPath.row].userFirstName ?? "") \(groupObject[indexPath.row].userLastName ?? "")"
                cell.lblContactPhone.text = groupObject[indexPath.row].userDetailPhone1
                cell.lblContactEmail.text = groupObject[indexPath.row].userEmail
                
                // getting img from url
                cell.contactImageView.layer.cornerRadius = cell.contactImageView.frame.width/2
                let remoteImageUrlString = groupObject[indexPath.row].profileImg ?? ""
                let imageUrl = URL(string:remoteImageUrlString)
                cell.contactImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    print("image \(indexPath.row) loaded")
                })
                
            }
        }else {
            let character = contactIndexTitles[indexPath.section]
            if let groupObject = sectionContactModel[character] {
                cell.lblContactName.text = "\(groupObject[indexPath.row].userFirstName ?? "") \(groupObject[indexPath.row].userLastName ?? "")"
                cell.lblContactPhone.text = groupObject[indexPath.row].userDetailPhone1
                cell.lblContactEmail.text = groupObject[indexPath.row].userEmail
                
                 // getting img from url
                cell.contactImageView.layer.cornerRadius = cell.contactImageView.frame.width/2
                let remoteImageUrlString = groupObject[indexPath.row].profileImg ?? ""
                let imageUrl = URL(string:remoteImageUrlString)
                cell.contactImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    print("image \(indexPath.row) loaded")
                })
                                
            }
        }
        return cell
    }
/*-----------------------------------------------------------------------------------------------------*/
    //MARK - didSelectRowAt
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        if !triggerOfflineMode {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProfileViewController") as! ViewProfileViewController
            if isSearching {
                let character = filteredIndexTitle[indexPath.section]
                if let groupObject = filteredContactModel[character] {
                    vc.userId = Int(groupObject[indexPath.row].userID ?? "0") ?? 0
                    vc.isLoginMember = false
                }
            }else {
                let character = contactIndexTitles[indexPath.section]
                if let groupObject = sectionContactModel[character] {
                    vc.userId = Int(groupObject[indexPath.row].userID ?? "0") ?? 0
                    vc.isLoginMember = false
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
/*-----------------------------------------------------------------------------------------------------*/
    //MARK - heightForRowAt
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 80 * (view.frame.size.width / 320)
        }else {
            return 130

        }
    }

/*-----------------------------------------------------------------------------------------------------*/
}
