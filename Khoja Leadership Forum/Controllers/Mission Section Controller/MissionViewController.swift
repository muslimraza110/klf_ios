
import UIKit
import WebKit
import Alamofire
import SwiftGifOrigin

class MissionViewController: UIViewController,WKNavigationDelegate {

    //Outlets
    @IBOutlet weak var lblMissionTitle: UILabel!
    @IBOutlet weak var missionContentWebView: WKWebView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    
    var missionListModel = MissionListModel()
    var Status = false
    var fontSize : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.missionContentWebView.navigationDelegate = self
        
        missionListApi(url: missionListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status{
                if UIDevice().userInterfaceIdiom == .phone {
                    self.fontSize = 35
                }else {
                    self.fontSize = 40
                }
                let fontSetting = "<span style=\"font-size: \(self.fontSize)\"</span>"
                self.missionContentWebView.loadHTMLString(fontSetting + (self.missionListModel.pagesContent ?? "Data Not Found"), baseURL: nil)
                print("data loaded")
            }else {
                self.showToast(message: "Data couldn't load")
                print("data couldn't load")
            }
            self.loaderView.isHidden = true
        }
        
       
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
     
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func missionListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        var missionData = [MissionListModel]()
                        for i in data {
                            missionData.append(MissionListModel(data: [i]))
                        }
                        self.missionListModel = missionData[0]
                        print("missionList data fetched")
                    }else {
                        print("missionList didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)

    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
}
