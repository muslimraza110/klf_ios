
import UIKit

class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //Outlets
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var lblProfileEmail: UILabel!
    @IBOutlet weak var btnLogoutOutlet: UIButton!
    @IBOutlet weak var sideMenuTableView: UITableView!
    var apiClass = ApiCalling2()
    //MARK:- Variables & Arrays
    var triggerOfflineMode: Bool = false
    var userRole = "A"
    var profileImage = ""
    var userId = 0
    var dropImage = UIImage.init(named: "dropdown.png")
    
    var adminArray = [
        ExpandableNames(isExpanded: true, names: ["Admin","Charities","Business","Others"]),
        ExpandableNames(isExpanded: false, names: ["News","Khoja Summits"]),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: ["View Profile","Edit Profile","Change Password"]),
        ExpandableNames(isExpanded: false, names: [])
    ]
    
    var memberArray = [
        ExpandableNames(isExpanded: true, names: ["Admin","Charities","Business","Others"]),
        ExpandableNames(isExpanded: false, names: ["News","Khoja Summits"]),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: []),
        ExpandableNames(isExpanded: false, names: ["View Profile","Edit Profile","Change Password"]),
        ExpandableNames(isExpanded: false, names: [])
    ]
    
    var selectedArray = [ExpandableNames(isExpanded: false, names: ["Admin","Charities","Business","Others"])]
    var titleSelectedArray = [String]()
    var titleSelectedImageArray = [String]()
    var titleSelectedOfflineImageArray = [String]()
    var controllerSelectedArray = [String]()
    
    /*
     0-forums
     1-Resources
     2-Mission
     3-Groups
     4-Contacts
     5-Projects
     6-Initiatives
     7-Khoja Care
     8-Events
     9-Feedback
     10-FAQs
     12-Settings
     13-Summit
    */
    let titleAdminArray =   ["Forums","Resources","Mission","Groups","Contacts","Projects","Initiatives","Khoja Care","Events","Feedback","FAQs","Settings", "Summit"]
    
    let titleMemberArray =  ["Forums","Resources","Mission","Groups","Contacts","Projects","Initiatives","Khoja Care","Events","Feedback","FAQs","Settings", "Summit"]
    
    let titleAdminImageArray = ["forums.png","resources.png","mission.png","groups.png","contacts.png","projects.png","initiatives.png","khojacare.png","events.png","feedback.png","faqs.png","administration.png", "administration.png"]
    
    let titleMemberImageArray = ["forums.png","resources.png","mission.png","groups.png","contacts.png","projects.png","initiatives.png","khojacare.png","events.png","feedback.png","faqs.png","administration.png", "administration.png"]
    
    let titleAdminOfflineImageArray = ["of-admin-forums.png","of-admin-resources.png","of-admin-mission.png","of-admin-groups.png","of-admin-contacts.png","of-admin-projects.png","of-admin-initiatives.png","of-admin-khojacare.png","of-admin-events.png","of-admin-feedback.png","of-admin-faqs.png","of-admin-administration.png", "of-admin-administration.png" ]
    
    let titleMemberOfflineImageArray = ["of-admin-forums.png","of-admin-resources.png","of-admin-mission.png","of-admin-groups.png","of-admin-contacts.png","of-admin-projects.png","of-admin-initiatives.png","of-admin-khojacare.png","of-admin-events.png","of-admin-feedback.png","of-admin-faqs.png","of-admin-administration.png", "of-admin-administration.png"]
    
    let adminControllersArray = ["","","MissionViewController","GroupsViewController","ContactsViewController","ProjectsViewController","InitiativeViewController","KhojaCareViewController","EventsViewController","FeedbackViewController","FAQsViewController","", "SummitEvent_Lists_ViewController"]
    
    let memberControllersArray = ["","","MissionViewController","GroupsViewController","ContactsViewController","ProjectsViewController","InitiativeViewController","KhojaCareViewController","EventsViewController","FeedbackViewController","FAQsViewController","", "SummitEvent_Lists_ViewController"]

    let didSelectRowAtIndexPathRowTapped = [
        ["ForumAdminController", "ForumCharityController", "ForumBusinessController", "ForumOtherController"],
        ["NewsViewController", "KhojaSummitViewController"],
        [""],
        [""],
        ["ContactsViewController", "RecommendationsViewController", "GBTViewController"],
        [""],
        [""],
        [""],
        [""],
        [""],
        [""],
        ["ViewProfileViewController", "EditProfileViewController", "ChangePasswordViewController"],
    ]
    

    //MARK:- Delegate Functions
/*-----------------------------------------------------------------------------------------------------------------------------------*/
     //MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        apiClass.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(changeLayout), name: NSNotification.Name("networkUnreachableNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeLayout), name: NSNotification.Name("networkReachableNotification"), object: nil)
        
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        profileImage = userData["profile_img"] as! String
        let firstName = userData["first_name"] as! String
        let lastName = userData["last_name"] as! String
        let email = userData["email"] as! String
        lblProfileName.text = "\(firstName) \(lastName)"
        lblProfileEmail.text = email
        let offlineMode = UserDefaults.standard.value(forKey: "triggerOffline") as? Bool ?? false
        triggerOfflineMode = offlineMode
        
        loadDataForSideMenu()
        
        self.sideMenuTableView.delegate = self
        self.sideMenuTableView.dataSource = self
        
        self.profileImageView.layer.cornerRadius = profileImageView.frame.width/2
        let url = URL(string: profileImage)!
        let imgData = try? Data(contentsOf: url)
        
        if let imageData = imgData {
            self.profileImageView.image = UIImage(data: imageData)
        }
        self.btnLogoutOutlet.layer.cornerRadius = 10
        
    }
    //MARK:- Custom Functions
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func logoutButtonPressed(_ sender: UIButton) {
        print("logout button pressed")
        //Removing all user defaults
        let fcmToken = UserDefaults.standard.value(forKey: Constants.Token.fcmToken) as? String ?? ""
        userId = userData["id"] as! Int
        if UserDefaults.standard.value(forKey: "IsRememberOn") as? Int == 0 {

            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
            UserDefaults.standard.set(fcmToken , forKey: Constants.Token.fcmToken)
            UserDefaults.standard.synchronize()
            
        }

        let param =
            ["user_id": "\(userId)",
             "fcm_token" : fcmToken]

            apiClass.loadApiCall(identifier: logoutUser, url: logoutUser, param: param, method: responseType.post.rawValue, header: headerToken, view: self)
        UserDefaults.standard.set("false", forKey: "islogin")
        UIApplication.shared.applicationIconBadgeNumber = 0
        //setting login as root
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = vc
        NotificationCenter.default.post(name: NSNotification.Name("logoutNotification"), object:nil)
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - loadDataForSideMenu
    func loadDataForSideMenu(){
        
        if self.userRole == "A"{
            self.selectedArray = self.adminArray
            self.titleSelectedArray = self.titleAdminArray
            self.titleSelectedImageArray = self.titleAdminImageArray
            self.titleSelectedOfflineImageArray = self.titleAdminOfflineImageArray
            self.controllerSelectedArray = self.adminControllersArray
        }
        else{
            self.selectedArray = self.memberArray
            self.titleSelectedArray = self.titleMemberArray
            self.titleSelectedImageArray = self.titleMemberImageArray
            self.titleSelectedOfflineImageArray = self.titleMemberOfflineImageArray
            self.controllerSelectedArray = self.memberControllersArray
    }
}
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
@objc func changeLayout(_ notification:Notification) {
    
        if notification.name.rawValue == "networkUnreachableNotification"
        {
            triggerOfflineMode = true
            loadDataForSideMenu()
            sideMenuTableView.reloadData()
        }
        if notification.name.rawValue == "networkReachableNotification"
        {
            triggerOfflineMode = false
            loadDataForSideMenu()
            sideMenuTableView.reloadData()
        }
    
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
     //MARK - detectSectionHeaderClick
    @objc func detectSectionHeaderClick(_ sender:UIButton) {
        
        var dataDict:[String: String] = ["valueTapped": ""]
        
        if userRole == "A"{
            if triggerOfflineMode == false{
                //sender = 12 (is Summit)
                print(sender.tag)
                if sender.tag == 2 || sender.tag == 3 || sender.tag == 4 || sender.tag == 5 || sender.tag == 6 || sender.tag == 7 || sender.tag == 8 || sender.tag == 9 || sender.tag == 10 || sender.tag == 12 {
                    // use this array to show the selected controller -- code will be here
                    print("\(self.controllerSelectedArray[sender.tag]) section pressed")
                    dataDict["valueTapped"] = self.controllerSelectedArray[sender.tag]
                    print("passing value dictionary + \(dataDict)")
                    NotificationCenter.default.post(name: NSNotification.Name("headerSectionTappedNotification"), object:nil, userInfo: dataDict)
                }
                else{
                    print("other section tapped")
                }
            }
            else{
                if sender.tag == 4 || sender.tag == 8{
                    // use this array to show the selected controller -- code will be here
                    dataDict["valueTapped"] = self.controllerSelectedArray[sender.tag]
                    print("\(self.controllerSelectedArray[sender.tag]) section pressed")
                    NotificationCenter.default.post(name: NSNotification.Name("headerSectionTappedNotification"), object:nil, userInfo: dataDict)
                }
            }
        }
        else{
            if triggerOfflineMode == false{
                if sender.tag != 0 && sender.tag != 1 && sender.tag != 11 {
                    // use this array to show the selected controller -- code will be here
                    dataDict["valueTapped"] = self.controllerSelectedArray[sender.tag]
                    print("passing value dictionary + \(dataDict)")
                    NotificationCenter.default.post(name: NSNotification.Name("headerSectionTappedNotification"), object:nil, userInfo: dataDict)
                }
                else{
                    print("other section tapped")
                }
            }
            else{
                if sender.tag == 4 || sender.tag == 8{
                    // use this array to show the selected controller -- code will be here
                    print("\(self.controllerSelectedArray[sender.tag]) section pressed")
                    dataDict["valueTapped"] = self.controllerSelectedArray[sender.tag]
                    NotificationCenter.default.post(name: NSNotification.Name("headerSectionTappedNotification"), object:nil, userInfo: dataDict)
                }
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
 //MARK - viewForHeaderInSection
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        //Custom Button to detect the Header section tap event
        let buttonWithTag = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width-80, height: 100))
        buttonWithTag.tag = section
        buttonWithTag.addTarget(self, action: #selector(self.detectSectionHeaderClick(_:)), for: .touchUpInside)
        
        
        // code for adding centered title
        headerView.backgroundColor = UIColor.white
        let headerLabel = UILabel(frame: CGRect(x: 55, y: 5, width:tableView.bounds.size.width, height: 30))
        headerLabel.textColor = UIColor.black
        headerLabel.text = titleSelectedArray[section]
        if section == 10
        {
            print(section)
        }
        print(headerLabel.text)
        if (UI_USER_INTERFACE_IDIOM() == .pad){
            headerLabel.font = UIFont.boldSystemFont(ofSize: 20)
        }else{
           headerLabel.font = UIFont.boldSystemFont(ofSize: 15)
         }
        
        headerLabel.textAlignment = .left
        headerLabel.contentMode = .scaleAspectFit
        
        let imageViewSideMenu = UIImageView(frame: CGRect(x: 15, y: 5, width: 25, height: 25))
        var image = UIImage()
        if triggerOfflineMode {
            image = UIImage(named: titleSelectedOfflineImageArray[section])!
            if section == 4 || section == 8{
                headerView.backgroundColor = UIColor.darkGray
                headerLabel.textColor = UIColor.white
            }
            else{
                headerView.backgroundColor = UIColor.lightGray
                headerLabel.textColor = UIColor.white
            }
        }
        else{
                image = UIImage(named: titleSelectedImageArray[section])!
            
            }
        imageViewSideMenu.image = image;
        headerView.addSubview(imageViewSideMenu)
        headerView.addSubview(headerLabel)
        
        let arrowButton = UIButton(frame: CGRect(x:headerView.frame.size.width - 100, y:0, width:100, height:30))
        if triggerOfflineMode {
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
        }
        else{
            dropImage = UIImage.init(named: "dropdown.png")
        }
        arrowButton.setImage(dropImage, for: .normal)
        arrowButton.setTitleColor(.black, for: .normal)
        arrowButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        arrowButton.backgroundColor = UIColor.clear
        arrowButton.addTarget(self, action: #selector(ExpandClose), for: .touchUpInside)
        arrowButton.tag = section
        arrowButton.imageView?.contentMode = .scaleAspectFit

        let countValue = self.selectedArray[section].names.count
        if countValue != 0{
            headerView.addSubview(arrowButton)
        }
        headerView.addSubview(buttonWithTag)
        
        
        //constraint for dropdown arrow
        
        imageViewSideMenu.translatesAutoresizingMaskIntoConstraints = false
        imageViewSideMenu.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imageViewSideMenu.heightAnchor.constraint(equalToConstant: 25).isActive = true
        imageViewSideMenu.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        imageViewSideMenu.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10).isActive = true
        imageViewSideMenu.setNeedsLayout()
        imageViewSideMenu.clipsToBounds = true
        
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        headerLabel.widthAnchor.constraint(equalToConstant: tableView.frame.width-120).isActive = true
        headerLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        headerLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo: imageViewSideMenu.trailingAnchor, constant: 20).isActive = true
        headerLabel.setNeedsLayout()
        headerLabel.clipsToBounds = true
        
        if countValue != 0{
        arrowButton.translatesAutoresizingMaskIntoConstraints = false
        arrowButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        arrowButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        arrowButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        arrowButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: 20).isActive = true
        arrowButton.setNeedsLayout()
        arrowButton.clipsToBounds = true

        }
        
        return headerView
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// expand close button functionality
     //MARK -ExpandClose
    @objc func ExpandClose (button: UIButton) {
        
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in selectedArray[section].names.indices{
            let indexpath = IndexPath(row: row, section: section)
            indexPaths.append(indexpath)
        }
        
        let isExpanded = selectedArray[section].isExpanded
        selectedArray[section].isExpanded = !isExpanded
        if isExpanded {
            button.isSelected = false
            if triggerOfflineMode {
                dropImage = UIImage.init(named: "of-admin-dropdown.png")
            }
            else{
                dropImage = UIImage.init(named: "dropdown.png")
            }
            button.setImage(dropImage, for: .normal)
            sideMenuTableView.deleteRows(at: indexPaths, with: .top)
        }
        else{
            button.isSelected = true
            if triggerOfflineMode {
                dropImage = UIImage.init(named: "of-admin-dropup.png")
            }
            else{
                dropImage = UIImage.init(named: "dropup.png")
            }
            button.setImage(dropImage, for: .normal)
            sideMenuTableView.insertRows(at: indexPaths, with: .bottom)
        }
        
    }
    //MARK:- TableViewƒ
/*-----------------------------------------------------------------------------------------------------------------------------------*/
     //MARK - cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        let name = self.selectedArray[indexPath.section].names[indexPath.row]
        cell.textLabel?.text = name
        cell.backgroundColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1.0)
        
        if (UI_USER_INTERFACE_IDIOM() == .pad){
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        }else{
           cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 12)
         }
        
        cell.indentationLevel = 4
        
        if triggerOfflineMode {
            if (indexPath.section == 4 && indexPath.row == 0){
                cell.backgroundColor = UIColor.white
            }
            else{
                cell.backgroundColor = UIColor.lightGray
            }
        }
        return cell
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - didSelectRowAtIndexPath
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //code here
        
        self.sideMenuTableView.deselectRow(at: indexPath, animated: true)
        
        var dataDictionaryToPass:[String: String] = ["valueTapped": ""]
        if userRole == "A"{
            switch indexPath.section as Int{
            case 0, 1, 11, 12:
                dataDictionaryToPass["valueTapped"] = self.didSelectRowAtIndexPathRowTapped[indexPath.section][indexPath.row]
                print("passing value dictionary + \(dataDictionaryToPass)")
                NotificationCenter.default.post(name: NSNotification.Name("didSelectRowNotification"), object:nil, userInfo: dataDictionaryToPass)
            default:
                print("case default")
            }
        }
        else{
                switch indexPath.section as Int{
                case 0, 1, 11:
                        dataDictionaryToPass["valueTapped"] = self.didSelectRowAtIndexPathRowTapped[indexPath.section][indexPath.row]
                    
                    print("passing value dictionary + \(dataDictionaryToPass)")
                    NotificationCenter.default.post(name: NSNotification.Name("didSelectRowNotification"), object:nil, userInfo: dataDictionaryToPass)
                default:
                    print("case default")
                }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// return number of rows in a section
     //MARK - numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !selectedArray[section].isExpanded {
           return 0
        }
        return self.selectedArray[section].names.count
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// setting row height
     //MARK - heightForRowAt
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (UI_USER_INTERFACE_IDIOM() == .pad){
            return 60
        }else{
            return 40
         }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// section spacing in a tableview
     //MARK - heightForFooterInSection
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.5
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// return number of section in a tableview
     //MARK - numberOfSections
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.selectedArray.count
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// Header height setting
     //MARK - heightForHeaderInSection
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (UI_USER_INTERFACE_IDIOM() == .pad){
            return 60
        }else{
            return 40
         }

    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// Log out functionality button
    @IBAction func btnLogout(_ sender: UIButton) {
        print("logout button pressed")
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// For blocking the cell selection and segue upon selection
     //MARK - shouldHighlightRowAt
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        
        var flag:Bool = false
        if triggerOfflineMode{
            if (indexPath.section == 4 && indexPath.row == 0) || indexPath.section == 8 {
                flag = true
            }
            else {
                flag = false
            }
        }
        else {flag = true}
        return flag
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

}

extension SideMenuViewController: ApiDelegate {
    func successApiCall(data: Data, identifier: String, status: Bool) {
        debugPrint("Successfully Logout")
    }

    func failureApiCall(message: String, identifier: String) {
        debugPrint("Failed Logout")
    }


}
