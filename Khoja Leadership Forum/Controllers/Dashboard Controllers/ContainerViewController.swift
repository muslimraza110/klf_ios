
import UIKit

class ContainerViewController: UIViewController {

    @IBOutlet weak var trailingConstraingOutlet: NSLayoutConstraint!
    @IBOutlet weak var slideViewConstraint: NSLayoutConstraint!
    var isVisibleAlready:Bool = false
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(changeSideMenuConstraint), name: NSNotification.Name("toggleButtonNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeSideMenuConstraint), name: NSNotification.Name("showRightNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeSideMenuConstraint), name: NSNotification.Name("showLeftNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeSideMenuConstraint), name: NSNotification.Name("tapGestureNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeSideMenuConstraint), name: NSNotification.Name("headerSectionTappedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeSideMenuConstraint), name: NSNotification.Name("didSelectRowNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginAgain), name: NSNotification.Name("loginAgainNotification"), object: nil)
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        if (UI_USER_INTERFACE_IDIOM() == .pad){
            self.slideViewConstraint.constant = -500
        }else{
            self.slideViewConstraint.constant = -280
         }

    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - changeSideMenuConstraint
    @objc func loginAgain(_ notification:Notification) {
        print("login again container")
        ConstantMessages.appearingAfterReLogin  = true
        
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
   //MARK - changeSideMenuConstraint
    @objc func changeSideMenuConstraint(_ notification:Notification) {
        
        if notification.name.rawValue == "toggleButtonNotification"
        {
            print("toggleButton notification is called")
            if isVisibleAlready{
                if (UI_USER_INTERFACE_IDIOM() == .pad){
                    self.slideViewConstraint.constant = -500
                }else{
                    self.slideViewConstraint.constant = -280
                 }

                NotificationCenter.default.post(name: NSNotification.Name("sideMenuClosedNotification"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name("enalbleNotificationIcon"), object: nil)
                isVisibleAlready = false
            }
            else{
                self.slideViewConstraint.constant = 0
                NotificationCenter.default.post(name: NSNotification.Name("disableNotificationIcon"), object: nil)
                isVisibleAlready = true
            }
            UIView.animate(withDuration: 0.3 ){
            self.view.layoutIfNeeded()
            }
        }
        
        if notification.name.rawValue == "didSelectRowNotification"
        {
            print("tableview section Row tapped notification is called")
            if isVisibleAlready{
                if (UI_USER_INTERFACE_IDIOM() == .pad){
                    self.slideViewConstraint.constant = -500
                }else{
                    self.slideViewConstraint.constant = -280
                 }

                let receivedDict = notification.userInfo
                NotificationCenter.default.post(name: NSNotification.Name("tableViewTappedNotificationForPushingView"), object: nil, userInfo: receivedDict)
                NotificationCenter.default.post(name: NSNotification.Name("enalbleNotificationIcon"), object: nil)
                isVisibleAlready = false
            }
            else{
                self.slideViewConstraint.constant = 0
                NotificationCenter.default.post(name: NSNotification.Name("disableNotificationIcon"), object: nil)
                isVisibleAlready = true
            }
            UIView.animate(withDuration: 0.3 ){
                self.view.layoutIfNeeded()
            }
        }
        
        if notification.name.rawValue == "headerSectionTappedNotification"
        {
            print("tableview header tapped notification is called")
            if isVisibleAlready{
                if (UI_USER_INTERFACE_IDIOM() == .pad){
                    self.slideViewConstraint.constant = -500
                }else{
                    self.slideViewConstraint.constant = -280
                 }

                let receivedDict = notification.userInfo
                NotificationCenter.default.post(name: NSNotification.Name("tableViewTappedNotificationForPushingView"), object: nil, userInfo: receivedDict)
                NotificationCenter.default.post(name: NSNotification.Name("enalbleNotificationIcon"), object: nil)
                isVisibleAlready = false
            }
            else{
                self.slideViewConstraint.constant = 0
                NotificationCenter.default.post(name: NSNotification.Name("disableNotificationIcon"), object: nil)
                isVisibleAlready = true
            }
            UIView.animate(withDuration: 0.3 ){
                self.view.layoutIfNeeded()
            }
        }

        if notification.name.rawValue == "showRightNotification"
        {
            if isVisibleAlready{}
            else{
                self.slideViewConstraint.constant = 0
                NotificationCenter.default.post(name: NSNotification.Name("disableNotificationIcon"), object: nil)
                isVisibleAlready = true
                UIView.animate(withDuration: 0.3 ){
                    self.view.layoutIfNeeded()
                }
            }
        }
        
        if notification.name.rawValue == "showLeftNotification"
        {
            if isVisibleAlready{
                if (UI_USER_INTERFACE_IDIOM() == .pad){
                    self.slideViewConstraint.constant = -500
                }else{
                    self.slideViewConstraint.constant = -280
                 }

                NotificationCenter.default.post(name: NSNotification.Name("sideMenuClosedNotification"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name("enalbleNotificationIcon"), object: nil)
                isVisibleAlready = false
                UIView.animate(withDuration: 0.3 ){
                self.view.layoutIfNeeded()
                }
            }
            else{}
        }
        
        if notification.name.rawValue == "tapGestureNotification"
        {
            if isVisibleAlready{
                if (UI_USER_INTERFACE_IDIOM() == .pad){
                    self.slideViewConstraint.constant = -500
                }else{
                    self.slideViewConstraint.constant = -280
                 }

                NotificationCenter.default.post(name: NSNotification.Name("sideMenuClosedNotification"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name("enalbleNotificationIcon"), object: nil)
                isVisibleAlready = false
                UIView.animate(withDuration: 0.3 ){
                    self.view.layoutIfNeeded()
                }
            }
            else{}
        }
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
}
