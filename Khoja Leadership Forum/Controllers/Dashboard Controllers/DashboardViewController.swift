
import UIKit
import Alamofire
import SwiftGifOrigin

var loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
var userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
var token = userData["auth_token"] as! String
var headerToken : HTTPHeaders = ["Auth-token":token,"Content-Type":"application/json"]




extension Notification.Name {
    static let PushNotification_Clicked = Notification.Name("PushNotification_Clicked")
}



class DashboardViewController: UIViewController,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var initiativeTableView: UITableView!
    @IBOutlet weak var notificationIconOutlet: UIButton!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var lineOutlet: UIView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var isRecordFound: UILabel!
    @IBOutlet weak var lblSelectedCategorySearch: ContentLabel!
    @IBOutlet weak var lblIsSearchRecordFound: UILabel!
    @IBOutlet weak var txtFieldMasterSearch: TextFieldPadding!
    @IBOutlet weak var viewMasterSearchDropDown: UIView!
    @IBOutlet weak var tableViewSearchData: UITableView!
    @IBOutlet weak var searchDataView: UIView!
    
    var dropImage = UIImage.init(named: "dropdown.png")
    
    var headersTitleArray = ["Pending Group Access Requests","Pending Project Access Requests","Initiatives","Forums Join the Discussion","Forum Subscriptions"]
    let headerTitlesImageArray = ["dashboard-icon-2.png","dashboard-icon-2.png","dashboard-icon-4.png","dashboard-icon-3","dashboard-icon-1"]
    
    var sectionDataArray = [ExpandableDashBoard]()
    var pendingGroupModel = [PendingGroupModel]()
    var pendingProjectModel = [PendingProjectModel]()
    var initiativeModel = [InitiativeModel]()
    var forumJoinModel = [ForumJoinModel]()
    var forumSubscriptionlModel = [ForumSubscriptionlModel]()
    
    var groupSearchModel = [ListOfAllGroupsModel]()
    var eventSearchModel = [EventListModel]()
    var projectSearchModel = [ProjectListModel]()
    var contactSearchModel = [ContactModel]()
    var newsSearchModel = [NewsListModel]()
    var forumSearchModel = [ForumSearchModel]()
    
    var paramForSearchApi : [String:Any]?
    var urlForSearchApi = ""
    var searchTxt = ""
    var isContact = false
    var isNews = false
    var isEvent = false
    var isGroups = false
    var isProjects = false
    var isForum = false
    var isSelectCategoryDropDownOpen = false
    var isAdmindata = 0
    var isCharitydata = 0
    var isBussinessdata = 0
    var isOtherdata = 0
    var isAdminDataExist = false
    var isCharityDataExist = false
    var isBusinessDataExist = false
    var isOtherDataDataExist = false
    
    var userId = 0
    var Status = false
    var sideMenuNotificationFlag = true
    var loginAgain:Bool = false
    var postBoolValueRight = ["value": true]
    var postBoolValueLeft = ["value": false]
    var rightFlagCount = 1
    var leftFlagCount = 1
    
    var leftSwipe = UISwipeGestureRecognizer()
    var rightSwipe = UISwipeGestureRecognizer()
    
    
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - sideMenuClosed
    @objc func sideMenuClosed(_ notification:Notification) {
        removeBlurEffect()
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    @objc func handlePushNotification(_ notification:Notification) {
        
        print(notification)
        
        if let json = notification.userInfo?["gcm.notification.push_data"] as? String {
            
            
            do {
                
                let data = Data(json.utf8)
                
                let decoder = JSONDecoder()
                let model = try decoder.decode(Model_PushNotifications_Parent.self,
                                               from: data)
                
                
              //  print(model)
             //   print(model.identifier)
                
                switch (model.identifier)
                {
                case .PushNotification_SummitEvents_MeetingRequest_Sent, .PushNotification_SummitEvents_MeetingRequest_Accepted, .PushNotification_SummitEvents_MeetingRequest_Rejected, .PushNotification_SummitEvents_MeetingRequest_Cancelled, .PushNotification_SummitEvents_MeetingRequest_Deleted, .PushNotification_SummitEvents_MeetingRequest_Updated:
                    
                        let vc = UIStoryboard.init(name: "SummitEvent", bundle: Bundle.main).instantiateViewController(withIdentifier: "MeetingDetails_ViewController") as? MeetingDetails_ViewController
                    if let meeting_parent_id = model.details?.meeting_parent_id {
                        vc?.meeting_parent_id = meeting_parent_id
                    }

//                    navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
//                        if vc.isKind(of: MeetingDetails_ViewController.self) {
//                            return true
//                        } else {
//                            return true
//                        }
//                    })

                        self.navigationController?.pushViewController(vc!, animated: false)
                        
                    break
                    
                default:
                    break
                }
                
                
                
            } catch let parsingError {
                
                print( parsingError.localizedDescription )
            }
        }
        
        
        
        
        
        
    }
    
    //MARK - forPushingView
    @objc func forPushingView(_ notification:Notification) {
        
        removeBlurEffect()
        
        if let receivedString  = notification.userInfo?["valueTapped"] as? String{
            
            //Forum Section
            if receivedString == "ForumAdminController" || receivedString == "ForumCharityController" || receivedString == "ForumBusinessController" || receivedString == "ForumOtherController"{
                
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForumMainViewController") as? ForumMainViewController
                
                if receivedString == "ForumAdminController" {vc?.rowTappedValue = "Admin"}
                if receivedString == "ForumCharityController" {vc?.rowTappedValue = "Charity"}
                if receivedString == "ForumBusinessController" {vc?.rowTappedValue = "Business"}
                if receivedString == "ForumOtherController" {vc?.rowTappedValue = "Other"}
                
                self.navigationController?.pushViewController(vc!, animated: false)
            }
                
                //Summit Event Controller
            else if receivedString == "SummitEvent_Lists_ViewController" {
                //SummitEvent_Lists_ViewController
                let vc = UIStoryboard.init(name: "SummitEvent", bundle: Bundle.main).instantiateViewController(withIdentifier: "SummitEvent_Lists_ViewController") //as? NewsViewController
                
                
                
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
                //Other Section
            else {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: receivedString) //as? NewsViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
            
        }
    }
    
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isRecordFound.isHidden = true
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        
        print(headerToken)
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        onScreenLoadApi()
        //        self.dashboardApiLoad()
        
        
        self.notificationIconOutlet.isEnabled = true
        self.notificationIconOutlet.tintColor = .clear
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(handlePushNotification), name: NSNotification.Name.PushNotification_Clicked, object: nil)
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(sideMenuClosed), name: NSNotification.Name("sideMenuClosedNotification"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(forPushingView), name: NSNotification.Name("tableViewTappedNotificationForPushingView"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(disableNotificationIcon), name: NSNotification.Name("disableNotificationIcon"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enableNotificationIcon), name: NSNotification.Name("enalbleNotificationIcon"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(popToRootController), name: NSNotification.Name("popToRootNotification"), object: nil)
        
        
        // TableView Delegate & Datasource
        self.initiativeTableView.delegate = self
        self.initiativeTableView.dataSource = self
        
        //Stop overscrolling for all tableviews
        //        self.initiativeTableView.bounces = false
        
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap(sender:)))
        leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        
        tapRecognizer.delegate = self
        leftSwipe.direction = .left
        leftSwipe.delegate = self
        rightSwipe.direction = .right
        rightSwipe.delegate = self
        
        self.view.addGestureRecognizer(tapRecognizer)
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
        tapRecognizer.cancelsTouchesInView = false
        self.leftSwipe.isEnabled = false
        self.rightSwipe.isEnabled = false
        
        
        
        // Do any additional setup after loading the view.
    }
    
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - hideNotificationIcon
    @objc func popToRootController () {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - hideNotificationIcon
    @objc func disableNotificationIcon () {
        self.notificationIconOutlet.isEnabled = false
        self.notificationIconOutlet.tintColor = .clear
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - showNotificationIcon
    @objc func enableNotificationIcon () {
        self.notificationIconOutlet.isEnabled = true
        self.notificationIconOutlet.tintColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
    }
    /*-----------------------------------------------------------------------------------------------------*/
    
    @objc func cancelGroupRequest (_ sender:UIButton) {
        let group = sectionDataArray[0].names[sender.tag] as! PendingGroupModel
        let param = ["group_requests_id":group.groupRequestsID ?? ""] as [String:Any]
        cancelRequestApi(url: cancelGroupRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.showToast(message: "Request has been cancelled")
                print("group request has been cancelled")
                self.dashboardApiLoad()
            }else{
                self.showToast(message: "Request couldn't cancel")
                print("group request couldn't cancelled")
            }
        }
        
    }
    /*-----------------------------------------------------------------------------------------------------*/
    
    @objc func cancelProjectRequest (_ sender:UIButton) {
        let project = sectionDataArray[1].names[sender.tag] as! PendingProjectModel
        let param = ["project_id":project.projectRequestsProjectID ?? "","user_id":userId] as [String:Any]
        cancelRequestApi(url: cancelProjectRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("project request has been cancelled")
                self.showToast(message: "Request has been cancelled")
                self.dashboardApiLoad()
            }else{
                self.showToast(message: "Request couldn't cancel")
                print("project request couldn't cancelled")
            }
        }
    }
    
    
    //MARK:- Additional Functional
    /*-----------------------------------------------------------------------------------------------------*/
    func onScreenLoadApi() {
        
        let userName = UserDefaults.standard.value(forKey: "username") as? String
        let password = UserDefaults.standard.value(forKey: "passsword") as? String
        let param = ["email":userName ?? "user@gmail.com","password":password ?? "1233"] as [String:Any]
        login(url: loginUrl, method: responseType.post.rawValue, param: param, header: nil) { (status) in
            if status{
                self.leftSwipe.isEnabled = true
                self.rightSwipe.isEnabled = true
                print(" again Login Successfully on dashborad")
                loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
                userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
                token = userData["auth_token"] as! String
                headerToken = ["auth-token":token,"Content-Type":"application/json"]
                print(headerToken)
                self.dashboardApiLoad()
            }else {
                self.leftSwipe.isEnabled = true
                self.rightSwipe.isEnabled = true
                if UserDefaults.standard.value(forKey: "IsRememberOn") as? Int == 0 {
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                    
                }
                //setting login as root
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window!.rootViewController = vc
                NotificationCenter.default.post(name: NSNotification.Name("logoutNotification"), object:nil)
                self.dashboardApiLoad()
            }
        }
        
        
        
    }
    /*-----------------------------------------------------------------------------------------------------*/
    func dashboardApiLoad() {
        let param = ["user_id":userId] as [String:Any]
        dashBoardApi(url: dashBoardUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.sectionDataArray.append(ExpandableDashBoard(isExpanded: false, names: self.pendingGroupModel))
                self.sectionDataArray.append(ExpandableDashBoard(isExpanded: false, names: self.pendingProjectModel))
                self.sectionDataArray.append(ExpandableDashBoard(isExpanded: false, names: self.initiativeModel))
                self.sectionDataArray.append(ExpandableDashBoard(isExpanded: false, names: self.forumJoinModel))
                self.sectionDataArray.append(ExpandableDashBoard(isExpanded: false, names: self.forumSubscriptionlModel))
                self.initiativeTableView.reloadData()
                print("dashboard data loaded")
            }else {
                print("dashboard data couldn't load")
            }
            self.loaderView.isHidden = true
        }
    }
    /*-----------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
    /*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func dashBoardApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        pendingGroupModel.removeAll()
        pendingProjectModel.removeAll()
        initiativeModel.removeAll()
        forumJoinModel.removeAll()
        forumSubscriptionlModel.removeAll()
        
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.isRecordFound.isHidden = true
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.sectionDataArray.removeAll()
                    self.Status = true
                    if let data = JSON["PendingGroupAccessRequests"] as? [[String:Any]] {
                        for i in data {
                            self.pendingGroupModel.append(PendingGroupModel(data: [i]))
                        }
                        print("pendingGroup data fetched")
                    }
                    if let data = JSON["PendingProjectAccessRequests"] as? [[String:Any]] {
                        for i in data {
                            self.pendingProjectModel.append(PendingProjectModel(data: [i]))
                        }
                        print("pendingProject data fetched")
                    }
                    if let data = JSON["Initiatives"] as? [[String:Any]] {
                        for i in data {
                            self.initiativeModel.append(InitiativeModel(data: [i]))
                        }
                        print("initiativeModel data fetched")
                    }
                    if let data = JSON["ForumsJointheDiscussion"] as? [[String:Any]] {
                        var joinModel = [ForumJoinModel]()
                        for i in data {
                            joinModel.append(ForumJoinModel(data: [i]))
                        }
                        var admin = [ForumJoinModel]()
                        var charity = [ForumJoinModel]()
                        var business = [ForumJoinModel]()
                        var other = [ForumJoinModel]()
                        
                        //appending dummy data on first index for checking that its category match or not
                        admin.append(ForumJoinModel(data: [["ForumPosts__category": "admin","ForumPosts__id": "A","ForumPosts__topic": "Admin","postapprovedpost": "1"]]))
                        charity.append(ForumJoinModel(data: [["ForumPosts__category": "charity","ForumPosts__id": "C","ForumPosts__topic": "Charity","postapprovedpost": "1"]]))
                        business.append(ForumJoinModel(data: [["ForumPosts__category": "business","ForumPosts__id": "B","ForumPosts__topic": "Business","postapprovedpost": "1"]]))
                        other.append(ForumJoinModel(data: [["ForumPosts__category": "other","ForumPosts__id": "O","ForumPosts__topic": "Other","postapprovedpost": "1"]]))
                        
                        for i in joinModel {
                            if i.categoryType?.lowercased() == "admin" {
                                admin.append(i)
                            }else if i.categoryType?.lowercased() == "charity" {
                                charity.append(i)
                            }else if i.categoryType?.lowercased() == "business" {
                                business.append(i)
                            }else if i.categoryType?.lowercased() == "other" || i.categoryType?.lowercased() == "public" {
                                other.append(i)
                            }
                        }
                        
                        if admin.count == 1 {
                            self.isAdminDataExist = false
                            self.isAdmindata = 0
                            admin.removeAll()
                        }else {
                            self.isAdminDataExist = true
                            self.isAdmindata = 1
                        }
                        
                        if charity.count == 1 {
                            self.isCharityDataExist = false
                            self.isCharitydata = 0
                            charity.removeAll()
                        }else {
                            self.isCharityDataExist = true
                            self.isCharitydata = 1
                        }
                        
                        if business.count == 1 {
                            self.isBusinessDataExist = false
                            self.isBussinessdata = 0
                            business.removeAll()
                        }else {
                            self.isBusinessDataExist = true
                            self.isBussinessdata = 1
                        }
                        
                        if other.count == 1 {
                            self.isOtherDataDataExist = false
                            self.isOtherdata = 0
                            other.removeAll()
                        }else {
                            self.isOtherDataDataExist = true
                            self.isOtherdata = 1
                        }
                        
                        self.forumJoinModel.append(contentsOf: admin)
                        self.forumJoinModel.append(contentsOf: charity)
                        //                        self.forumJoinModel.append(contentsOf: business)
                        self.forumJoinModel.append(contentsOf: other)
                        
                        print("forumJoinModel data fetched")
                    }
                    if let data = JSON["ForumSubscriptions"] as? [[String:Any]] {
                        for i in data {
                            self.forumSubscriptionlModel.append(ForumSubscriptionlModel(data: [i]))
                        }
                        print("forumSubscriptionlModel data fetched")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.isRecordFound.isHidden = false
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
    
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    
    func forumSearchApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        forumSearchModel.removeAll()
        eventSearchModel.removeAll()
        newsSearchModel.removeAll()
        groupSearchModel.removeAll()
        contactSearchModel.removeAll()
        projectSearchModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.isRecordFound.isHidden = true
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.forumSearchModel.append(ForumSearchModel(data: [i]))
                        }
                    }
                }
                completion(self.Status)
            }else {
                self.isRecordFound.isHidden = false
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    func masterSearchApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        eventSearchModel.removeAll()
        newsSearchModel.removeAll()
        groupSearchModel.removeAll()
        contactSearchModel.removeAll()
        projectSearchModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.isRecordFound.isHidden = true
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if self.isContact {
                    
                    self.Status = true
                    if status == "success" {
                        if let data = JSON["data"] as? [[String:Any]] {
                            var contactData = [ContactModel]()
                            for i in data {
                                contactData.append(ContactModel(data: [i]))
                            }
                            for i in contactData {
                                if (i.userFirstName ?? "").lowercased().contains(self.searchTxt.lowercased()) || (i.userLastName ?? "").lowercased().contains(self.searchTxt.lowercased()) || (i.userDetailAddress1 ?? "").lowercased().contains(self.searchTxt.lowercased()) || (i.userDetailCity ?? "").lowercased().contains(self.searchTxt.lowercased()) {
                                    
                                    self.contactSearchModel.append(i)
                                    
                                }
                            }
                            
                        }
                        print("Contact loaded")
                    }
                    
                }else if self.isNews {
                    if status == "success" {
                        
                        self.Status = true
                        if let data = JSON["data"] as? [[String:Any]] {
                            var newsData = [NewsListModel]()
                            for i in data {
                                newsData.append(NewsListModel(data: [i]))
                            }
                            
                            for i in newsData {
                                if (i.postsTopic ?? "").lowercased().contains(self.searchTxt.lowercased()) || (i.postsContent ?? "").lowercased().contains(self.searchTxt.lowercased()) {
                                    
                                    self.newsSearchModel.append(i)
                                    
                                }
                            }
                            
                        }
                        print("News loaded")
                    }
                    
                }else if self.isEvent {
                    if status == "success" {
                        
                        self.Status = true
                        if let data = JSON["data"] as? [[String:Any]] {
                            var eventData = [EventListModel]()
                            for i in data {
                                eventData.append(EventListModel(data: [i]))
                            }
                            
                            for i in eventData {
                                if (i.name ?? "").lowercased().contains(self.searchTxt.lowercased()) || (i.datumDescription ?? "").lowercased().contains(self.searchTxt.lowercased()) {
                                    
                                    self.eventSearchModel.append(i)
                                    
                                }
                            }
                            
                        }
                        print("event loaded")
                    }
                    
                }else if self.isProjects {
                    if status == "success" {
                        
                        self.Status = true
                        if let data = JSON["data"] as? [[String:Any]] {
                            var projectData = [ProjectListModel]()
                            for i in data {
                                projectData.append(ProjectListModel(data: [i]))
                            }
                            
                            for i in projectData {
                                if (i.projectsName ?? "").lowercased().contains(self.searchTxt.lowercased()) || (i.projectsDescription ?? "").lowercased().contains(self.searchTxt.lowercased()) {
                                    
                                    self.projectSearchModel.append(i)
                                    
                                }
                            }
                            
                        }
                        print("project loaded")
                        
                    }
                    
                }else if self.isGroups {
                    if status == "success" {
                        
                        self.Status = true
                        if let data = JSON["data"] as? [[String:Any]] {
                            var groupData = [ListOfAllGroupsModel]()
                            for i in data {
                                groupData.append(ListOfAllGroupsModel(data: [i]))
                            }
                            
                            for i in groupData {
                                if (i.name ?? "").lowercased().contains(self.searchTxt.lowercased()) || (i.datumDescription ?? "").lowercased().contains(self.searchTxt.lowercased()) {
                                    
                                    self.groupSearchModel.append(i)
                                    
                                }
                            }
                            
                        }
                        print("group loaded")
                        
                    }
                    
                }
                else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.isRecordFound.isHidden = false
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func cancelRequestApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
            if status {
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    print("Data deleted")
                }else {
                    self.Status = false
                    print("Data couldn't delete")
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    func login (url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON, status) in
            if status {
                self.Status = false
                let JSON = JSON
                let isStatus = JSON["status"] as! String
                if isStatus == "success"{
                    self.Status = true
                    let data = JSON["data"] as! NSDictionary
                    print(data)
                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: data)
                    UserDefaults.standard.set(encodedData, forKey: "Login")
                    //                    UserDefaults.standard.set(username, forKey: "username")
                    //                    UserDefaults.standard.set(password, forKey: "passsword")
                    print("Login data fetched")
                }else {
                    self.Status = false
                    print("login data didn't fetch")
                }
            }else {
                self.Status = false
                self.loaderView.isHidden = true
                print("Internet is Unavailable")
            }
            completion(self.Status)
        }
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    //MARK - addBlurEffect
    func addBlurEffect () {
        
        view.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.tag = 1
        view.addSubview(blurEffectView)
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - removeBlurEffect
    func removeBlurEffect () {
        
        for subview in self.view.subviews {
            if (subview.tag == 1) {
                subview.removeFromSuperview()
            }
        }
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        viewMasterSearchDropDown.isHidden = true
        searchDataView.isHidden = true
        lblIsSearchRecordFound.isHidden = true
        txtFieldMasterSearch.text = ""
        txtFieldMasterSearch.delegate = self
        
        self.navigationController?.isNavigationBarHidden = true
        let flag = ConstantMessages.appearingFirstTime
        let reLoginFlag = ConstantMessages.appearingAfterReLogin
        if flag == true {
            ConstantMessages.appearingFirstTime = false
            ConstantMessages.appearingAfterReLogin = false
            //            addBlurEffect()
        }
        else{
            removeBlurEffect()
        }
        if reLoginFlag == true {
            ConstantMessages.appearingAfterReLogin = false
            //            addBlurEffect()
        }
        else{
        }
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        
        let nav = self.navigationController?.navigationBar
        //        nav?.barStyle = UIBarStyle.black
        nav?.barTintColor =  UIColor.lightText
        //        nav?.tintColor = UIColor.yellow
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo.png")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtFieldMasterSearch {
            searchDataView.isHidden = true
        }
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewSearchData {
            let cell = self.tableViewSearchData.dequeueReusableCell(withIdentifier: "MasterSearchTableViewCell", for: indexPath) as! MasterSearchTableViewCell
            if isContact {
                cell.lblSearchDataName.text = "\(contactSearchModel[indexPath.row].userFirstName ?? "") \(contactSearchModel[indexPath.row].userLastName ?? "")"
                
            }else if isEvent {
                cell.lblSearchDataName.text = eventSearchModel[indexPath.row].name
                
            }else if isNews {
                cell.lblSearchDataName.text = newsSearchModel[indexPath.row].postsTopic
                
            }else if isProjects {
                cell.lblSearchDataName.text = projectSearchModel[indexPath.row].projectsName
                
            }else if isGroups {
                cell.lblSearchDataName.text = groupSearchModel[indexPath.row].name
                
            }else if isForum {
                cell.lblSearchDataName.text = forumSearchModel[indexPath.row].forumTopic
            }
            
            return cell
        }else {
            let iniCell = self.initiativeTableView.dequeueReusableCell(withIdentifier: "iniCell", for: indexPath) as! DashboardTableViewCell
            if indexPath.section == 0 {
                iniCell.cancelRequest.isHidden = false
                iniCell.cancelRequest.tag = indexPath.row
                iniCell.cancelRequest.addTarget(self, action: #selector(self.cancelGroupRequest), for: .touchUpInside)
                let name = self.sectionDataArray[indexPath.section].names[indexPath.row] as! PendingGroupModel
                if UIDevice().userInterfaceIdiom == .pad {
                    iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                }
                else{
                    iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                }
                iniCell.lblCellTitle.text = name.groupsName
            }else if indexPath.section == 1 {
                iniCell.cancelRequest.isHidden = false
                iniCell.cancelRequest.tag = indexPath.row
                iniCell.cancelRequest.addTarget(self, action: #selector(self.cancelProjectRequest), for: .touchUpInside)
                let name = self.sectionDataArray[indexPath.section].names[indexPath.row] as! PendingProjectModel
                if UIDevice().userInterfaceIdiom == .pad {
                    iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                }else{
                    iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                }
                iniCell.lblCellTitle?.text = name.projectsName
            }else if indexPath.section == 2 {
                iniCell.cancelRequest.isHidden = true
                let name = self.sectionDataArray[indexPath.section].names[indexPath.row] as! InitiativeModel
                if UIDevice().userInterfaceIdiom == .pad {
                    iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                }else{
                    iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                }
                iniCell.lblCellTitle.text = name.initiativesName
                /*---------------------------------------------------------------------------------------------------------------------------------------------------------*/
            }else if indexPath.section == 3 {
                iniCell.cancelRequest.isHidden = true
                let name = self.sectionDataArray[indexPath.section].names[indexPath.row] as! ForumJoinModel
                
                if name.categoryType?.lowercased() == "admin" {
                    if isAdminDataExist && name.forumPostsID == "A" {
                        iniCell.lblCellTitle.textAlignment = .left
                        
                        if UIDevice().userInterfaceIdiom == .pad {
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold)
                        }else{
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
                        }
                        
                        iniCell.lblCellTitle.text = "Admin"
                    }else {
                        iniCell.lblCellTitle.textAlignment = .left
                        if UIDevice().userInterfaceIdiom == .pad {
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.regular)
                        }else{
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                        }
                        iniCell.lblCellTitle.text = "   - \(name.forumPostsTopic ?? "")"
                    }
                    
                }else if name.categoryType?.lowercased() == "charity" {
                    if isCharityDataExist && name.forumPostsID == "C" {
                        iniCell.lblCellTitle.textAlignment = .left
                        if UIDevice().userInterfaceIdiom == .pad {
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold)
                        }else{
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
                        }
                        iniCell.lblCellTitle.text = "Charity"
                    }else {
                        iniCell.lblCellTitle.textAlignment = .left
                        if UIDevice().userInterfaceIdiom == .pad {
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                        }else{
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                        }
                        iniCell.lblCellTitle.text = "   - \(name.forumPostsTopic ?? "")"
                    }
                    
                }else if name.categoryType?.lowercased() == "business" {
                    if isBusinessDataExist && name.forumPostsID == "B" {
                        iniCell.lblCellTitle.textAlignment = .left
                        if UIDevice().userInterfaceIdiom == .pad {
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold)
                        }else{
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
                        }
                        
                        iniCell.lblCellTitle.text = "Business"
                    }else {
                        iniCell.lblCellTitle.textAlignment = .left
                        if UIDevice().userInterfaceIdiom == .pad {
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                        }else{
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                        }
                        
                        iniCell.lblCellTitle.text = "   - \(name.forumPostsTopic ?? "")"
                    }
                    
                }else if name.categoryType?.lowercased() == "other" || name.categoryType?.lowercased() == "public" {
                    if isOtherDataDataExist && name.forumPostsID == "O" {
                        iniCell.lblCellTitle.textAlignment = .left
                        if UIDevice().userInterfaceIdiom == .pad {
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold)
                        }else{
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
                        }
                        
                        
                        iniCell.lblCellTitle.text = "Other"
                    }else {
                        iniCell.lblCellTitle.textAlignment = .left
                        if UIDevice().userInterfaceIdiom == .pad {
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                        }else{
                            iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                        }
                        
                        
                        iniCell.lblCellTitle.text = "   - \(name.forumPostsTopic ?? "")"
                    }
                }
                
            }
                /*---------------------------------------------------------------------------------------------------------------------------------------------------------*/
            else if indexPath.section == 4 {
                iniCell.cancelRequest.isHidden = true
                let name = self.sectionDataArray[indexPath.section].names[indexPath.row] as! ForumSubscriptionlModel
                if UIDevice().userInterfaceIdiom == .pad {
                    iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                }else{
                    iniCell.lblCellTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                }
                iniCell.lblCellTitle.text = name.forumPostsTopic
            }
            iniCell.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
            iniCell.indentationLevel = 1
            iniCell.layer.cornerRadius = 5
            
            return iniCell
        }
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == initiativeTableView {
            
            if indexPath.section == 2 {
                let name = self.sectionDataArray[indexPath.section].names[indexPath.row] as! InitiativeModel
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InitiativeDetailViewController") as! InitiativeDetailViewController
                vc.initiativeId = name.initiativesID ?? ""
                vc.initiativeTitle = name.initiativesName ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if indexPath.section == 3 {
                let name = self.sectionDataArray[indexPath.section].names[indexPath.row] as! ForumJoinModel
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumSubTopicPost") as! ForumSubTopicPost
                if name.forumPostsID == "A" || name.forumPostsID == "C" || name.forumPostsID == "B" || name.forumPostsID == "O" {
                    print("its label")
                }else {
                    vc.forumPostId = Int(name.forumPostsID ?? "")!
                    vc.titleName = name.forumPostsTopic ?? ""
                    vc.postApprovePost = name.postApprove ?? "0"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            if indexPath.section == 4 {
                let name = self.sectionDataArray[indexPath.section].names[indexPath.row] as! ForumSubscriptionlModel
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumSubTopicPost") as! ForumSubTopicPost
                vc.forumPostId = Int(name.forumPostsID ?? "")!
                vc.titleName = name.forumPostsTopic ?? ""
                vc.postApprovePost = name.postApprove ?? "0"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else if tableView == tableViewSearchData {
            if isContact {
                
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProfileViewController") as! ViewProfileViewController
                vc.userId = Int(contactSearchModel[indexPath.row].userID ?? "0") ?? 0
                vc.isLoginMember = false
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if isEvent {
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
                vc.eventData = eventSearchModel[indexPath.row]
                vc.isMasterSearchView = true
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if isForum {
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumSubTopicPost") as! ForumSubTopicPost
                vc.forumPostId = Int(forumSearchModel[indexPath.row].forumPostId ?? "0") ?? 0
                vc.titleName = forumSearchModel[indexPath.row].forumTopic ?? ""
                vc.postApprovePost = forumSearchModel[indexPath.row].postApprovePost ?? "0"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if isNews {
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewNewsViewController") as! ViewNewsViewController
                vc.screenTitle = "News Detail"
                vc.newListModel = newsSearchModel[indexPath.row]
                vc.imageURLReceived = newsSearchModel[indexPath.row].profileImage ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if isProjects {
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewProjectViewController") as! ViewProjectViewController
                vc.projectId = projectSearchModel[indexPath.row].projectsID!
                vc.projectReadOnlyStatus = projectSearchModel[indexPath.row].projectsStatus ?? "r"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if isGroups {
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewGroupViewController") as! ViewGroupViewController
                if String(userId) == groupSearchModel[indexPath.row].createdByUserID {
                    vc.isGroupOwnwe = true
                }else {
                    vc.isGroupOwnwe = false
                }
                vc.groupsId = groupSearchModel[indexPath.row].groupID ?? ""
                vc.aboutUs = groupSearchModel[indexPath.row].bio ?? ""
                vc.address = groupSearchModel[indexPath.row].address ?? ""
                vc.email = groupSearchModel[indexPath.row].email ?? ""
                vc.phoneNumber = groupSearchModel[indexPath.row].postalCode ?? ""
                vc.groupName = groupSearchModel[indexPath.row].name ?? ""
                vc.groupDescriptionName = groupSearchModel[indexPath.row].datumDescription ?? ""
                vc.profileImage = groupSearchModel[indexPath.row].groupsImage ?? ""
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
            
        }
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //viewForFooterInSection Changing its backgroud colour
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 15))
        headerView.backgroundColor = .white
        return headerView
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewForHeaderInSection
    // Header for a section settings
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headerView = UIView()
        if tableView != tableViewSearchData {
            
            headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
            var labelFont:CGFloat = 13
            // code for adding centered title
            headerView.backgroundColor = UIColor.white
            if UIDevice().userInterfaceIdiom == .pad {
                labelFont = 20
            }
            else{
                labelFont = 14
            }
            let headerLabel = UILabel(frame: CGRect(x: 45, y: 5, width:230 , height: 30))
            headerLabel.textColor = UIColor.black
            if headersTitleArray.count != 0 {
                headerLabel.text = headersTitleArray[section]
            }
            
            headerLabel.font = UIFont.boldSystemFont(ofSize: labelFont)
            headerLabel.textAlignment = .left
            headerLabel.contentMode = .scaleAspectFit
            
            let imageViewleft = UIImageView(frame: CGRect(x: 15, y: 10, width: 20, height: 20))
            var image = UIImage()
            
            headerView.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
            headerLabel.textColor = UIColor.white
            image = UIImage(named: headerTitlesImageArray[section])!
            
            imageViewleft.image = image;
            headerView.addSubview(imageViewleft)
            headerView.addSubview(headerLabel)
            
            let arrowButton = UIButton(frame: CGRect(x:headerView.frame.size.width - 80, y:5, width:60, height:30))
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
            
            arrowButton.setImage(dropImage, for: .normal)
            arrowButton.setTitleColor(.black, for: .normal)
            arrowButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
            arrowButton.backgroundColor = UIColor.clear
            arrowButton.addTarget(self, action: #selector(ExpandClose), for: .touchUpInside)
            arrowButton.tag = section
            arrowButton.imageView?.contentMode = .scaleAspectFit
            
            var countValue = 0
            if self.sectionDataArray.count != 0{
                
                if self.sectionDataArray[section].names.count != 0{
                    
                    countValue = self.sectionDataArray[section].names.count
                    headerView.addSubview(arrowButton)
                    
                }
            }
            else{}
            
            imageViewleft.translatesAutoresizingMaskIntoConstraints = false
            imageViewleft.widthAnchor.constraint(equalToConstant: 25).isActive = true
            imageViewleft.heightAnchor.constraint(equalToConstant: 25).isActive = true
            imageViewleft.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
            imageViewleft.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10).isActive = true
            imageViewleft.setNeedsLayout()
            imageViewleft.clipsToBounds = true
            
            headerLabel.translatesAutoresizingMaskIntoConstraints = false
            headerLabel.widthAnchor.constraint(equalToConstant: tableView.frame.width-120).isActive = true
            headerLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
            headerLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
            headerLabel.leadingAnchor.constraint(equalTo: imageViewleft.trailingAnchor, constant: 20).isActive = true
            headerLabel.setNeedsLayout()
            headerLabel.clipsToBounds = true
            
            if countValue != 0{
                arrowButton.translatesAutoresizingMaskIntoConstraints = false
                arrowButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
                arrowButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
                arrowButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
                arrowButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: 20).isActive = true
                arrowButton.setNeedsLayout()
                arrowButton.clipsToBounds = true
            }
            
            
            
            headerView.layer.cornerRadius = 10
        }
        return headerView
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - ExpandClose
    // expand close button functionality
    @objc func ExpandClose (button: UIButton) {
        
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in sectionDataArray[section].names.indices{
            let indexpath = IndexPath(row: row, section: section)
            indexPaths.append(indexpath)
        }
        
        let isExpanded = sectionDataArray[section].isExpanded
        sectionDataArray[section].isExpanded = !isExpanded
        if isExpanded {
            button.isSelected = false
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
            
            button.setImage(dropImage, for: .normal)
            initiativeTableView.deleteRows(at: indexPaths, with: .top)
        }
        else{
            button.isSelected = true
            dropImage = UIImage.init(named: "of-admin-dropup.png")
            
            button.setImage(dropImage, for: .normal)
            initiativeTableView.insertRows(at: indexPaths, with: .bottom)
        }
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    // return number of rows in a section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewSearchData {
            if isContact {
                return contactSearchModel.count
            }else if isEvent {
                return eventSearchModel.count
            }else if isNews {
                return newsSearchModel.count
            }else if isProjects {
                return projectSearchModel.count
            }else if isGroups {
                return groupSearchModel.count
            }else if isForum{
                return forumSearchModel.count
            }
            
        }else if tableView == initiativeTableView {
            if !sectionDataArray[section].isExpanded {
                return 0
            }
            return self.sectionDataArray[section].names.count
        }
        return 0
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    // setting row height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 40
        }else {
            return 60
        }
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    // section spacing in a tableview
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == tableViewSearchData {
            return 0
        }
        return 15
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    // return number of section in a tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableViewSearchData {
            return 1
        }
        return self.sectionDataArray.count
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    // Header height setting
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var value:CGFloat = 0.0
        if (UI_USER_INTERFACE_IDIOM() == .pad){
            if tableView != tableViewSearchData{ value = 60}
        }else{
            if tableView != tableViewSearchData{ value = 40}
        }
        return value
    }
    
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - sideMenuBarButton
    @IBAction func sideMenuBarButton(_ sender: Any) {
        print("side menu bar button pressed")
        
        if sideMenuNotificationFlag{
            sideMenuNotificationFlag = false
        }
        else{
            sideMenuNotificationFlag = true
        }
        var passDic = ["menuopen": sideMenuNotificationFlag]
        
        passDic["menuopen"] = sideMenuNotificationFlag
        addBlurEffect()
        NotificationCenter.default.post(name: NSNotification.Name("toggleButtonNotification"), object: nil, userInfo: passDic)
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - notificationBarButton
    @IBAction func notificationBarButton(_ sender: Any) {
        dashboardApiLoad()
        print("Refresh Button Pressed")
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - respondToSwipeGesture
    // For recognizing the gesture and posting the notification
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
                
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
                NotificationCenter.default.post(name: NSNotification.Name("showRightNotification"), object: nil)
                
                addBlurEffect()
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
                NotificationCenter.default.post(name: NSNotification.Name("showLeftNotification"), object: nil)
                removeBlurEffect()
                
            default:
                break
            }
        }
        
    }
    
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - didTap
    // tap gesture selector function definition
    @objc func didTap(sender: UITapGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name("tapGestureNotification"), object: nil)
        
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func btnSearchData(_ sender: Any) {
        searchDataView.isHidden = true
        lblIsSearchRecordFound.isHidden = true
        txtFieldMasterSearch.resignFirstResponder()
        searchTxt = txtFieldMasterSearch.text ?? ""
        /*--------------------------------------------------------------------------*/
        if isForum {
            if isForum {
                if txtFieldMasterSearch.text != "" {
                    let forumApiUrl = urlForSearchApi + (txtFieldMasterSearch.text ?? "")
                    forumSearchApi(url: forumApiUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
                        if status {
                            if self.forumSearchModel.count == 0 {
                                self.lblIsSearchRecordFound.isHidden = false
                            }else {
                                self.lblIsSearchRecordFound.isHidden = true
                            }
                            self.tableViewSearchData.reloadData()
                        }else {
                            
                        }
                        self.loaderView.isHidden = true
                        self.searchDataView.isHidden = false
                    }
                }else {
                    showToast(message: "Search keyword missing")
                }
            }else {
                showToast(message: "Select the category first")
            }
            /*--------------------------------------------------------------------------*/
        }else {
            if (isGroups || isContact || isEvent || isNews || isProjects) {
                if txtFieldMasterSearch.text != "" {
                    masterSearchApi(url: urlForSearchApi, method: responseType.get.rawValue, param: paramForSearchApi, header: headerToken) { (status) in
                        if status {
                            print("search data loaded")
                            if self.isContact {
                                
                                if self.contactSearchModel.count == 0 {
                                    self.lblIsSearchRecordFound.isHidden = false
                                }else {
                                    self.lblIsSearchRecordFound.isHidden = true
                                }
                                
                            }else if self.isEvent {
                                
                                if self.eventSearchModel.count == 0 {
                                    self.lblIsSearchRecordFound.isHidden = false
                                }else {
                                    self.lblIsSearchRecordFound.isHidden = true
                                }
                                
                            }else if self.isNews {
                                
                                if self.newsSearchModel.count == 0 {
                                    self.lblIsSearchRecordFound.isHidden = false
                                }else {
                                    self.lblIsSearchRecordFound.isHidden = true
                                }
                                
                            }else if self.isProjects {
                                
                                if self.projectSearchModel.count == 0 {
                                    self.lblIsSearchRecordFound.isHidden = false
                                }else {
                                    self.lblIsSearchRecordFound.isHidden = true
                                }
                                
                            }else if self.isGroups {
                                
                                if self.groupSearchModel.count == 0 {
                                    self.lblIsSearchRecordFound.isHidden = false
                                }else {
                                    self.lblIsSearchRecordFound.isHidden = true
                                }
                                
                            }
                            self.tableViewSearchData.reloadData()
                        }else{
                            print("search data couldn't load")
                        }
                        self.loaderView.isHidden = true
                        self.searchDataView.isHidden = false
                    }
                }else {
                    showToast(message: "Search keyword missing")
                }
            }else {
                showToast(message: "Select the category first")
            }
            /*--------------------------------------------------------------------------*/
        }
        
        
        
        
    }
    
    @IBAction func btnOpenDropDownView(_ sender: Any) {
        lblIsSearchRecordFound.isHidden = true
        if isSelectCategoryDropDownOpen {
            viewMasterSearchDropDown.isHidden = true
            isSelectCategoryDropDownOpen = false
        }else {
            viewMasterSearchDropDown.isHidden = false
            isSelectCategoryDropDownOpen = true
        }
        
        searchDataView.isHidden = true
    }
    
    @IBAction func btnContact(_ sender: Any) {
        isContact = true
        isNews = false
        isEvent = false
        isGroups = false
        isProjects = false
        isForum = false
        paramForSearchApi = nil
        urlForSearchApi = contactListUrl
        lblSelectedCategorySearch.text = "Contacts"
        viewMasterSearchDropDown.isHidden = true
        isSelectCategoryDropDownOpen = false
        
    }
    
    @IBAction func btnEvent(_ sender: Any) {
        isContact = false
        isNews = false
        isEvent = true
        isGroups = false
        isProjects = false
        isForum = false
        paramForSearchApi = nil
        urlForSearchApi = eventListUrl
        lblSelectedCategorySearch.text = "Events"
        viewMasterSearchDropDown.isHidden = true
        isSelectCategoryDropDownOpen = false
        
    }
    
    @IBAction func btnForums(_ sender: Any) {
        isContact = false
        isNews = false
        isEvent = false
        isGroups = false
        isProjects = false
        isForum = true
        paramForSearchApi = nil
        urlForSearchApi = forumSearchUrl
        lblSelectedCategorySearch.text = "Forums"
        viewMasterSearchDropDown.isHidden = true
        isSelectCategoryDropDownOpen = false
    }
    
    @IBAction func btnGroups(_ sender: Any) {
        isContact = false
        isNews = false
        isEvent = false
        isGroups = true
        isProjects = false
        isForum = false
        paramForSearchApi = nil
        urlForSearchApi = listOfAllGroupsUrl
        lblSelectedCategorySearch.text = "Groups"
        viewMasterSearchDropDown.isHidden = true
        isSelectCategoryDropDownOpen = false
        
        
    }
    
    @IBAction func btnNews(_ sender: Any) {
        isContact = false
        isNews = true
        isEvent = false
        isGroups = false
        isProjects = false
        isForum = false
        paramForSearchApi = nil
        urlForSearchApi = newsListUrl
        lblSelectedCategorySearch.text = "News"
        viewMasterSearchDropDown.isHidden = true
        isSelectCategoryDropDownOpen = false
        
    }
    
    @IBAction func btnProjects(_ sender: Any) {
        isContact = false
        isNews = false
        isEvent = false
        isGroups = false
        isProjects = true
        isForum = false
        paramForSearchApi = nil
        urlForSearchApi = projectListUrl
        lblSelectedCategorySearch.text = "Projects"
        viewMasterSearchDropDown.isHidden = true
        isSelectCategoryDropDownOpen = false
        
    }
    
    
}



