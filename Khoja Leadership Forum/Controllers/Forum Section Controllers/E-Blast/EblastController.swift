

import UIKit
import Alamofire
import SwiftGifOrigin
import RichEditorView

class EblastController: UIViewController {

    //MARK:- Btn Outlets
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet var htmlTextView: UITextView!
    @IBOutlet weak var addTopicTitle: UITextField!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loader: UIView!
    @IBOutlet weak var loaderImage: UIImageView!
    @IBOutlet weak var btnLabelTxtField: UITextField!
    @IBOutlet weak var btnLink: UILabel!
    
    //MARK:- Custom Variables
    var Status = false
    var userId = 0
    var email = ""
    var topic = ""
    var content = ""
    var postNotification = ""
    var firstName = ""
    var lastName = ""
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTopicTitle.text = "Subject"
        loader.isHidden = true
        loaderImage.image = UIImage.gif(name: "loader_gif")
        headerHeight()
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        email = userData["email"] as! String
        firstName = userData["first_name"] as! String
        lastName = userData["last_name"] as! String
        
        
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.html = "\(firstName )\(lastName) sent you a post that you may find it interesting. \(content)"
        toolbar.delegate = self
        toolbar.editor = editorView
        hideKeyboard()
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let itemDone = RichEditorOptionItem(image: nil, title: "Done") { (toolBar) in
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(itemDone)
        toolbar.options = options
        
    }
    
    //MARK:- Button Actions
    @IBAction func backBtn(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "SelectedEmail")
        UserDefaults.standard.removeObject(forKey: "SelectedUser")
        for controller in self.navigationController!.viewControllers as Array {
            if controller is SubTopicsDetail  {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func sendBtn(_ sender: Any) {
        let url = eblastUrl
        addPost(url: url, method: responseType.post.rawValue, header: headerToken) { (status) in
            if status {
                UserDefaults.standard.removeObject(forKey: "SelectedEmail")
                UserDefaults.standard.removeObject(forKey: "SelectedUser")
                print("eblast has been sent")
            }else {
                print("eblast didn't send")
            }
            self.loader.isHidden = true
            for controller in self.navigationController!.viewControllers as Array {
                if controller is SubTopicsDetail  {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func recipientsBtn(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecipientsController") as! RecipientsController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func addPost(url:String,method:HTTPMethod,header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        loader.isHidden = false
        if editorView.html != "" && addTopicTitle.text != "" && btnLabelTxtField.text != "" {
            if UserDefaults.standard.value(forKey: "SelectedEmail") as? [String] != nil {
                let data = UserDefaults.standard.value(forKey: "SelectedEmail") as! [String]
                let recipientEmails = data.joined(separator: ",")
                let param = ["emails":recipientEmails,"content":editorView.html,"subject":addTopicTitle.text!,"buttonlabel":btnLabelTxtField.text!,"buttonlink":btnLink.text!] as [String:Any]
                Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status) in
                    if status {
                        self.Status = false
                        let JSON = JSON
                        let status = JSON["status"] as? String
                        if status == "success" {
                            self.Status = true
                            completion(self.Status)
                        }
                    }else {
                        self.loader.isHidden = true
                    }
                }
            }else {
                loader.isHidden = true
                showToast(message: "Please select recipient first!")
            }
            
        }else {
            showToast(message: "All fields are mandatory")
            loader.isHidden = true
            print("Please Enter title or description")
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
}
//MARK:- Extensions
extension EblastController: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
        
}

extension EblastController: RichEditorToolbarDelegate {
        
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}

