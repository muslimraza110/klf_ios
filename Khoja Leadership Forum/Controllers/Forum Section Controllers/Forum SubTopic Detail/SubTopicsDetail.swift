
import UIKit
import Alamofire
import SwiftGifOrigin

class SubTopicsDetail: UIViewController {

    //MARK:- Btn Outlets
    @IBOutlet weak var subtopicTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImage: UIImageView!
    @IBOutlet weak var isRecordFound: UILabel!
    @IBOutlet weak var deleteAlert: UIView!
    @IBOutlet weak var btnAddSubtopic: UIButton!
    @IBOutlet weak var imgAddSubtopic: UIImageView!
    
    //MARK:- Custom Variables
    var Status = false
    var rowTappedValue = ""
    var userId = 0
    var ScreenTitle : String!
    var forumId:Int!
    var deleteForumId = ""
    var SubModel = [SubTopicDetailModel]()
    var userRole = ""
    
    //MARK:- Life Cycle Mehtods
    override func viewDidLoad() {
        super.viewDidLoad()

        loaderImage.image = UIImage.gif(name: "loader_gif")
        loaderView.isHidden = true
        subtopicTitle.text = ScreenTitle
        headerHeight()
        hideKeyboard()
        
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {
        deleteAlert.isHidden = true
        isRecordFound.isHidden = true
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        userId = userData["id"] as! Int

        if rowTappedValue == "Admin" {
            if userRole != "A" {
                btnAddSubtopic.isHidden = true
                imgAddSubtopic.isHidden = true
            }else {
                btnAddSubtopic.isHidden = false
                imgAddSubtopic.isHidden = false
            }
        }
        
        SubModel.removeAll()
        getSubPostData(url: SubTopic, method: responseType.get.rawValue, header: headerToken, completion: { (status) in
            if status {
                if self.SubModel.count == 0 {
                     self.isRecordFound.isHidden = false
                }else {
                     self.isRecordFound.isHidden = true
                }
                self.loaderView.isHidden = true
                print("SubTopic Data Fetched")
            }else{
                self.loaderView.isHidden = true
                print("SubTopic Data didn't Fetch")
            }
            
        })
    }
    //MARK:- Btn Actions
    @objc func deletePost(sender : UIButton) {
        deleteForumId = SubModel[sender.tag].forumPostsID ?? ""
        deleteAlert.isHidden = false
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    @objc func editPost(sender : UIButton) {
        let postId = SubModel[sender.tag].id
        let name = SubModel[sender.tag].forumPostsTopic
        let pinned = SubModel[sender.tag].forumPostsPinned
        let sos = SubModel[sender.tag].forumPostsSod
        let postApprove = SubModel[sender.tag].postApprove
        var visibility = ""
        if SubModel[sender.tag].forumPostsVisibility != nil {
            visibility = SubModel[sender.tag].forumPostsVisibility ?? "P"
        }else {
            visibility = SubModel[sender.tag].forumPostsPostVisibility ?? "P"
        }
        let content = SubModel[sender.tag].content
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditSubtopic") as! EditSubtopic
        vc.postId = Int(postId ?? "0")
        vc.titleName = name
        vc.pinned = Int(pinned ?? "0")!
        vc.sos = Int(sos ?? "0")!
        vc.Visibility = visibility
        vc.postAprove = Int(postApprove ?? "1")!
        vc.content = content ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func addPostBtn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AddSubTopicPost") as! AddSubTopicPost
        vc.subform_id = forumId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func deletePostBtn(_ sender: UIButton) {
        deleteAlert.isHidden = true
        loaderView.isHidden = false
        self.Status = false
        let param = ["forum_post_id":deleteForumId] as [String:Any]
        deleteData(url: deleteSubPostUrl, method: responseType.post.rawValue, param: param, header: headerToken,completion: { (status) in
            if status{
                self.SubModel.removeAll()
                self.getSubPostData(url: SubTopic, method: responseType.get.rawValue, header: headerToken, completion: { (status) in
                    if status{
                        self.loaderView.isHidden = true
                        print("SubTopic Data Fetched")
                    }else {
                        self.loaderView.isHidden = true
                        print("SubTopic Data didn't Fetch")
                    }
                })
                print("Selected post has been deleted")
            }else {
                print("Selected post couldn't delete")
            }
            
        })
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func hideDeleteAlertBtn(_ sender: UIButton) {
        deleteAlert.isHidden = true
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func homeBtn(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                height.constant += 24
            case 2688:
                height.constant += 24
            case 1792:
                height.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            height.constant += 4
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    func getSubPostData(url:String,method:HTTPMethod,header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        loaderView.isHidden = false
        guard let id = forumId else {return}
        let param = ["sub_forum_id":id,"page_no":1,"pagesize":1000,"user_id":userId] as [String:Any]
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.SubModel.append(SubTopicDetailModel(data: [i]))
                        }
                        self.tableView.reloadData()
                    }else {
                        self.tableView.reloadData()

                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    func deleteData(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
            if status {
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    print("Data deleted")
                }else {
                    self.Status = false
                    print("Data couldn't delete")
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

}
//MARK:- Extensions
extension SubTopicsDetail:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(SubModel.count)
        return SubModel.count
    }
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubTopicDetailCell") as! SubTopicDetailCell
        cell.like.text = "(" + SubModel[indexPath.row].likes! + ")"
        cell.dislike.text = "(" + SubModel[indexPath.row].dislike! + ")"
        cell.comment.text = "(" + SubModel[indexPath.row].comments! + ")"
        cell.Mtitle.text = SubModel[indexPath.row].forumPostsTopic
        cell.warning.text =  "(" + (SubModel[indexPath.row].pendingcomments ?? "0") + ")"
        cell.add.text =  "(" + String(SubModel[indexPath.row].particptatinguser ?? 0) + ")"
        if rowTappedValue == "Admin" {
            if userRole != "A" {
                cell.deleteBtn.isHidden = true
                cell.deleteImg.isHidden = true
                cell.editBtn.isHidden = true
                cell.editImg.isHidden = true
            }else {
                cell.deleteBtn.isHidden = false
                cell.deleteImg.isHidden = false
                cell.editBtn.isHidden = false
                cell.editImg.isHidden = false
                cell.deleteBtn.tag = indexPath.row
                cell.editBtn.tag = indexPath.row
                cell.editBtn.addTarget(self, action: #selector(self.editPost), for: .touchUpInside)
                cell.deleteBtn.addTarget(self, action: #selector(self.deletePost), for: .touchUpInside)
            }
        }else {
            if userRole == "A" {
                cell.deleteBtn.tag = indexPath.row
                cell.editBtn.tag = indexPath.row
                cell.editBtn.addTarget(self, action: #selector(self.editPost), for: .touchUpInside)
                cell.deleteBtn.addTarget(self, action: #selector(self.deletePost), for: .touchUpInside)
            }else {
                if String(userId) == SubModel[indexPath.row].forumPostsUserID {
                    cell.deleteBtn.isHidden = false
                    cell.deleteImg.isHidden = false
                    cell.editBtn.isHidden = false
                    cell.editImg.isHidden = false
                    cell.deleteBtn.tag = indexPath.row
                    cell.editBtn.tag = indexPath.row
                    cell.editBtn.addTarget(self, action: #selector(self.editPost), for: .touchUpInside)
                    cell.deleteBtn.addTarget(self, action: #selector(self.deletePost), for: .touchUpInside)
                }else {
                    cell.deleteBtn.isHidden = true
                    cell.deleteImg.isHidden = true
                    cell.editBtn.isHidden = true
                    cell.editImg.isHidden = true
                }
            }
        }
        return cell
    }
    
    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 80 * (view.frame.size.width / 320)

        }else {
            return 230
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ForumSubTopicPost") as! ForumSubTopicPost
        vc.forumPostId = Int(SubModel[indexPath.row].forumPostsID!)!
        vc.titleName = SubModel[indexPath.row].forumPostsTopic ?? ""
        vc.postApprovePost = SubModel[indexPath.row].postApprove ?? "0"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
}
