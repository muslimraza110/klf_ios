
import UIKit
import Alamofire
import SwiftGifOrigin
import WebKit
import SDWebImage

class ForumSubTopicPost: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {
   
    //MARK:- Btn Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var subcribeBtn: UIButton!
    @IBOutlet weak var immediatlyBtn: UIButton!
    @IBOutlet weak var dayBtn: UIButton!
    @IBOutlet weak var WeekBtn: UIButton!
    @IBOutlet weak var subcriptionBox: UIView!
    @IBOutlet weak var UnSubcribeBox: UIView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var deleteAlert: UIView!
    @IBOutlet weak var headerTilteName: UILabel!
    @IBOutlet weak var btnRply: UIButton!
    @IBOutlet weak var btnAddComment: AdaptiveButton!
    
    //MARK:- Custom Variables
    var isFirstPostIndex = 0
    var forumPostId = 0
    var postApprovePost = ""
    var titleName = ""
    var frequency = ""
    var userRole = ""
    var userId = 0
    var pageNo = 1
    var curruntPage = 0
    var totalPage = 0
    var Status = false
    var isFrequency = false
    var fetchingMore = false
    var deletePostId = ""
    var forumPostModel = ForumSubTopicPostModel()
    var frequencyModel = [ForumPostFrequencyModel]()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerTilteName.text = titleName
        headerHeight()
        frequency = "I"
        subcriptionBox.isHidden = true
        UnSubcribeBox.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        loaderView.isHidden = true
        tableView.register(UINib.init(nibName: "ForumPostTableViewCell", bundle: nil), forCellReuseIdentifier: "ForumPostTableViewCell")

    }

/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        userId = userData["id"] as! Int
        
        btnAddComment.isHidden = true
        deleteAlert.isHidden = true
        callApiOnScreenLoad()
        
    }
    
    //MARK:- ScrollView Methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        print("offsetY \(offsetY)  content height \(contentHeight) final height \(contentHeight - scrollView.frame.height)")
        if offsetY > contentHeight - scrollView.frame.height {
            if !fetchingMore {
                print("trying to reload")
                if totalPage != 0 {
                    if (curruntPage + 1) <= totalPage {
                         print("now going for reload")
                        beginBatchFetch()
                    }
                }
            }
        }
        
    }
    
    //MARK:- Btn Actions
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func hideDeleteAlertBtn(_ sender: UIButton) {
        deleteAlert.isHidden = true
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func deletePostBtn(_ sender: UIButton) {
        deleteAlert.isHidden = true
        let param = ["forum_post_id":deletePostId] as [String:Any]
        likeDislikePost(url: forumPostDelete, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status{
                print("post delete update")
                if self.isFirstPostIndex == 0 {
                    self.navigationController?.popViewController(animated: true)
                }
                self.getPost(url: forumPost, method: responseType.get.rawValue, header: headerToken) { (status) in
                    if status {
                        print("forum post has been reloaded with deleted post")
                    }else {
                        print("forum couldn't reload with deleted post")
                    }
                    self.loaderView.isHidden = true
                }
            }else {
                print("delete post didn't update")
            }
        }
        
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backToDashBoardBtn(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func UnsubcribeBoxHideBtn(_ sender: Any) {
        UnSubcribeBox.isHidden = true
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func subcriberBtn(_ sender: UIButton) {
        
        if sender.isSelected{
            let param = ["forum_post_id":forumPostId,"user_id":userId] as [String:Any]
            likeDislikePost(url: forumPostUnsubcribe, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status{
                    sender.isSelected = false
                    self.UnSubcribeBox.isHidden = false
                    let param = ["sub_topics_post_id":self.forumPostId,"user_id":self.userId] as [String:Any]
                    self.postFrequency(url: forumPostFrequency, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                        if status{
                            print("frequency got")
                            if self.frequencyModel.count >= 1 {
                                self.subcribeBtn.isSelected = true
                                self.frequency = self.frequencyModel[0].frequency ?? "I"
                                
                            }else {
                                self.subcribeBtn.isSelected = false
                                self.frequency = "I"
//                                self.immediatlyBtn.isSelected = true
                            }
                            self.loaderView.isHidden = true
                        }else {
                            print("frequency didn't get")
                            self.loaderView.isHidden = true
                        }
                    }
                    self.loaderView.isHidden = true
                    print("post has been unsubcribed")
                }else {
                    print("post didn't unsubcribe")
                }
            }
        }else {
            let param = ["sub_topics_post_id":forumPostId,"user_id":userId] as [String:Any]
            postFrequency(url: forumPostFrequency, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                if status{
                    self.immediatlyBtn.isSelected = true
                    self.dayBtn.isSelected = false
                    self.WeekBtn.isSelected = false
                    print("frequency has been received")
                }else {
                    print("frequency didn't receive")
                }
                self.loaderView.isHidden = true
                self.subcriptionBox.isHidden = false
                
            }
        }
     
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func subcriptionType(_ sender: UIButton) {
        if sender.tag == 1 {
            frequency = "I"
            immediatlyBtn.isSelected = true
            dayBtn.isSelected = false
            WeekBtn.isSelected = false
        }else if sender.tag == 2 {
            frequency = "D"
            immediatlyBtn.isSelected = false
            dayBtn.isSelected = true
            WeekBtn.isSelected = false
        }else if sender.tag == 3 {
            frequency = "W"
            immediatlyBtn.isSelected = false
            dayBtn.isSelected = false
            WeekBtn.isSelected = true
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func hideSubcriptionBoxBtn(_ sender: Any) {
        subcriptionBox.isHidden = true
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func subcriptionSendBtn(_ sender: Any) {
        let param = ["forum_post_id":forumPostId,"user_id":userId,"frequency":frequency] as [String:Any]
        likeDislikePost(url: forumPostSubcribe, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status{
                self.subcribeBtn.isSelected = true
                print("post has been subcribed")
            }else {
                print("post didn't subcribe")
            }
            self.subcriptionBox.isHidden = true
            self.loaderView.isHidden = true
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func addComment(_ sender: UIButton) {
        let parentId = forumPostId
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumPostEdit") as! ForumPostEdit
        vc.parentIdForComment = parentId
        if sender.tag == 1 {
            vc.titleName = "Reply Post"
        }else {
           vc.titleName = "Add Comment"
        }
        if userRole != "A" && postApprovePost == "1" {
            vc.statusPost = "P"
        }else {
            vc.statusPost = "A"
        }
        vc.comment = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func likeBtn(sender : UIButton) {
        var isLike : Int?
        if sender.tag == 0 {
            isLike = forumPostModel.islike
        }else {
            isLike = forumPostModel.comments[sender.tag - 1].islike
        }
        
        if isLike == 0 {
            var postId = ""
            if sender.tag == 0 {
                postId = forumPostModel.forumPostsID ?? ""
            }else {
                postId = forumPostModel.comments[sender.tag - 1].forumPostsID ?? ""
            }
            let param = ["forum_post_id":postId,"user_id":userId,"likes":1,"dislikes":0] as [String:Any]
            likeDislikePost(url: forumPostLike, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status{
                    print("new likes update")
                    self.getPost(url: forumPost, method: responseType.get.rawValue, header: headerToken) { (status) in
                        if status {
                            print("forum post has been reloaded with Likes Update")
                        }else {
                            print("forum couldn't reload with new Likes Update")
                        }
                        self.loaderView.isHidden = true
                    }
                }else {
                    print("new likes didn't update")
                }
            }
        }
        
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func dislikeBtn(sender : UIButton) {
        var isdislike : Int?
        if sender.tag == 0 {
            isdislike = forumPostModel.isdislike
        }else {
            isdislike = forumPostModel.comments[sender.tag - 1].isdislike
        }
        
        if isdislike == 0 {
            var postId = ""
            if sender.tag == 0 {
                postId = forumPostModel.forumPostsID ?? ""
            }else {
                postId = forumPostModel.comments[sender.tag - 1].forumPostsID ?? ""
            }
            let param = ["forum_post_id":postId,"user_id":userId,"likes":0,"dislikes":1] as [String:Any]
            likeDislikePost(url: forumPostLike, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status{
                    print("dislikes update")
                    self.getPost(url: forumPost, method: responseType.get.rawValue, header: headerToken) { (status) in
                        if status {
                            print("forum post has been reloaded with disLikes Update")
                        }else {
                            print("forum couldn't reload with new disLikes Update")
                        }
                        self.loaderView.isHidden = true
                    }
                }else {
                    print("dislikes didn't update")
                }
            }
        }
        
        
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func deleteBtn(sender : UIButton) {
        
        if sender.tag == 0 {
            deletePostId = forumPostModel.forumPostsID ?? ""
            isFirstPostIndex = sender.tag

        }else {
            isFirstPostIndex = sender.tag
            deletePostId = forumPostModel.comments[sender.tag - 1].forumPostsID ?? ""

        }
        deleteAlert.isHidden = false
       
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func editBtn(sender : UIButton) {
        var forumPostId = ""
        var content = ""
        var title = ""
        
        if sender.tag == 0 {
            forumPostId = forumPostModel.forumPostsID ?? ""
            content = forumPostModel.forumPostsContent ?? ""
            title = forumPostModel.forumPostsTopic ?? ""
        }else {
            forumPostId = forumPostModel.comments[sender.tag - 1].forumPostsID ?? ""
            content = forumPostModel.comments[sender.tag - 1].forumPostsContent ?? ""
            title = forumPostModel.comments[sender.tag - 1].forumPostsTopic ?? ""
        }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumPostEdit") as! ForumPostEdit
        vc.forumId = forumPostId
        vc.content = content
        vc.topicTitle = title
        vc.titleName = "Edit Comment"
        vc.edit = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func addQouteBtn(sender : UIButton) {
        var userIdForQoute = ""
        var parentId = ""
        var content = ""
        var title = ""
        var firstName = ""
        var lastName = ""
        var createdDetail = ""
        
        if sender.tag == 0 {
            userIdForQoute = String(userId)
            parentId = forumPostModel.forumPostsID ?? ""
            content = forumPostModel.forumPostsContent ?? ""
            title = forumPostModel.forumPostsTopic ?? ""
            firstName = forumPostModel.usersFirstName ?? ""
            lastName = forumPostModel.usersLastName ?? ""
            createdDetail = forumPostModel.forumPostsCreated ?? ""
        }else {
            userIdForQoute = String(userId)
            parentId = forumPostModel.forumPostsID ?? ""
            content = forumPostModel.comments[sender.tag - 1].forumPostsContent ?? ""
            title = forumPostModel.comments[sender.tag - 1].forumPostsTopic ?? ""
            firstName = forumPostModel.comments[sender.tag - 1].usersFirstName ?? ""
            lastName = forumPostModel.comments[sender.tag - 1].usersLastName ?? ""
            createdDetail = forumPostModel.comments[sender.tag - 1].forumPostsCreated ?? ""
        }
       
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumPostEdit") as! ForumPostEdit
        vc.userId = userIdForQoute
        vc.parentId = parentId
        vc.content = content
        vc.topicTitle = title
        vc.titleName = "Quote"
        vc.qouteCreatorName = "\(firstName) \(lastName)"
        vc.qouteDateAndTime = createdDetail
        if userRole != "A" && postApprovePost == "1" {
            vc.statusPost = "P"
        }else {
            vc.statusPost = "A"
        }
        vc.qoute = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    @objc func eblastBtn(sender : UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EblastController") as! EblastController
        if sender.tag == 0 {
            vc.content = forumPostModel.forumPostsContent ?? ""
        }else {
           vc.content = forumPostModel.comments[sender.tag - 1].forumPostsContent ?? ""
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    @objc func commentApproveBtn(sender : UIButton) {
        var id = ""
        if sender.tag == 0 {
            id = forumPostModel.forumPostsID ?? ""
        }else {
           id = forumPostModel.comments[sender.tag - 1].forumPostsID ?? ""
        }
        
        let param = ["reply_id":id ] as [String:Any]
        likeDislikePost(url: forumPostApprove, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status{
                self.getPost(url: forumPost, method: responseType.get.rawValue, header: headerToken) { (status) in
                    if status {
                        print("forum post has been reloaded with comment approve Update")
                    }else {
                        print("forum couldn't reload with new comment approve Update")
                    }
                    self.loaderView.isHidden = true
                }
                print("comment approved")
            }else {
                self.loaderView.isHidden = true
                print("comment didn't approve")
            }
        }
    }
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func callApiOnScreenLoad(){
        getPost(url: forumPost, method: responseType.get.rawValue, header: headerToken) { (status) in
            if status {
                let param = ["sub_topics_post_id":self.forumPostId,"user_id":self.userId] as [String:Any]
                self.postFrequency(url: forumPostFrequency, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
                    if status{
                        print("frequency got")
                        
                        self.pageNo = self.forumPostModel.pageNo ?? 1
                        self.totalPage = self.forumPostModel.totalNoOfPages ?? 0
                        self.curruntPage = self.curruntPage + self.pageNo
                        
                        if self.frequencyModel.count >= 1 {
                            self.subcribeBtn.isSelected = true
                            self.frequency = self.frequencyModel[0].frequency ?? "I"
                            
                        }else {
                            self.subcribeBtn.isSelected = false
                            self.frequency = "I"
                            self.immediatlyBtn.isSelected = true
                        }
                        self.loaderView.isHidden = true
                    }else {
                        print("frequency didn't get")
                        self.loaderView.isHidden = true
                    }
                }
                if self.userRole != "A" {
                    if self.forumPostModel.forumPostsVisibility?.lowercased() == "p" {
                          
                          self.btnRply.isHidden = false
                          self.btnAddComment.isHidden = false
                          
                      }else if self.forumPostModel.forumPostsVisibility?.lowercased() == "a" {
                          
                          if self.userRole == "A" || self.forumPostModel.isModerator == 1 {
                              self.btnRply.isHidden = false
                              self.btnAddComment.isHidden = false
                          }else {
                              self.btnRply.isHidden = true
                              self.btnAddComment.isHidden = true
                          }

                      }else if self.forumPostModel.forumPostsVisibility?.lowercased() == "s" {
                          
                          self.btnRply.isHidden = false
                          self.btnAddComment.isHidden = false
                          
                      }else if self.forumPostModel.forumPostsVisibility?.lowercased() == "w" {
                          
                          self.btnRply.isHidden = false
                          self.btnAddComment.isHidden = false
                          
                      }
                }else {
                    self.btnRply.isHidden = false
                    self.btnAddComment.isHidden = false
                }
              
                
                print("forum post has been received")
            }else {
                self.loaderView.isHidden = true
            }
            
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func callApiForNextData(curruntPage:Int) {
        
        let param = ["forum_post_id":forumPostId,"pagesize":"25","page_no":curruntPage,"user_id":userId] as [String:Any]
        getPostForLoadNextData(url: forumPost, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.curruntPage = self.curruntPage + 1
                self.tableView.reloadData()
                self.fetchingMore = false

            }else {
                
            }
            self.loaderView.isHidden = true
            
        }
        
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func getPost(url:String,method:HTTPMethod,header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        
        loaderView.isHidden = false
        let param = ["forum_post_id":forumPostId,"pagesize":"25","page_no":pageNo,"user_id":userId] as [String:Any]
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? NSDictionary {
                        self.forumPostModel = ForumSubTopicPostModel(i: data)
                        self.tableView.reloadData()
                    }
                    
                }else {
                    self.tableView.reloadData()
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func getPostForLoadNextData(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
           
           loaderView.isHidden = false
           Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
               if status {
                   self.Status = false
                   let JSON = JSON
                   let status = JSON["status"] as? String
                   if status == "success" {
                       self.Status = true
                       if let data = JSON["data"] as? NSDictionary {
                        if let comment = data["comments"] as? [[String:Any]] {
                            for i in comment {
                                self.forumPostModel.comments.append(Comments(data: [i]))
                            }
                          }
                       }
                       
                   }else {
                       self.Status = false
                   }
                   completion(self.Status)
               }else {
                   self.loaderView.isHidden = true
               }
           }
       }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func likeDislikePost(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func postFrequency(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        frequencyModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    if let data = JSON["data"] as! [[String:Any]]? {
                        for i in data {
                            self.frequencyModel.append(ForumPostFrequencyModel(data: [i]))
                        }
                    }
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func beginBatchFetch (){
        fetchingMore = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute:  {
            self.curruntPage = self.curruntPage + 1
            self.callApiForNextData(curruntPage:self.curruntPage)
        })
        
    }
    

    
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if forumPostModel.forumPostsUserID != nil {
            if userRole != "A" && forumPostModel.isModerator != 1 {
                 var activeMemberId = [String]()
                //checking for semi private post
                if forumPostModel.forumPostsVisibility?.lowercased() == "s" {
                    for i in forumPostModel.comments {
                        if i.forumPostsStatus?.lowercased() == "a" && i.usersRole == "A"{
                               activeMemberId.append(i.forumPostsID ?? "")
                           }
                       }
                }else {  
                    //checking for all other visibilities except semi private
                    for i in forumPostModel.comments {
                           if i.forumPostsStatus?.lowercased() == "a" {
                               activeMemberId.append(i.forumPostsID ?? "")
                           }
                       }
                }
               
               //removing elements from array which is not active
                if activeMemberId.count != 0 {
                   var commentsCount = forumPostModel.comments.count - 1
                   var n = 0
                   while (n <= commentsCount) {
                       if !activeMemberId.contains(forumPostModel.comments[n].forumPostsID ?? "") {
                           forumPostModel.comments.remove(at: n)
                           commentsCount = commentsCount - 1
                       }
                       else {
                           n = n + 1
                       }
                   }
                    
                }
                    
                
                return activeMemberId.count + 1
            }else {
                return forumPostModel.comments.count + 1

            }
            
        }
        return 0
        
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForumPostTableViewCell") as! ForumPostTableViewCell
        
        //For Main Post
        if indexPath.row == 0 {
        /*-----------------------------------------------------------------------------------------------------*/
            var fontSize : CGFloat = 0.0
            if UIDevice().userInterfaceIdiom == .phone {
               fontSize = (32 * (self.view.frame.width / 320))

            }else {
                fontSize = 40
            }
            let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
            cell.postContent.loadHTMLString(fontSetting + (forumPostModel.forumPostsContent ?? "") , baseURL: nil)
            cell.postedByImageView.layer.cornerRadius = cell.postedByImageView.frame.size.height/2
            
        /*-----------------------------------------------------------------------------------------------------*/
            // Getting counts for like and dislike
            if let likeCount = forumPostModel.likescount {
                cell.likeLbl.text = String(likeCount)
            }
            if let dislikeCount = forumPostModel.dislikescount {
                cell.dislikeLbl.text = String(dislikeCount)
            }
            
        /*-----------------------------------------------------------------------------------------------------*/
            // Setting flag for like and dislike
            let islike = forumPostModel.islike
            let isdislike = forumPostModel.isdislike
            if islike == 1 {
                cell.islikeImg.image = UIImage(named: "ic_like_blue")
            }else if islike == 0 {
                cell.islikeImg.image = UIImage(named: "ic_like")
            }
            
            if isdislike == 1 {
                cell.isDislikeImg.image = UIImage(named: "ic_dislike_blue")
            }else if isdislike == 0 {
                cell.isDislikeImg.image = UIImage(named: "ic_dislike")
            }
            
            /*-----------------------------------------------------------------------------------------------------*/
            //User Role Checking
                if userRole == "A" || forumPostModel.isModerator == 1 {
                    if forumPostModel.forumPostsStatus == "P" {
                        cell.approveView.isHidden = false
                        cell.approveBtn.tag = indexPath.row
                        cell.approveBtn.addTarget(self, action: #selector(self.commentApproveBtn), for: .touchUpInside)
                    }else {
                        cell.approveView.isHidden = true
                    }
                    
                    if userRole == "A" {
                        cell.eblastView.isHidden = false
                        cell.eblastBtn.tag = indexPath.row
                        cell.eblastBtn.addTarget(self, action: #selector(self.eblastBtn), for: .touchUpInside)
                    }else {
                        cell.eblastView.isHidden = true
                    }
                    
                    cell.editBtn.tag = indexPath.row
                    cell.deleteBtn.tag = indexPath.row
                    cell.qouteBtn.tag = indexPath.row
                    cell.deleteBtn.addTarget(self, action: #selector(self.deleteBtn), for: .touchUpInside)
                    cell.editBtn.addTarget(self, action: #selector(self.editBtn), for: .touchUpInside)
                    cell.qouteBtn.addTarget(self, action: #selector(self.addQouteBtn), for: .touchUpInside)
                }else {
                    if String(userId) == forumPostModel.forumPostsUserID {
                        cell.addView.isHidden = false
                        cell.editBtn.isHidden = false
                        cell.deleteView.isHidden = false
                        cell.deleteBtn.isHidden = false
                        cell.deleteBtn.tag = indexPath.row
                        cell.editBtn.tag = indexPath.row
                        cell.deleteBtn.addTarget(self, action: #selector(self.deleteBtn), for: .touchUpInside)
                        cell.editBtn.addTarget(self, action: #selector(self.editBtn), for: .touchUpInside)
                    }else {
                        cell.addView.isHidden = true
                        cell.editBtn.isHidden = true
                        cell.deleteView.isHidden = true
                        cell.deleteBtn.isHidden = true
                    }
                                        
                    // For post visibility Admin check
                    if forumPostModel.forumPostsVisibility?.lowercased() == "a" {
                        cell.qouteImg.isHidden = true
                        cell.qouteBtn.isHidden = true
                        
                    }else {
                        cell.qouteImg.isHidden = false
                        cell.qouteBtn.isHidden = false
                        cell.qouteBtn.tag = indexPath.row
                        cell.qouteBtn.addTarget(self, action: #selector(self.addQouteBtn), for: .touchUpInside)
                        
                    }
                    
                    cell.approveView.isHidden = true
                    cell.eblastView.isHidden = true
                    
                }
            
            /*-----------------------------------------------------------------------------------------------------*/
            
            let firstName = forumPostModel.usersFirstName
            let lastName = forumPostModel.usersLastName
            cell.lblPostedByName.text = "\(firstName ?? "") " + "\(lastName ?? "")"
            cell.lblPostedOnDateTime.text = forumPostModel.forumPostsCreated
            cell.lblTotalPosts.text = forumPostModel.posts
            cell.lblLastLoginDateTime.text = forumPostModel.usersLastLogin
            cell.likeBtn.tag = indexPath.row
            cell.dislikeBtn.tag = indexPath.row
            cell.likeBtn.addTarget(self, action: #selector(self.likeBtn), for: .touchUpInside)
            cell.dislikeBtn.addTarget(self, action: #selector(self.dislikeBtn), for: .touchUpInside)
            
            cell.postedByImageView.layer.cornerRadius = cell.postedByImageView.frame.width/2
            let remoteImageUrlString = forumPostModel.profileImgName ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.postedByImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
               print("image \(indexPath.row) loaded")
            })
            
        /*-----------------------------------------------------------------------------------------------------*/

        }else {
        /*-----------------------------------------------------------------------------------------------------*/
            // Comment list
            // FOR Admin and moderated

            if userRole == "A" || forumPostModel.isModerator == 1 {
                
                var fontSize : CGFloat = 0.0
                if UIDevice().userInterfaceIdiom == .phone {
                   fontSize = (32 * (self.view.frame.width / 320))

                }else {
                    fontSize = 40
                }
                let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
                cell.postContent.loadHTMLString(fontSetting + (forumPostModel.comments[indexPath.row - 1].forumPostsContent ?? "") , baseURL: nil)
                cell.postedByImageView.layer.cornerRadius = cell.postedByImageView.frame.size.height/2
                
                /*-----------------------------------------------------------------------------------------------------*/
                // Getting counts for like and dislike
                if let likeCount = forumPostModel.comments[indexPath.row - 1].likescount {
                    cell.likeLbl.text = String(likeCount)
                }
                if let dislikeCount = forumPostModel.comments[indexPath.row - 1].dislikescount {
                    cell.dislikeLbl.text = String(dislikeCount)
                }
                
                /*-----------------------------------------------------------------------------------------------------*/
                // Setting flag for like and dislike
                let islike = forumPostModel.comments[indexPath.row - 1].islike
                let isdislike = forumPostModel.comments[indexPath.row - 1].isdislike
                if islike == 1 {
                    cell.islikeImg.image = UIImage(named: "ic_like_blue")
                }else if islike == 0 {
                    cell.islikeImg.image = UIImage(named: "ic_like")
                }
                
                if isdislike == 1 {
                    cell.isDislikeImg.image = UIImage(named: "ic_dislike_blue")
                }else if isdislike == 0 {
                    cell.isDislikeImg.image = UIImage(named: "ic_dislike")
                }
                
                /*-----------------------------------------------------------------------------------------------------*/

                if forumPostModel.comments[indexPath.row - 1].forumPostsStatus == "P" {
                    cell.approveView.isHidden = false
                    cell.approveBtn.tag = indexPath.row
                    cell.approveBtn.addTarget(self, action: #selector(self.commentApproveBtn), for: .touchUpInside)
                }else {
                    cell.approveView.isHidden = true
                }
                
                /*-----------------------------------------------------------------------------------------------------*/

                if userRole == "A" {
                    cell.eblastView.isHidden = false
                    cell.eblastBtn.tag = indexPath.row
                    cell.eblastBtn.addTarget(self, action: #selector(self.eblastBtn), for: .touchUpInside)
                }else {
                    cell.eblastView.isHidden = true
                }
                
                cell.editBtn.tag = indexPath.row
                cell.deleteBtn.tag = indexPath.row
                cell.qouteBtn.tag = indexPath.row
                cell.deleteBtn.addTarget(self, action: #selector(self.deleteBtn), for: .touchUpInside)
                cell.editBtn.addTarget(self, action: #selector(self.editBtn), for: .touchUpInside)
                cell.qouteBtn.addTarget(self, action: #selector(self.addQouteBtn), for: .touchUpInside)
                
                
                
                let firstName = forumPostModel.comments[indexPath.row - 1].usersFirstName
                let lastName = forumPostModel.comments[indexPath.row - 1].usersLastName
                cell.lblPostedByName.text = "\(firstName ?? "") " + "\(lastName ?? "")"
                cell.lblPostedOnDateTime.text = forumPostModel.comments[indexPath.row - 1].forumPostsCreated
                cell.lblTotalPosts.text = forumPostModel.comments[indexPath.row - 1].posts
                cell.lblLastLoginDateTime.text = forumPostModel.comments[indexPath.row - 1].usersLastLogin
                cell.likeBtn.tag = indexPath.row
                cell.dislikeBtn.tag = indexPath.row
                cell.likeBtn.addTarget(self, action: #selector(self.likeBtn), for: .touchUpInside)
                cell.dislikeBtn.addTarget(self, action: #selector(self.dislikeBtn), for: .touchUpInside)
                
                cell.postedByImageView.layer.cornerRadius = cell.postedByImageView.frame.width/2
                let remoteImageUrlString = forumPostModel.comments[indexPath.row - 1].profileImgName ?? ""
                let imageUrl = URL(string:remoteImageUrlString)
                cell.postedByImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                   print("image \(indexPath.row) loaded")
                })
                
                /*-----------------------------------------------------------------------------------------------------*/

            }else {
                /*-----------------------------------------------------------------------------------------------------*/

                if forumPostModel.comments[indexPath.row - 1].forumPostsStatus?.lowercased() == "a" {
                    
                    var fontSize : CGFloat = 0.0
                    if UIDevice().userInterfaceIdiom == .phone {
                       fontSize = (32 * (self.view.frame.width / 320))

                    }else {
                        fontSize = 40
                    }
                    let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
                    cell.postContent.loadHTMLString(fontSetting + (forumPostModel.comments[indexPath.row - 1].forumPostsContent ?? "" ) , baseURL: nil)
                    cell.postedByImageView.layer.cornerRadius = cell.postedByImageView.frame.size.height/2
                    
                /*-----------------------------------------------------------------------------------------------------*/
                    // Getting counts for like and dislike
                    if let likeCount = forumPostModel.comments[indexPath.row - 1].likescount {
                        cell.likeLbl.text = String(likeCount)
                    }
                    if let dislikeCount = forumPostModel.comments[indexPath.row - 1].dislikescount {
                        cell.dislikeLbl.text = String(dislikeCount)
                    }
                    
                /*-----------------------------------------------------------------------------------------------------*/
                    // Setting flag for like and dislike
                    let islike = forumPostModel.comments[indexPath.row - 1].islike
                    let isdislike = forumPostModel.comments[indexPath.row - 1].isdislike
                    if islike == 1 {
                        cell.islikeImg.image = UIImage(named: "ic_like_blue")
                    }else if islike == 0 {
                        cell.islikeImg.image = UIImage(named: "ic_like")
                    }
                    
                    if isdislike == 1 {
                        cell.isDislikeImg.image = UIImage(named: "ic_dislike_blue")
                    }else if isdislike == 0 {
                        cell.isDislikeImg.image = UIImage(named: "ic_dislike")
                    }
                    
                /*-----------------------------------------------------------------------------------------------------*/

                    
                    if String(userId) == forumPostModel.comments[indexPath.row - 1].forumPostsUserID {
                        cell.addView.isHidden = false
                        cell.editBtn.isHidden = false
                        cell.deleteView.isHidden = false
                        cell.deleteBtn.isHidden = false
                        cell.deleteBtn.tag = indexPath.row
                        cell.editBtn.tag = indexPath.row
                        cell.deleteBtn.addTarget(self, action: #selector(self.deleteBtn), for: .touchUpInside)
                        cell.editBtn.addTarget(self, action: #selector(self.editBtn), for: .touchUpInside)
                    }else {
                        cell.addView.isHidden = true
                        cell.editBtn.isHidden = true
                        cell.deleteView.isHidden = true
                        cell.deleteBtn.isHidden = true
                    }
                    
                    //Qoute checking on post visibiltiy
                    if forumPostModel.forumPostsVisibility?.lowercased() == "a" {
                        cell.qouteImg.isHidden = true
                        cell.qouteBtn.isHidden = true
                        
                    }else {
                        cell.qouteImg.isHidden = false
                        cell.qouteBtn.isHidden = false
                        cell.qouteBtn.tag = indexPath.row
                        cell.qouteBtn.addTarget(self, action: #selector(self.addQouteBtn), for: .touchUpInside)
                        
                    }
                /*-----------------------------------------------------------------------------------------------------*/
                    let firstName = forumPostModel.comments[indexPath.row - 1].usersFirstName
                     let lastName = forumPostModel.comments[indexPath.row - 1].usersLastName
                     cell.lblPostedByName.text = "\(firstName ?? "") " + "\(lastName ?? "")"
                     cell.lblPostedOnDateTime.text = forumPostModel.comments[indexPath.row - 1].forumPostsCreated
                     cell.lblTotalPosts.text = forumPostModel.comments[indexPath.row - 1].posts
                     cell.lblLastLoginDateTime.text = forumPostModel.comments[indexPath.row - 1].usersLastLogin
                     cell.likeBtn.tag = indexPath.row
                     cell.dislikeBtn.tag = indexPath.row
                     cell.likeBtn.addTarget(self, action: #selector(self.likeBtn), for: .touchUpInside)
                     cell.dislikeBtn.addTarget(self, action: #selector(self.dislikeBtn), for: .touchUpInside)
                     
                     cell.approveView.isHidden = true
                     cell.eblastView.isHidden = true
  
                    cell.postedByImageView.layer.cornerRadius = cell.postedByImageView.frame.width/2
                    let remoteImageUrlString = forumPostModel.comments[indexPath.row - 1].profileImgName ?? ""
                    let imageUrl = URL(string:remoteImageUrlString)
                    cell.postedByImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                        print("image \(indexPath.row) loaded")
                    })
                    
                }
               
                  
            }
        

            
        }
        
   
        return cell
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.size.height / 2
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

}
