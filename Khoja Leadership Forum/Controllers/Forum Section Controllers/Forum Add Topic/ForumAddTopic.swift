
import UIKit
import RichEditorView
import Alamofire
import SwiftGifOrigin

class ForumAddTopic: UIViewController{
    
    //MARK:- Btn Outlets
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet var htmlTextView: UITextView!
    @IBOutlet weak var addTopicTitle: UITextField!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loader: UIView!
    @IBOutlet weak var loaderImage: UIImageView!
    
    //MARK:- Custom Variables
    var Status = false
    var categoryReceived = ""
    var userId = 0
    var model = [ForumAddModel]()
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        loader.isHidden = true
        loaderImage.image = UIImage.gif(name: "loader_gif")
        headerHeight()
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Description"

        toolbar.delegate = self
        toolbar.editor = editorView
        
        hideKeyboard()
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let itemDone = RichEditorOptionItem(image: nil, title: "Done") { (toolBar) in
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(itemDone)
        toolbar.options = options
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
    }
    
    //MARK:- Button Actions
    @IBAction func backBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func publishBtn(_ sender: Any) {
        
        if self.addTopicTitle.text!.count > 0 && self.editorView.html.count > 0 {
        
            loader.isHidden = false
            RichEditorDefaultOption.header(5).action(toolbar)
            let url = mainUrl+forumAddTopicUrl
            addPost(url: url, method: responseType.post.rawValue, header: headerToken) { (status) in
                if status {
                    self.loader.isHidden = true
                    self.navigationController?.popViewController(animated: true)
                    print("Ad has been posted")
                }else {
                    self.loader.isHidden = true
                    self.navigationController?.popViewController(animated: true)
                    print("Ad didn't posted")
                }
           
            }
        }
        else{
            showToast(message: "Both fields are mandatory")
        }
     }
    
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    func addPost(url:String,method:HTTPMethod,header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        
        if editorView.html != "" && addTopicTitle.text != "" {
        let param = ["user_id" : userId,"category_name" : categoryReceived,"subtopic_title" : addTopicTitle.text!,"subtopic_description" : editorView.html] as [String:Any]
            Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status) in
                if status {
                    self.editorView.html = ""
                    self.addTopicTitle.text = ""
                    self.Status = false
                    let JSON = JSON
                    let status = JSON["status"] as? String
                    if status == "success" {
                        self.Status = true
                        if let data = JSON["data"] as? NSDictionary {
                        self.model.append(ForumAddModel(data: data))
                    }else {
                        self.Status = false
                    }
                    completion(self.Status)
                    }
                }else {
                    self.loader.isHidden = true
                }
            }
        }else {
            loader.isHidden = true
            print("Please Enter title or description")
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

}
//MARK:- Extensions
extension ForumAddTopic: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
//         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
        
}

extension ForumAddTopic: RichEditorToolbarDelegate {
        
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/

    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
/*------------------------------------------------------------------------------------------------------------------------------------*/
}

