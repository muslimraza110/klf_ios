
import UIKit

class SubTopicDetailCell: UITableViewCell {

    @IBOutlet weak var Mtitle: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var like: UILabel!
    @IBOutlet weak var dislike: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var warning: UILabel!
    @IBOutlet weak var add: UILabel!
    @IBOutlet weak var editImg: UIImageView!
    @IBOutlet weak var deleteImg: UIImageView!
    
}
