
import UIKit
import WebKit

class ForumPostTableViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var postedByImageView: UIImageView!
    @IBOutlet weak var lblPostedByName: UILabel!
    @IBOutlet weak var lblPostedOnDateTime: UILabel!
    @IBOutlet weak var lblTotalPosts: UILabel!
    @IBOutlet weak var lblLastLoginDateTime: UILabel!
    @IBOutlet var postContent: WKWebView!
    @IBOutlet weak var likeLbl: UILabel!
    @IBOutlet weak var dislikeLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var dislikeBtn: UIButton!
    @IBOutlet weak var eblastBtn: UIButton!
    @IBOutlet weak var qouteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var islikeImg: UIImageView!
    @IBOutlet weak var isDislikeImg: UIImageView!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var eblastView: UIView!
    @IBOutlet weak var qouteImg: UIImageView!
    @IBOutlet weak var approveView: UIView!
    @IBOutlet weak var approveBtn: UIButton!
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - likeButtonPressed
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        print("like Button Pressed")
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - dislikeButtonPressed
    @IBAction func dislikeButtonPressed(_ sender: UIButton) {
        print("dislike Button Pressed")
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - sendEblastButtonPressed
    @IBAction func sendEblastButtonPressed(_ sender: UIButton) {
        print("eblast Button Pressed")
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - quoteButtonPressed
    @IBAction func quoteButtonPressed(_ sender: UIButton) {
        print("quote Button Pressed")
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - editButtonPressed
    @IBAction func editButtonPressed(_ sender: UIButton) {
        print("edit Button Pressed")
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - deleteButtonPressed
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        print("delete Button Pressed")
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
}
