
import UIKit

class RecipientTableViewCell: UITableViewCell {

    //Outlets
    
    @IBOutlet weak var imgViewContactPicture: UIImageView!
    @IBOutlet weak var lblContactName: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
