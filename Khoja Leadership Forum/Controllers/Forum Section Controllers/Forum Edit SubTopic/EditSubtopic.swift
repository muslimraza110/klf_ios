
import UIKit
import RichEditorView
import Alamofire
import SwiftGifOrigin

class EditSubtopic: UIViewController {

    //MARK:- Btn Outlets
    @IBOutlet weak var editorView: RichEditorView!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var pinnedBtn: UIButton!
    @IBOutlet weak var dashboardBtn: UIButton!
    @IBOutlet weak var eblastBtn: UIButton!
    @IBOutlet weak var moderatorBtn: UIButton!
    @IBOutlet weak var publicBtn: UIButton!
    @IBOutlet weak var adminBtn: UIButton!
    @IBOutlet weak var restrictedBtn: UIButton!
    @IBOutlet weak var privateBtn: UIButton!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var btnPublistSecondConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnPublishFirstConstrain: NSLayoutConstraint!
    @IBOutlet weak var lblTitleSecondConstrain: NSLayoutConstraint!
    @IBOutlet weak var lblTitleFirstConstrain: NSLayoutConstraint!
    @IBOutlet weak var pinnedView: UIView!
    @IBOutlet weak var moderatorView: UIView!
    @IBOutlet weak var visibilityView: UIView!
    @IBOutlet weak var whiteListView: UIView!
    
    //MARK:- Custom Variables
    var userId = 0
    var pinned = 0
    var sos = 0
    var eblast = 0
    var Visibility = "P"
    var content = ""
    var postAprove = 0
    var postId:Int!
    var Status = false
    var userRole = ""
    var titleName:String!
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        headerHeight()
        whiteListView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        loaderView.isHidden = true
        titleField.text = titleName
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.html = content

        toolbar.delegate = self
        toolbar.editor = editorView
        hideKeyboard()
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        
        let itemDone = RichEditorOptionItem(image: nil, title: "Done") { (toolBar) in
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(itemDone)
        toolbar.options = options
        
        topicDetailChk()
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        userRole = userData["role"] as! String
       
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewWillLayoutSubviews() {
        btnPublishFirstConstrain.isActive = true
        btnPublistSecondConstraint.isActive = false
        lblTitleFirstConstrain.isActive = true
        lblTitleSecondConstrain.isActive = false
        
        if userRole != "A" {
            btnPublishFirstConstrain.isActive = false
            btnPublistSecondConstraint.isActive = true
            lblTitleFirstConstrain.isActive = false
            lblTitleSecondConstrain.isActive = true
            pinnedView.isHidden = true
            moderatorView.isHidden = true
            visibilityView.isHidden = true
            whiteListView.isHidden = true
        }else {
            btnPublishFirstConstrain.isActive = true
            btnPublistSecondConstraint.isActive = false
            lblTitleFirstConstrain.isActive = true
            lblTitleSecondConstrain.isActive = false
            pinnedView.isHidden = false
            moderatorView.isHidden = false
            visibilityView.isHidden = false
        }
        
        
    }
    
  //MARK:- Btn Actions
    @IBAction func visibilityChkBtn(_ sender: UIButton) {
        if sender.tag == 1 {
            Visibility = "P"
            publicBtn.isSelected = true
            adminBtn.isSelected = false
            privateBtn.isSelected = false
            restrictedBtn.isSelected = false
            whiteListView.isHidden = true
        }else if sender.tag == 2 {
            Visibility = "A"
            publicBtn.isSelected = false
            adminBtn.isSelected = true
            privateBtn.isSelected = false
            restrictedBtn.isSelected = false
            whiteListView.isHidden = true
        }else if sender.tag == 3 {
            Visibility = "S"
            publicBtn.isSelected = false
            adminBtn.isSelected = false
            privateBtn.isSelected = true
            restrictedBtn.isSelected = false
            whiteListView.isHidden = true
        }else if sender.tag == 4 {
            Visibility = "W"
            publicBtn.isSelected = false
            adminBtn.isSelected = false
            privateBtn.isSelected = false
            restrictedBtn.isSelected = true
            whiteListView.isHidden = false
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func eblastBtn(_ sender: UIButton) {
        if sender.isSelected {
            eblast = 0
            sender.isSelected = false
        }else {
            eblast = 1
            sender.isSelected = true
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func dashboardBtn(_ sender: UIButton) {
        if sender.isSelected {
            sos = 0
            sender.isSelected = false
        }else {
            sos = 1
            sender.isSelected = true
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func pinnedBtn(_ sender: UIButton) {
        if sender.isSelected {
            pinned = 0
            sender.isSelected = false
        }else {
            pinned = 1
            sender.isSelected = true
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func publishBtn(_ sender: Any) {
        RichEditorDefaultOption.header(5).action(toolbar)
        let param = ["user_id":userId,"post_id":postId!,"topic":titleField.text!,"content":editorView.html,"post_visibility":Visibility,"pinned":pinned,"sod":sos,"eblast":eblast,"post_approved_post":postAprove] as [String:Any]
        editPost(url:editSubPostUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status{
                print("Subpost added")
            }else {
                print("Subpost didn't add")
            }
            self.loaderView.isHidden = true
            if self.eblast == 1 {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "EblastController") as! EblastController
                vc.content = self.editorView.html
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
               self.navigationController?.popViewController(animated: true)
            }
         
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func whiteListContactBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchController") as! SearchController
        vc.whiteListType = "contact"
        vc.forumPostId = postId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func whiteListGroupBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchController") as! SearchController
        vc.whiteListType = "group"
        vc.forumPostId = postId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func goToModeratorScreenBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ModeratorController") as! ModeratorController
        vc.forumPostId = postId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func moderatorApproveBtn(_ sender: UIButton) {
        if sender.isSelected{
            postAprove = 0
            sender.isSelected = false
        }else {
            postAprove = 1
            sender.isSelected = true
        }
        
    }
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func editPost(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        if postId != nil && titleField.text != "" && editorView.html != "" {
            Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
                if status {
                    self.Status = false
                    let JSON = JSON
                    let status = JSON["status"] as? String
                    if status == "success" {
                        self.Status = true
                    }
                    completion(self.Status)
                }else {
                    self.loaderView.isHidden = true
                }
            }
        }else{
            showToast(message: "Title or description shouldn't empty")
            self.loaderView.isHidden = true
            print("Enter title name or description")
        }
        
    }

/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func topicDetailChk () {
        if pinned == 1 {
            pinnedBtn.isSelected = true
        }else {
            pinnedBtn.isSelected = false
        }
        
        if sos == 1 {
            dashboardBtn.isSelected = true
        }else {
            dashboardBtn.isSelected = false
        }
        
        if eblast == 1 {
            eblastBtn.isSelected = true
        }else {
            eblastBtn.isSelected = false
        }
        
        if postAprove == 1 {
            moderatorBtn.isSelected = true
        }else {
            moderatorBtn.isSelected = false
        }
        
        
        if Visibility == "P" {
            publicBtn.isSelected = true
        }else if Visibility == "A" {
            adminBtn.isSelected = true
        }else if Visibility == "S"{
             privateBtn.isSelected = true
        }else if Visibility == "W" {
            restrictedBtn.isSelected = true
            whiteListView.isHidden = false
        }
        
    }
        
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
}

//MARK:- Extensions
extension EditSubtopic: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
        
}

extension EditSubtopic: RichEditorToolbarDelegate {
        
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
}
