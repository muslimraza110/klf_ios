
import UIKit
import Alamofire
import SwiftGifOrigin
import RichEditorView

class ForumPostEdit: UIViewController {

    //MARK:- Btn OutLets
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet weak var loader: UIView!
    @IBOutlet weak var loaderImage: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var nameOfScreen: UILabel!
    
    //MARK:- Custom Variables
    var Status = false
    var qouteCreatorName = ""
    var qouteDateAndTime = ""
    var userId = ""
    var statusPost = ""
    var useridForComment = 0
    var parentIdForComment = 0
    var parentId = ""
    var forumId = ""
    var topicTitle = ""
    var content = ""
    var qoute = false
    var comment = false
    var edit = false
    var titleName = ""
    var model = [ForumAddModel]()
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()

    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        useridForComment = userData["id"] as! Int
        nameOfScreen.text = titleName
        headerHeight()
        loader.isHidden = true
        loaderImage.image = UIImage.gif(name: "loader_gif")
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar

        if qoute{
            editorView.html = "\"\(qouteCreatorName) wrote on \(qouteDateAndTime) \(content)\""
        }
        if edit{
             editorView.html = content
        }
       
        toolbar.delegate = self
        toolbar.editor = editorView
        
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let itemDone = RichEditorOptionItem(image: nil, title: "Done") { (toolBar) in
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(itemDone)
        toolbar.options = options
        hideKeyboard()
    }
    //MARK:- Button Actions
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func publishBtn(_ sender: Any) {
        
        if self.editorView.html.count > 0 {
        
        var param:[String:Any]
        var url = ""
        if qoute{
            RichEditorDefaultOption.bold.action(toolbar)
            param = ["user_id":userId,"parent_id":parentId,"lft":"300", "rght":"300","topic":topicTitle,"content":editorView.html,"statuspost":statusPost]
            url = forumPostAddQoute
        }else if comment{
//            param = ["user_id":useridForComment,"parent_id":parentIdForComment,"lft":"300", "rght":"300","topic":"","content":editorView.html,"statuspost":statusPost]
            param = ["user_id":useridForComment,"parent_id":parentIdForComment,"topic":"","content":editorView.html,"statuspost":statusPost]
            url = forumPostAddComment
        }else {
            param = ["forum_post_id":forumId,"topic":topicTitle,"content":editorView.html]
            url = forumPostEdit
        }
        sendPost(url: url, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                if self.qoute{
                    print("Qoute has been added")
                }else if self.comment{
                    print("Comment has been added")
                }else {
                    print("Post has been edit")
                }
            }else {
                if self.qoute{
                    print("Qoute couldn't add")
                }else if self.comment {
                    print("comment couldn't add")
                }else {
                    print("Post couldn't edit")
                }
            }
            self.loader.isHidden = true
            self.navigationController?.popViewController(animated: true)
        }
        }
        else{
            showToast(message: "Field mandatory")
        }
    }
        
    //MARK:- Custom Funtions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
    func sendPost(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        loader.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loader.isHidden = true
            }
        }
    }
    
    
/*------------------------------------------------------------------------------------------------------------------------------------*/
    
}

//MARK:- Extensions
extension ForumPostEdit: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
        
}

extension ForumPostEdit: RichEditorToolbarDelegate {
        
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
}
