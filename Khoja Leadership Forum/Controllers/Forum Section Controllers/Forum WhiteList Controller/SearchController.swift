
import UIKit
import Alamofire
import SwiftGifOrigin

class SearchController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
   
    //MARK:- Btn Outlets
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var isRecordFound: UILabel!
    @IBOutlet weak var titleHeader: UILabel!
    
    //MARK:- Custom Variables
    var whiteListType = ""
    var Status = false
    var forumPostId = 0
    var ContactModel = [whiteList]()
    var groupModel = [WhiteListUserGroup]()
    var data = ["San Francisco","New York","San Jose","Chicago","Los Angeles","Austin","Seattle"]
    var filteredContact = [whiteList]()
    var filteredGroup = [WhiteListUserGroup]()
    var isSearching = false
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isRecordFound.isHidden = true
        headerHeight()
        searchBar.delegate = self
        tableView.register(UINib.init(nibName: "AddMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "AddMemberTableViewCell")
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        loadApi()
        hideKeyboard()
        searchBar.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        searchBar.returnKeyType = UIReturnKeyType.done
        if whiteListType == "contact" {
            titleHeader.text = "Contact WhiteList"
        }else{
            titleHeader.text = "Group WhiteList"
        }
   
    }
    
    //MARK:- Btn Actions
    @IBAction func searchBtn(_ sender: Any) {
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func done(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func addRemoveWhitelist(sender : UIButton) {
        //For Contact WhiteList
        if whiteListType == "contact" {
            var id = ""
            if isSearching {
                id = filteredContact[sender.tag].id ?? ""
            }else {
                id = ContactModel[sender.tag].id ?? ""
            }
            let param = ["user_id":id,"post_id":forumPostId] as [String:Any]
            if isSearching{
                searchBar.text = ""
                isSearching = false
            }
            if sender.isSelected {
                AddRemoveWhiteList(url: removeContact, param: param, method: responseType.post.rawValue, headers: nil,completion: { (status) in
                    if status{
                        self.ContactModel.removeAll()
                        self.getWhiteList(url: whiteListContact, method: responseType.get.rawValue, headers: nil) { (status) in
                            if status{
                                print("Contact list has been Reloaded")
                            }else {
                                print("Contact list couldn't Reload")
                            }
                            self.loaderView.isHidden = true
                        }
                        print("Contact has been removed in whiteList")
                    }else {
                        print("Contact couldn't remove in whiteList")
                    }
                })
                sender.isSelected = false
            }else{
                AddRemoveWhiteList(url: addInContact, param: param, method: responseType.post.rawValue, headers: nil,completion: { (status) in
                    if status{
                        self.ContactModel.removeAll()
                        self.getWhiteList(url: whiteListContact, method: responseType.get.rawValue, headers: nil) { (status) in
                        if status{
                            print("Contact list has been Reloaded")
                        }else {
                            print("Contact list couldn't Reload")
                        }
                        self.loaderView.isHidden = true
                        }
                        print("Contact has been added in whiteList")
                    }else {
                        self.loaderView.isHidden = true
                        print("Contact couldn't add in whiteList")
                    }
                })
                sender.isSelected = true
            }
        }
        //For Group WhiteList
        else {
            var id = ""
            if isSearching {
                id = filteredGroup[sender.tag].id ?? ""
            }else {
                id = groupModel[sender.tag].id ?? ""
            }
            let param = ["group_id":id,"post_id":forumPostId] as [String:Any]
            if isSearching{
                searchBar.text = ""
                isSearching = false
            }
            if sender.isSelected {
                AddRemoveWhiteList(url: removeGroup, param: param, method: responseType.post.rawValue, headers: nil,completion: { (status) in
                    if status{
                        self.groupModel.removeAll()
                        self.getWhiteList(url: whiteListGroup, method: responseType.get.rawValue, headers: nil) { (status) in
                            if status{
                                print("Group list has been Reloaded")
                            }else{
                                print("Group list couldn't Reload")
                            }
                            self.loaderView.isHidden = true
                        }
                        print("Group has been removed in whiteList")
                    }else {
                        print("Group couldn't remove in whiteList")
                    }
                })
                sender.isSelected = false
            }else{
                AddRemoveWhiteList(url: addInGroup, param: param, method: responseType.post.rawValue, headers: nil,completion: { (status) in
                    if status{
                        self.groupModel.removeAll()
                        self.getWhiteList(url: whiteListGroup, method: responseType.get.rawValue, headers: nil) { (status) in
                            if status{
                                print("Group list has been Reloaded")
                            }else{
                                print("Group list couldn't Reload")
                            }
                            self.loaderView.isHidden = true
                        }
                        print("Group has been added in whiteList")
                    }else {
                         print("Group couldn't add in whiteList")
                    }
                })
                sender.isSelected = true

            }
        }
    }
    
   //MARK:- Textfield delegate functions
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil  {
            
            isSearching = false
            isRecordFound.isHidden = true
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            tableView.reloadData()
        }else {
            isSearching = true
            let searchText  = textField.text
            if whiteListType == "contact" {
                filteredContact = ContactModel.filter{
                    $0.firstName!.localizedCaseInsensitiveContains(String(searchText!)) || $0.lastName!.localizedCaseInsensitiveContains(String(searchText!))
                }
            }else {
                filteredGroup = groupModel.filter{
                    $0.firstName!.localizedCaseInsensitiveContains(String(searchText!))
                }
            }
            if whiteListType == "contact" {
                if filteredContact.count == 0 {
                    isRecordFound.isHidden = false
                }else{
                    isRecordFound.isHidden = true
                }
            }else {
                if filteredGroup.count == 0 {
                    isRecordFound.isHidden = false
                }else {
                    isRecordFound.isHidden = true
                }
            }
            tableView.reloadData()
        }
        
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchBar {
            textField.resignFirstResponder()
        }
        return true
    }
    
   //MARK:- Custom Function
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func loadApi(){
        
        if whiteListType == "contact" {
            getWhiteList(url: whiteListContact, method: responseType.get.rawValue, headers: nil) { (status) in
                if status{
                    self.loaderView.isHidden = true
                    print("Contact list has been come")
                }else {
                    self.loaderView.isHidden = true
                    print("Contact list couldn't come")
                }
            }
        }else {
            getWhiteList(url: whiteListGroup, method: responseType.get.rawValue, headers: nil) { (status) in
                if status{
                    self.loaderView.isHidden = true
                    print("Group list has been come")
                }else{
                    self.loaderView.isHidden = true
                    print("Group list couldn't load")
                }
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func getWhiteList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        let param = ["post_id":forumPostId]
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "ok" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        if self.whiteListType == "contact" {
                            for i in data {
                                self.ContactModel.append(whiteList(data: [i]))
                            }
                            self.ContactModel = self.ContactModel.sorted(by: { ($0.firstName as String?) ?? "" < ($1.firstName as String?) ?? "" })
                        }else {
                            for i in data {
                                self.groupModel.append(WhiteListUserGroup(data: [i]))
                            }
                            self.groupModel = self.groupModel.sorted(by: { ($0.firstName as String?) ?? "" < ($1.firstName as String?) ?? "" })
                        }
                        self.tableView.reloadData()
                    }else {
                        self.tableView.reloadData()
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func AddRemoveWhiteList(url:String,param:[String:Any],method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    

    
    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            if whiteListType == "contact" {
                return filteredContact.count
            }
            return filteredGroup.count
        }
        if whiteListType == "contact"{
            return ContactModel.count
        }
        return groupModel.count
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddMemberTableViewCell") as! AddMemberTableViewCell
        //For when we Start Searching
        if isSearching{
            if whiteListType == "contact"{
                let firstname = filteredContact[indexPath.row].firstName
                let lastname = filteredContact[indexPath.row].lastName
                cell.lblMemberName.text = "\(firstname ?? "" ) \(lastname ?? "")"
//                cell.lblMemberDesignation.text = ""
//                cell.lblMemberCompany.text = ""
                let isAdded = filteredContact[indexPath.row].iswhitelisted
                if isAdded == 1 {
                    cell.checkBoxButtonOutlet.isSelected = true
                }else {
                    cell.checkBoxButtonOutlet.isSelected = false
                }
                cell.checkBoxButtonOutlet.tag = indexPath.row
                cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveWhitelist), for: .touchUpInside)
            }else {
                cell.lblMemberName.text = filteredGroup[indexPath.row].firstName
//                cell.lblMemberDesignation.text = ""
//                cell.lblMemberCompany.isHidden = true
                let isAdded = filteredGroup[indexPath.row].iswhitelisted
                if isAdded == 1 {
                    cell.checkBoxButtonOutlet.isSelected = true
                }else {
                    cell.checkBoxButtonOutlet.isSelected = false
                }
                cell.checkBoxButtonOutlet.tag = indexPath.row
                cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveWhitelist), for: .touchUpInside)
            }
                    
        }else {
            if whiteListType == "contact"{
                let firstname = ContactModel[indexPath.row].firstName
                let lastname = ContactModel[indexPath.row].lastName
                cell.lblMemberName.text = "\(firstname ?? "" ) \(lastname ?? "")"
//                cell.lblMemberDesignation.text = ""
//                cell.lblMemberCompany.text = ""
                let isAdded = ContactModel[indexPath.row].iswhitelisted
                if isAdded == 1 {
                    cell.checkBoxButtonOutlet.isSelected = true
                }else {
                    cell.checkBoxButtonOutlet.isSelected = false
                }
                cell.checkBoxButtonOutlet.tag = indexPath.row
                cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveWhitelist), for: .touchUpInside)
            }else {
                cell.lblMemberName.text = groupModel[indexPath.row].firstName
//                cell.lblMemberDesignation.text = ""
//                cell.lblMemberCompany.isHidden = true
                let isAdded = groupModel[indexPath.row].iswhitelisted
                if isAdded == 1 {
                    cell.checkBoxButtonOutlet.isSelected = true
                }else {
                    cell.checkBoxButtonOutlet.isSelected = false
                }
                cell.checkBoxButtonOutlet.tag = indexPath.row
                cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveWhitelist), for: .touchUpInside)
            }
        }
        return cell

    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * (view.frame.size.width / 320)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

}
