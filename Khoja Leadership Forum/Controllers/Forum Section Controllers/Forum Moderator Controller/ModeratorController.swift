
import UIKit
import Alamofire
import SwiftGifOrigin

class ModeratorController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var isRecordFound: UILabel!
    
    //MARK:- Custom Variables
    var Status = false
    var isSearching = false
    var forumPostId = 0
    var moderatorList = [ModeratorList]()
    var data = ["San Francisco","New York","San Jose","Chicago","Los Angeles","Austin","Seattle"]
    var filtered = [ModeratorList]()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isRecordFound.isHidden = true
        headerHeight()
        searchBar.delegate = self
        tableView.register(UINib.init(nibName: "AddMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "AddMemberTableViewCell")
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        loadApi()
        
        searchBar.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        hideKeyboard()
        searchBar.returnKeyType = UIReturnKeyType.done
        
    }
    
    //MARK:- Btn Actions
    @IBAction func searchBtn(_ sender: Any) {
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func done(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func addRemoveModeratorBtn(sender : UIButton) {
        var id = ""
        if isSearching{
            id = filtered[sender.tag].id ?? ""
        }else {
            id = moderatorList[sender.tag].id ?? ""
        }
        let param = ["post_id":forumPostId,"user_id":id] as [String : Any]
        if isSearching {
            searchBar.text = ""
            isSearching = false
        }
        if sender.isSelected{
            addRemoveModerator(url: removeModerator, param: param, method: responseType.post.rawValue, headers: nil) { (status) in
                if status{
                    self.moderatorList.removeAll()
                    self.getModeratorList(url: (moderatorListUrl + String(self.forumPostId)), method: responseType.get.rawValue, headers: nil) { (status) in
                        if status {
                            print("Moderator list has been reloaded")
                        }else {
                            print("Moderator list couldn't load")
                        }
                        self.loaderView.isHidden = true
                    }
                    print("Moaderator has been removed in list")
                }else {
                    print("Moderator couldn't remove from list")
                    self.loaderView.isHidden = true

                }
            }
            sender.isSelected = false
        }else{
            addRemoveModerator(url: addModerator, param: param, method: responseType.post.rawValue, headers: nil) { (status) in
                if status{
                    self.moderatorList.removeAll()
                    self.getModeratorList(url: (moderatorListUrl + String(self.forumPostId)), method: responseType.get.rawValue, headers: nil) { (status) in
                        if status {
                            print("Moderator list has been reloaded")
                        }else {
                            print("Moderator list couldn't load")
                        }
                        self.loaderView.isHidden = true
                    }
                    print("Moaderator has been added in list")
                }else {
                    print("Moderator couldn't add in list")
                    self.loaderView.isHidden = true
                }
            }
            sender.isSelected = true
        }
    }
    
    //MARK:- Text Field Methods
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil  {
            
            isSearching = false
            isRecordFound.isHidden = true
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            tableView.reloadData()
        }else {
            isSearching = true
            let searchText  = textField.text
            filtered = moderatorList.filter{
                $0.firstName!.localizedCaseInsensitiveContains(String(searchText!)) || $0.lastName!.localizedCaseInsensitiveContains(String(searchText!))
            }
            if filtered.count == 0 {
                isRecordFound.isHidden = false
            }else {
                isRecordFound.isHidden = true
            }
            tableView.reloadData()
        }
     
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchBar {
            textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func loadApi(){
        getModeratorList(url: (moderatorListUrl + String(forumPostId)), method: responseType.get.rawValue, headers: nil) { (status) in
            if status {
                print("Moderator list has been come")
            }else {
                print("Moderator list couldn't load")
            }
            self.loaderView.isHidden = true
        }
        
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func getModeratorList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: nil, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.moderatorList.append(ModeratorList(data: [i]))
                        }
                        
                        self.moderatorList = self.moderatorList.sorted(by: { ($0.firstName as String?) ?? "" < ($1.firstName as String?) ?? "" })
                        self.tableView.reloadData()
                    }else {
                        self.tableView.reloadData()
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func addRemoveModerator(url:String,param:[String:Any],method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    

    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filtered.count
        }
        return moderatorList.count
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddMemberTableViewCell") as! AddMemberTableViewCell
        if isSearching{
            let firstname = filtered[indexPath.row].firstName
            let lastname = filtered[indexPath.row].lastName
            cell.lblMemberName.text = "\(firstname ?? "") \(lastname ?? "")"
//            cell.lblMemberDesignation.isHidden = true
//            cell.lblMemberCompany.isHidden = true
            let isModerator = filtered[indexPath.row].isModerator
            if isModerator == 1 {
                cell.checkBoxButtonOutlet.isSelected = true
            }else {
                cell.checkBoxButtonOutlet.isSelected = false
            }
            cell.checkBoxButtonOutlet.tag = indexPath.row
            cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveModeratorBtn), for: .touchUpInside)
            
        }else {
            let firstname = moderatorList[indexPath.row].firstName
            let lastname = moderatorList[indexPath.row].lastName
            cell.lblMemberName.text = "\(firstname ?? "") \(lastname ?? "")"
//            cell.lblMemberDesignation.isHidden = true
//            cell.lblMemberCompany.isHidden = true
            let isModerator = moderatorList[indexPath.row].isModerator
            if isModerator == 1 {
                cell.checkBoxButtonOutlet.isSelected = true
            }else {
                cell.checkBoxButtonOutlet.isSelected = false
            }
            cell.checkBoxButtonOutlet.tag = indexPath.row
            cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveModeratorBtn), for: .touchUpInside)
        }
        
        return cell
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * (view.frame.size.width / 320)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

}
