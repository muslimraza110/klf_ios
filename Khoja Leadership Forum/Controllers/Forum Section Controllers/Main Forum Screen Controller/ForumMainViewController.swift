
import UIKit
import Alamofire
import SwiftGifOrigin

class ForumMainViewController: UIViewController {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImage: UIImageView!
    @IBOutlet weak var topicCategory: UILabel!
    @IBOutlet weak var isRecordFound: UILabel!
    @IBOutlet weak var deleteAlert: UIView!
    @IBOutlet weak var btnAddPost: UIButton!
    @IBOutlet weak var imgAddPost: UIImageView!
    
    //MARK:- Custom Variables
    var rowTappedValue:String = ""
    var Status :Bool = false
    var model = [MainScreenData]()
    var userRole = ""
    var forumId = ""
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        print("row tapped = \(rowTappedValue)")
        headerHeight()
        
    }
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {
     
        isRecordFound.isHidden = true
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        topicCategory.text = rowTappedValue + " Forum"
        
        if rowTappedValue == "Admin" {
            if userRole != "A" {
                btnAddPost.isHidden = true
                imgAddPost.isHidden = true
            }else {
                btnAddPost.isHidden = false
                imgAddPost.isHidden = false
            }
        }
        loaderView.isHidden = true
        deleteAlert.isHidden = true
        loaderImage.image = UIImage.gif(name: "loader_gif")
        adminData(url: forumMainScreenUrl, method: responseType.get.rawValue, header: headerToken, completion: { (status) in
            if status{
                print("Data fetched")
                if self.model.count == 0 {
                    self.isRecordFound.isHidden = false
                }else {
                    self.isRecordFound.isHidden = true
                }
                self.loaderView.isHidden = true
                
            }else {
                print("Data didn't fetch")
                self.loaderView.isHidden = true
            }
           
        })
    }

    //MARK:- Btn Actions
    @IBAction func addButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ForumAddTopic") as! ForumAddTopic
        vc.categoryReceived = rowTappedValue
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func hideDeleteAlertBtn(_ sender: Any) {
        deleteAlert.isHidden = true
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func deletePostBtn(_ sender:UIButton) {
        deleteAlert.isHidden = true
        loaderView.isHidden = false
        self.Status = false
        let param = ["subforum_id":forumId] as [String:Any]
        deleteData(url: deleteMainScreenPost, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.model.removeAll()
                self.adminData(url: forumMainScreenUrl, method: responseType.get.rawValue, header: headerToken, completion: { (status) in
                    if status{
                        if self.model.count == 0 {
                            self.isRecordFound.isHidden = false
                        }else {
                            self.isRecordFound.isHidden = true
                        }
                        self.loaderView.isHidden = true
                        print("Data updated")
                    }else {
                        self.loaderView.isHidden = true
                        print("Data didn't update")
                    }
                })
                print("Selected post has been deleted")
            }else {
                print("Selected post couldn't delete")
            }
            
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func deletePost(sender : UIButton) {
        forumId = model[sender.tag].subforumsID ?? ""
        deleteAlert.isHidden = false
    }
        
    //MARK:- Custom Function
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func adminData(url:String,method:HTTPMethod,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        model.removeAll()
        loaderView.isHidden = false
        let param = ["searchcatagory":rowTappedValue,"page_no":1,"pagesize":1000] as [String : Any]
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.model.append(MainScreenData(data: [i]))
                        }
                        self.tableView.reloadData()
                    }else {
                        self.tableView.reloadData()
                    }
                }else {
                    if self.model.count == 0 {
                        self.isRecordFound.isHidden = false
                    }else {
                        self.isRecordFound.isHidden = true
                    }
                    self.tableView.reloadData()
                    self.loaderView.isHidden = true
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func deleteData(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
            if status{
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    

/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
}

//MARK:- Extensions
extension ForumMainViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forumAdminCell") as! forumAdminCell
        cell.decription.text = model[indexPath.row].subforumsTitle
        if rowTappedValue == "Admin" {
            if userRole != "A" {
                cell.deleteBtn.isHidden = true
                cell.deleteImg.isHidden = true
            }else {
                cell.deleteBtn.isHidden = false
                cell.deleteImg.isHidden = false
                cell.deleteBtn.tag = indexPath.row
                cell.deleteBtn.addTarget(self, action: #selector(self.deletePost), for: .touchUpInside)
            }
        }else {
            if userRole == "A" {
                cell.deleteBtn.isHidden = false
                cell.deleteImg.isHidden = false
                cell.deleteBtn.tag = indexPath.row
                cell.deleteBtn.addTarget(self, action: #selector(self.deletePost), for: .touchUpInside)
            }else {
                cell.deleteBtn.isHidden = true
                cell.deleteImg.isHidden = true
            }
        }
        
        return cell
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return (45 * (self.view.frame.width/320))

        }else {
            return 80
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SubTopicsDetail") as! SubTopicsDetail
        vc.forumId = Int(model[indexPath.row].subforumsID!)
        vc.ScreenTitle = model[indexPath.row].subforumsTitle
        vc.rowTappedValue = rowTappedValue
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
}


extension UIViewController{
    
    func hideKeyboard() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Dissmisskeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func Dissmisskeyboard() {
        view.endEditing(true)
    }
    
}
