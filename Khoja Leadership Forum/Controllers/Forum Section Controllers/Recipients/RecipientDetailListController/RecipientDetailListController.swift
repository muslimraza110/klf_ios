
import UIKit

class RecipientDetailListController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var isRecordFound: UILabel!
    @IBOutlet weak var lblTitle: NavigationLabel!
    
    //MARK:- Custom Variables
    var isSearching = false
    var contactListName = ""
    var selectedIndex = [String]()
    var userId = [String]()
    var selectedEmail = [String]()
    var completeEmail = [String]()
    var contactDetail = NSDictionary()
    var recipientDetailListModel = [RecipientDetailModel]()
    var filtered = [RecipientDetailModel]()

    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib.init(nibName: "AddMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "AddMemberTableViewCell")

        if UserDefaults.standard.value(forKey: "SelectedEmail") as? [String] != nil {
            
        }else {
            UserDefaults.standard.set(completeEmail, forKey: "SelectedEmail")
        }
        
        if UserDefaults.standard.value(forKey: "SelectedUser") as? [String] != nil {
            let data = UserDefaults.standard.value(forKey: "SelectedUser") as! [String]
            selectedIndex = data
        }else {
            UserDefaults.standard.set(selectedIndex, forKey: "SelectedUser")
        }

        isRecordFound.isHidden = true
        lblTitle.text = contactListName
        
        headerHeight()
        hideKeyboard()
        loadApi()
        
        searchBar.delegate = self
        searchBar.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        searchBar.returnKeyType = UIReturnKeyType.done
        
    }
    
    //MARK:- Btn Actions
    @IBAction func searchBtn(_ sender: Any) {
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func done(_ sender: Any) {

        //Checking for searchfield is active or not
//        if isSearching {
//            for i in selectedIndex {
//                selectedEmail.append(filtered[i].email ?? "No Email")
//            }
//        }else {
//            for i in selectedIndex {
//                selectedEmail.append(recipientDetailListModel[i].email ?? "No Email")
//            }
//        }
//
//        //data appending for userdefault array
//        for i in selectedEmail {
//            completeEmail.append(i)
//        }
//
//        if UserDefaults.standard.value(forKey: "SelectedEmail") as? [String] != nil {
//            var data = UserDefaults.standard.value(forKey: "SelectedEmail") as! [String]
//            for i in completeEmail {
//                if data.contains(i) {
//
//                }
//                else {
//                    data.append(i)
//                }
//            }
//            completeEmail.removeAll()
//            for i in data {
//                completeEmail.append(i)
//            }
//            UserDefaults.standard.set(completeEmail, forKey: "SelectedEmail")
//        }
        
        if isSearching {
            for i in selectedIndex{
                for a in 0 ..< filtered.count {
                    if i == filtered[a].userID {
                        selectedEmail.append(filtered[a].email ?? "No Email")
                    }
                }
            }
        }else {
            for i in selectedIndex{
                for a in 0 ..< recipientDetailListModel.count {
                    if i == recipientDetailListModel[a].userID {
                        if !selectedEmail.contains(recipientDetailListModel[a].email ?? "No Email"){
                             selectedEmail.append(recipientDetailListModel[a].email ?? "No Email")
                        }
                       
                    }
                }
            }
        }
        
        
        for i in selectedEmail {
            completeEmail.append(i)
        }
        
        if UserDefaults.standard.value(forKey: "SelectedUser") as? [String] != nil {
            var data = UserDefaults.standard.value(forKey: "SelectedUser") as! [String]
            for i in selectedIndex {
                if data.contains(i) {
                    
                }
                else {
                    data.append(i)
                }
            }
            UserDefaults.standard.set(selectedIndex, forKey: "SelectedUser")
            let data1 = UserDefaults.standard.value(forKey: "SelectedUser") as! [String]
            selectedIndex.removeAll()
            for i in data1 {
                selectedIndex.append(i)
            }
            UserDefaults.standard.set(selectedIndex, forKey: "SelectedUser")
        }

        if UserDefaults.standard.value(forKey: "SelectedEmail") as? [String] != nil {
            var data = UserDefaults.standard.value(forKey: "SelectedEmail") as! [String]
            for i in completeEmail {
                if data.contains(i) {

                }
                else {
                    data.append(i)
                }
            }
            UserDefaults.standard.set(completeEmail, forKey: "SelectedEmail")
            let data1 = UserDefaults.standard.value(forKey: "SelectedEmail") as! [String]
            completeEmail.removeAll()
            for i in data1 {
                completeEmail.append(i)
            }
            UserDefaults.standard.set(completeEmail, forKey: "SelectedEmail")
        }

        //Go to specific controller
        for controller in self.navigationController!.viewControllers as Array {
            if controller is EblastController  {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }

    }
    
/*---------------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func addRemoveMemberBtn(sender : UIButton) {
        if sender.isSelected{
//           for i in selectedIndex
//            {
//                if i == sender.tag {
//                    selectedIndex.remove(at: selectedIndex.firstIndex(of: sender.tag)!)
//                }
//            }
            
            for i in selectedIndex
              {
                if isSearching{
                    if i == filtered[sender.tag].userID! {
                        selectedIndex.remove(at: selectedIndex.firstIndex(of: filtered[sender.tag].userID!)!)
                    }
                }else {
                    if i == recipientDetailListModel[sender.tag].userID! {
                        selectedIndex.remove(at: selectedIndex.firstIndex(of: recipientDetailListModel[sender.tag].userID!)!)
                    }
                }
                
              }
            
            sender.isSelected = false
            tableView.reloadData()
        }else{
//            selectedIndex.append(sender.tag)
            if isSearching {
                selectedIndex.append(filtered[sender.tag].userID!)
            }else {
                selectedIndex.append(recipientDetailListModel[sender.tag].userID!)
            }
            sender.isSelected = true
            tableView.reloadData()
        }
    }
    
    //MARK:- TextField Delegate Function
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchBar {
           textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK:- Custom Functions
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil  {
            
            isSearching = false
            isRecordFound.isHidden = true
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            tableView.reloadData()
        }else {
            isSearching = true
            let searchText  = textField.text
            filtered = recipientDetailListModel.filter{
                $0.firstName!.localizedCaseInsensitiveContains(String(searchText!)) || $0.lastName!.localizedCaseInsensitiveContains(String(searchText!))
            }
            if filtered.count == 0 {
                isRecordFound.isHidden = false
            }else {
                isRecordFound.isHidden = true
            }
            tableView.reloadData()
        }
        
    }
        
/*---------------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*---------------------------------------------------------------------------------------------------------------------------------------*/
    
    func loadApi(){
        recipientDetailListModel.removeAll()
        let data = contactDetail[contactListName] as! [[String:Any]]
        for i in data {
            recipientDetailListModel.append(RecipientDetailModel(data: [i]))
        }
        recipientDetailListModel = recipientDetailListModel.sorted(by: { ($0.firstName as String?) ?? "" < ($1.firstName as String?) ?? "" })
        tableView.reloadData()
    }
    
    

    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filtered.count
        }
        return recipientDetailListModel.count
    }
    
/*---------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddMemberTableViewCell") as! AddMemberTableViewCell
        if isSearching{
            let firstname = filtered[indexPath.row].firstName
            let lastname = filtered[indexPath.row].lastName
            if selectedIndex.contains(filtered[indexPath.row].userID!) {
                cell.checkBoxButtonOutlet.isSelected = true
            }else {
                cell.checkBoxButtonOutlet.isSelected = false
            }
            cell.lblMemberName.text = "\(firstname ?? "") \(lastname ?? "")"
            cell.checkBoxButtonOutlet.tag = indexPath.row
            cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveMemberBtn), for: .touchUpInside)
            
        }else {
            let firstname = recipientDetailListModel[indexPath.row].firstName
            let lastname = recipientDetailListModel[indexPath.row].lastName
            if selectedIndex.contains(recipientDetailListModel[indexPath.row].userID!) {
                cell.checkBoxButtonOutlet.isSelected = true
            }else {
                cell.checkBoxButtonOutlet.isSelected = false
            }
            
            cell.lblMemberName.text = "\(firstname ?? "") \(lastname ?? "")"
            cell.checkBoxButtonOutlet.tag = indexPath.row
            cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveMemberBtn), for: .touchUpInside)
        }
        
        cell.layer.cornerRadius = 10
        
        return cell
    }
    
/*---------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * (view.frame.size.width / 320)
    }
    

}
