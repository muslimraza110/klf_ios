

import UIKit
import Alamofire
import SwiftGifOrigin

class RecipientsController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var isRecipientDataFound: UILabel!
    
    //MARK:- Custom Variables
    var Status = false
    var listOfRecipient = [String]()
    var detailOfRecipient = [NSDictionary]()

    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
       
        isRecipientDataFound.isHidden = true
        headerHeight()
        registerTableViewCell()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        
        eblastData(url: recipientListUrl, method: responseType.get.rawValue, header: headerToken) { (status) in
            if status{
                print("data Loaded")
                self.tableView.reloadData()
            }else{
                self.isRecipientDataFound.isHidden = false
                print("data couldn't load")
            }
            self.loaderView.isHidden = true
            
        }
    }
    
    //MARK:- Btn Actions
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func eblastData(url:String,method:HTTPMethod,header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: nil ,method: method, header: header) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? NSDictionary {
                        for i in data {
                            self.listOfRecipient.append(i.key as! String)
                            self.detailOfRecipient.append([i.key:i.value])
                        }
                    }else {
                        print("Json data couldn't load")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                completion(false)
                self.loaderView.isHidden = true
            }
        }
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func registerTableViewCell ()
    {
        self.tableView.register(UINib(nibName: "MainRecipientTableViewCell", bundle: nil), forCellReuseIdentifier: "MainRecipientTableViewCell")
    }
    
   
    
    //MARK:- TableView Delegate Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfRecipient.count
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainRecipientTableViewCell") as! MainRecipientTableViewCell
        cell.lblRecipient.text = listOfRecipient[indexPath.row]
        cell.layer.cornerRadius = 10
        return cell
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecipientDetailViewController") as! RecipientDetailViewController
        vc.detailList = detailOfRecipient[indexPath.row]
        vc.key = listOfRecipient[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*--------------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
        
    
}
