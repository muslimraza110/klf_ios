
import UIKit


class RecipientDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    //MARK:- Btn Outlets
    @IBOutlet weak var tableViewRDetail: UITableView!
    @IBOutlet weak var header: NSLayoutConstraint!
    
    //MARK:- Custom Functions
    var detailList = NSDictionary()
    var key = ""
    var listOfSubRecipient = [String]()
    var detailfSubRecipient = [NSDictionary]()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        headerHeight()
        registerTableViewCell()
        let vc = detailList[key] as! NSDictionary
        for i in vc {
            listOfSubRecipient.append(i.key as! String)
            detailfSubRecipient.append([i.key:i.value])
        }
        tableViewRDetail.reloadData()
    }
    
    //MARK:- Btn Actions
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func registerTableViewCell ()
    {
        self.tableViewRDetail.register(UINib(nibName: "MainRecipientTableViewCell", bundle: nil), forCellReuseIdentifier: "MainRecipientTableViewCell")
    }
    
    //MARK:- TableView Delegate Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfSubRecipient.count
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainRecipientTableViewCell") as! MainRecipientTableViewCell
        cell.lblRecipient.text = listOfSubRecipient[indexPath.row]
        return cell
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecipientDetailListController") as! RecipientDetailListController
        vc.contactDetail = detailfSubRecipient[indexPath.row]
        vc.contactListName = listOfSubRecipient[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
