
import UIKit
import RichEditorView
import Alamofire
import SwiftGifOrigin

class AddSubTopicPost: UIViewController {

    //MARK:- Btn Outlet
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var editorView: RichEditorView!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var pinnedBtn: UIButton!
    @IBOutlet weak var dashBoardBtn: UIButton!
    @IBOutlet weak var sendEBlast: UIButton!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImage: UIImageView!
    @IBOutlet weak var pinnedView: UIView!
    @IBOutlet weak var titleSecondConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleFirstConstraint: NSLayoutConstraint!
    @IBOutlet weak var publishBtnSecondConstraint: NSLayoutConstraint!
    @IBOutlet weak var publishBtnFirstConstrain: NSLayoutConstraint!
    @IBOutlet weak var visibilityBox: UIView!
    @IBOutlet weak var moderatorBox: UIView!
    
    //MARK:- Custom Variables
    var userId = 0
    var pinned = 0
    var sos = 0
    var eblast = 0
    var postAprove = 0
    var subform_id:Int!
    var userRole = ""
    var Status = false
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.translatesAutoresizingMaskIntoConstraints = true
  
        headerHeight()
        loaderImage.image = UIImage.gif(name: "loader_gif")
        loaderView.isHidden = true
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Description"
        toolbar.delegate = self
        toolbar.editor = editorView
        hideKeyboard()
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let itemDone = RichEditorOptionItem(image: nil, title: "Done") { (toolBar) in
            self.view.endEditing(true)
        }
        var options = toolbar.options
        options.append(item)
        options.append(itemDone)
        toolbar.options = options
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        userRole = userData["role"] as! String
  
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewWillLayoutSubviews() {
        titleFirstConstraint.isActive = true
        titleSecondConstraint.isActive = false
        if userRole != "A" {
            titleSecondConstraint.isActive = true
            titleFirstConstraint.isActive = false
            publishBtnFirstConstrain.isActive = false
            publishBtnSecondConstraint.isActive = true
            pinnedView.isHidden = true
            visibilityBox.isHidden = true
            moderatorBox.isHidden = true
        }else {
            titleFirstConstraint.isActive = true
            titleSecondConstraint.isActive = false
            publishBtnFirstConstrain.isActive = true
            publishBtnSecondConstraint.isActive = false
            pinnedView.isHidden = false
            visibilityBox.isHidden = false
            moderatorBox.isHidden = false

        }
    }
    
    //MARK:- Btn Actions
    @IBAction func pinnedBtn(_ sender: UIButton) {
        if sender.isSelected {
            pinned = 0
            sender.isSelected = false
        }else {
            pinned = 1
            sender.isSelected = true
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func moderatorApproveBtn(_ sender: UIButton) {
        if sender.isSelected{
            postAprove = 0
            sender.isSelected = false
        }else {
            postAprove = 1
            sender.isSelected = true
        }
        
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func dashBoardBtn(_ sender: UIButton) {
        if sender.isSelected {
            sos = 0
            sender.isSelected = false
        }else {
            sos = 1
            sender.isSelected = true
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func sendEBlast(_ sender: UIButton) {
        if sender.isSelected {
            eblast = 0
            sender.isSelected = false
        }else {
            eblast = 1
            sender.isSelected = true
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func publishBtn(_ sender: Any) {
        loaderView.isHidden = false
        RichEditorDefaultOption.header(3).action(toolbar)
        AddPost(url:AddSubTopicUrl, method: responseType.post.rawValue, header: headerToken) { (status) in
            if status{
                self.loaderView.isHidden = true
                self.navigationController?.popViewController(animated: true)
                print("Subpost added")
            }else {
                self.loaderView.isHidden = true
                self.navigationController?.popViewController(animated: true)
                print("Subpost didn't add")
            }
            
        }
    }
        
    //MARK:- Custom Fundtion
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func AddPost(url:String,method:HTTPMethod,header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        if subform_id != nil && titleField.text != "" && editorView.html != "" {
            let param = ["user_id":userId,"subforum_id":subform_id!,"topic":titleField.text!,"content":editorView.html,"pinned":pinned,"sod":sos,"eblast":eblast,"post_visibility":"P","post_approved_post":postAprove] as [String:Any]
            Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status) in
                if status {
                    self.editorView.html = ""
                    self.titleField.text = ""
                    self.Status = false
                    let JSON = JSON
                    let status = JSON["status"] as? String
                    if status == "success" {
                        self.Status = true
                    }
                    completion(self.Status)
                }else {
                    self.loaderView.isHidden = true
                }
        }
        }else {
            loaderView.isHidden = true
            print("Enter the data in field or description view")
        }

    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
}

//MARK:- Extensions
extension AddSubTopicPost: RichEditorDelegate {
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
        
}

extension AddSubTopicPost: RichEditorToolbarDelegate {
        
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    
}
