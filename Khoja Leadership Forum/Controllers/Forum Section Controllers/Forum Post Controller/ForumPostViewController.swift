
import UIKit

class ForumPostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Btn Outlet
    @IBOutlet weak var lblSubTopicTitle: UILabel!
    @IBOutlet weak var forumPostTableView: UITableView!
    
    //MARK:- Life Cycle Delegate
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerTableViewCell()
        
        self.forumPostTableView.delegate = self
        self.forumPostTableView.dataSource = self
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back_button.png")?.withRenderingMode(.alwaysOriginal)
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back_button.png")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        let nav = self.navigationController?.navigationBar
        nav?.barTintColor =  UIColor.lightText
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo.png")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    //MARK:- Btn Actions
    @IBAction func replyButtonPressed(_ sender: UIButton) {
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func subscribeButtonPressed(_ sender: UIButton) {
    }
    
    //MARK:- Custom Functions
    func registerTableViewCell ()
    {
        self.forumPostTableView.register(UINib(nibName: "ForumPostTableViewCell", bundle: nil), forCellReuseIdentifier: "ForumPostCell")
    }
    
    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.forumPostTableView.deselectRow(at: indexPath, animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 16
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.forumPostTableView.dequeueReusableCell(withIdentifier: "ForumPostCell", for: indexPath) as! ForumPostTableViewCell
        return cell
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
}

