
import UIKit
class EventsTableViewCell : UITableViewCell {
    
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblDecription: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var editImg: UIImageView!
    
}
