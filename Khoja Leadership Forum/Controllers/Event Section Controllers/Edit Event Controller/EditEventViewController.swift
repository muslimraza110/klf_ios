
import UIKit
import Alamofire
import SwiftGifOrigin
import RichEditorView
import FSCalendar


class EditEventViewController: UIViewController, UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,FSCalendarDelegate,FSCalendarDataSource {

    //MARK:- Btn Outlets
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var txtFieldName: TextFieldPadding!
    @IBOutlet weak var txtFieldBusinessCharities: TextFieldPadding!
    @IBOutlet weak var txtFieldProject: TextFieldPadding!
    @IBOutlet weak var txtFieldCity: TextFieldPadding!
    @IBOutlet weak var txtFieldCountry: TextFieldPadding!
    @IBOutlet weak var txtFieldStartDate: TextFieldPadding!
    @IBOutlet weak var txtFieldEndDate: TextFieldPadding!
    @IBOutlet weak var txtFieldStartTime: TextFieldPadding!
    @IBOutlet weak var txtFieldEndTime: TextFieldPadding!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calanderView: UIView!
    @IBOutlet weak var calander: FSCalendar!
    @IBOutlet weak var tableBox: UIView!
    @IBOutlet weak var titleTable: UILabel!
    @IBOutlet weak var btnGroup: AdaptiveButton!
    @IBOutlet weak var btnProject: AdaptiveButton!
    @IBOutlet weak var isRecordFound: UILabel!
    @IBOutlet weak var projectDropDownOutlet: UIButton!
    
    //MARK:- Custom Variables
    var startDateFlag = false
    var nameFlag = false
    var cityFlag = false
    var countryFlag = false
    var bncFlag = false
    var endDateFlag = false
    var startTimeFlag = false
    var endTimeFlag = false
    let datePickerStartTime = UIDatePicker()
    let datePickerEndTime = UIDatePicker()
    var userGroupModel = [UserGroupsModel]()
    var userProjectModel = [UserProjectModel]()
    var selectedUserProjectModel = [UserProjectModel]()
    var eventList = EventListModel()
    var Status = false
    var isStarDateCalanderOpen = false
    var isbusinessDropDown = true
    var groupId = ""
    var projectId = "0"
    var userId = 0
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isRecordFound.isHidden = true
        registerTableCell()
        calanderView.isHidden = true
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        headerHeight()
        tableBox.isHidden = true
        
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Description"
        
        
        
        toolbar.delegate = self
        toolbar.editor = editorView
        hideKeyboard()
        
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let itemDone = RichEditorOptionItem(image: nil, title: "Done") { (toolBar) in
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(itemDone)
        toolbar.options = options
        
        //TextFields Delegate
        self.txtFieldName.delegate = self
        self.txtFieldCity.delegate = self
        self.txtFieldCountry.delegate = self
        self.txtFieldEndDate.delegate = self
        self.txtFieldEndTime.delegate = self
        self.txtFieldProject.delegate = self
        self.txtFieldStartDate.delegate = self
        self.txtFieldStartTime.delegate = self
        self.txtFieldBusinessCharities.delegate = self
        
        self.txtFieldBusinessCharities.rightViewMode = .always
        let imageViewForGroups = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldBusinessCharities.contentMode = .scaleAspectFit
        imageViewForGroups.image = UIImage(named: "dropdown.png")
        let rightViewForGroups = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForGroups.addSubview(imageViewForGroups)
        self.txtFieldBusinessCharities.rightView = rightViewForGroups
        
        self.txtFieldProject.rightViewMode = .always
        let imageViewForProjects = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldProject.contentMode = .scaleAspectFit
        imageViewForProjects.image = UIImage(named: "dropdown.png")
        let rightViewForProjects = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForProjects.addSubview(imageViewForProjects)
        self.txtFieldProject.rightView = rightViewForProjects
        
        
        showDatePicker()
        loginDetail()
        stringToDate()

        
    }
    
    //MARK:- Custom Functions
    func stringToDate() {
        
        let startDate = eventList.startDate!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let sDate = dateFormatter.date(from: startDate)!
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        txtFieldStartDate.text = formatter.string(from: sDate)
        
        formatter.dateFormat = "HH:mm:ss"
        txtFieldStartTime.text = formatter.string(from: sDate)
        
        if eventList.endDate != "" && eventList.endDate != nil {
            let endDate = eventList.endDate!
            let endDateFormatter = DateFormatter()
            endDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            endDateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let eDate = endDateFormatter.date(from: endDate)!
            let endformatter = DateFormatter()
            endformatter.dateFormat = "yyyy-MM-dd"
            endformatter.locale = Locale(identifier: "en_US_POSIX")
            txtFieldEndDate.text = endformatter.string(from: eDate)
            
            endformatter.dateFormat = "HH:mm:ss"
            txtFieldEndTime.text = endformatter.string(from: eDate)
        }else {
             txtFieldEndDate.text = ""
             txtFieldEndTime.text = ""
        }

        self.projectDropDownOutlet.isUserInteractionEnabled = false
        txtFieldCountry.text = eventList.country
        txtFieldCity.text = eventList.city
        txtFieldName.text = eventList.name
        editorView.html = eventList.datumDescription ?? ""
        groupId = eventList.groupID ?? ""
        projectId = eventList.projectID ?? ""
        
        for i in 0 ..< userGroupModel.count {
            if eventList.groupID == userGroupModel[i].groupsID {
                txtFieldBusinessCharities.text = userGroupModel[i].groupsName
                break
            }
        }
        
        for i in 0 ..< userProjectModel.count {
            if eventList.projectID == userProjectModel[i].projectsID {
                txtFieldProject.text = userProjectModel[i].projectsName
                break
            }
        }
 
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func loginDetail(){
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        
        let groupData = UserDefaults.standard.object(forKey: "groups") as? NSData
        if groupData != nil {
            let userGroup = NSKeyedUnarchiver.unarchiveObject(with: groupData! as Data) as? [[String:Any]]
            if let group = userGroup {
                for i in group {
                    userGroupModel.append(UserGroupsModel(data: [i]))
                }
            }
        }
      
        let loginProjectData = UserDefaults.standard.object(forKey: "LoginUserProjects") as? NSData
        if loginProjectData != nil {
            let userProject = NSKeyedUnarchiver.unarchiveObject(with: loginProjectData! as Data) as? [[String:Any]]
            if let project = userProject {
                for i in project {
                    userProjectModel.append(UserProjectModel(data: [i]))
                }
            }
        }

        tableView.reloadData()
        
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func textFieldFormatCheck() -> Bool {
        
        if self.txtFieldName.text?.count == 0 {
            self.txtFieldName.borderColor = .red
            nameFlag = false
        }
        else{
            nameFlag = true
        }
        
        if self.txtFieldBusinessCharities.text?.count == 0 {
            self.txtFieldBusinessCharities.borderColor = .red
            bncFlag = false
        }
        else{
            bncFlag = true
        }
        
        if self.txtFieldCity.text?.count == 0 {
            self.txtFieldCity.borderColor = .red
            cityFlag = false
        }
        else{
            cityFlag = true
        }
        
        if self.txtFieldCountry.text?.count == 0 {
            self.txtFieldCountry.borderColor = .red
            countryFlag = false
        }
        else{
            countryFlag = true
        }
        
        if self.txtFieldStartDate.text?.count == 0 {
            self.txtFieldStartDate.borderColor = .red
            startDateFlag = false
        }
        else{
            startDateFlag = true
        }
        
        if self.txtFieldEndDate.text?.count == 0 {
            self.txtFieldEndDate.borderColor = .red
            endDateFlag = false
        }
        else{
            endDateFlag = true
        }
        
        if self.txtFieldStartTime.text?.count == 0 {
            self.txtFieldStartTime.borderColor = .red
            startTimeFlag = false
        }
        else{
            startTimeFlag = true
        }
        
        if self.txtFieldEndTime.text?.count == 0 {
            self.txtFieldEndTime.borderColor = .red
            endTimeFlag = false
        }
        else{
            endTimeFlag = true
        }
                
        if nameFlag && bncFlag && cityFlag && countryFlag && startDateFlag && endDateFlag && startTimeFlag && endTimeFlag == true {return true}
        else{return false}
        
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtFieldName {
            
            self.txtFieldName.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else if textField == self.txtFieldCity {
            
            self.txtFieldCity.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else if textField == self.txtFieldCountry {
            
            self.txtFieldCountry.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else{
            if textField == self.txtFieldStartDate{
                let maxLength = 100
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
            else {
                return true
                
            }
        }

    }

/*-----------------------------------------------------------------------------------------------------*/

    func registerTableCell(){
        
        tableView.register(UINib(nibName: "GroupProjectDataTableViewCell", bundle: nil), forCellReuseIdentifier: "GroupProjectDataTableViewCell")
        
    }
/*-----------------------------------------------------------------------------------------------------*/

    func showDatePicker(){
        //Formate Date
        datePickerStartTime.datePickerMode = .time
        datePickerEndTime.datePickerMode = .time
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtFieldStartTime.inputAccessoryView = toolbar
        txtFieldStartTime.inputView = datePickerStartTime
        
        txtFieldEndTime.inputAccessoryView = toolbar
        txtFieldEndTime.inputView = datePickerEndTime
    
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss"
        if txtFieldStartTime.isFirstResponder{
            self.txtFieldStartTime.borderColor = .darkGray
            txtFieldStartTime.text = formatter.string(from: datePickerStartTime.date)
        }else if txtFieldEndTime.isFirstResponder {
            self.txtFieldEndTime.borderColor = .darkGray
            txtFieldEndTime.text = formatter.string(from: datePickerEndTime.date)
        }
        self.view.endEditing(true)
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    func editEventApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
    //MARK:- Btn Actions
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func saveBtn(_ sender: Any) {
        if textFieldFormatCheck(){
            let param = ["group_id":groupId,"user_id":eventList.userID!,"project_id":projectId,"name":txtFieldName.text!, "start_date":"\(txtFieldStartDate.text ?? "") \(txtFieldStartTime.text ?? "")", "end_date":"\(txtFieldEndDate.text ?? "") \(txtFieldEndTime.text ?? "")","all_day":eventList.allDay!,"description":editorView.html,"city":txtFieldCity.text!, "country":txtFieldCountry.text!,"event_id":eventList.id!] as [String:Any]
            
            editEventApi(url: editEventUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print("Event has been edited")
                    self.navigationController?.popViewController(animated: true)
                }else {
                    self.showToast(message: "Event couldn't edit try again later")
                    print("event couldn't edit")
                }
                self.loaderView.isHidden = true
            }
        }else{
            showToast(message: "Few fields are mandatory")
            print("one or more fields are empty.")
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    @IBAction func deleteBtn(_ sender: Any) {
        
        let param = ["event_id":eventList.id!] as [String:Any]
        editEventApi(url: deleteEventUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.navigationController?.popViewController(animated: true)
                print("Deleted")
            }else {
                print("couldn't delete")
            }
            self.loaderView.isHidden = true
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func showStartDateCalanderBtn(_ sender: Any) {
        isStarDateCalanderOpen = true
        calanderView.isHidden = false
        
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    @IBAction func showEndDateCalanderBtn(_ sender: Any) {
        isStarDateCalanderOpen = false
        calanderView.isHidden = false
        
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func businessDropDownBtn(_ sender: Any) {
        isbusinessDropDown = true
        titleTable.text = "Groups"
        tableView.reloadData()
        tableBox.isHidden = false
        if userGroupModel.count == 0 {
            isRecordFound.isHidden = false
        }else {
            isRecordFound.isHidden = true
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    @IBAction func projectDorpDownBtn(_ sender: Any) {
        isbusinessDropDown = false
        titleTable.text = "Projects"
        tableView.reloadData()
        tableBox.isHidden = false
        if selectedUserProjectModel.count == 0 {
            isRecordFound.isHidden = false
        }else {
            isRecordFound.isHidden = true
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    @IBAction func hideTableBox(_ sender: Any) {
        tableBox.isHidden = true
    }
    
/*-----------------------------------------------------------------------------------------------------*/

    //MARK:- Tableview Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isbusinessDropDown {
            return userGroupModel.count
        }
        return selectedUserProjectModel.count
    }
/*-----------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupProjectDataTableViewCell") as! GroupProjectDataTableViewCell
        if isbusinessDropDown {
            cell.name.text = userGroupModel[indexPath.row].groupsName
        }else {
            cell.name.text = selectedUserProjectModel[indexPath.row].projectsName
        }
        return cell
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isbusinessDropDown {
            selectedUserProjectModel.removeAll()
            groupId = userGroupModel[indexPath.row].groupID ?? ""
            self.txtFieldBusinessCharities.borderColor = .darkGray
            txtFieldBusinessCharities.text = userGroupModel[indexPath.row].groupsName
            txtFieldProject.text = ""
            projectId = "0"
            
            self.projectDropDownOutlet.isUserInteractionEnabled = true
            
            for i in userProjectModel {
                if i.groupID == groupId {
                    selectedUserProjectModel.append(i)
                }
            }
            tableView.reloadData()
            
        }else {
            self.txtFieldProject.borderColor = .darkGray
            projectId = selectedUserProjectModel[indexPath.row].projectID ?? ""
            txtFieldProject.text = selectedUserProjectModel[indexPath.row].projectsName
        }
        tableBox.isHidden = true
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 50
        }else {
             return 70
        }
    }
    
    //MARK:- FSCalander Delegate Methods
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        if isStarDateCalanderOpen {
            self.txtFieldStartDate.borderColor = .darkGray
            txtFieldStartDate.text = self.formatter.string(from: date)
        }else {
            self.txtFieldEndDate.borderColor = .darkGray
            txtFieldEndDate.text = self.formatter.string(from: date)
        }
        calanderView.isHidden = true
        
    }
    
/*-----------------------------------------------------------------------------------------------------*/

}

/*-----------------------------------------------------------------------------------------------------*/
//MARK:- Extensions
extension EditEventViewController: RichEditorDelegate {
    
/*-----------------------------------------------------------------------------------------------------*/
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
}

extension EditEventViewController: RichEditorToolbarDelegate {
    
/*-----------------------------------------------------------------------------------------------------*/
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
