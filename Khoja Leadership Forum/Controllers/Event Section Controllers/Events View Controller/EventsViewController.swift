
import UIKit
import Alamofire
import SwiftGifOrigin
import FSCalendar
import WebKit

class EventsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,FSCalendarDelegate,FSCalendarDataSource {
   
    //MARK:- Btn Outlets
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var calander: FSCalendar!
    @IBOutlet weak var popUpTableView: UITableView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var monthlyButton: UIButton!
    @IBOutlet weak var weeklyButton: UIButton!
    @IBOutlet weak var calanderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cellDetailPopUp: UIView!
    @IBOutlet weak var lblPopUpTitle: ContentLabel!
    @IBOutlet weak var lblTimePopUp: ContentLabel!
    @IBOutlet weak var lblCalanderPopUp: BlueViewLabel!
    @IBOutlet weak var lblLoactionPopUp: ContentLabel!
    @IBOutlet weak var btnAddEvent: UIButton!
    @IBOutlet weak var isRecordFoundView: UIScrollView!
    @IBOutlet weak var lblIsRecordFound: UILabel!
    @IBOutlet weak var webViewDescriptionPopUp: WKWebView!
    
    //MARK:- Custom Variables
    var Status = false
    var triggerOfflineMode = false
    var userId = 0
    var userRole = ""
    var eventDate = [String]()
    var eventListModel = [EventListModel]()
    var popEventListModel = [EventListModel]()
    var eventData = EventListModel()
    var isMasterSearchView = false
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        triggerOfflineMode = UserDefaults.standard.value(forKey: "triggerOffline") as? Bool ?? false
        lblIsRecordFound.isHidden = true
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        userRole = userData["role"] as! String
        
        calander.setScope(FSCalendarScope.month, animated: true)
        calander.appearance.headerMinimumDissolvedAlpha = 0
        calander.scope = .month
        self.navigationController?.isNavigationBarHidden = true
        popUpView.isHidden = true
        cellDetailPopUp.isHidden = true
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        
        
        self.weeklyButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.monthlyButton.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.2431372549, blue: 0.4235294118, alpha: 1)
        self.weeklyButton.setTitleColor(#colorLiteral(red: 0.1607843137, green: 0.2431372549, blue: 0.4235294118, alpha: 1), for: .normal)
        monthlyButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
  
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    override func viewWillAppear(_ animated: Bool) {

        registerTableViewCell()
        
         triggerOfflineMode = UserDefaults.standard.value(forKey: "triggerOffline") as? Bool ?? false
        
        if !triggerOfflineMode {
            btnAddEvent.isHidden = false
            onlineLoadApi()
        }else {
            btnAddEvent.isHidden = true
            offlineLoadData()
        }
        
        let param = ["user_id":userId] as [String:Any]
        groupAndProjectApi(url: completeBioUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("bio updated")
            }else {
                print("bio couldn't update")
            }
            self.loaderView.isHidden = true
        }
        
        if isMasterSearchView {
            onMasterSearchDidSelect()
        }
        
    }

    //MARK:- Custom Functions
    func registerTableViewCell ()
    {
        self.tableView.register(UINib(nibName: "EventsTableViewCell", bundle: nil), forCellReuseIdentifier: "EventsTableViewCell")
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func onlineLoadApi () {
        eventListApi(url: eventListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                print("Events Updated")
            }else {
                print("Events couldn't Update")
            }
            self.tableView.reloadData()
            self.calander.reloadData()
            
        }
    }
   
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func offlineLoadData() {
        let archiveEvents = UserDefaults.standard.object(forKey: "events") as? NSData
        if archiveEvents != nil {
            let unarchiveEvents = NSKeyedUnarchiver.unarchiveObject(with: archiveEvents! as Data) as? [[String:Any]]
            if let event = unarchiveEvents {
                for i in event {
                    self.eventListModel.append(EventListModel(data: [i]))
                }
                for i in self.eventListModel{
                    self.eventDate.append(i.startDate!)
                }
                
                tableView.reloadData()
            }
        }else {
            isRecordFoundView.isHidden = true
            lblIsRecordFound.isHidden = false
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func eventListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        eventListModel.removeAll()
        eventDate.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: data)
                        UserDefaults.standard.set(encodedData, forKey: "events")
                        for i in data {
                            self.eventListModel.append(EventListModel(data: [i]))
                        }
                        for i in self.eventListModel{
                            self.eventDate.append(i.startDate!)
                        }

                        
                        
                        print("newsListdata fetched")
                    }else {
                        print("newsList didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func groupAndProjectApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["groups"] as? [[String:Any]] {
                        let userProjectEncodeData = NSKeyedArchiver.archivedData(withRootObject: data)
                        UserDefaults.standard.set(userProjectEncodeData, forKey: "groups")
                    }
                    if let data = JSON["groupsproject"] as? [[String:Any]] {
                        let userProjectEncodeData = NSKeyedArchiver.archivedData(withRootObject: data)
                        UserDefaults.standard.set(userProjectEncodeData, forKey: "LoginUserProjects")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func convertDataToStringInSpecificFormat (strDate:String) -> String{
        let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
               dateFormatter.locale = Locale(identifier: "en_US_POSIX")
               let date = dateFormatter.date(from:strDate)!
               let formatterDate = DateFormatter()
               formatterDate.dateFormat = "yyyy-MM-dd"
               
               return formatterDate.string(from: date)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func convertDataToStringForEventTime (strDate:String?) -> String?{
        if strDate != "" && strDate != "0000-00-00 00:00:00" {
            let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
             dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let date = dateFormatter.date(from:strDate ?? "")!
             let formatterDate = DateFormatter()
             formatterDate.dateFormat = "HH:MM a"
             
             return formatterDate.string(from: date)
        }else {
            return ""
        }
          
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func convertDataToStringForEventCreatedDate (strDate:String?) -> String?{
        if strDate != "" && strDate != "0000-00-00 00:00:00" {
            let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
             dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let date = dateFormatter.date(from:strDate ?? "")!
             let formatterDate = DateFormatter()
             formatterDate.dateFormat = "EEEE, MMMM d, yyyy"
             
             return formatterDate.string(from: date)
        }else {
            return ""
        }
          
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func onMasterSearchDidSelect() {
         cellDetailPopUp.isHidden = false
         let firstName = eventData.firstName
         let lastName = eventData.lastName
         let startEvent = convertDataToStringForEventTime(strDate: eventData.startDate ?? "")
         let endEvent = convertDataToStringForEventTime(strDate: eventData.endDate ?? "")
         let createdDate = convertDataToStringForEventCreatedDate(strDate: eventData.created ?? "")
         lblPopUpTitle.text = "\(firstName ?? "") \(lastName ?? "") ,\(eventData.name ?? "")"
         
         lblTimePopUp.text = createdDate
         lblLoactionPopUp.text = "\(eventData.city ?? "city not defind" ), \(eventData.country ?? "country not defind")"
         
         let fontSize = (30 * (self.view.frame.width / 320))
         let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
         webViewDescriptionPopUp.loadHTMLString(fontSetting + (eventData.datumDescription ?? ""), baseURL: nil)
         
         if eventData.allDay == "1" {
             lblCalanderPopUp.text = "All Day"
         }else {
             lblCalanderPopUp.text = "From \(startEvent ?? "") to \(endEvent ?? "")"
         }
        
    }
    
    //MARK:- Btn Actions
    @IBAction func weeklyButton(_ sender: Any) {
        self.monthlyButton.backgroundColor=#colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.monthlyButton.setTitleColor(#colorLiteral(red: 0.1606475115, green: 0.2427221537, blue: 0.4253162146, alpha: 1) ,for: .normal)
        self.weeklyButton.backgroundColor=#colorLiteral(red: 0.1606475115, green: 0.2427221537, blue: 0.4253162146, alpha: 1)
        self.weeklyButton.setTitleColor(.white, for: .normal)
        calander.setScope(FSCalendarScope.week, animated: true)
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func monthlyButton(_ sender: Any) {
        self.weeklyButton.backgroundColor=#colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.weeklyButton.setTitleColor(#colorLiteral(red: 0.1606475115, green: 0.2427221537, blue: 0.4253162146, alpha: 1), for: .normal)
        self.monthlyButton.backgroundColor=#colorLiteral(red: 0.1606475115, green: 0.2427221537, blue: 0.4253162146, alpha: 1)
        self.monthlyButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) ,for: .normal)
        calander.setScope(FSCalendarScope.month, animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func editBtn (sender:UIButton){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditEventViewController") as! EditEventViewController
        vc.eventList = eventListModel[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func addBtn(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddEventViewController") as! AddEventViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func hidePopUpView(_ sender: Any) {
        popUpView.isHidden = true
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func hideCellPopupView(_ sender: Any) {
        cellDetailPopUp.isHidden = true

    }
    
    //MARK:- TableView Delegate and DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popUpTableView {
            return popEventListModel.count
        }
        return eventListModel.count
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            if tableView == popUpTableView{
               return 260
            }
            return 100
        }else {
            if tableView == popUpTableView{
               return 280
            }
            return 120
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popUpTableView {
            let cell = popUpTableView.dequeueReusableCell(withIdentifier: "eventPopUpTableViewCell") as! eventPopUpTableViewCell
            
            let firstName = popEventListModel[indexPath.row].firstName
            let lastName = popEventListModel[indexPath.row].lastName
            let startEvent = convertDataToStringForEventTime(strDate: popEventListModel[indexPath.row].startDate ?? "")
            let endEvent = convertDataToStringForEventTime(strDate: popEventListModel[indexPath.row].endDate ?? "")
            let createdDate = convertDataToStringForEventCreatedDate(strDate: popEventListModel[indexPath.row].created ?? "")
            cell.name.text = "\(firstName ?? "") \(lastName ?? "") ,\(popEventListModel[indexPath.row].name ?? "")"
            cell.cityCountry.text = "\(popEventListModel[indexPath.row].city ?? "city not defind" ), \(popEventListModel[indexPath.row].country ?? "country not defind")"
            
            var fontSize : CGFloat = 0.0
            if UIDevice().userInterfaceIdiom == .phone {
                fontSize = (30 * (self.view.frame.width / 320))
            }else {
                fontSize = 33
            }
            let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
            cell.dataDescriptionWebView.loadHTMLString(fontSetting + (popEventListModel[indexPath.row].datumDescription ?? ""), baseURL: nil)
            cell.date.text = createdDate
         
            if popEventListModel[indexPath.row].allDay == "1" {
                cell.allDay.text = "All Day"
            }else {
                cell.allDay.text = "From \(startEvent ?? "") to \(endEvent ?? "")"
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventsTableViewCell") as! EventsTableViewCell
        let strDate = convertDataToStringInSpecificFormat(strDate: eventListModel[indexPath.row].startDate!)
        
        cell.lblEventDate.text = strDate
        cell.lblDecription.text = eventListModel[indexPath.row].name
        if triggerOfflineMode {
            cell.btnEdit.isHidden = true
            cell.editImg.isHidden = true

        }else {
            if userRole == "A" {
                cell.btnEdit.tag = indexPath.row
                cell.btnEdit.addTarget(self, action: #selector(editBtn), for: .touchUpInside)
            }else {
                if userId == Int(eventListModel[indexPath.row].userID ?? "") {
                    cell.btnEdit.isHidden = false
                    cell.editImg.isHidden = false
                    cell.btnEdit.tag = indexPath.row
                    cell.btnEdit.addTarget(self, action: #selector(editBtn), for: .touchUpInside)
                }else {
                    cell.btnEdit.isHidden = true
                    cell.editImg.isHidden = true
                }
            }
        }
        return cell
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableView {
            cellDetailPopUp.isHidden = false
            let firstName = eventListModel[indexPath.row].firstName
            let lastName = eventListModel[indexPath.row].lastName
            let startEvent = convertDataToStringForEventTime(strDate: eventListModel[indexPath.row].startDate ?? "")
            let endEvent = convertDataToStringForEventTime(strDate: eventListModel[indexPath.row].endDate ?? "")
            let createdDate = convertDataToStringForEventCreatedDate(strDate: eventListModel[indexPath.row].created ?? "")
            lblPopUpTitle.text = "\(firstName ?? "") \(lastName ?? "") ,\(eventListModel[indexPath.row].name ?? "")"
            
            lblTimePopUp.text = createdDate
            lblLoactionPopUp.text = "\(eventListModel[indexPath.row].city ?? "city not defind" ), \(eventListModel[indexPath.row].country ?? "country not defind")"
            
            var fontSize : CGFloat = 0.0
            if UIDevice().userInterfaceIdiom == .phone {
                fontSize = (30 * (self.view.frame.width / 320))
            }else {
                fontSize = 33
            }
            let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
            webViewDescriptionPopUp.loadHTMLString(fontSetting + (eventListModel[indexPath.row].datumDescription ?? ""), baseURL: nil)
            
            if eventListModel[indexPath.row].allDay == "1" {
                lblCalanderPopUp.text = "All Day"
            }else {
                lblCalanderPopUp.text = "From \(startEvent ?? "") to \(endEvent ?? "")"
            }
           
        }
        

    }
    
    //MARK:- FSCalander Delegate Methods
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        popEventListModel.removeAll()
        for i in eventListModel {
            let strDate = i.startDate!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let shortDate = dateFormatter.date(from:strDate)!
            let formatterDate = DateFormatter()
            formatterDate.dateFormat = "yyyy-MM-dd"
            if self.formatter.string(from: date) == formatterDate.string(from: shortDate) {
               popEventListModel.append(i)
            }
        }
        
        if popEventListModel.count == 0 {
            
        }else {
            popUpTableView.reloadData()
            popUpView.isHidden = false
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        
        for i in 0 ..< eventDate.count {
            let strDate = eventDate[i]
            if strDate != "0000-00-00 00:00:00" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let shortDate = dateFormatter.date(from:strDate)!
            let formatterDate = DateFormatter()
            formatterDate.dateFormat = "yyyy-MM-dd"
            if self.formatter.string(from: date) == formatterDate.string(from: shortDate) {
                for a in eventListModel{
                    if eventDate[i] == a.startDate {
                         return a.name
                    }
                }
               
            }
            }
        }
        return ""
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        for i in 0 ..< eventDate.count {
            let strDate = eventDate[i]
            if strDate !=  "0000-00-00 00:00:00" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let shortDate = dateFormatter.date(from:strDate)!
            let formatterDate = DateFormatter()
            formatterDate.dateFormat = "yyyy-MM-dd"
            if self.formatter.string(from: date) == formatterDate.string(from: shortDate) {
                   
               return 1
                }
            }
        }
        return 0
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calanderHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

}

/*-----------------------------------------------------------------------------------------------------*/

class eventPopUpTableViewCell : UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var allDay: UILabel!
    @IBOutlet weak var cityCountry: UILabel!
    @IBOutlet weak var dataDescriptionWebView: WKWebView!
    
    
/*-----------------------------------------------------------------------------------------------------*/
}
