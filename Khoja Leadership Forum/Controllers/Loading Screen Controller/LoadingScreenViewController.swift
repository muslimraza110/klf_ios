//
//  LoadingScreenViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/7/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import MBProgressHUD


class LoadingScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        MBProgressHUD.showAdded(to: self.view, animated: true)
        // Do any additional setup after loading the view.
    }
    
}
