
import UIKit
import Alamofire
import SwiftGifOrigin

class InitiativeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK:- Btn Outlets
    @IBOutlet weak var activeButtonOutlet: UIButton!
    @IBOutlet weak var archivedButtonOutlet: UIButton!
    @IBOutlet weak var tableViewInitiatives: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    
    //MARK:- Custom Variables
    var Status = false
    var initiativeListModel = [InitiativeListModel]()
    var isEventInitiative = false
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
//        if isEventInitiative {
//            navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
//                if vc.isKind(of: InitiativeDetailViewController.self)  {
//                    return true
//                } else {
//                    return false
//                }
//            })
//        }
        registerTableViewCell()
        self.navigationController?.isNavigationBarHidden = true
        
        headerHeight()
        
        self.tableViewInitiatives.delegate = self
        self.tableViewInitiatives.dataSource = self
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewInitiativeApi(url: initiativeListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                self.tableViewInitiatives.reloadData()
                print("initiative list has been fetched")
            }else {
                print("initiative list couldn't load")
            }
            self.loaderView.isHidden = true
        }
    }
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func registerTableViewCell () {
        self.tableViewInitiatives.register(UINib(nibName: "InitiativesTableViewCell", bundle: nil), forCellReuseIdentifier: "inCell")
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func viewInitiativeApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        initiativeListModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.initiativeListModel.append(InitiativeListModel(data: [i]))
                        }
                        print("initiativeList fetched")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
    
    //MARK:- Btn Actions
    @IBAction func activeButtonPressed(_ sender: UIButton) {
        
        self.activeButtonOutlet.isSelected = true
        if self.activeButtonOutlet.isSelected{
            self.activeButtonOutlet.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
            self.archivedButtonOutlet.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
            self.activeButtonOutlet.setTitleColor(.white, for: .normal)
            self.archivedButtonOutlet.setTitleColor(UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0), for: .normal)
            self.archivedButtonOutlet.isSelected = false
        }
        else{}
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func ArchivedButtonPressed(_ sender: UIButton) {
   
        self.archivedButtonOutlet.isSelected = true
        if self.archivedButtonOutlet.isSelected{
            self.archivedButtonOutlet.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
            self.activeButtonOutlet.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
            self.archivedButtonOutlet.setTitleColor(.white, for: .normal)
            self.activeButtonOutlet.setTitleColor(UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0), for: .normal)
            self.activeButtonOutlet.isSelected = false
        }
        else{}
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden =  isEventInitiative // false
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Tableview Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return initiativeListModel.count
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 100
        }else {
            return 130
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableViewInitiatives.deselectRow(at: indexPath, animated: true)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "InitiativeDetailViewController") as! InitiativeDetailViewController
        vc.initiativeId = initiativeListModel[indexPath.row].initiativesID ?? ""
        vc.initiativeTitle = initiativeListModel[indexPath.row].initiativesName ?? ""
        self.navigationController?.pushViewController(vc, animated: true)


       // let gotoInitiativeVC = self.storyboard?.instantiateViewController(withIdentifier: "Initiative_ViewController") as! Initiative_ViewController
        
       // gotoInitiativeVC?.initiativeViewID = Int(self.initiativeListModel[indexPath.row].initiativesID ?? "0")
      //  gotoInitiativeVC?.userId = Int(self.initiativeListModel[indexPath.row].initiativesUserID ?? "0")
       // self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let initiativeCell = self.tableViewInitiatives.dequeueReusableCell(withIdentifier: "inCell", for: indexPath) as! InitiativesTableViewCell
        initiativeCell.lblInitiativeTitle.text = initiativeListModel[indexPath.row].initiativesName
        initiativeCell.lblInitiativeCreator.isHidden = (UserDefaults.standard.value(forKey: Constants.User.role.rawValue) as? String) != "A"
        initiativeCell.lblInitiativeCreator.text = "\(initiativeListModel[indexPath.row].usersTitle ?? "")  \(initiativeListModel[indexPath.row].usersFirstName ?? "") \(initiativeListModel[indexPath.row].usersLastName ?? "")"
        initiativeCell.totalAmount.text = "USD \(initiativeListModel[indexPath.row].amountCollected ?? "0") / USD \(initiativeListModel[indexPath.row].fundraisingsAmmount ?? "0")"
        let collectedAmmount = initiativeListModel[indexPath.row].amountCollected
        let fullAmmount = initiativeListModel[indexPath.row].fundraisingsAmmount
        if collectedAmmount != "" && fullAmmount != "" {
            let numberFormatter = NumberFormatter()
            let number = numberFormatter.number(from: collectedAmmount!)
            let floatCollectedAmount = number!.floatValue
            
            let numberFormatter1 = NumberFormatter()
            let number1 = numberFormatter1.number(from: fullAmmount!)
            let floatfullAmount = number1!.floatValue
            
            initiativeCell.progressBarInitiative.progress = 0.0
            UIView.animate(withDuration: 1.0) {
                initiativeCell.progressBarInitiative.setProgress((floatCollectedAmount/floatfullAmount), animated: true)
            }
            
        }else {
            initiativeCell.progressBarInitiative.progress = 0.0
            UIView.animate(withDuration: 1.0) {
                initiativeCell.progressBarInitiative.setProgress(0.0, animated: true)
            }
        }
        return initiativeCell
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
  
    
}
