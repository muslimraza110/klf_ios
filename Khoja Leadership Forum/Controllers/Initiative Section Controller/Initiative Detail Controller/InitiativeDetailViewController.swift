

import UIKit
import Alamofire
import SwiftGifOrigin

class InitiativeDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    //MARK:- Btn Outets
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var lblCuurentAmount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblProjectCurrentAmount: UILabel!
    @IBOutlet weak var lblProjectTotalAmount: UILabel!
    @IBOutlet weak var lblBlocksPledged: UILabel!
    @IBOutlet weak var lblNeededBlocks: UILabel!
    @IBOutlet weak var lblMaximumBlocks: UILabel!
    @IBOutlet weak var lblPledgeBlockAmount: UILabel!
    @IBOutlet weak var txtFieldFundraising: TextFieldPadding!
    @IBOutlet weak var txtFieldCharity: TextFieldPadding!
    @IBOutlet weak var textFieldBlocks: TextFieldPadding!
    @IBOutlet weak var textFieldPledgeAmount: TextFieldPadding!
    @IBOutlet weak var tableViewPledgers: UITableView!
    @IBOutlet weak var tableViewFundraising: UITableView!
    @IBOutlet weak var tableViewProjects: UITableView!
    @IBOutlet weak var tableViewMatcher: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var progessView: UIProgressView!
    @IBOutlet weak var progressView1: UIProgressView!
    @IBOutlet weak var tableViewAddPledge: UITableView!
    @IBOutlet weak var btnFundraisingOutlet: UIButton!
    @IBOutlet weak var btnAnonymous: UIButton!
    @IBOutlet weak var btnHideAmount: UIButton!
    @IBOutlet weak var btnBlocksOutlet: UIButton!
    @IBOutlet weak var isPledgeFound: UILabel!
    @IBOutlet weak var isPorjectFound: UILabel!
    @IBOutlet weak var isFundraisingFound: UILabel!
    @IBOutlet weak var isGroupRecordFound: UILabel!
    @IBOutlet weak var lblInitiativeDetailScreen: UILabel!
    @IBOutlet weak var dropdownView: UIView!
    @IBOutlet weak var btnAddPledgeOutlet: AdaptiveButton!
    @IBOutlet weak var lblStack1: UIStackView!
    @IBOutlet weak var lblStack2: UIStackView!
    @IBOutlet weak var lblStack3: UIStackView!
    @IBOutlet weak var lblStack4: UIStackView!
    @IBOutlet weak var fundarisingConstrantMatch: NSLayoutConstraint!
    @IBOutlet weak var fundraisingConstraintBlock: NSLayoutConstraint!
    @IBOutlet weak var selectFundraisingBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var pledgerTopConstraintAfterActiveMatcherBox: NSLayoutConstraint!
    @IBOutlet weak var pledgerTopConstraintAfterHiddenMatcherBox: NSLayoutConstraint!
    @IBOutlet weak var matcherBox: UIView!
    @IBOutlet weak var blocksView: UIView!
    @IBOutlet weak var pldegeAmountView: UIView!
    @IBOutlet weak var hideAmountAndAnonymousView: UIView!
    @IBOutlet weak var isMatcherFound: UILabel!
    @IBOutlet weak var imageViewAnonymousCheckbox: UIImageView!
    @IBOutlet weak var imageViewHideAmountCheckbox: UIImageView!

    //MARK:- Custom Variables
    var groupFlag = false
    var maxBlockFlag = false
    var charityFlag = false
    var blockFlag = false
    var isMatchType = false
    var initiativeId = ""
    var initiativeTitle = ""
    var fundraisingId = ""
    var groupId = "0"
    var amount = ""
    var anonymous = "0"
    var hideAmount = "0"
    var userRole = ""
    var encryptedData = ""
    var selectedblock = 0
    var matcherVariable = 0
    var userId = 0
    var Status = false
    var projectListModel = [Project]()
    var fundraisingListModel = [Fundraising]()
     var fundraisingListModelForAddPledge = [Fundraising]()
    var pledgersListModel = [Pledger]()
    var groupsListModel = [Groups]()
    var initialiveInfo = InitiativeListModel()
    var initialiveGroupModel = [InitiativeGroupModel]()
    var blocks : Int?
    var currencyLabel: UILabel?
    var fundraisingsType = ""
    var isEventInitiative = false
    //MARK:- Life Cycle Methods
    override func viewWillAppear(_ animated: Bool) {
        
        loadDataOnScreenLoad()
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerHeight()
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
    }
    
    //MARK:- Button Actions
    @IBAction func hideDropDownView(_ sender: Any) {
        self.tableViewAddPledge.isHidden = true
        dropdownView.isHidden = true
        isGroupRecordFound.isHidden = true
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func pledgeButtonPressed(_ sender: UIButton) {
        let amount = (self.fundraisingsType == "P" || self.fundraisingsType == "M") ? textFieldPledgeAmount.text : nil
        let pledgeBlocks = self.fundraisingsType == "B" ? textFieldBlocks.text : nil
        if txtFieldFundraising.text == "" {
            self.showToast(message: "Select fundraising first")
        } else if txtFieldCharity.text == "" {
            self.showToast(message: "Select Charity first")
        } else if textFieldBlocks.text == "" && self.fundraisingsType == "B" {
            self.showToast(message: "Select blocks first")
        } else if textFieldPledgeAmount.text == "" && (self.fundraisingsType == "P" || self.fundraisingsType == "M") {
            self.showToast(message: "Select Amount first")
        } else {
            // "initative_id":initiativeId,
            let param = ["fundraising_id":fundraisingId,
                         "user_id":userId,
                         "anonymous" : btnAnonymous.isSelected ? 1: 0,
                         "hide_amount" : btnHideAmount.isSelected ? 1 : 0,
                         "group_id":groupId,"pledge_blocks" : pledgeBlocks ?? "", "amount" : amount ?? ""]   as [String:Any]
            self.loaderView.backgroundColor = UIColor(red: 4, green: 51, blue: 255, alpha: 0.24)

            addPledgeApi(url: makePledge, param: param, method: responseType.post.rawValue, headers: headerToken) { (status) in
                if status { // makePledge
                   // self.loadDataOnScreenLoad()
                    self.loaderView.isHidden = true
//                    if self.isEventInitiative {
//
//                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "InitiativeViewController") as? InitiativeViewController
//                        vc?.isEventInitiative = true
//                        self.navigationController?.pushViewController(vc!, animated: false)

                  //  } else {
                        self.navigationController?.popViewController(animated: true)
                   // }
                    self.showToast(message: "You have successfully PLEDGED")
                }else{
                    self.loaderView.isHidden = true
                    self.showToast(message: "Sorry request can't process")

                }
            }

        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @objc func willEnterForeground() {
        loadDataOnScreenLoad()
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

     @IBAction func btnCharity(_ sender: Any) {
        self.view.endEditing(true)
        
         groupFlag = true
         maxBlockFlag = false
         tableViewAddPledge.isHidden = false
         dropdownView.isHidden = false
         if self.initialiveGroupModel.count == 0 {
             isGroupRecordFound.isHidden = false
             isGroupRecordFound.text = "No group found!"
         }else {
             isGroupRecordFound.isHidden = true
         }
         tableViewAddPledge.reloadData()
     }
     
/*-----------------------------------------------------------------------------------------------------------------------------------*/

     @IBAction func btnFundraising(_ sender: Any) {
        self.view.endEditing(true)
         isGroupRecordFound.isHidden = true
         groupFlag = false
         maxBlockFlag = false
         tableViewAddPledge.isHidden = false
         dropdownView.isHidden = false
         tableViewAddPledge.reloadData()
     }
    
    @IBAction func btnBlocks(_ sender: UIButton) {
        groupFlag = false
        maxBlockFlag = true
        tableViewAddPledge.isHidden = false
        dropdownView.isHidden = false
        if self.initialiveGroupModel.count == 0 {
            isGroupRecordFound.isHidden = false
            isGroupRecordFound.text = "No Blocks found!"
        }else {
            isGroupRecordFound.isHidden = true
        }
        tableViewAddPledge.reloadData()
    }

    @IBAction func btnAnonymous(_ sender: UIButton) {
        if !sender.isSelected {
            self.imageViewAnonymousCheckbox.image = UIImage(named: "ic_check") // checkedIcon
            sender.isSelected = true
        }else {
            sender.isSelected = false
            self.imageViewAnonymousCheckbox.image = UIImage(named: "ic_uncheck") // uncheckedIcon
        }
    }

    @IBAction func btnHideAmount(_ sender: UIButton) {
        if !sender.isSelected {
            self.imageViewHideAmountCheckbox.image = UIImage(named: "ic_check") // checkedIcon
            sender.isSelected = true
        }else {
            sender.isSelected = false
            self.imageViewHideAmountCheckbox.image = UIImage(named: "ic_uncheck") // uncheckedIcon
        }
    }

    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func loadDataOnScreenLoad() {
        lblInitiativeDetailScreen.text = initiativeTitle
        textFieldPledgeAmount.text = ""
        txtFieldCharity.text = ""
        textFieldBlocks.text = ""
        txtFieldFundraising.text = ""
        self.progressView1.isHidden = false
        self.navigationController?.isNavigationBarHidden = true
        self.tableViewAddPledge.isHidden = true
        dropdownView.isHidden = true
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        userRole = userData["role"] as! String
        
        isFundraisingFound.isHidden = true
        isPorjectFound.isHidden = true
        isPledgeFound.isHidden = true
        isGroupRecordFound.isHidden = true
        isMatcherFound.isHidden = false
        self.pldegeAmountView.isHidden = true
        self.blocksView.isHidden = true
        self.hideAmountAndAnonymousView.isHidden = true
        self.lblStack1.isHidden = false
        self.lblStack2.isHidden = false
        self.lblStack3.isHidden = false
        self.lblStack4.isHidden = false

       // self.selectFundraisingBottomConstraint.constant =  (self.scrollContentView.frame.height *  -0.05)
        self.btnFundraisingOutlet.isHidden = true
        //self.txtFieldFundraising.isHidden = true
        
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        
        self.tableViewPledgers.delegate = self
        self.tableViewPledgers.dataSource = self
        
        self.txtFieldCharity.delegate = self
        self.textFieldBlocks.delegate = self

        self.showTextFieldRightIcon(isShow: true)
        self.showTextFieldLeftText()
        self.txtFieldFundraising.rightViewMode = .always
        let imageViewForFundRaising = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldFundraising.contentMode = .scaleAspectFit
        imageViewForFundRaising.image = UIImage(named: "dropdown.png")
        let rightViewForFundRaising = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForFundRaising.addSubview(imageViewForFundRaising)
        self.txtFieldFundraising.rightView = rightViewForFundRaising

        self.txtFieldCharity.rightViewMode = .always
        let imageViewForCharityType = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldCharity.contentMode = .scaleAspectFit
        imageViewForCharityType.image = UIImage(named: "dropdown.png")
        let rightViewForCharityType = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForCharityType.addSubview(imageViewForCharityType)
        self.txtFieldCharity.rightView = rightViewForCharityType
 
        registerTableViewCell()
        loadApi()
                       
    }

    private func showTextFieldRightIcon(isShow: Bool) {
        self.textFieldBlocks.rightViewMode = .always
        let imageViewForFundRaising = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.textFieldBlocks.contentMode = .scaleAspectFit
        imageViewForFundRaising.image = UIImage(named: "dropdown.png")
        let rightViewForFundRaising = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForFundRaising.addSubview(imageViewForFundRaising)
        self.textFieldBlocks.rightView = isShow ? rightViewForFundRaising : nil
    }

    private func showTextFieldLeftText() {
        self.currencyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 60, height: 25))
        self.textFieldPledgeAmount.leftViewMode = .always
        self.currencyLabel?.text = UIDevice.current.userInterfaceIdiom == .phone ?  "      USD" : " USD"
        self.currencyLabel?.font = textFieldPledgeAmount.font
        self.currencyLabel?.textColor = UIColor.lightGray
        self.textFieldPledgeAmount.leftView = currencyLabel
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func loadApi() {
        
        let param = ["initiative_id":initiativeId,"user_id":userId] as [String:Any]
            initiativeDetailListApi(url: initiativeDetailListUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
               if status {
                   self.tableViewPledgers.reloadData()
                   self.tableViewFundraising.reloadData()
                   self.tableViewProjects.reloadData()
                   self.tableViewMatcher.reloadData()
                self.lblInitiativeDetailScreen.text  = self.initiativeTitle == "" ?  self.initiativeTitle : self.initialiveInfo.initiativesName
                    self.lblCuurentAmount.text = "Total Initiative: USD \(self.initialiveInfo.amountCollected ?? "")"
                    self.lblTotalAmount.text = "USD \(self.initialiveInfo.fundraisingsAmmount ?? "")"
                self.lblPledgeBlockAmount.isHidden = true
                   //setting initial values of progess view and blocks count
                   if self.fundraisingListModel.count != 0 {
                    self.fundraisingsType = self.fundraisingListModel[0].fundraisingsType?.uppercased() ?? "B"
                    if self.fundraisingListModel[0].fundraisingsType?.uppercased() == "B" {
                        if self.fundraisingListModel[0].isPledge == 1 {
                            self.btnAddPledgeOutlet.isUserInteractionEnabled = false
                            self.btnAddPledgeOutlet.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                          
                        }else {
                            self.btnAddPledgeOutlet.isUserInteractionEnabled = true
                            self.btnAddPledgeOutlet.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.2392156863, blue: 0.4078431373, alpha: 1)
                        }
                        self.pldegeAmountView.isHidden = true
                        self.blocksView.isHidden = false
                        

                        self.isMatchType = false
                        

                        
                        self.fundraisingConstraintBlock.isActive = true
                        self.fundarisingConstrantMatch.isActive = false
                        
                    }else if self.fundraisingListModel[0].fundraisingsType?.uppercased() == "M" || self.fundraisingListModel[0].fundraisingsType?.uppercased() == "P" {

                        self.isMatchType = true
                        self.lblPledgeBlockAmount.isHidden = true
                        self.pldegeAmountView.isHidden = false
                        self.blocksView.isHidden = true
                        self.lblStack1.isHidden = true
                        self.lblStack2.isHidden = true
                        self.lblStack3.isHidden = true
                       
                        self.fundraisingConstraintBlock.isActive = false
                        self.fundarisingConstrantMatch.isActive = true
                        
                    }
                    
                    if self.fundraisingListModel[0].fundraisingsType?.uppercased() == "M" {
                        self.matcherBox.isHidden = false
                      //  self.pledgerTopConstraintAfterActiveMatcherBox.isActive = true
                     //   self.pledgerTopConstraintAfterHiddenMatcherBox.isActive = false
                    }else {
//                         self.matcherBox.isHidden = true
//                        self.pledgerTopConstraintAfterActiveMatcherBox.isActive = false
//                        self.pledgerTopConstraintAfterHiddenMatcherBox.isActive = true
                        self.matcherBox.isHidden = false
                        //self.pledgerTopConstraintAfterActiveMatcherBox.isActive = true
                       // self.pledgerTopConstraintAfterHiddenMatcherBox.isActive = false
                    }
                    
                       self.blocks = Int(self.fundraisingListModel[0].fundraisingsMaxBlocks ?? "")
                       self.fundraisingId = self.fundraisingListModel[0].fundraisingsID ?? ""
                       self.lblProjectCurrentAmount.text = "USD \(self.fundraisingListModel[0].fundraisingCollected ?? "")"
                       self.lblProjectTotalAmount.text = "USD \(self.fundraisingListModel[0].fundraisingsAmount ?? "")"
                       self.lblMaximumBlocks.text =  self.fundraisingListModel[0].fundraisingsMaxBlocks ?? ""
                       self.lblPledgeBlockAmount.text = "Pledge 1 Block (= USD \(self.fundraisingListModel[0].pledgeBlockAmount ?? 0))"
                       
                       //Assiging values to need block and user block
                       if self.fundraisingListModel[0].pledgeBlockAmount == 0 {
                        self.lblBlocksPledged.text = "Please select the fundraising" //"0 block"
                           self.lblNeededBlocks.text = "0 block"
                           
                       }else {
                        //calculation of remaining and currunt blocks
                           let curruntAmount =  Float(self.fundraisingListModel[0].fundraisingCollected ?? "0")
                           let usedblock = Int(curruntAmount ?? 0) / Int(self.fundraisingListModel[0].pledgeBlockAmount!)
                           self.lblBlocksPledged.text = "Please select the fundraising"//"\(String(usedblock)) blocks have been Pledged"
                           let collectAmount = Int(self.fundraisingListModel[0].fundraisingsAmount ?? "0")
                           let needBlock = (collectAmount ?? 0) / Int(self.fundraisingListModel[0].pledgeBlockAmount ?? 0)
                           self.lblNeededBlocks.text = "\(String(needBlock - usedblock)) blocks"
                       }
                       
                       //fundraising progress view value setting
                    if let fundraisingName = self.fundraisingListModel[0].fundraisingsName {
                        self.txtFieldFundraising.text = fundraisingName
                    } else {
                        self.txtFieldFundraising.placeholder = "-- Fundraisings --"
                    }


                    //self.txtFieldFundraising.text = self.fundraisingListModel[0].fundraisingsName ?? "" != "" ? self.fundraisingListModel[0].fundraisingsName : "-- Fundraisings --"
                       if self.fundraisingListModel[0].fundraisingCollected != "" && self.fundraisingListModel[0].fundraisingsAmount != "" {
                           let numberFormatter = NumberFormatter()
                           let number = numberFormatter.number(from: self.fundraisingListModel[0].fundraisingCollected!)
                           let floatCollectedAmount = number!.floatValue
                           
                           let numberFormatter1 = NumberFormatter()
                           let number1 = numberFormatter1.number(from: self.fundraisingListModel[0].fundraisingsAmount!)
                           let floatfullAmount = number1!.floatValue

                           self.progressView1.progress = 0.0
                           UIView.animate(withDuration: 1.0) {
                               self.progressView1.setProgress((floatCollectedAmount/floatfullAmount), animated: true)
                           }
                           
                       }else {
                           self.progressView1.progress = 0.0
                           UIView.animate(withDuration: 1.0) {
                               self.progressView1.setProgress((0.0), animated: true)
                           }
                           
                       }
                       
                   }
                   
                   //checking the fundraising count for hide and show fundraising button
                   if self.fundraisingListModelForAddPledge.count == 1 && self.fundraisingListModelForAddPledge.count != 0 {
                        self.btnFundraisingOutlet.isHidden = false
                        self.txtFieldFundraising.isHidden = false
                   }else {
                        self.btnFundraisingOutlet.isHidden = false
                        self.txtFieldFundraising.isHidden = false
                   }
                   
                   //setting main progressview
                if (self.initialiveInfo.amountCollected != "" && self.initialiveInfo.amountCollected != nil) && (self.initialiveInfo.fundraisingsAmmount != "" && self.initialiveInfo.fundraisingsAmmount != nil) {
                       let numberFormatter = NumberFormatter()
                       let number = numberFormatter.number(from: self.initialiveInfo.amountCollected!)
                       let floatCollectedAmount = number!.floatValue
                       
                       let numberFormatter1 = NumberFormatter()
                       let number1 = numberFormatter1.number(from: self.initialiveInfo.fundraisingsAmmount!)
                       let floatfullAmount = number1!.floatValue
                       
                       self.progessView.progress = 0.0
                       UIView.animate(withDuration: 1.0) {
                           self.progessView.setProgress((floatCollectedAmount/floatfullAmount), animated: true)
                       }
                       
                   }else {
                       self.progessView.progress = 0.0
                       UIView.animate(withDuration: 1.0) {
                           self.progessView.setProgress((0.0), animated: true)
                       }
                       
                   }
                       
                if self.projectListModel.count == 0 {
                    self.isPorjectFound.isHidden = false
                }else {
                    self.isPorjectFound.isHidden = true
                }
                
                if self.fundraisingListModel.count == 0 {
                    self.isFundraisingFound.isHidden = false
               }else {
                    self.isFundraisingFound.isHidden = true
               }
                
                if self.pledgersListModel.count == 0 {
                    self.isPledgeFound.isHidden = false
               }else {
                    self.isPledgeFound.isHidden = true
               }
                   
               //Calling group intiative list
               
               let param = ["initiative_id":self.initialiveInfo.initiativesID!] as[String:Any]
               self.initiativeGroupListApi(url: initiativeGroupListUrl, method: responseType.get.rawValue, param: param, header: headerToken, completion: { (status) in
                   if status {
                       self.tableViewAddPledge.reloadData()
                   }else {
                       
                   }
                   self.loaderView.isHidden = true
               })
                   

               }else {
                   self.loaderView.isHidden = true

               }
           }
               
               
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func registerTableViewCell ()
    {
        
        self.tableViewPledgers.register(UINib(nibName: "PledgersTableViewCell", bundle: nil), forCellReuseIdentifier: "PCell")
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func initiativeDetailListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        projectListModel.removeAll()
        fundraisingListModel.removeAll()
        fundraisingListModelForAddPledge.removeAll()
        pledgersListModel.removeAll()
        initialiveGroupModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? NSDictionary {
                       let info = data
                        if let detailInfo = info["basicinfo"] as? [[String:Any]]{
                            var dumyInfo = [InitiativeListModel]()
                            for i in detailInfo {
                                dumyInfo.append(InitiativeListModel(data: [i]))
                            }
                            
                            if dumyInfo.count > 0 {
                                self.initialiveInfo = dumyInfo[0]
                            }
                        }
                        print("Project list data fetched")
                        
                        if let data = info["projects"] as? [[String:Any]] {
                            for i in data {
                                self.projectListModel.append(Project(data: [i]))
                            }
                        }
                        
                        if let data = info["fundraising"] as? [[String:Any]] {
                            for i in data {
                                self.fundraisingListModel.append(Fundraising(data: [i]))
                            }
                        }
                        
                        for i in self.fundraisingListModel {
                            self.fundraisingListModelForAddPledge.append(i)
                        }
                        
                        if let data = info["Pledgers"] as? [[String:Any]] {
                            for i in data {
                                self.pledgersListModel.append(Pledger(data: [i]))
                            }
                        }

                        if let data = info["Groups"] as? [[String: Any]] {
                            for i in data {
                               // self.groupsListModel.append(Groups(data: [i]))
                                self.initialiveGroupModel.append(InitiativeGroupModel(data: [i]))
                            }
                        }
                        
                    }
                   
                    
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
    
    
    func initiativeGroupListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
      //  initialiveGroupModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let _ = JSON["data"] as? [[String:Any]] {
        
                 //       for i in data {
                     //       self.initialiveGroupModel.append(InitiativeGroupModel(data: [i]))
                   //     }
                        
                    }
                    
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func addPledgeApi(url:String,param:[String:Any],method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    if let data = JSON["data"] as? String {
                        self.encryptedData = data
                    }
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - Dissmiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    // textfield format Check function
    func textFieldFormatCheck() -> Bool {
        
        if self.txtFieldCharity.text?.count == 0 {
            
            self.txtFieldCharity.borderColor = .red
            charityFlag = false
            
        }
        else{
            self.txtFieldCharity.borderColor = .darkGray
            charityFlag = true
        }
        if charityFlag == true {return true}
        else{return false}
    }

 /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK:- TABLEVIEW DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewProjects {
            return projectListModel.count
        }else if tableView == tableViewFundraising {
            return fundraisingListModel.count
        }else if tableView == tableViewMatcher {
            if fundraisingListModel.count > 0 {
                return fundraisingListModel[matcherVariable].matcher.count
            }else {
                return 0
            }
        }else if tableView == tableViewAddPledge {
            if groupFlag {
                return initialiveGroupModel.count
            }else if maxBlockFlag {
                return blocks ?? 0
            }else{
                return fundraisingListModelForAddPledge.count
            }
        }
        return pledgersListModel.count
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewPledgers {
            let pledgersCell = self.tableViewPledgers.dequeueReusableCell(withIdentifier: "PCell", for: indexPath) as! PledgersTableViewCell
            pledgersCell.lblProjectName.text = pledgersListModel[indexPath.row].fundaraisingName
            pledgersCell.lblCharity.text = pledgersListModel[indexPath.row].charityName
            
            if userRole == "A" {
                pledgersCell.lblAmountPledged.text = "USD \(pledgersListModel[indexPath.row].fundraisingPledgesAmount ?? "0")"
            }else {
                if pledgersListModel[indexPath.row].fundraisingPledgesHideAmount ==  "1" {
                    if pledgersListModel[indexPath.row].fundraisingPledgesUserID == String(userId) {
                        pledgersCell.lblAmountPledged.text  = "" // USD \(pledgersListModel[indexPath.row].fundraisingPledgesAmount ?? "0")
                    }else {
                        pledgersCell.lblAmountPledged.text  = "" // Anonymous
                    }
                }else {
                    pledgersCell.lblAmountPledged.text = "USD \(pledgersListModel[indexPath.row].fundraisingPledgesAmount ?? "0")"
                }
            }
            
            if userRole == "A" {
                pledgersCell.lblPledgerName.text  = "\(pledgersListModel[indexPath.row].usersFirstName ?? "") \(pledgersListModel[indexPath.row].usersLastName ?? "")"
            }else {
                if pledgersListModel[indexPath.row].fundraisingPledgesAnonymous == "1" {
                    if pledgersListModel[indexPath.row].fundraisingPledgesUserID == String(userId) {
                        pledgersCell.lblPledgerName.text  = "*(Anonymous)" // "\(pledgersListModel[indexPath.row].usersFirstName ?? "") \(pledgersListModel[indexPath.row].usersLastName ?? "") *(Anonymous)"
                    }else {
                        pledgersCell.lblPledgerName.text  = "*(Anonymous)"
                    }
                }else {
                     pledgersCell.lblPledgerName.text  = "\(pledgersListModel[indexPath.row].usersFirstName ?? "") \(pledgersListModel[indexPath.row].usersLastName ?? "")"
                }
            }
            
            
            
            return pledgersCell
        }
        else if tableView == tableViewFundraising{
            let cell = self.tableViewFundraising.dequeueReusableCell(withIdentifier: "FundraisingTableViewCell", for: indexPath) as! FundraisingTableViewCell
            cell.lblFundraisingName.text = fundraisingListModel[indexPath.row].fundraisingsName
            let fundraisingType = fundraisingListModel[indexPath.row].fundraisingsType
            var valueOfType = ""
            if fundraisingType?.uppercased() == "B" {
                valueOfType = "Block"
            }else if fundraisingType?.uppercased() == "P"{
                valueOfType = "Pledge"
            }else if fundraisingType?.uppercased() == "M"{
                valueOfType = "Match"
            }
            cell.lblBlock.text = valueOfType
            cell.lblAmount.text = "USD \(fundraisingListModelForAddPledge[indexPath.row].fundraisingCollected ?? "0") / USD \(fundraisingListModelForAddPledge[indexPath.row].fundraisingsAmount ?? "" )"
            return cell

        }else if tableView == tableViewAddPledge {
            let cell = self.tableViewAddPledge.dequeueReusableCell(withIdentifier: "AddPledgeTableViewCell", for: indexPath) as! AddPledgeTableViewCell
            if groupFlag {
                cell.lblName.text = initialiveGroupModel[indexPath.row].groupName
            }else if maxBlockFlag {
                cell.lblName.text = String(indexPath.row + 1)
            }else {
                cell.lblName.text = fundraisingListModelForAddPledge[indexPath.row].fundraisingsName
            }
            return cell
            
        }else if tableView == tableViewMatcher {
            let cell = self.tableViewMatcher .dequeueReusableCell(withIdentifier: "MatcherTableViewCell", for: indexPath) as! MatcherTableViewCell
             if userRole == "A" {
                cell.lblMatcherAmount.text = fundraisingListModel[matcherVariable].matcher[indexPath.row].matcherAmount
             }else {
                if fundraisingListModel[matcherVariable].matcher[indexPath.row].hideAmount == "1" {
                    cell.lblMatcherAmount.text = ""
                }else {
                    cell.lblMatcherAmount.text = fundraisingListModel[matcherVariable].matcher[indexPath.row].matcherAmount
                }
            }
            
            if userRole == "A" {
                 cell.lblMatcherName.text = fundraisingListModel[matcherVariable].matcher[indexPath.row].matcherName
            }else {
                if fundraisingListModel[matcherVariable].matcher[indexPath.row].anonymous == "1" {
                    cell.lblMatcherName.text = "<Anonymous>"
                }else {
                    cell.lblMatcherName.text = fundraisingListModel[matcherVariable].matcher[indexPath.row].matcherName
                }
            }
            return cell
            
        }else {
            let cell = self.tableViewProjects.dequeueReusableCell(withIdentifier: "ProjectsTableViewCell", for: indexPath) as! ProjectsTableViewCell
            cell.lblProjectName.text = projectListModel[indexPath.row].projectsName
            cell.lblPercentage.text = (projectListModel[indexPath.row].initiativesProjectsPercentage ?? "") + "%"
            cell.lblAmount.text = "USD \(projectListModel[indexPath.row].projectFundraisingsAmount ?? "0.0")" //initialiveInfo.fundraisingsAmmount
            cell.lblCharitiesName.attributedText = projectListModel[indexPath.row].groupDetails?.htmlToAttributedStringWithFont

            return cell
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            if tableView == tableViewFundraising {
                return 100
            }else if tableView == tableViewProjects {
                return UITableView.automaticDimension
            }else if tableView == tableViewAddPledge {
                return 50
            }
            else if tableView == tableViewMatcher {
                return 50
            }
            return 100
        }else {
            if tableView == tableViewFundraising || tableView == tableViewProjects {
                return 160
            }else if tableView == tableViewAddPledge {
                return 70
            }
            else if tableView == tableViewMatcher {
                return 70
            }
            return 180
        }
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {



        if tableView == tableViewAddPledge {
            if groupFlag {

                groupId = initialiveGroupModel[indexPath.row].groupId ?? ""
                txtFieldCharity.text = initialiveGroupModel[indexPath.row].groupName ?? ""
            }else if maxBlockFlag {
                selectedblock = indexPath.row + 1
                amount = String(Int(fundraisingListModelForAddPledge[0].pledgeBlockAmount!) * selectedblock)
                self.textFieldBlocks.text = "\(selectedblock)"
                
            }else {
                matcherVariable = indexPath.row
                tableViewMatcher.reloadData()
                self.isMatcherFound.isHidden = false
               // self.selectFundraisingBottomConstraint.constant = 25
                self.progressView1.isHidden = false
                txtFieldFundraising.text = fundraisingListModelForAddPledge[indexPath.row].fundraisingsName
                //checking fundraising is already added or not in pledge list
                self.textFieldPledgeAmount.text = nil
                self.textFieldBlocks.text = nil
                self.fundraisingsType = self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsType?.uppercased() ?? ""
                self.hideAmountAndAnonymousView.isHidden =  !(fundraisingListModelForAddPledge[indexPath.row].fundraisingsAllowAnonymous == "1")
                self.lblStack4.isHidden = false
                if self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsType?.uppercased() == "B" {
                   // self.showTextFieldLeftIcon(isShow: true)
                   // self.textFieldBlocks.isHidden = false
                    self.pldegeAmountView.isHidden = true
                    self.blocksView.isHidden = false

                   // self.textFieldBlocks.isUserInteractionEnabled = false
                    if fundraisingListModelForAddPledge[indexPath.row].isPledge == 1 {
                        btnAddPledgeOutlet.isUserInteractionEnabled = false
                        btnAddPledgeOutlet.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                        self.showToast(message: "You have already pledged.")


                    }else {
                        btnAddPledgeOutlet.isUserInteractionEnabled = true
                        btnAddPledgeOutlet.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.2392156863, blue: 0.4078431373, alpha: 1)
                    }
                    
                    
                    self.isMatchType = false
                    self.lblPledgeBlockAmount.isHidden = false

                    
                    
                    lblStack1.isHidden = false
                    lblStack2.isHidden = false
                    lblStack3.isHidden = false
                    
                    fundraisingConstraintBlock.isActive = true
                    fundarisingConstrantMatch.isActive = false
                    
                }else if fundraisingListModelForAddPledge[indexPath.row].fundraisingsType?.uppercased() == "M" || fundraisingListModelForAddPledge[indexPath.row].fundraisingsType?.uppercased() == "P"{
                    //self.textFieldBlocks.isHidden = false
                    self.blocksView.isHidden = true
                    self.pldegeAmountView.isHidden = false
                   // self.showTextFieldLeftIcon(isShow: false)
                   // self.textFieldBlocks.isUserInteractionEnabled = true
                    btnAddPledgeOutlet.isUserInteractionEnabled = true
                    btnAddPledgeOutlet.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.2392156863, blue: 0.4078431373, alpha: 1)
                  
                    self.isMatchType = true
                    self.lblPledgeBlockAmount.isHidden = true

                    
                  lblStack1.isHidden = true
                  lblStack2.isHidden = true
                  lblStack3.isHidden = true
                  
                  fundraisingConstraintBlock.isActive = false
                  fundarisingConstrantMatch.isActive = true
                    
                }
                if fundraisingListModelForAddPledge[indexPath.row].fundraisingsType?.uppercased() == "M" {
                    matcherVariable = indexPath.row
                    if fundraisingListModelForAddPledge[matcherVariable].matcher.count == 0 {
                        isMatcherFound.isHidden = false
                    }else {
                         isMatcherFound.isHidden = true
                    }
//                    pledgerTopConstraintAfterActiveMatcherBox.isActive = true
//                    pledgerTopConstraintAfterHiddenMatcherBox.isActive = false
//                    matcherBox.isHidden = false
//
                //    pledgerTopConstraintAfterActiveMatcherBox.isActive = false
                  //  pledgerTopConstraintAfterHiddenMatcherBox.isActive = true
                   // matcherBox.isHidden = true
                    tableViewMatcher.reloadData()

                }
                else {
                 //   pledgerTopConstraintAfterActiveMatcherBox.isActive = false
                  //  pledgerTopConstraintAfterHiddenMatcherBox.isActive = true
                 //   matcherBox.isHidden = true
                }
                
                    self.blocks = Int(self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsMaxBlocks ?? "")
                    self.fundraisingId = self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsID ?? ""
                    self.lblProjectCurrentAmount.text = "USD \(self.fundraisingListModelForAddPledge[indexPath.row].fundraisingCollected ?? "")"
                    self.lblProjectTotalAmount.text = "USD \(self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsAmount ?? "")"
                    self.lblMaximumBlocks.text =  self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsMaxBlocks ?? ""
                    self.lblPledgeBlockAmount.text = "Pledge 1 Block (= USD \(self.fundraisingListModelForAddPledge[indexPath.row].pledgeBlockAmount ?? 0))"
                    
                    //Assiging values to need block and user block
                    if self.fundraisingListModelForAddPledge[indexPath.row].pledgeBlockAmount == 0 {
                        self.lblBlocksPledged.text = "0"
                        self.lblNeededBlocks.text = "0"
                        
                    }else {
                        let curruntAmount = Float(self.fundraisingListModelForAddPledge[indexPath.row].fundraisingCollected ?? "0")
                        let usedblock = Int(curruntAmount ?? 0 ) / Int(self.fundraisingListModelForAddPledge[indexPath.row].pledgeBlockAmount!)
                        self.lblBlocksPledged.text = "\(String(usedblock)) have been Pledged"
                        let collectAmount = Int(self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsAmount ?? "0")
                        let needBlock = (collectAmount ?? 0) / Int(self.fundraisingListModelForAddPledge[indexPath.row].pledgeBlockAmount ?? 0)
                        self.lblNeededBlocks.text = String(needBlock - usedblock)
                    }
                    
                    //fundraising progress view value setting
                    if self.fundraisingListModelForAddPledge[indexPath.row].fundraisingCollected != "" && self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsAmount != "" {
                        let numberFormatter = NumberFormatter()
                        let number = numberFormatter.number(from: self.fundraisingListModelForAddPledge[indexPath.row].fundraisingCollected!)
                        let floatCollectedAmount = number!.floatValue
                        
                        let numberFormatter1 = NumberFormatter()
                        let number1 = numberFormatter1.number(from: self.fundraisingListModelForAddPledge[indexPath.row].fundraisingsAmount!)
                        let floatfullAmount = number1!.floatValue
                        
                        self.progressView1.progress = 0.0
                        UIView.animate(withDuration: 1.0) {
                            self.progressView1.setProgress((floatCollectedAmount/floatfullAmount), animated: true)
                        }
                        
                    }else {
                        self.progressView1.progress = 0.0
                        UIView.animate(withDuration: 1.0) {
                            self.progressView1.setProgress((0.0), animated: true)
                        }
                        
                    }
                    
                    
            }
            
            tableViewAddPledge.isHidden = true
            dropdownView.isHidden = true

        }
    }
    
    
}
