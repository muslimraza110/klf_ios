//
//  MatcherTableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 21/10/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import UIKit
class MatcherTableViewCell:UITableViewCell {
    
    @IBOutlet weak var lblMatcherName: UILabel!
    @IBOutlet weak var lblMatcherAmount: UILabel!
}
