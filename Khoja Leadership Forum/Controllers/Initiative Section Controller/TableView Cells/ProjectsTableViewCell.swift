
//
//  Created by Hamza Saeed on 11/09/2019.
//

import UIKit
class ProjectsTableViewCell:UITableViewCell {
    
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblCharitiesName: UILabel!
    
    
}
