
import UIKit

class PledgersTableViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var lblPledgerName: UILabel!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblCharity: UILabel!
    @IBOutlet weak var lblAmountPledged: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
