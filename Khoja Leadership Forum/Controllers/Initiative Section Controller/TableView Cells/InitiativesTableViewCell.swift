
import UIKit

class InitiativesTableViewCell: UITableViewCell {

    //Outelets
    @IBOutlet weak var lblInitiativeTitle: UILabel!
    @IBOutlet weak var lblInitiativeCreator: UILabel!
    @IBOutlet weak var progressBarInitiative: UIProgressView!
    @IBOutlet weak var totalAmount: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
