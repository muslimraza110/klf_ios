
import UIKit
import WebKit

class FaqsTableViewCell: UITableViewCell {
    //Outlets
    @IBOutlet weak var contentWebView: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
