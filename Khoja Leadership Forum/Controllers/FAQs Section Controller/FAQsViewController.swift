
import UIKit
import Alamofire
import SwiftGifOrigin

class FAQsViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var faqsTableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    
    //MARK:- Custom Variables
    var Status = false
    var faqsListModel = [FAQsListModel]()
    var dropImage = UIImage.init(named: "dropdown.png")
    var headersTitleArray = [String]()
    var sectionDataArray = [ExpandableNames]()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.navigationController?.isNavigationBarHidden = true
        
        // TableView Delegate & Datasource
        self.faqsTableView.delegate = self
        self.faqsTableView.dataSource = self
        
        registerTableViewCell()
        faqsListApi(url: faqsListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                print("loaded")
            }else {
                print("didn't load")
            }
            self.faqsTableView.reloadData()
            self.loaderView.isHidden = true
        }

    }
    
    //MARK:- Custom Functions
    func registerTableViewCell ()
    {
        self.faqsTableView.register(UINib(nibName: "FaqsTableViewCell", bundle: nil), forCellReuseIdentifier: "FaqsCell")
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func faqsListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        faqsListModel.removeAll()
//        headersTitleArray.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.faqsListModel.append(FAQsListModel(data: [i]))
                        }
                        self.headersTitleArray.removeAll()
                        self.sectionDataArray.removeAll()
                        for i in self.faqsListModel {
                            self.headersTitleArray.append(i.title!)
                            self.sectionDataArray.append(ExpandableNames(isExpanded: false, names: [i.datumDescription ?? "No Data"]))
                        }
                        
                        
                        
                        print("newsListdata fetched")
                    }else {
                        print("newsList didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    //MARK:- TableView Delegate and DataSource Methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let faqCell = self.faqsTableView.dequeueReusableCell(withIdentifier: "FaqsCell", for: indexPath) as! FaqsTableViewCell
        
        let sampleHTML = self.sectionDataArray[indexPath.section].names[indexPath.row]
        var fontSize : CGFloat = 0.0
        if UIDevice().userInterfaceIdiom == .phone {
            fontSize = (32 * (self.view.frame.width/320))
        }else {
            fontSize = 35
        }
        let fontName = "Avenir-Medium"
        let fontSetting = "<span style=\"font-family: \(fontName);font-size: \(fontSize)\"</span>"
        faqCell.contentWebView.loadHTMLString(fontSetting + sampleHTML , baseURL: nil)
        
        return faqCell
        
    }
        
    //MARK:- Btn Actions
    @objc func ExpandClose (button: UIButton) {
        
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in sectionDataArray[section].names.indices{
            let indexpath = IndexPath(row: row, section: section)
            indexPaths.append(indexpath)
        }
        
        let isExpanded = sectionDataArray[section].isExpanded
        sectionDataArray[section].isExpanded = !isExpanded
        if isExpanded {
            button.isSelected = false
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
            
            button.setImage(dropImage, for: .normal)
            faqsTableView.deleteRows(at: indexPaths, with: .top)
        }
        else{
            button.isSelected = true
            dropImage = UIImage.init(named: "of-admin-dropup.png")
            
            button.setImage(dropImage, for: .normal)
            faqsTableView.insertRows(at: indexPaths, with: .bottom)
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !sectionDataArray[section].isExpanded {
            return 0
        }
        return 1
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 200
        }else {
            return 300
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionDataArray.count
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 40
        }else {
            return 50
        }
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           
       let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 150))
       
       // code for adding centered title
       headerView.backgroundColor = UIColor.white
       let headerLabel = UILabel(frame: CGRect(x: 15, y: 5, width:tableView.frame.width-80, height: 30))
       headerLabel.textColor = UIColor.black
       if headersTitleArray.count != 0 {
           headerLabel.text = headersTitleArray[section]
       }
       if UIDevice().userInterfaceIdiom == .phone {
           headerLabel.font = UIFont.boldSystemFont(ofSize: 14)

       }else {
           headerLabel.font = UIFont.boldSystemFont(ofSize: 18)
       }
       headerLabel.textAlignment = .left
       headerLabel.contentMode = .scaleAspectFit
       
       headerView.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
       headerLabel.textColor = UIColor.white
       headerView.addSubview(headerLabel)
       
       let arrowButton = UIButton(frame: CGRect(x:headerView.frame.size.width - 80, y:0, width:60, height:30))
       dropImage = UIImage.init(named: "of-admin-dropdown.png")
       
       arrowButton.setImage(dropImage, for: .normal)
       arrowButton.setTitleColor(.black, for: .normal)
       arrowButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
       arrowButton.backgroundColor = UIColor.clear
       arrowButton.addTarget(self, action: #selector(ExpandClose), for: .touchUpInside)
       arrowButton.tag = section
       arrowButton.imageView?.contentMode = .scaleAspectFit
       
       let countValue = self.sectionDataArray[section].names.count
       if countValue != 0{
           headerView.addSubview(arrowButton)
       }
       //constraint for dropdown arrow
       arrowButton.translatesAutoresizingMaskIntoConstraints = false
       arrowButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
       arrowButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
       arrowButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
       arrowButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
       
       arrowButton.setNeedsLayout()
       arrowButton.layoutIfNeeded()
       arrowButton.clipsToBounds = true
       
       headerView.layer.cornerRadius = 10
       return headerView
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    
}
