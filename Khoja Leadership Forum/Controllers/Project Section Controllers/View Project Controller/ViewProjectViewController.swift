
import UIKit
import Alamofire
import SwiftGifOrigin
import SDWebImage

class ViewProjectViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //Outlets
    @IBOutlet weak var lblTargetAnnual: UILabel!
    @IBOutlet weak var lblTargetAudiance: UILabel!
    @IBOutlet weak var lblCharity: UILabel!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var projectMemberTableView: UITableView!
    @IBOutlet weak var relatedProjectTableView: UITableView!
    @IBOutlet weak var pendingAccessTableView: UITableView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var isMemberRecordFound: UILabel!
    @IBOutlet weak var isGroupRecordFound: UILabel!
    @IBOutlet weak var isPendingListFound: UILabel!
    @IBOutlet weak var isTagFound: UILabel!
    @IBOutlet weak var lblCharityTitle: ContentLabel!
    @IBOutlet weak var lblTargerTitle: ContentLabel!
    @IBOutlet weak var lblTargetAnnualTitle: ContentLabel!
    @IBOutlet weak var btnJoinProject: UIButton!
    @IBOutlet weak var viewPending: UIView!
    @IBOutlet weak var btnAddMember: UIButton!
    @IBOutlet weak var projectTitle: BlueViewLabel!
    
    var cancelRequest = false
    var leaveRequest = false
    var Status = false
    var userId = 0
    var userRole = ""
    var projectId = ""
    var projectReadOnlyStatus = ""
    var projectInfoModel = ProjectInfoModel()
    var projectNotesModel = [ProjectNotesModel]()
    var projectGroupModel = [ProjectGroupModel]()
    var projectTagsModel = [ProjectTagModel]()
    var projectUserModel = [ProjectUserModel]()
    var fullProjectMemberlist = [ProjectMemberListModel]()
    var selectedProjectMemberlist = [ProjectMemberListModel]()
    var projectPendingRequestModel = [ProjectPendingRequestModel]()
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        registerTableViewCell()
        headerHeight()
        
        isMemberRecordFound.isHidden = true
        isGroupRecordFound.isHidden = true
        isPendingListFound.isHidden = true
        
        lblType.isHidden = true
        isTagFound.isHidden = true
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
      
       
        
       
        // Do any additional setup after loading the view.
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func joinProjectBtnAction(_ sender: Any) {
        
        
        if cancelRequest {
            
            //for request cancel api which member has sent
            
            for i in projectPendingRequestModel {
                if userId == Int(i.userId ?? "") {
                    let param = ["user_id":userId,"project_id":projectId] as [String:Any]
                    removeMemberApi(url: cancelProjectRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                       if status {
                           self.pendingProjectListApi(url: (projectPendingListUrl + self.projectId), method: responseType.get.rawValue, param:nil, headers: nil) { (status) in
                               if status{
                                    
                                self.cancelRequest = false
                                self.leaveRequest = false
                                self.btnJoinProject.setImage(UIImage(named: "ic_key"), for: .normal)

                                self.showToast(message: "Request has been canceled")
                                self.loaderView.isHidden = true
                                self.pendingAccessTableView.reloadData()
                                
                                if self.userRole == "A" {
                                    if self.projectPendingRequestModel.count == 0 {
                                        self.isPendingListFound.isHidden = true
                                        self.pendingAccessTableView.isHidden = true
                                        self.viewPending.isHidden = true
                                    }else {
                                        self.isPendingListFound.isHidden = true
                                        self.pendingAccessTableView.isHidden = false
                                        self.viewPending.isHidden = false
                                    }
                                }
                               
                               }else{
                                   self.loaderView.isHidden = true
                               }
                           }
                       }else{
                           self.loaderView.isHidden = true
                       }
                   }
                }
            }
            
        }else if leaveRequest {
            
            //for leave project request if member has been approved
            
            let param = ["user_id":userId,"project_id":projectId] as [String : Any]
               removeMemberApi(url: removeMemberFromProjectUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                   if status {
                       print("Group has been removed")
                       self.memberProjectList(url: (projectMemberListUrl + self.projectId), method: responseType.get.rawValue, headers: nil) { (status) in
                           if status {
                                self.cancelRequest = false
                                self.leaveRequest = false
                                self.btnJoinProject.setImage(UIImage(named: "ic_key"), for: .normal)
                                self.projectMemberTableView.reloadData()
                            
                                if self.selectedProjectMemberlist.count == 0 {
                                    self.isMemberRecordFound.isHidden = false
                                }else {
                                    self.isMemberRecordFound.isHidden = true
                                }
                                print("Data Fetched")
                           }else {
                                print("Data couldn't Fetch")
                           }
                           self.loaderView.isHidden = true
                       }
                   }else{
                       self.loaderView.isHidden = true
                       print("Group couldn't remove")
                   }
               }
            
            
        }else {
            
            //for submit join request for project
            let param = ["user_id":userId,"project_id":projectId] as [String:Any]
            removeMemberApi(url: submitProjectRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                   if status {
                       self.pendingProjectListApi(url: (projectPendingListUrl + self.projectId), method: responseType.get.rawValue, param:nil, headers: nil) { (status) in
                           if status{
                            
                                for i in self.projectPendingRequestModel {
                                    if self.userId == Int(i.userId ?? "") {
                                        self.cancelRequest = true
                                        self.leaveRequest = false
                                        self.btnJoinProject.setImage(UIImage(named: "ic_exit"), for: .normal)
                                    }
                                }
                            
                                self.showToast(message: "Join request has been sent")
                                self.loaderView.isHidden = true
                                self.pendingAccessTableView.reloadData()
                            
                               if self.userRole == "A" {
                                   if self.projectPendingRequestModel.count == 0 {
                                       self.isPendingListFound.isHidden = true
                                        self.pendingAccessTableView.isHidden = true
                                        self.viewPending.isHidden = true
                                   }else {
                                       self.isPendingListFound.isHidden = true
                                        self.pendingAccessTableView.isHidden = false
                                        self.viewPending.isHidden = false
                                   }
                               }
                               
                           }else{
                               self.loaderView.isHidden = true
                           }
                       }
                   }else{
                       self.loaderView.isHidden = true
                   }
               }
            
            
        }
        
        
    }
/*-----------------------------------------------------------------------------------------------------*/
    @IBAction func homeBtn(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    //MARK - calenderButtonPressed
    @IBAction func calenderButtonPressed(_ sender: UIButton) {
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - addMemberButtonPressed
    @IBAction func addMemberButtonPressed(_ sender: UIButton) {
       let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddMemberInProjectViewController") as! AddMemberInProjectViewController
        vc.projectId = projectId
        self.navigationController?.pushViewController(vc, animated: true)
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - editProjectButtonPressed
    @IBAction func editProjectButtonPressed(_ sender: UIButton) {
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func removeGroupBtn(sender : UIButton) {
        let id = selectedProjectMemberlist[sender.tag].id
        let param = ["user_id":id!,"project_id":projectId] as [String : Any]
        removeMemberApi(url: removeMemberFromProjectUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                print("Group has been removed")
                self.memberProjectList(url: (projectMemberListUrl + self.projectId), method: responseType.get.rawValue, headers: nil) { (status) in
                    if status {
                        
                        var isMatched = true
                        for i in self.selectedProjectMemberlist {
                            if self.userId == Int(i.id ?? "") {
                               isMatched = false
                            }
                        }
                       
                        if isMatched {
                           self.leaveRequest = false
                           self.cancelRequest = false
                           self.btnJoinProject.setImage(UIImage(named: "ic_key"), for: .normal)
                        }
                        
                        if self.selectedProjectMemberlist.count == 0 {
                            self.isMemberRecordFound.isHidden = false
                        }else {
                            self.isMemberRecordFound.isHidden = true
                        }
                        self.projectMemberTableView.reloadData()
                        print("Data Fetched")
                    }else {
                        print("Data couldn't Fetch")
                    }
                    self.loaderView.isHidden = true
                }
            }else{
                self.loaderView.isHidden = true
                print("Group couldn't remove")
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @objc func approveBtn(sender : UIButton) {
        print(sender.tag)
        let userId = projectPendingRequestModel[sender.tag].userId
        let projectId = projectPendingRequestModel[sender.tag].projectId
        let param = ["project_id":projectId!,"user_id":userId!] as [String:Any]
        removeMemberApi(url: acceptProjectPendingRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.pendingProjectListApi(url: (projectPendingListUrl + self.projectId), method: responseType.get.rawValue, param:nil, headers: nil) { (status) in
                    if status{
                        
                        self.leaveRequest = true
                        self.cancelRequest = false
                        self.btnJoinProject.setImage(UIImage(named: "leave"), for: .normal)
                        
                        if self.userRole == "A" {
                            if self.projectPendingRequestModel.count == 0 {
                                self.isPendingListFound.isHidden = true
                                self.pendingAccessTableView.isHidden = true
                                self.viewPending.isHidden = true
                            }else {
                                self.isPendingListFound.isHidden = true
                                self.pendingAccessTableView.isHidden = false
                                self.viewPending.isHidden = false
                            }
                        }
                        self.memberProjectList(url: (projectMemberListUrl + self.projectId), method: responseType.get.rawValue, headers: nil) { (status) in
                            if status {
                                if self.selectedProjectMemberlist.count == 0 {
                                    self.isMemberRecordFound.isHidden = false
                                }else {
                                    self.isMemberRecordFound.isHidden = true
                                }
                                self.loaderView.isHidden = true
                                self.projectMemberTableView.reloadData()
                        /*----------------------------------------------------------------------------------------*/
                                print("Data Fetched")
                            }else {
                                self.loaderView.isHidden = true
                                print("Data couldn't Fetch")
                            }
                        }
                        self.pendingAccessTableView.reloadData()
                        print("Pending list has been loaded")
                    }else{
                        self.loaderView.isHidden = true
                        print("Pending list couldn't load")
                    }
                }
            }else {
                print("Request couldn't accept")
                self.loaderView.isHidden = true
            }
        }
        
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @objc func rejectBtn(sender : UIButton) {
        let id = projectPendingRequestModel[sender.tag].userId
        let param = ["user_id":id!,"project_id":projectId] as [String:Any]
        removeMemberApi(url: cancelProjectRequestUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.pendingProjectListApi(url: (projectPendingListUrl + self.projectId), method: responseType.get.rawValue, param:nil, headers: nil) { (status) in
                    if status{
                        
                        self.leaveRequest = false
                        self.cancelRequest = false
                        self.btnJoinProject.setImage(UIImage(named: "ic_key"), for: .normal)
                        self.loaderView.isHidden = true
                        self.pendingAccessTableView.reloadData()
                        
                        if self.userRole == "A" {
                            if self.projectPendingRequestModel.count == 0 {
                                self.isPendingListFound.isHidden = true
                                self.pendingAccessTableView.isHidden = true
                                self.viewPending.isHidden = true
                            }else {
                                self.isPendingListFound.isHidden = true
                                self.pendingAccessTableView.isHidden = false
                                self.viewPending.isHidden = false
                            }
                        }
                      
                    }else{
                        self.loaderView.isHidden = true
                    }
                }
            }else{
                self.loaderView.isHidden = true
            }
        }
        
    }
 /*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        userRole = userData["role"] as! String
        
        if userRole == "A" {
            if projectReadOnlyStatus.localizedCaseInsensitiveContains("r") {
                btnJoinProject.isHidden = true
                btnAddMember.isHidden = true
            }else {
                btnJoinProject.isHidden = false
                btnAddMember.isHidden = false
                viewPending.isHidden = false
                pendingAccessTableView.isHidden = false
            }
          
        }else {
            if projectReadOnlyStatus.localizedCaseInsensitiveContains("r") {
                btnJoinProject.isHidden = true
            }else {
                btnJoinProject.isHidden = false
            }
            btnAddMember.isHidden = true
            isPendingListFound.isHidden = true
            viewPending.isHidden = true
            pendingAccessTableView.isHidden = true
          
        }
        
        loadApi()
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func loadApi() {
        let param = ["project_id":projectId,"page_no":"1","pagesize":"10","UserId":userId] as [String:Any]
        viewProjectApi(url: detailViewProjectUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status{
                self.relatedProjectTableView.reloadData()
                
                if self.projectTagsModel.count == 3 {
                    if self.projectTagsModel[1].tagsName == ""{
                        self.lblTargetAnnual.text = "No Tag Found!"
                    }else {
                        self.lblTargetAnnual.text = self.projectTagsModel[1].tagsName
                    }
                    if self.self.projectTagsModel[0].tagsName == ""{
                        self.lblTargetAudiance.text = "No Tag Found!"
                    }else {
                        self.lblTargetAudiance.text = self.projectTagsModel[0].tagsName
                    }
                    
                    if self.projectTagsModel[2].tagsName == ""{
                        self.lblCharity.text = "No Tag Found!"
                    }else {
                        self.lblCharity.text = self.projectTagsModel[2].tagsName
                    }
                }
                
                if self.projectGroupModel.count == 0 {
                    self.isGroupRecordFound.isHidden = false
                }else {
                    self.isGroupRecordFound.isHidden = true
                }
                
                self.projectTitle.text = self.projectInfoModel.projectsName
                self.lblProjectName.text = self.projectInfoModel.projectsName
                self.lblCity.text = "\(self.projectInfoModel.projectsCity ?? "") \(self.projectInfoModel.projectsRegion  ?? "") \(self.projectInfoModel.projectsCountry  ?? "")"
                self.lblDate.text = self.projectInfoModel.projectsCreated
                self.lblType.text = self.projectInfoModel.projectsStatus
                self.lblDescription.text = self.projectInfoModel.projectsDescription
                
                
                self.memberAndPending()
                /*----------------------------------------------------------------------------------------*/
                print("Data Fetched")
            }else{
                self.memberAndPending()
                self.loaderView.isHidden = true
                print("Data couldn't fetch")
            }
            
        }
        
    }
/*-----------------------------------------------------------------------------------------------------*/

    func memberAndPending() {
        //MemberList Api
       self.memberProjectList(url: (projectMemberListUrl + self.projectId), method: responseType.get.rawValue, headers: nil) { (status) in
           if status {
               
               if self.selectedProjectMemberlist.count == 0 {
                   self.isMemberRecordFound.isHidden = false
               }else {
                   self.isMemberRecordFound.isHidden = true
               }
               self.projectMemberTableView.reloadData()
               self.pendingRequest()
               print("Data Fetched")
           }else {
               self.pendingRequest()
               self.loaderView.isHidden = true
               print("Data couldn't Fetch")
           }
       }
    }
/*-----------------------------------------------------------------------------------------------------*/

    func pendingRequest() {
        //Pending Api
       self.pendingProjectListApi(url: (projectPendingListUrl + self.projectId), method: responseType.get.rawValue, param:nil, headers: nil) { (status) in
           if status{
            
                for i in self.projectPendingRequestModel {
                    if self.userId == Int(i.userId ?? "") {
                        self.cancelRequest = true
                        self.leaveRequest = false
                        self.btnJoinProject.setImage(UIImage(named: "ic_exit"), for: .normal)
                    }
                }
                
                for i in self.selectedProjectMemberlist {
                    if self.userId == Int(i.id ?? "") {
                        self.cancelRequest = false
                        self.leaveRequest = true
                        self.btnJoinProject.setImage(UIImage(named: "leave"), for: .normal)
                    }
                }
                
                if !self.cancelRequest && !self.leaveRequest {
                    self.cancelRequest = false
                    self.leaveRequest = false
                    self.btnJoinProject.setImage(UIImage(named: "ic_key"), for: .normal)
                }
                
               if self.userRole == "A" {
                   if self.projectPendingRequestModel.count == 0 {
                       self.isPendingListFound.isHidden = true
                        self.pendingAccessTableView.isHidden = true
                        self.viewPending.isHidden = true
                   }else {
                       self.isPendingListFound.isHidden = true
                        self.pendingAccessTableView.isHidden = false
                        self.viewPending.isHidden = false
                   }
               }
               
               self.loaderView.isHidden = true
               self.pendingAccessTableView.reloadData()
               print("Pending list has been loaded")
           }else{
               self.loaderView.isHidden = true
               print("Pending list couldn't load")
           }
       }
        
    }
/*-----------------------------------------------------------------------------------------------------*/

    func registerTableViewCell ()
    {
        self.projectMemberTableView.register(UINib(nibName: "ProjectAssociatedGroupsTableViewCell", bundle: nil), forCellReuseIdentifier: "AssociatedGroupCell")
//       projectMemberTableView.register(UINib(nibName: "ProjectViewCell", bundle: nil), forCellReuseIdentifier: "ProjectViewCell")
        relatedProjectTableView.register(UINib(nibName: "ProjectViewCell", bundle: nil), forCellReuseIdentifier: "ProjectViewCell")
        pendingAccessTableView.register(UINib(nibName: "PendingAccessTableViewCell", bundle: nil), forCellReuseIdentifier: "PARCell")
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
 
/*-----------------------------------------------------------------------------------------------------------------------------------*/

func viewProjectApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        projectUserModel.removeAll()
        projectTagsModel.removeAll()
        projectNotesModel.removeAll()
        projectGroupModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
//                    if let projectUsers = JSON["project_users"] as? [[String:Any]] {
//                        for i in projectUsers {
//                            self.projectUserModel.append(ProjectUserModel(data: [i]))
//                        }
//                        print("project User data fetched")
//                    }
                    if let projectTags = JSON["project_tags"] as? [[String:Any]] {
                        for i in projectTags {
                            self.projectTagsModel.append(ProjectTagModel(data: [i]))
                        }
                        print("project Tags fetched")
                    }
//                    if let projectNotes = JSON["project_notes"] as? [[String:Any]] {
//                        for i in projectNotes {
//                            self.projectNotesModel.append(ProjectNotesModel(data: [i]))
//                        }
//                        print("project Notes data fetched")
//                    }
                    if let projectGroup = JSON["project_group"] as? [[String:Any]] {
                        for i in projectGroup {
                            self.projectGroupModel.append(ProjectGroupModel(data: [i]))
                        }
                        print("project Group data fetched")
                    }
                    if let projectInfo = JSON["project_info"] as? NSDictionary {
                        
                        self.projectInfoModel = ProjectInfoModel(data: projectInfo)
                        print("projectInfo data fetched")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")

            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func memberProjectList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        fullProjectMemberlist.removeAll()
        selectedProjectMemberlist.removeAll()
        Api.sharedInstance.sendData(url: url, param: nil, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.fullProjectMemberlist.append(ProjectMemberListModel(data: [i]))
                        }
                        
                        for i in self.fullProjectMemberlist {
                            if i.ismember == 1 {
                                self.selectedProjectMemberlist.append(i)
                            }
                        }
                        
                        
                    }else {
                        print("in response Error")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func pendingProjectListApi(url:String,method:HTTPMethod,param:[String:Any]?,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        projectPendingRequestModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: nil, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.projectPendingRequestModel.append(ProjectPendingRequestModel(data: [i]))
                        }
                    }else {
                        print("in pending response Error")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func removeMemberApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == projectMemberTableView{
            return selectedProjectMemberlist.count
        }else if tableView == relatedProjectTableView{
            return projectGroupModel.count
        }else if tableView == pendingAccessTableView{
            return projectPendingRequestModel.count
        }
        return 0
    }
/*-----------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = relatedProjectTableView.dequeueReusableCell(withIdentifier: "ProjectViewCell") as! ProjectViewCell
        
        if tableView == projectMemberTableView {
            
            let cell = self.projectMemberTableView.dequeueReusableCell(withIdentifier: "AssociatedGroupCell", for: indexPath) as! ProjectAssociatedGroupsTableViewCell
            cell.lblAssociatedGroupName?.attributedText = NSAttributedString(string: "\(selectedProjectMemberlist[indexPath.row].title ?? "") \(selectedProjectMemberlist[indexPath.row].firstName ?? "") \(selectedProjectMemberlist[indexPath.row].lastName ?? "")", attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            if self.userRole == "A" {
                cell.deleteBtn.isHidden = false
            }else {
                cell.deleteBtn.isHidden = true
            }
            
            cell.groupImage.layer.cornerRadius = cell.groupImage.frame.width/2
            let remoteImageUrlString = selectedProjectMemberlist[indexPath.row].profileImage ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.groupImage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
            
            cell.deleteBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(self.removeGroupBtn), for: .touchUpInside)
            return cell
        
        }else if tableView == pendingAccessTableView {
        
            let cell = self.pendingAccessTableView.dequeueReusableCell(withIdentifier: "PARCell", for: indexPath) as! PendingAccessTableViewCell
            cell.memberName.text = projectPendingRequestModel[indexPath.row].fullName
            cell.approveBtn.tag = indexPath.row
            cell.rejectBtn.tag = indexPath.row
            cell.approveBtn.addTarget(self, action: #selector(approveBtn), for: .touchUpInside)
            cell.rejectBtn.addTarget(self, action: #selector(rejectBtn), for: .touchUpInside)
            return cell
        
        }else if tableView == relatedProjectTableView {
        
            cell.title.text = projectGroupModel[indexPath.row].groupsName
            cell.content.text = projectGroupModel[indexPath.row].groupsDescription
            
            cell.imgProjectGroup.layer.cornerRadius = cell.imgProjectGroup.frame.width/2
            let remoteImageUrlString = projectGroupModel[indexPath.row].groupsImage ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.imgProjectGroup.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
               print("image \(indexPath.row) loaded")
            })
            
        }
        
        return cell
    }
/*-----------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == relatedProjectTableView {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewGroupViewController") as! ViewGroupViewController
            vc.groupsId = projectGroupModel[indexPath.row].groupsID ?? ""
            vc.aboutUs = projectGroupModel[indexPath.row].groupsBio ?? ""
            vc.address = projectGroupModel[indexPath.row].groupsAddress ?? ""
            vc.email = projectGroupModel[indexPath.row].groupsEmail ?? ""
            vc.phoneNumber = projectGroupModel[indexPath.row].groupsPhone ?? ""
            vc.groupName = projectGroupModel[indexPath.row].groupsName ?? ""
            vc.groupDescriptionName = projectGroupModel[indexPath.row].groupsDescription ?? ""
            vc.profileImage = projectGroupModel[indexPath.row].groupsImage ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if tableView == projectMemberTableView {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProfileViewController") as! ViewProfileViewController
            vc.userId = Int(selectedProjectMemberlist[indexPath.row].id ?? "0") ?? 0
            vc.isLoginMember = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
