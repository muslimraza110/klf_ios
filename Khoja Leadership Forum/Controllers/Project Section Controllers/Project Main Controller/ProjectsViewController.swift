
import UIKit
import Alamofire
import SwiftGifOrigin


class ProjectsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Btn Outlets
    @IBOutlet weak var projectListTableView: UITableView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var deleteAlert: UIView!
    
    //MARK:- Custom Variables
    var projectList = [ProjectListModel]()
    var Status = false
    var indexofArray = 0
    var projectId = ""
    var userRole = ""
    var createdByUserId = ""
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        registerTableViewCell()
        self.navigationController?.isNavigationBarHidden = true
        self.projectListTableView.delegate = self
        self.projectListTableView.dataSource = self
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    override func viewWillAppear(_ animated: Bool) {
        
        deleteAlert.isHidden = true
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        createdByUserId = String(userData["id"] as! Int)
        
        projectListApi(url: projectListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status{
                self.projectListTableView.reloadData()
                self.loaderView.isHidden = true
                print("project list has been updated")
            }else {
                self.projectListTableView.reloadData()
                self.loaderView.isHidden = true
                print("project list couldn't update")
            }
        }
        
    }
    
    //MARK:- Btn Actions
    @IBAction func addProjectBtn(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddProjectViewController") as? AddProjectViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)

    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func hideDeleteBtn(_ sender: Any) {
        deleteAlert.isHidden = true
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func deleteProjectBtn(_ sender: Any) {
        deleteAlert.isHidden = true
        let param = ["project_id":projectId] as [String:Any]
        deleteProject(url: deleteProjectUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
            if status {
                self.projectListApi(url: projectListUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
                    if status{
                        self.projectListTableView.reloadData()
                        print("project list has been updated")
                    }else {
                        print("project list couldn't update")
                    }
                    self.loaderView.isHidden = true
                    self.projectListTableView.reloadData()
                }
                print("project has been deleted")
            }else {
                print("project couldn't delete")
                self.loaderView.isHidden = true
                
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func deleteProjectBtn(sender : UIButton) {
        projectId = projectList[sender.tag].projectsID ?? ""
        deleteAlert.isHidden = false
        
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func editProjectBtn(sender : UIButton) {
        let projectListData = projectList[sender.tag]
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProjectViewController") as! EditProjectViewController
        vc.projectListData = projectListData
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.projectListTableView.deselectRow(at: indexPath, animated: true)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProjectViewController") as! ViewProjectViewController
        vc.projectId = projectList[indexPath.row].projectsID!
        vc.projectReadOnlyStatus = projectList[indexPath.row].projectsStatus ?? "r"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 100
        }else{
            return 140
        }
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectList.count
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.projectListTableView.dequeueReusableCell(withIdentifier: "ProjectCell", for: indexPath) as! ProjectListTableViewCell
        cell.lblCity.text = projectList[indexPath.row].projectsCity
        cell.lblCreatedBy.text = projectList[indexPath.row].projectsCreated
        cell.lblProjecttitle.text = projectList[indexPath.row].projectsName
        
        if userRole == "A" {
            cell.deleteBtn.tag = indexPath.row
            cell.editBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(self.deleteProjectBtn), for: .touchUpInside)
            cell.editBtn.addTarget(self, action: #selector(self.editProjectBtn), for: .touchUpInside)
        }else {
            if createdByUserId == projectList[indexPath.row].projectsCreatorID {
                cell.deleteBtn.isHidden = false
                cell.deleteImg.isHidden = false
                cell.editBtn.isHidden = false
                cell.editImg.isHidden = false
                cell.deleteBtn.tag = indexPath.row
                cell.editBtn.tag = indexPath.row
                cell.deleteBtn.addTarget(self, action: #selector(self.deleteProjectBtn), for: .touchUpInside)
                cell.editBtn.addTarget(self, action: #selector(self.editProjectBtn), for: .touchUpInside)
            }else {
                cell.deleteBtn.isHidden = true
                cell.editBtn.isHidden = true
                cell.editImg.isHidden = true
                cell.deleteImg.isHidden = true
            }
        }

        return cell
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    //MARK:- Custom Functions
    func registerTableViewCell ()
    {
        self.projectListTableView.register(UINib(nibName: "ProjectListTableViewCell", bundle: nil), forCellReuseIdentifier: "ProjectCell")
    }
        
/*-----------------------------------------------------------------------------------------------------------------------------------*/
        
    func projectListApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
            loaderView.isHidden = false
            projectList.removeAll()
            Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
                if status {
                    self.Status = false
                    let JSON = JSON
                    let status = JSON["status"] as? String
                    if status == "success" {
                        self.Status = true
                        if let data = JSON["data"] as? [[String:Any]] {
                            for i in data {
                                self.projectList.append(ProjectListModel(data: [i]))
                            }
                            print("Project list data fetched")
                        }else {
                            print("Project list didn't fetch")
                        }
                    }else {
                        self.Status = false
                    }
                    completion(self.Status)
                }else {
                    self.loaderView.isHidden = true
                    print("internet is unavailable")
                   
                }
            }
        }
        
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func deleteProject(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
            loaderView.isHidden = false
            Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
                if status {
                    self.Status = false
                    let JSON = JSON
                    let status = JSON["status"] as? String
                    if status == "success" {
                        self.Status = true
                    }else {
                        self.Status = false
                    }
                    completion(self.Status)
                }else {
                    self.loaderView.isHidden = true
                    print("internet is unavailable")
                }
            }
        }
     
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
        
        
}
 
