
import UIKit

class ProjectAssociatedGroupsTableViewCell: UITableViewCell {

    //Outlets
    
    @IBOutlet weak var lblAssociatedGroupName: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var groupImage: UIImageView!
    

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
}
