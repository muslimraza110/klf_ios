
import UIKit

//protocol MainProjectScreenTableViewCellDelegate{
//    func didTapEditProject()
//    func didTapDeleteProject()
//
//}
/*-----------------------------------------------------------------------------------------------------------------------------------*/

class ProjectListTableViewCell: UITableViewCell {

    //Outlets
    
    @IBOutlet weak var lblProjecttitle: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editImg: UIImageView!
    @IBOutlet weak var deleteImg: UIImageView!
    
//    var delegate: MainProjectScreenTableViewCellDelegate?
//    var memberItem: Member!
//
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
//    func setMember(member: Member){
//        memberItem = member
//        lblProjecttitle.text = member.memberName
//        lblCreatedBy.text = member.createdBy
//        lblCity.text = member.city
//    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func deleteProjectButtonPressed(_ sender: UIButton) {
//        print("Project delete button pressed")
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func editProjectButtonPressed(_ sender: UIButton) {
//        print("Project edit button pressed")
//        delegate?.didTapEditProject()
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
}
