
import UIKit
import Alamofire
import SwiftGifOrigin

class PercentageViewController: UIViewController, UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
   //Outlets
  
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    
    
    var txtfielsPercentage = [[String:String]]()
    var projectId = ""
    var percentageValue = 0
    var percentKey = [String:Any]()
    var percentArray = [[String:Any]]()
    var Status = false
    var listOfGroupProjectModel = [ListOfGroupProjectModel]()
    var selectedGroupInProject = [ListOfGroupProjectModel]()
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func savePercentageButtonPressed(_ sender: UIButton) {
        percentageValue = 0
        percentArray.removeAll()

        for i in txtfielsPercentage {
            let a = i
            for n in a {
                percentKey.removeAll()
                if n.value != "" {
                    let txtFieldPercentage = Int(n.value)
                    percentageValue = percentageValue + txtFieldPercentage!
                    if txtFieldPercentage! > 0 && txtFieldPercentage! <= 100{
                        if percentageValue <= 100 {
                            percentKey["groupId"] = n.key
                            percentKey["percentageText"] = n.value
                            percentArray.append(percentKey)
                        }
                    }
                }
            }
        }


        //String Joining
        var strGroupId = [String]()
        var strPercentage = [String]()
        for i in 0..<percentArray.count {
            strGroupId.append(percentArray[i]["groupId"] as! String)
            strPercentage.append(percentArray[i]["percentageText"] as! String)
        }
        let combinedGroupId = strGroupId.joined(separator: ",")
        let combinedPercentageValue = strPercentage.joined(separator: ",")

        //API Calling
        print("Percentage value received \(percentageValue)")
        if percentageValue == 100 {
            let param = [ "project_id": "5","groups_id":combinedGroupId,"percentagevalue":combinedPercentageValue] as [String:Any]
            addPercentage(url: addPercentageUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print("Added")
                }else {
                    print("Didn't Add")
                }
                self.loaderView.isHidden = true
                self.navigationController?.popViewController(animated: true)
            }
        }else {
            self.showToast(message: "Percentage must be equal to 100")
        }
//
        print("save percentage button pressed")
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*----------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        percentArray.removeAll()
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        loadApi()
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - Dissmiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {

    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func loadApi(){
        
        getGroupProjectList(url: (listOfGroupProjectUrl + projectId), method: responseType.get.rawValue, headers: headerToken) { (status) in
            if status {
                print("GroupProject list has been come")
            }else {
                print("GroupProject list couldn't load")
            }
            self.tableView.reloadData()
            self.loaderView.isHidden = true
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func getGroupProjectList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        listOfGroupProjectModel.removeAll()
        selectedGroupInProject.removeAll()
        Api.sharedInstance.sendData(url: url, param: nil, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.listOfGroupProjectModel.append(ListOfGroupProjectModel(data: [i]))
                        }
                        
                        for i in self.listOfGroupProjectModel {
                            if i.isSelected == 1 {
                                self.selectedGroupInProject.append(i)
                            }
                        }
                        
                        
                    }else {
                        print("in response Error")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func addPercentage(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func textFieldDidEndEditing(_ textField: UITextField) {

        var tempFlag = true
        let containeddictionary  = [String(textField.tag):textField.text]
        for singleDictionary in containeddictionary {
            if txtfielsPercentage.count == 0 {
                txtfielsPercentage.append([singleDictionary.key:singleDictionary.value ?? ""])
            }else {
                for (index, element) in txtfielsPercentage.enumerated() {
                    for arraySingleDictionary in element {
                        if arraySingleDictionary.key == singleDictionary.key {
                        tempFlag = false
                        txtfielsPercentage[index] = [arraySingleDictionary.key:singleDictionary.value] as! [String : String]
                        }
                    }
                }
                if tempFlag {
                    txtfielsPercentage.append([singleDictionary.key:singleDictionary.value ?? ""])
                }
            }
        }
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedGroupInProject.count
    }
/*-----------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PercentageCell") as! PercentageCell
        cell.txtFieldPercentage.delegate = self
        cell.txtFieldPercentage.tag = Int(selectedGroupInProject[indexPath.row].groupID!)!
        cell.groupName.text = selectedGroupInProject[indexPath.row].name
        return cell
    }
/*-----------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 80
        }else{
            return 100
        }
        
    }
/*-----------------------------------------------------------------------------------------------------*/
}
