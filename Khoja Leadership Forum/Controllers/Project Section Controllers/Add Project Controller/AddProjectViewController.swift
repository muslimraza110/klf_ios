
import UIKit
import Alamofire
import SwiftGifOrigin
import FSCalendar

class AddProjectViewController: UIViewController, UITextFieldDelegate,FSCalendarDelegate,FSCalendarDataSource {

    //MARK:- Btn Outlets
    @IBOutlet weak var txtFieldProjectName: UITextField!
    @IBOutlet weak var txtFieldWebsite: UITextField!
    @IBOutlet weak var txtFieldDescription: UITextField!
    @IBOutlet weak var txtFieldStartDate: UITextField!
    @IBOutlet weak var txtFieldEndDate: UITextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldCountry: UITextField!
    @IBOutlet weak var txtFieldCharityType: UITextField!
    @IBOutlet weak var txtFieldTargetAudience: UITextField!
    @IBOutlet weak var txtFieldTargetAnnualSpend: UITextField!
    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var webTextFieldOuterView: UIView!
    @IBOutlet weak var webTextFieldInnerView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var charityTableView: UITableView!
    @IBOutlet weak var audianceTableView: UITableView!
    @IBOutlet weak var annualTableView: UITableView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var calanderView: UIView!
    @IBOutlet weak var calander: FSCalendar!
    
    //MARK:- Custom Variables
    var isStarDateCalanderOpen = false
    var creatorId = 0
    var charityId = ""
    var groupId = ""
    var projectId = ""
    var targetAudiance = ""
    var targetAnnual = ""
    var Status = false
    var nameFieldFlag = false
    var descriptionFieldFlag = false
    var startDateFieldFlag = false
    var cityFieldFlag = false
    var countryFieldFlag = false
    var isGroupAddProject = false
    var charityModel = [CharityModel]()
    var targetAudianceModel = [TargetAudianceModel]()
    var targetAnnualModel = [TargetAnnualModel]()
    var listOfGroupProjectModel = [ListOfGroupProjectModel]()
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    //MARK: - Life Cycle Methods
     override func viewDidLoad() {
         super.viewDidLoad()

         calanderView.isHidden = true
         let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
         let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
         creatorId = userData["id"] as! Int
         loaderView.isHidden = true
         loaderImg.image = UIImage.gif(name: "loader_gif")
         charityTableView.isHidden = true
         audianceTableView.isHidden = true
         annualTableView.isHidden = true
         initialLayout()
         headerHeight()
         charityTableView.register(UINib(nibName: "CategoryDropDownCell", bundle: nil), forCellReuseIdentifier: "CategoryDropDownCell")
         audianceTableView.register(UINib(nibName: "CategoryDropDownCell", bundle: nil), forCellReuseIdentifier: "CategoryDropDownCell")
         annualTableView.register(UINib(nibName: "CategoryDropDownCell", bundle: nil), forCellReuseIdentifier: "CategoryDropDownCell")
         
         
         charityApi(url: charityDropDownUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
             if status {
                 print("charity list updated")
             }else {
                 print("charity list couldn't update")
             }
             self.charityTableView.reloadData()
         }
         
         audienceApi(url: audianceDropDownUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
             if status {
                 print("audience list updated")
             }else {
                 print("audience list couldn't update")
             }
             self.audianceTableView.reloadData()
         }
         
         annualApi(url: annualDropDownUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
             if status {
                 print("annualApi list updated")
             }else {
                 print("annualApi list couldn't update")
             }
             self.loaderView.isHidden = true
             self.annualTableView.reloadData()
         }
     }
    
    //MARK:- Btn Actions
    @IBAction func saveProjectButtonPressed(_ sender: UIButton) {
        
        if textFieldFormatCheck() {
            print("all mendatory fields are defined")
            
            let param = ["creator_id":creatorId,"khoja_care_category_id":"1","name":txtFieldProjectName.text!,"website":txtFieldWebsite.text!, "description": txtFieldDescription.text!,"start_date":txtFieldStartDate.text!,"est_completion_date":txtFieldEndDate.text!,"city": txtFieldCity.text!,"country": txtFieldCountry.text!,"status": "a","charity_type":charityId,"targetaudience":targetAudiance,"targetanualspent":targetAnnual] as [String:Any]
            addProject(url: addProjectUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print("Project has been added")
                    if self.isGroupAddProject {
                        self.loadApi()
                    }else {
                        self.navigationController?.popViewController(animated: true)
                        self.loaderView.isHidden = true
                    }
                }else {
                    self.showToast(message: "Project couldn't add try again")
                     print("Project couldn't add")
                    self.loaderView.isHidden = true
                }
            }
        }
        else{
            print("one or more mendatory fields are empty.")
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func charityBtn(_ sender: Any) {
        charityTableView.isHidden = false
    }
  
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func audianceBtn(_ sender: Any) {
        audianceTableView.isHidden = false
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func annualBtn(_ sender: Any) {
        annualTableView.isHidden = false
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func showStartDateCalanderBtn(_ sender: Any) {
        isStarDateCalanderOpen = true
        calanderView.isHidden = false
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func showEndDateCalanderBtn(_ sender: Any) {
        isStarDateCalanderOpen = false
        calanderView.isHidden = false
        
    }
    
    //MARK:- Custom Functions
    func getGroupProjectList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        listOfGroupProjectModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: nil, method: method, header: headers) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.listOfGroupProjectModel.append(ListOfGroupProjectModel(data: [i]))
                        }
                    }else {
                        print("in response Error")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func addRemoveGroup(url:String,param:[String:Any],method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headers) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func loadApi(){
        getGroupProjectList(url: (listOfGroupProjectUrl + projectId), method: responseType.get.rawValue, headers: headerToken) { (status) in
            if status {
                for i in self.listOfGroupProjectModel {
                    if self.groupId == i.groupID {
                        let param = ["group_id":i.groupID ?? "","project_id":self.projectId] as [String : Any]
                        self.addRemoveGroup(url: addGroupInProjectUrl, param: param, method: responseType.post.rawValue, headers: headerToken) { (status) in
                            if status{
                                print("Group has been added in list")
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller is ViewGroupViewController  {
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }else {
                                print("Group couldn't add in list")
                            }
                            self.loaderView.isHidden = true
                        }
                    }
                }
        
                print("GroupProject list has been come")
            }else {
                self.loaderView.isHidden = true
                print("GroupProject list couldn't load")
            }
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func initialLayout() {
       
        // TextFields Delegate Settings
        self.txtFieldProjectName.delegate = self
        self.txtFieldWebsite.delegate = self
        self.txtFieldDescription.delegate = self
        self.txtFieldStartDate.delegate = self
        self.txtFieldEndDate.delegate = self
        self.txtFieldCity.delegate = self
        self.txtFieldCountry.delegate = self
        self.txtFieldCharityType.delegate = self
        self.txtFieldTargetAudience.delegate = self
        self.txtFieldTargetAnnualSpend.delegate = self
        
        self.txtFieldWebsite.leftViewMode = UITextField.ViewMode.always
        let leftViewForWebsite = UIView()
        leftViewForWebsite.frame = CGRect(x: 0, y: 0, width: 35, height: 15)
        self.txtFieldWebsite.leftView = leftViewForWebsite
        
        self.txtFieldCharityType.rightViewMode = .always
        let imageViewForCharityType = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldCharityType.contentMode = .scaleAspectFit
        imageViewForCharityType.image = UIImage(named: "dropdown.png")
        let rightViewForCharityType = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForCharityType.addSubview(imageViewForCharityType)
        self.txtFieldCharityType.rightView = rightViewForCharityType
        
        self.txtFieldTargetAudience.rightViewMode = .always
        let imageViewForTargetAudience = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldTargetAudience.contentMode = .scaleAspectFit
        imageViewForTargetAudience.image = UIImage(named: "dropdown.png")
        let rightViewForTargetAudience = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForTargetAudience.addSubview(imageViewForTargetAudience)
        self.txtFieldTargetAudience.rightView = rightViewForTargetAudience
        
        self.txtFieldTargetAnnualSpend.rightViewMode = .always
        let imageViewForTargetAnnualSpend = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldTargetAnnualSpend.contentMode = .scaleAspectFit
        imageViewForTargetAnnualSpend.image = UIImage(named: "dropdown.png")
        let rightViewForTargetAnnualSpend = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForTargetAnnualSpend.addSubview(imageViewForTargetAnnualSpend)
        self.txtFieldTargetAnnualSpend.rightView = rightViewForTargetAnnualSpend
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func textFieldFormatCheck() -> Bool {
        
       if self.txtFieldCountry.text?.count == 0 {
           self.txtFieldCountry.borderColor = .red
           self.txtFieldCountry.becomeFirstResponder()
           countryFieldFlag = false
       }
       else{countryFieldFlag = true}
       
       if self.txtFieldCity.text?.count == 0 {
           self.txtFieldCity.borderColor = .red
           self.txtFieldCity.becomeFirstResponder()
           cityFieldFlag = false
       }
       else{cityFieldFlag = true}
       
       if self.txtFieldStartDate.text?.count == 0 {
           self.txtFieldStartDate.borderColor = .red
           self.txtFieldStartDate.becomeFirstResponder()
           startDateFieldFlag = false
       }
       else{startDateFieldFlag = true}
       
       if self.txtFieldDescription.text?.count == 0 {
           self.txtFieldDescription.borderColor = .red
           self.txtFieldDescription.becomeFirstResponder()
           descriptionFieldFlag = false
       }
       else{descriptionFieldFlag = true}
       
       if self.txtFieldProjectName.text?.count == 0 {
           self.txtFieldProjectName.borderColor = .red
           self.txtFieldProjectName.becomeFirstResponder()
           nameFieldFlag = false
       }
       else{nameFieldFlag = true}
       
       if nameFieldFlag && descriptionFieldFlag && startDateFieldFlag && cityFieldFlag && countryFieldFlag == true {return true}
       else{return false}
   }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.containerView.endEditing(true)
    }
    
    //MARK:- TextField Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.becomeFirstResponder() {
            charityTableView.isHidden = true
            audianceTableView.isHidden = true
            annualTableView.isHidden = true
        }
    }
   
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.txtFieldProjectName {
            self.txtFieldProjectName.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        if textField == self.txtFieldWebsite {
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        if textField == self.txtFieldDescription {
            self.txtFieldDescription.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldStartDate {
            self.txtFieldStartDate.borderColor = .darkGray
            
            let maxLength = 40
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldEndDate {
            
            let maxLength = 40
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldCity {
            self.txtFieldCity.borderColor = .darkGray
            
            let maxLength = 80
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldCountry {
            self.txtFieldCountry.borderColor = .darkGray
            
            let maxLength = 300
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else{
            return true
        }
    }
   
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func addProject(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    let data = JSON["data"] as? NSDictionary
                    if let projectData = data {
                        let id = projectData["project_id"] as? String
                        self.projectId = id ?? ""
                    }
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
  
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func charityApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        charityModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.charityModel.append(CharityModel(data: [i]))
                        }
                        print("Charity list data fetched")
                    }else {
                        print("Charity list didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
    
    func audienceApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        targetAudianceModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.targetAudianceModel.append(TargetAudianceModel(data: [i]))
                        }
                        print("Audiance list data fetched")
                    }else {
                        print("Audiance list didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
    func annualApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        targetAnnualModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.targetAnnualModel.append(TargetAnnualModel(data: [i]))
                        }
                        print("Annual list data fetched")
                    }else {
                        print("Annual list didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}


extension AddProjectViewController:UITableViewDelegate,UITableViewDataSource{
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == charityTableView{
            return charityModel.count
        }else if tableView == audianceTableView {
            return targetAudianceModel.count
        }
        return targetAnnualModel.count
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryDropDownCell") as! CategoryDropDownCell
        if tableView == charityTableView{
            if charityModel[indexPath.row].tagsName == "Education" || charityModel[indexPath.row].tagsName == "Khoja Eradication of Poverty" || charityModel[indexPath.row].tagsName == "International Relief Aid" || charityModel[indexPath.row].tagsName == "Orphan Relief" || charityModel[indexPath.row].tagsName == "Water" || charityModel[indexPath.row].tagsName == "Medical" {
                
                cell.name.textAlignment = .left
                cell.name.font = UIFont.systemFont(ofSize: 19, weight: UIFont.Weight.bold)
                cell.name.text = "  - \(charityModel[indexPath.row].tagsName!)"
                
            }else {
                cell.name.textAlignment = .left
                cell.name.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                cell.name.text = "       \(charityModel[indexPath.row].tagsName!)"
            }
            
        }else if tableView == audianceTableView {
            cell.name.text = "      - \(targetAudianceModel[indexPath.row].tagsName!)"
        }else {
            cell.name.text = "      - \(targetAnnualModel[indexPath.row].tagsName!)"
        }
        return cell
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 35
        }else{
            return 50
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == charityTableView{
            charityTableView.isHidden = true
            charityId = charityModel[indexPath.row].tagsID ?? "9"
            txtFieldCharityType.text = charityModel[indexPath.row].tagsName
        }else if tableView == audianceTableView {
            targetAudiance = targetAudianceModel[indexPath.row].tagsID ?? "58"
            audianceTableView.isHidden = true
            txtFieldTargetAudience.text = targetAudianceModel[indexPath.row].tagsName
        }else if tableView == annualTableView {
            targetAnnual = targetAnnualModel[indexPath.row].tagsID ?? "50"
            annualTableView.isHidden = true
            txtFieldTargetAnnualSpend.text = targetAnnualModel[indexPath.row].tagsName
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        if isStarDateCalanderOpen {
            txtFieldStartDate.text = self.formatter.string(from: date)
        }else {
            txtFieldEndDate.text = self.formatter.string(from: date)
        }
        calanderView.isHidden = true
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
}
