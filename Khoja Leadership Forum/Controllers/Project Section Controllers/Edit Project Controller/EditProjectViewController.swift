
import UIKit
import Alamofire
import SwiftGifOrigin
import FSCalendar
import SDWebImage

class EditProjectViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource,FSCalendarDelegate,FSCalendarDataSource {

    //Outlets
    
    @IBOutlet weak var checkButtonStampOfApprovalOutlet: UIButton!
    @IBOutlet weak var checkButtonReadOnlyOutlet: UIButton!
    @IBOutlet weak var txtFieldKhojaCareCategory: UITextField!
    @IBOutlet weak var txtFieldSearchGroups: UITextField!
    @IBOutlet weak var txtFieldProjectName: UITextField!
    @IBOutlet weak var txtFieldWebsite: UITextField!
    @IBOutlet weak var txtFieldDescription: UITextField!
    @IBOutlet weak var txtFieldStartDate: UITextField!
    @IBOutlet weak var txtFieldEndDate: UITextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldCountry: UITextField!
    @IBOutlet weak var txtFieldCharityType: UITextField!
    @IBOutlet weak var txtFieldTargetAudience: UITextField!
    @IBOutlet weak var txtFieldTargetAnnualSpend: UITextField!
    @IBOutlet weak var charityTableView: UITableView!
    @IBOutlet weak var audianceTableView: UITableView!
    @IBOutlet weak var annualTableView: UITableView!
    @IBOutlet weak var searchResultTableView: UITableView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var dropDown: UIView!
    @IBOutlet weak var calanderView: UIView!
    @IBOutlet weak var calander: FSCalendar!
    @IBOutlet weak var isGroupProjectFound: UILabel!
    @IBOutlet weak var btnPercentage: AdaptiveButton!
    @IBOutlet weak var btnAddGroup: UIButton!
    
    
    // flags for textfield validations
    var isStarDateCalanderOpen = false
    var userRole = ""
    var KhojaCategoryId = ""
    var status = ""
    var stampApproval = ""
    var charityId = ""
    var targetAudiance = ""
    var targetAnnual = ""
    var Status = false
    var nameFieldFlag = false
    var descriptionFieldFlag = false
    var startDateFieldFlag = false
    var cityFieldFlag = false
    var countryFieldFlag = false
    var projectListData = ProjectListModel()
    var listOfGroupProjectModel = [ListOfGroupProjectModel]()
    var selectedGroupInProject = [ListOfGroupProjectModel]()
    var charityModel = [CharityModel]()
    var targetAudianceModel = [TargetAudianceModel]()
    var targetAnnualModel = [TargetAnnualModel]()
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func addGroup(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddGroupInProjectController") as! AddGroupInProjectController
        vc.projectId = projectListData.projectsID!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - editProjectSaveButtonPressed
    @IBAction func editProjectSaveButtonPressed(_ sender: UIButton) {
        
        if textFieldFormatCheck() {
            print("all mendatory fields are defined")
            
            let param = ["creator_id":projectListData.projectsCreatorID!,"khoja_care_category_id":KhojaCategoryId,"name":txtFieldProjectName.text!,"website":txtFieldWebsite.text!, "description": txtFieldDescription.text!,"start_date":txtFieldStartDate.text!,"est_completion_date":txtFieldEndDate.text!,"city": txtFieldCity.text!,"country": txtFieldCountry.text!,"status": status,"project_id":projectListData.projectsID!,"approval_stamp":stampApproval,"charity_type":charityId,"targetaudience":targetAudiance,"targetanualspent":targetAnnual] as [String:Any]
            editProject(url: editProjectUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    self.showToast(message: "Project has been edit")
                    print("Project has been edited")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                         self.navigationController?.popViewController(animated: true)
                    }
                }else {
                    self.showToast(message: "Project couldn't edit try again")
                    print("Project couldn't add")
                }
                self.loaderView.isHidden = true
            }
            
            //API Call for Edit Project needs to be added here with a custom loader
            
        }
        else{
            print("one or more mendatory fields are empty.")
        }
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - editProjectPercentageButtonPressed
    @IBAction func editProjectPercentageButtonPressed(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PercentageViewController") as? PercentageViewController
        vc?.projectId = projectListData.projectsID!
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func checkButtonStampOfApproval(_ sender: UIButton) {
        if sender.isSelected{
            stampApproval = "0"
            sender.isSelected = false
        }else {
            stampApproval = "1"
            sender.isSelected = true
        }
       
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func checkButtonReadOnly(_ sender: UIButton) {
        if sender.isSelected{
            status = "a"
            sender.isSelected = false
        }else {
            status = "r"
            sender.isSelected = true
        }

        if status == "r" {
            btnAddGroup.isHidden = true
        }else{
            btnAddGroup.isHidden = false
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------*/
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-------------------------------------------------------------------------------------------------------------------*/
    
    @objc func removeGroupBtn(sender : UIButton) {
        let id = selectedGroupInProject[sender.tag].groupID
         let param = ["group_id":id!,"project_id":projectListData.projectsID!] as [String : Any]
        removeGroup(url: removeGroupFromProjectUrl, param: param, method: responseType.post.rawValue, headers: headerToken) { (status) in
            if status {
                print("Group has been removed")
                self.loadApi()
            }else{
                print("Group couldn't remove")
            }
        }

    }
/*-------------------------------------------------------------------------------------------------------------------*/
    @IBAction func categoryHideBtn(_ sender: Any) {
        dropDown.isHidden = false
    }
/*-----------------------------------------------------------------------------------------------------*/
    @IBAction func categorySelect(_ sender: UIButton) {
        if sender.tag == 1 {
            txtFieldKhojaCareCategory.text = "Education For All"
            KhojaCategoryId = "1"
            dropDown.isHidden = true
        }else if  sender.tag == 2 {
            txtFieldKhojaCareCategory.text = "Eradication of Poverty"
            KhojaCategoryId = "2"
            dropDown.isHidden = true
        }
        else if  sender.tag == 3 {
            txtFieldKhojaCareCategory.text = "Humanitarian Releif"
            KhojaCategoryId = "3"
            dropDown.isHidden = true
        }
        else if  sender.tag == 4 {
            txtFieldKhojaCareCategory.text = "Other"
            KhojaCategoryId = "4"
            dropDown.isHidden = true
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func charityBtn(_ sender: Any) {
        charityTableView.isHidden = false
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func audianceBtn(_ sender: Any) {
        audianceTableView.isHidden = false
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func annualBtn(_ sender: Any) {
        annualTableView.isHidden = false
    }
/*-----------------------------------------------------------------------------------------------------*/
    @IBAction func showStartDateCalanderBtn(_ sender: Any) {
        isStarDateCalanderOpen = true
        calanderView.isHidden = false
        
    }
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func showEndDateCalanderBtn(_ sender: Any) {
        isStarDateCalanderOpen = false
        calanderView.isHidden = false
        
    }
/*-----------------------------------------------------------------------------------------------------*/

    override func viewDidLoad() {
        super.viewDidLoad()

        isGroupProjectFound.isHidden = true
        calanderView.isHidden = true
        dropDown.isHidden = true
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        registerTableViewCell()
        initialLayout()
        
        self.searchResultTableView.delegate = self
        self.searchResultTableView.dataSource = self

        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userRole = userData["role"] as! String
        if userRole != "A" {
            btnPercentage.isHidden = true
        }
        
        charityApi(url: charityDropDownUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                print("charity list updated")
            }else {
                print("charity list couldn't update")
            }
            self.charityTableView.reloadData()
        }
        
        audienceApi(url: audianceDropDownUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                print("audience list updated")
            }else {
                print("audience list couldn't update")
            }
            self.audianceTableView.reloadData()
        }
        
        annualApi(url: annualDropDownUrl, method: responseType.get.rawValue, param: nil, header: headerToken) { (status) in
            if status {
                print("annualApi list updated")
            }else {
                print("annualApi list couldn't update")
            }
            self.annualTableView.reloadData()
        }
        
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        loadApi()
        editDataOfProject()
        charityTableView.isHidden = true
        audianceTableView.isHidden = true
        annualTableView.isHidden = true
        
    }
/*-----------------------------------------------------------------------------------------------------*/
    func editDataOfProject () {
        txtFieldCity.text = projectListData.projectsCity
        txtFieldCountry.text = projectListData.projectsCountry
        txtFieldEndDate.text = projectListData.projectsEstCompletionDate
        txtFieldWebsite.text = projectListData.projectsWebsite
        txtFieldStartDate.text = projectListData.projectsStartDate
        txtFieldCharityType.text = projectListData.charityType.name
        txtFieldDescription.text = projectListData.projectsDescription
        txtFieldProjectName.text = projectListData.projectsName
        txtFieldTargetAudience.text = projectListData.targetAudience.name
        txtFieldTargetAnnualSpend.text = projectListData.targetAnnualSpend.name
        
        charityId = projectListData.charityType.id ?? "9"
        targetAudiance = projectListData.targetAudience.id ?? "58"
        targetAnnual = projectListData.targetAnnualSpend.id ?? "50"
        
        KhojaCategoryId = projectListData.projectsKhojaCareCategoryID ?? "1"
        if KhojaCategoryId == "1" {
            txtFieldKhojaCareCategory.text = "Education for all"
        }else if KhojaCategoryId == "2" {
            txtFieldKhojaCareCategory.text = "Eradication of Poverty"
        }else if KhojaCategoryId == "3" {
            txtFieldKhojaCareCategory.text = "Humanitarian Releif"
        }else if KhojaCategoryId == "4" {
            txtFieldKhojaCareCategory.text = "Other"
        }

      
        status = projectListData.projectsStatus ?? "a"
        if status == "a" {
             btnAddGroup.isHidden = false
            checkButtonReadOnlyOutlet.isSelected = false
        }else if status == "r" {
             btnAddGroup.isHidden = true
            checkButtonReadOnlyOutlet.isSelected = true
        }
        
        stampApproval = projectListData.projectsApprovalStamp ?? "0"
        if stampApproval == "1"{
            checkButtonStampOfApprovalOutlet.isSelected = true
        }else{
            checkButtonStampOfApprovalOutlet.isSelected = false
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - initialLayout
    func initialLayout() {
        
        // TextFields Delegate Settings
        self.txtFieldProjectName.delegate = self
        self.txtFieldWebsite.delegate = self
        self.txtFieldDescription.delegate = self
        self.txtFieldStartDate.delegate = self
        self.txtFieldEndDate.delegate = self
        self.txtFieldCity.delegate = self
        self.txtFieldCountry.delegate = self
        self.txtFieldCharityType.delegate = self
        self.txtFieldTargetAudience.delegate = self
        self.txtFieldTargetAnnualSpend.delegate = self
        
        
        self.txtFieldWebsite.leftViewMode = UITextField.ViewMode.always
        let leftViewForWebsite = UIView()
        leftViewForWebsite.frame = CGRect(x: 0, y: 0, width: 35, height: 15)
        self.txtFieldWebsite.leftView = leftViewForWebsite
        
        
        
        self.txtFieldCharityType.rightViewMode = .always
        let imageViewForCharityType = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldCharityType.contentMode = .scaleAspectFit
        imageViewForCharityType.image = UIImage(named: "dropdown.png")
        let rightViewForCharityType = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForCharityType.addSubview(imageViewForCharityType)
        self.txtFieldCharityType.rightView = rightViewForCharityType
        
        self.txtFieldTargetAudience.rightViewMode = .always
        let imageViewForTargetAudience = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldTargetAudience.contentMode = .scaleAspectFit
        imageViewForTargetAudience.image = UIImage(named: "dropdown.png")
        let rightViewForTargetAudience = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForTargetAudience.addSubview(imageViewForTargetAudience)
        self.txtFieldTargetAudience.rightView = rightViewForTargetAudience
        
        self.txtFieldTargetAnnualSpend.rightViewMode = .always
        let imageViewForTargetAnnualSpend = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldTargetAnnualSpend.contentMode = .scaleAspectFit
        imageViewForTargetAnnualSpend.image = UIImage(named: "dropdown.png")
        let rightViewForTargetAnnualSpend = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForTargetAnnualSpend.addSubview(imageViewForTargetAnnualSpend)
        self.txtFieldTargetAnnualSpend.rightView = rightViewForTargetAnnualSpend
        
        self.txtFieldKhojaCareCategory.rightViewMode = .always
        let imageViewForCategory = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.txtFieldKhojaCareCategory.contentMode = .scaleAspectFit
        imageViewForCategory.image = UIImage(named: "dropdown.png")
        let rightViewForCategory = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 12))
        rightViewForCategory.addSubview(imageViewForCategory)
        self.txtFieldKhojaCareCategory.rightView = rightViewForCategory
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - Dissmiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.becomeFirstResponder() {
            charityTableView.isHidden = true
            audianceTableView.isHidden = true
            annualTableView.isHidden = true
            dropDown.isHidden = true
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - textFieldFormatCheck
    // textfield format Check function
    func textFieldFormatCheck() -> Bool {
        
        if self.txtFieldCountry.text?.count == 0 {
            self.txtFieldCountry.borderColor = .red
            self.txtFieldCountry.becomeFirstResponder()
            countryFieldFlag = false
        }
        else{countryFieldFlag = true}
        
        if self.txtFieldCity.text?.count == 0 {
            self.txtFieldCity.borderColor = .red
            self.txtFieldCity.becomeFirstResponder()
            cityFieldFlag = false
        }
        else{cityFieldFlag = true}
        
        if self.txtFieldStartDate.text?.count == 0 {
            self.txtFieldStartDate.borderColor = .red
            self.txtFieldStartDate.becomeFirstResponder()
            startDateFieldFlag = false
        }
        else{startDateFieldFlag = true}
        
        if self.txtFieldDescription.text?.count == 0 {
            self.txtFieldDescription.borderColor = .red
            self.txtFieldDescription.becomeFirstResponder()
            descriptionFieldFlag = false
        }
        else{descriptionFieldFlag = true}
        
        if self.txtFieldProjectName.text?.count == 0 {
            self.txtFieldProjectName.borderColor = .red
            self.txtFieldProjectName.becomeFirstResponder()
            nameFieldFlag = false
        }
        else{nameFieldFlag = true}
        
        if nameFieldFlag && descriptionFieldFlag && startDateFieldFlag && cityFieldFlag && countryFieldFlag == true {return true}
        else{return false}
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - shouldChangeCharactersIn
    // TextFiled Delegate Functions when change characters in textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.txtFieldProjectName {
            self.txtFieldProjectName.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        if textField == self.txtFieldWebsite {
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        if textField == self.txtFieldDescription {
            self.txtFieldDescription.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldStartDate {
            self.txtFieldStartDate.borderColor = .darkGray
            
            let maxLength = 40
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldEndDate {
            
            let maxLength = 40
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldCity {
            self.txtFieldCity.borderColor = .darkGray
            
            let maxLength = 80
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == self.txtFieldCountry {
            self.txtFieldCountry.borderColor = .darkGray
            
            let maxLength = 300
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else{
            return true
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - didSelectRowAt
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == charityTableView{
            charityTableView.isHidden = true
            txtFieldCharityType.text = charityModel[indexPath.row].tagsName
            charityId = charityModel[indexPath.row].tagsID ?? "9"
        }else if tableView == audianceTableView {
            audianceTableView.isHidden = true
            txtFieldTargetAudience.text = targetAudianceModel[indexPath.row].tagsName
            targetAudiance = targetAudianceModel[indexPath.row].tagsID ?? "58"
        }else if tableView == annualTableView {
            annualTableView.isHidden = true
            txtFieldTargetAnnualSpend.text = targetAnnualModel[indexPath.row].tagsName
            targetAnnual = targetAnnualModel[indexPath.row].tagsID ?? "50"
        }
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
           if tableView == searchResultTableView {
                return 50
            }
            return 35
        }else{
            if tableView == searchResultTableView {
                return 65
            }
            return 50
        }
        
    }
    
    // return number of section in a tableview
    //MARK - numberOfSections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    // return number of rows in a section
    //MARK - numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == charityTableView{
            return charityModel.count
        }else if tableView == audianceTableView {
            return targetAudianceModel.count
        }else if tableView == annualTableView {
            return targetAnnualModel.count
        }else if tableView == searchResultTableView {
            return selectedGroupInProject.count
        }else {
            return 0
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == searchResultTableView {
            let cell = self.searchResultTableView.dequeueReusableCell(withIdentifier: "AssociatedGroupCell", for: indexPath) as! ProjectAssociatedGroupsTableViewCell
            cell.lblAssociatedGroupName?.text = selectedGroupInProject[indexPath.row].name
            cell.deleteBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(self.removeGroupBtn), for: .touchUpInside)
            
            cell.groupImage.layer.cornerRadius = cell.groupImage.frame.width/2
            let remoteImageUrlString = selectedGroupInProject[indexPath.row].profileImg ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.groupImage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
               print("image \(indexPath.row) loaded")
            })
            
        return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryDropDownCell") as! CategoryDropDownCell
            if tableView == charityTableView{
                if charityModel[indexPath.row].tagsName == "Education" || charityModel[indexPath.row].tagsName == "Khoja Eradication of Poverty" || charityModel[indexPath.row].tagsName == "International Relief Aid" || charityModel[indexPath.row].tagsName == "Orphan Relief" || charityModel[indexPath.row].tagsName == "Water" || charityModel[indexPath.row].tagsName == "Medical" {
                    
                    cell.name.textAlignment = .left
                    cell.name.font = UIFont.systemFont(ofSize: 19, weight: UIFont.Weight.bold)
                    cell.name.text = "  - \(charityModel[indexPath.row].tagsName!)"
                    
                }else {
                    cell.name.textAlignment = .left
                    cell.name.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
                    cell.name.text = "       \(charityModel[indexPath.row].tagsName!)"
                }
                
            }else if tableView == audianceTableView {
                cell.name.text = "      - \(targetAudianceModel[indexPath.row].tagsName!)"
            }else {
                cell.name.text = "      - \(targetAnnualModel[indexPath.row].tagsName!)"
            }
            
            return cell
        }
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - registerTableViewCell
    func registerTableViewCell ()
    {
        self.searchResultTableView.register(UINib(nibName: "ProjectAssociatedGroupsTableViewCell", bundle: nil), forCellReuseIdentifier: "AssociatedGroupCell")
        charityTableView.register(UINib(nibName: "CategoryDropDownCell", bundle: nil), forCellReuseIdentifier: "CategoryDropDownCell")
        audianceTableView.register(UINib(nibName: "CategoryDropDownCell", bundle: nil), forCellReuseIdentifier: "CategoryDropDownCell")
        annualTableView.register(UINib(nibName: "CategoryDropDownCell", bundle: nil), forCellReuseIdentifier: "CategoryDropDownCell")
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func loadApi(){
        
        getGroupProjectList(url: (listOfGroupProjectUrl + projectListData.projectsID!), method: responseType.get.rawValue, headers: headerToken) { (status) in
            if status {
                print("GroupProject list has been come")
            }else {
                print("GroupProject list couldn't load")
            }
            if self.selectedGroupInProject.count == 0 {
                self.isGroupProjectFound.isHidden = false
            }else {
                self.isGroupProjectFound.isHidden = true
            }
            self.searchResultTableView.reloadData()
            self.loaderView.isHidden = true
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func getGroupProjectList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        listOfGroupProjectModel.removeAll()
        selectedGroupInProject.removeAll()
        Api.sharedInstance.sendData(url: url, param: nil, method: method, header: headers) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.listOfGroupProjectModel.append(ListOfGroupProjectModel(data: [i]))
                        }
                        
                        for i in self.listOfGroupProjectModel {
                            if i.isSelected == 1 {
                                self.selectedGroupInProject.append(i)
                            }
                        }
                   
                        
                    }else {
                        print("in response Error")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    
    func removeGroup(url:String,param:[String:Any],method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headers) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func charityApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        charityModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.charityModel.append(CharityModel(data: [i]))
                        }
                        print("Charity list data fetched")
                    }else {
                        print("Charity list didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
    
    func audienceApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        targetAudianceModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.targetAudianceModel.append(TargetAudianceModel(data: [i]))
                        }
                        print("Audiance list data fetched")
                    }else {
                        print("Audiance list didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
    func annualApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        targetAnnualModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.targetAnnualModel.append(TargetAnnualModel(data: [i]))
                        }
                        print("Annual list data fetched")
                    }else {
                        print("Annual list didn't fetch")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    func editProject(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
                //                self.loaderView.isHidden = true
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        if isStarDateCalanderOpen {
            txtFieldStartDate.text = self.formatter.string(from: date)
        }else {
            txtFieldEndDate.text = self.formatter.string(from: date)
        }
        calanderView.isHidden = true
        
    }
/*-----------------------------------------------------------------------------------------------------*/
}
