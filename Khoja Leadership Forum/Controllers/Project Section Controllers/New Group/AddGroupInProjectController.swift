
import UIKit
import Alamofire
import SwiftGifOrigin
import SDWebImage

class AddGroupInProjectController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    //MARK:- Button Outlets
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var isRecordFound: UILabel!
    
    //MARK:- Variables
    var Status = false
    var isSearching = false
    var projectId = ""
    var listOfGroupProjectModel = [ListOfGroupProjectModel]()
    var filtered = [ListOfGroupProjectModel]()

/*-----------------------------------------------------------------------------------------------------*/
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isRecordFound.isHidden = true
        headerHeight()
        searchBar.delegate = self
        tableView.register(UINib.init(nibName: "AddMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "AddMemberTableViewCell")
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        loadApi()
        
        searchBar.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        hideKeyboard()
        searchBar.returnKeyType = UIReturnKeyType.done
        
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    //MARK:- Custom Functions
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil  {
            
            isSearching = false
            isRecordFound.isHidden = true
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            tableView.reloadData()
        }else {
            isSearching = true
            let searchText  = textField.text
            filtered = listOfGroupProjectModel.filter{
                $0.name!.localizedCaseInsensitiveContains(String(searchText!))
            }
            if filtered.count == 0 {
                isRecordFound.isHidden = false
            }else {
                isRecordFound.isHidden = true
            }
            tableView.reloadData()
        }
        
    }
/*-----------------------------------------------------------------------------------------------------*/
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchBar {
            textField.resignFirstResponder()
        }
        return true
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func loadApi(){
        getGroupProjectList(url: (listOfGroupProjectUrl + projectId), method: responseType.get.rawValue, headers: nil) { (status) in
            if status {
                print("GroupProject list has been come")
            }else {
                print("GroupProject list couldn't load")
            }
            self.loaderView.isHidden = true
            self.tableView.reloadData()
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func getGroupProjectList(url:String,method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        listOfGroupProjectModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: nil, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? [[String:Any]] {
                        for i in data {
                            self.listOfGroupProjectModel.append(ListOfGroupProjectModel(data: [i]))
                        }
                    }else {
                        print("in response Error")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
    func addRemoveGroup(url:String,param:[String:Any],method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    //MARK:- Button Actions
    @IBAction func searchBtn(_ sender: Any) {
    }
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
/*-----------------------------------------------------------------------------------------------------*/
    
    @IBAction func done(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    @objc func addRemoveModeratorBtn(sender : UIButton) {
        var id = ""
        if isSearching{
            id = filtered[sender.tag].groupID ?? ""
        }else {
            id = listOfGroupProjectModel[sender.tag].groupID ?? ""
        }
        let param = ["group_id":id,"project_id":projectId] as [String : Any]
        if isSearching {
            searchBar.text = ""
            isSearching = false
        }
        if sender.isSelected{
            addRemoveGroup(url: removeGroupFromProjectUrl, param: param, method: responseType.post.rawValue, headers: nil) { (status) in
                if status{
                    self.loadApi()
                    sender.isSelected = false
                    self.showToast(message: "Group has been removed")
                    print("Group has been removed in list")
                }else {
                    self.showToast(message: "Group has been removed")
                    print("Group couldn't remove from list")
                    self.loaderView.isHidden = true
                    
                }
            }
        }else{
            addRemoveGroup(url: addGroupInProjectUrl, param: param, method: responseType.post.rawValue, headers: nil) { (status) in
                if status{
                    self.loadApi()
                    sender.isSelected = true
                    self.showToast(message: "Group has been added")
                    print("Group has been added in list")
                }else {
                    self.showToast(message: "Group couldn't add")
                    print("Group couldn't add in list")
                    self.loaderView.isHidden = true
                }
            }
        }
    }
    //MARK:- TableView Delegates
/*-----------------------------------------------------------------------------------------------------*/
    //MARK:- numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filtered.count
        }
        return listOfGroupProjectModel.count
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    //MARK - cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddMemberTableViewCell") as! AddMemberTableViewCell
        if isSearching{
            cell.lblMemberName.text = filtered[indexPath.row].name
//            cell.lblMemberDesignation.isHidden = true
//            cell.lblMemberCompany.isHidden = true
            let isModerator = filtered[indexPath.row].isSelected
            if isModerator == 1 {
                cell.checkBoxButtonOutlet.isSelected = true
            }else {
                cell.checkBoxButtonOutlet.isSelected = false
            }
            cell.checkBoxButtonOutlet.tag = indexPath.row
            cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveModeratorBtn), for: .touchUpInside)
            
            cell.memberImageView.layer.cornerRadius = cell.memberImageView.frame.width/2
            let remoteImageUrlString = filtered[indexPath.row].profileImg ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.memberImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
            
        }else {
            cell.lblMemberName.text = listOfGroupProjectModel[indexPath.row].name
//            cell.lblMemberDesignation.isHidden = true
//            cell.lblMemberCompany.isHidden = true
            let isModerator = listOfGroupProjectModel[indexPath.row].isSelected
            if isModerator == 1 {
                cell.checkBoxButtonOutlet.isSelected = true
            }else {
                cell.checkBoxButtonOutlet.isSelected = false
            }
            cell.checkBoxButtonOutlet.tag = indexPath.row
            cell.checkBoxButtonOutlet.addTarget(self, action: #selector(self.addRemoveModeratorBtn), for: .touchUpInside)
            
            cell.memberImageView.layer.cornerRadius = cell.memberImageView.frame.width/2
            let remoteImageUrlString = listOfGroupProjectModel[indexPath.row].profileImg ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.memberImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
               print("image \(indexPath.row) loaded")
            })
            
        }
        
        
        return cell
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    //MARK - heightForRowAt
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 80 * (view.frame.size.width / 320)
        }else{
            return 100
        }
    }    
/*-----------------------------------------------------------------------------------------------------*/

}
