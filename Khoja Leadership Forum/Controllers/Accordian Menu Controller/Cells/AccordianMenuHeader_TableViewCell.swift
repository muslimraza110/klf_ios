//
//  AccordianMenuHeader_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/29/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable

protocol AccordianMenuHeader_TableViewCellDelegate: AnyObject {
    func didTitleTapped(isPlenary: Bool, programDetails: SummitEventProgramDetailsModel?)
}
enum toggleHideShow:String {
    case show = "Show"
    case hide = "Hide"
}

enum inputList:String {
    case dividerView = "dividerView"
    case btnAwaitingResponse = "btnAwaitingResponse"
    case btnMeetingRequest = "btnMeetingRequest"
    case btnMeetingScheduled = "btnMeetingScheduled"
    case lblRequestMeeting = "lblRequestMeeting"
    case btnCheckbox = "btnCheckbox"
    case imgDropDown = "imgDropDown"
    case viewRightSpacer = "viewRightSpacer"
    case lblTitle = "lblTitle"
    case btnMeetingNotes = "btnMeetingNotes"
}


class AccordianMenuHeader_TableViewCell: AccordionTableHeaderFooterView {
    
    
    var DEFAULT_constraintlblTitle_Trailing:CGFloat = 153.0
    @IBOutlet weak var constraintlblTitle_Trailing: NSLayoutConstraint!
    weak var delegate: AccordianMenuHeader_TableViewCellDelegate?
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var btnMeetingRequest: UIButton!
    @IBOutlet weak var viewRightSpacer: UIView!
    @IBOutlet weak var lblRequestMeeting: UILabel!
    @IBOutlet weak var btnAwaitingResponse: UIButton!
    @IBOutlet weak var btnMeetingScheduled: UIButton!
    @IBOutlet weak var stackViewItems: UIStackView!
    @IBOutlet weak var btnCheckbox: KLCheckbox!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnMeetingNotes: UIButton!
    lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(expandCollapse))
        gesture.numberOfTapsRequired = 1
        return gesture
    }()
    
    var checkboxDelegate:AccordianHeaderCheckboxDelegate?
    var listIndex = 0
    
    var data: SummitEventProgramDetailsModel?
    
    var onBtnMeetingNotesTap: ((SummitEventProgramDetailsModel?) -> ())?
    var isPlenary: Bool?
    
//    var onBtnCheckboxTap: ((SummitEventProgramDetailsModel?) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        //remove previous gestures.
        if let recognizers = self.gestureRecognizers {
            for recognizer in recognizers {
                self.removeGestureRecognizer(recognizer)
            }
        }
        
        contentView.cornerRadius = 8

        //add new gestures.
        imgDropDown.addGestureRecognizer(tapGesture)
        //imgBackground.cornerRadius = 8
        
        btnCheckbox.delegate = self
        //btnCheckbox.allowedValues = [KLCheckbox.KLCheckboxTypes.tick, KLCheckbox.KLCheckboxTypes.line ]
        
//        constraintlblTitle_Trailing.constant = DEFAULT_constraintlblTitle_Trailing
        self.lblRequestMeeting.isUserInteractionEnabled = true
        self.lblRequestMeeting.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.titleLabelPressed)))
        
    }
    

    @objc func titleLabelPressed() {
       // debugPrint("Meeting Label Pressed.")
        self.delegate?.didTitleTapped(isPlenary: self.isPlenary ?? false, programDetails: data)
    }
    @IBAction func btnMeetingNotesTapped(_ sender: UIButton) {
        onBtnMeetingNotesTap?(data)
    }
    
    
    @objc func expandCollapse( anything:Any ) {
        self.contentView.isUserInteractionEnabled = false
        
        (self.superview as? AccordionTableView)?.contentInset.bottom = 50

        (self.superview as? AccordionTableView)?.toogleSection(section: self.section)
                
        (self.superview as? AccordionTableView)?.contentInset.bottom = 0

        self.contentView.isUserInteractionEnabled = true
    }
    
    func toggleHideShowInputs( showHide:toggleHideShow )
    {
        if showHide == .hide {
            btnAwaitingResponse.isHidden = true
            btnMeetingRequest.isHidden = true
            btnMeetingScheduled.isHidden = true
            lblRequestMeeting.isHidden = true
            btnCheckbox.isHidden = true
            imgDropDown.isHidden = true
            viewRightSpacer.isHidden = true
            lblTitle.isHidden = true
            dividerView.isHidden = true
            btnMeetingNotes.isHidden = true
        }
        else {
            btnAwaitingResponse.isHidden = false
            btnMeetingRequest.isHidden = false
            btnMeetingScheduled.isHidden = false
            lblRequestMeeting.isHidden = false
            btnCheckbox.isHidden = false
            imgDropDown.isHidden = false
            viewRightSpacer.isHidden = false
            lblTitle.isHidden = false
            dividerView.isHidden = false
            btnMeetingNotes.isHidden = false
        }
    }
    
    func toggleHideShowInputs( showHide:toggleHideShow, inputList:[inputList] )
    {
        
        
        var isHide = false
        
        if showHide == .hide {
            isHide = true
        }
        else {
            isHide = false
        }
        
        for iL in inputList {
            if iL == .btnAwaitingResponse {
                btnAwaitingResponse.isHidden = isHide
            }
            
            if iL == .btnCheckbox {
                btnCheckbox.isHidden = isHide
            }
            
            if iL == .btnMeetingRequest {
                btnMeetingRequest.isHidden = isHide
            }
            
            if iL == .btnMeetingScheduled {
                btnMeetingScheduled.isHidden = isHide
            }
            
            if iL == .imgDropDown {
                imgDropDown.isHidden = isHide
            }
            
            if iL == .lblRequestMeeting {
                lblRequestMeeting.isHidden = isHide
            }
            
            if iL == .viewRightSpacer {
                viewRightSpacer.isHidden = isHide
            }
            
            if iL == .lblTitle {
                lblTitle.isHidden = isHide
            }
            
            if iL == .dividerView {
                dividerView.isHidden = isHide
            }
            
            if iL == .btnMeetingNotes {
                btnMeetingNotes.isHidden = isHide
            }
            
        }
        
    }
}


extension AccordianMenuHeader_TableViewCell: KLCheckboxDelegate
{
    func KL_afterSelect(button: UIButton) {

        print( section )
        
        checkboxDelegate?.afterCheckboxSelect(button: button, sectionIndex: section)
         
     }
     
     func KL_beforeSelect(button: UIButton) {
         
         print( section )
        
        checkboxDelegate?.beforeCheckboxSelect(button: button, sectionIndex: section)
     }
}
