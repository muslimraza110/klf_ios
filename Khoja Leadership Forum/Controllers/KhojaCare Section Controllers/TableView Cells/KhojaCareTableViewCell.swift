

import UIKit

class KhojaCareTableViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var lblProjectTitle: UILabel!
    @IBOutlet weak var lblCharity: UILabel!
    @IBOutlet weak var lblCreator: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
