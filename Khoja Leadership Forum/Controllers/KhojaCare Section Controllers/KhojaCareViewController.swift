
import UIKit
import SwiftGifOrigin
import Alamofire

class KhojaCareViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Btn Outlets
    @IBOutlet weak var tableViewKhojaCare: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    
    //MARK:- Custom Variables
    var dropImage = UIImage.init(named: "dropdown.png")
    var headersTitleArray = [String]()
    var Status = false
    var khojaCareListModel = [KhojaCareListModel]()
    var sectionDataArray = [ExpandableNamesKhoja]()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerTableViewCell()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        headerHeight()
        self.navigationController?.isNavigationBarHidden = true
        
        self.tableViewKhojaCare.delegate = self
        self.tableViewKhojaCare.dataSource = self


        khojaCareListApi(url: khojaCareListUrl, method: responseType.get.rawValue, header: headerToken) { (status) in
            if status {
                self.tableViewKhojaCare.reloadData()
            }else {
                
            }
            self.loaderView.isHidden = true
            
        }

    }
    
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func khojaCareListApi(url:String,method:HTTPMethod,header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: nil ,method: method, header: header) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["data"] as? NSDictionary {
                        self.sectionDataArray.removeAll()
                        self.headersTitleArray.removeAll()

                        for dic in data {
                            self.headersTitleArray.append(dic.key as! String)
                            let dicData = dic
                            self.khojaCareListModel.removeAll()
                          for obj in dicData.value as! [[String:Any]] {
                            self.khojaCareListModel.append(KhojaCareListModel(data: [obj]))
                           print(dic.value)
                        }
                            self.sectionDataArray.append(ExpandableNamesKhoja(isExpanded: true, names: self.khojaCareListModel))
                        }
                    }else {
                        print("Json data couldn't load")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                completion(false)
                self.loaderView.isHidden = true
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func registerTableViewCell ()
    {
        self.tableViewKhojaCare.register(UINib(nibName: "KhojaCareTableViewCell", bundle: nil), forCellReuseIdentifier: "KhojaCell")
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    //MARK:- Button Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
    }
        
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let khojaCell = self.tableViewKhojaCare.dequeueReusableCell(withIdentifier: "KhojaCell", for: indexPath) as! KhojaCareTableViewCell
        khojaCell.lblProjectTitle.text = sectionDataArray[indexPath.section].names[indexPath.row].projectsName
        khojaCell.lblCity.text = "\(sectionDataArray[indexPath.section].names[indexPath.row].projectsCity ?? "city not defind"), \(sectionDataArray[indexPath.section].names[indexPath.row].projectsCountry ?? "country not defind")"
        khojaCell.lblCharity.text = sectionDataArray[indexPath.section].names[indexPath.row].charity
        let creatorTitle = sectionDataArray[indexPath.section].names[indexPath.row].creatorsTitle
        let firstName = sectionDataArray[indexPath.section].names[indexPath.row].creatorsFirstName
        let lastName = sectionDataArray[indexPath.section].names[indexPath.row].creatorsLastName
        khojaCell.lblCreator.text = "\(creatorTitle ?? "") \(firstName ?? "") \(lastName ?? "")"

        khojaCell.layer.cornerRadius = 5
        return khojaCell
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 150))
        
        // code for adding centered title
        headerView.backgroundColor = UIColor.white
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 5, width:tableView.frame.width-80, height: 30))
        headerLabel.textColor = UIColor.black
        if headersTitleArray.count != 0 {
            headerLabel.text = headersTitleArray[section]
        }
        if UIDevice().userInterfaceIdiom == .phone {
             headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
         }else {
            headerLabel.font = UIFont.boldSystemFont(ofSize: 18)
        }
        headerLabel.textAlignment = .left
        headerLabel.contentMode = .scaleAspectFit
        
        headerView.backgroundColor = UIColor(red:0.18, green:0.23, blue:0.38, alpha:1.0)
        headerLabel.textColor = UIColor.white
        headerView.addSubview(headerLabel)
        
        let arrowButton = UIButton(frame: CGRect(x:headerView.frame.size.width - 80, y:0, width:60, height:30))
        dropImage = UIImage.init(named: "of-admin-dropdown.png")
        
        arrowButton.setImage(dropImage, for: .normal)
        arrowButton.setTitleColor(.black, for: .normal)
        arrowButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        arrowButton.backgroundColor = UIColor.clear
        arrowButton.addTarget(self, action: #selector(ExpandClose), for: .touchUpInside)
        arrowButton.tag = section
        arrowButton.imageView?.contentMode = .scaleAspectFit
        
        let countValue = self.sectionDataArray[section].names.count
        if countValue != 0{
            headerView.addSubview(arrowButton)
        }
        //constraint for dropdown arrow
        arrowButton.translatesAutoresizingMaskIntoConstraints = false
        arrowButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        arrowButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        arrowButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        arrowButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
        
        arrowButton.setNeedsLayout()
        arrowButton.layoutIfNeeded()
        arrowButton.clipsToBounds = true
        
        headerView.layer.cornerRadius = 10
        return headerView
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @objc func ExpandClose (button: UIButton) {
        
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in sectionDataArray[section].names.indices{
            let indexpath = IndexPath(row: row, section: section)
            indexPaths.append(indexpath)
        }
        
        let isExpanded = sectionDataArray[section].isExpanded
        sectionDataArray[section].isExpanded = !isExpanded
        if isExpanded {
            button.isSelected = false
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
            
            button.setImage(dropImage, for: .normal)
            tableViewKhojaCare.deleteRows(at: indexPaths, with: .top)
        }
        else{
            button.isSelected = true
            dropImage = UIImage.init(named: "of-admin-dropup.png")
            
            button.setImage(dropImage, for: .normal)
            tableViewKhojaCare.insertRows(at: indexPaths, with: .bottom)
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !sectionDataArray[section].isExpanded {
            return 0
        }
        return self.sectionDataArray[section].names.count
//        return 1
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 90

        }else {
            return 150

        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.headersTitleArray.count
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            return 40

        }else {
           return 50

       }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProjectViewController") as! ViewProjectViewController
        vc.projectId = sectionDataArray[indexPath.section].names[indexPath.row].projectsID!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    
}
