//
//  BaseViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/22/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable

class BaseViewController: UIViewController {

    var apiClass = ApiCalling2()
    
    var headerImage: UIImageView?
    
    var btnRefresh: UIButton?
    
    var btnNotification: UIButton?
    
    var lblNotificationCount: UILabel?
    
    
    var btnNotificationWidthAnchor: NSLayoutConstraint?
    var btnNotificationWidth: CGFloat = 30
    
    var btnNotificationTrailingAnchor: NSLayoutConstraint?
    var btnNotificationTrailing: CGFloat = -20


    
    //MARK:- Custom Functions
    func setNewDesignHeader(  ) {
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let background = UIImage(named: "app_bar_bg")
        headerImage = UIImageView(frame: view.bounds)
        headerImage?.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: 130)
        headerImage?.contentMode =  .scaleToFill
        headerImage?.clipsToBounds = true
        headerImage?.image = background
        view.addSubview(headerImage!)
        self.view.sendSubviewToBack(headerImage!)

        
        
        /*
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back_button.png")?.withRenderingMode(.alwaysOriginal)
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back_button.png")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        */
        
        
        if let headerImage = headerImage {
            
            
            // Logo
            let logo = UIImage(named: "khoja_logo_white")
            let logoImageView = UIImageView()
            logoImageView.image = logo
            view.addSubview(logoImageView)
            logoImageView.translatesAutoresizingMaskIntoConstraints = false
            logoImageView.widthAnchor.constraint(equalToConstant: 155).isActive = true
            logoImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
            logoImageView.centerYAnchor.constraint(equalTo: headerImage.centerYAnchor, constant: 0).isActive = true
            logoImageView.centerXAnchor.constraint(equalTo: headerImage.centerXAnchor, constant: 0).isActive = true
            logoImageView.contentMode = .scaleAspectFit
            
            
            
            
            // Back Button
            let backImage = UIImage(named: "whiteBackArrow")?
                .withAlignmentRectInsets(UIEdgeInsets(
                    top: -8,
                    left: -8,
                    bottom: -8,
                    right: -8)
            )
                .withRenderingMode(.alwaysTemplate)
            let btnBack = UIButton(type: .custom)
            
            view.addSubview(btnBack)
            btnBack.setImage(backImage, for: .normal)
            btnBack.imageView?.tintColor = UIColor.white
            
            btnBack.contentMode = .scaleAspectFit
            btnBack.translatesAutoresizingMaskIntoConstraints = false
            btnBack.centerYAnchor.constraint(equalTo: headerImage.centerYAnchor, constant: 0).isActive = true
            btnBack.heightAnchor.constraint(equalToConstant: 40).isActive = true
            btnBack.widthAnchor.constraint(equalToConstant: 40).isActive = true
            btnBack.leadingAnchor.constraint(equalTo: headerImage.leadingAnchor, constant: 10).isActive = true
            
            
            btnBack.addTarget(self, action: #selector(btnBackTapped(_:)), for: .touchUpInside)
            
            
            
            // Notification Button
            let notficationImage = UIImage(named: "ic_notification")?.withRenderingMode(.alwaysTemplate)
            
            btnNotification = UIButton(type: .custom)
            
            view.addSubview(btnNotification!)
            btnNotification?.setImage(notficationImage, for: .normal)
            btnNotification?.imageView?.tintColor = UIColor.white
            
            btnNotification?.contentMode = .scaleAspectFit
            btnNotification?.translatesAutoresizingMaskIntoConstraints = false
            btnNotification?.centerYAnchor.constraint(equalTo: headerImage.centerYAnchor, constant: 0).isActive = true
            btnNotification?.heightAnchor.constraint(equalToConstant: btnNotificationWidth).isActive = true
            
                //Initially hidden
            btnNotificationWidthAnchor = btnNotification?.widthAnchor.constraint(equalToConstant: 0)
            btnNotificationWidthAnchor?.isActive = true
            
            btnNotificationTrailingAnchor = btnNotification?.trailingAnchor.constraint(equalTo: headerImage.trailingAnchor, constant: 0)
            btnNotificationTrailingAnchor?.isActive = true
            
            btnNotification?.addTarget(self, action: #selector(btnNotificationTapped), for: .touchUpInside)
            btnNotification?.isHidden = true
            
            
            
            
            // Notification badge
            lblNotificationCount = UILabel()
            view.addSubview(lblNotificationCount!)

            lblNotificationCount?.translatesAutoresizingMaskIntoConstraints = false
            
            lblNotificationCount?.centerYAnchor.constraint(equalTo: btnNotification!.centerYAnchor, constant: -12).isActive = true
            lblNotificationCount?.centerXAnchor.constraint(equalTo: btnNotification!.centerXAnchor, constant: 12).isActive = true
            lblNotificationCount?.widthAnchor.constraint(equalToConstant: 24).isActive = true
            lblNotificationCount?.heightAnchor.constraint(equalToConstant: 24).isActive = true
            
            lblNotificationCount?.layer.cornerRadius = 12
            lblNotificationCount?.clipsToBounds = true
            lblNotificationCount?.backgroundColor = UIColor.appRed
            lblNotificationCount?.textColor = UIColor.white
            lblNotificationCount?.font = UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 12)
            lblNotificationCount?.text = "99"
            lblNotificationCount?.textAlignment = .center

            
                //Initially Hidden
            lblNotificationCount?.isHidden = true
            
        }
        

        
        
        

//        let logo = UIImage(named: "khoja_logo_white")
//        let imageView = UIImageView(image:logo)
//        imageView.frame = CGRect(x: 0, y: -20, width: 155, height: 40)
//        imageView.contentMode = .scaleAspectFit
//        self.navigationItem.titleView = imageView
//
//
//
//        self.navigationController?.navigationBar.backItem?.title = ""
//        self.navigationController?.navigationBar.tintColor = .white
//        self.navigationController?.isNavigationBarHidden = false
//
//
//
//        // If you need custom positioning for your back button, in this example button will be 1 px up compared to default one. Also only vertical positioning works. For horizontal one add offsets directly to the image (yeah, that sucks!)
//        let backImageInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
//        // Get image, change of rendering (so it preserves offsets made in image file) and finally apply offsets
//        let backImage = UIImage(named: "whiteBackArrow")?.withAlignmentRectInsets(backImageInsets)

        	
//        // Setting images
//
//        self.navigationController?.navigationBar.backIndicatorImage = backImage
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
//
//        navigationItem.title = ""
        
        
    }
    
    
    func toggleNotificationButton(visible: Bool){
        if(visible){
            btnNotificationWidthAnchor?.constant = btnNotificationWidth
            btnNotificationTrailingAnchor?.constant = btnNotificationTrailing
        }else{
            btnNotificationWidthAnchor?.constant = 0
            btnNotificationTrailingAnchor?.constant = 0
            lblNotificationCount?.isHidden = true
        }
    }
    
    
    
    @objc func btnBackTapped(_ sender: UIButton){
        if (self.presentingViewController != nil) {
            self.dismiss(animated: true)
            return
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnNotificationTapped(_ sender: UIButton){
       
    }
    
    
    func setUpRefreshButton(){
        
        if let btnNotification = btnNotification {
            
            let refreshImage = UIImage(named: "refresh")?
//                .withAlignmentRectInsets(UIEdgeInsets(
//                    top: -20,
//                    left: -20,
//                    bottom: -20,
//                    right: -20)
//            )
                .withRenderingMode(.alwaysTemplate)
            btnRefresh = UIButton(type: .custom)
            
            view.addSubview(btnRefresh!)
            btnRefresh?.setImage(refreshImage, for: .normal)
            btnRefresh?.imageView?.tintColor = UIColor.white
            
            btnRefresh?.contentMode = .scaleAspectFit
            btnRefresh?.translatesAutoresizingMaskIntoConstraints = false
            btnRefresh?.centerYAnchor.constraint(equalTo: btnNotification.centerYAnchor, constant: 0).isActive = true
            btnRefresh?.heightAnchor.constraint(equalToConstant: 26).isActive = true
            btnRefresh?.widthAnchor.constraint(equalToConstant: 26).isActive = true
            btnRefresh?.trailingAnchor.constraint(equalTo: btnNotification.leadingAnchor, constant: -20).isActive = true
            
            btnRefresh?.addTarget(self, action: #selector(btnRefreshTapped), for: .touchUpInside)
        }

    }
    
    @objc func btnRefreshTapped(_ sender: UIButton){
       
    }
    
    
    func setupWireBg() {
        let image = #imageLiteral(resourceName: "bg_wire")
        let wireBg = UIImageView(image: image)
        wireBg.translatesAutoresizingMaskIntoConstraints = false
        
        view.insertSubview(wireBg, at: 0)
        
        wireBg.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        wireBg.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        wireBg.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        wireBg.heightAnchor.constraint(equalToConstant: view.frame.height / 1.8).isActive = true
    }
    
    
    func toggleDropDown(section: Int, request_to:Openable, tableView:UITableView, allCornersAlwaysRounded: Bool = true) {
            
        let myHeaderView = tableView.headerView(forSection: section)
        let mainView = myHeaderView?.contentView.subviews.compactMap { $0  }
        if mainView?.count ?? 0 > 0
        {
//            if let backgroundImage = mainView?.first?.viewWithTag(99) as? UIImageView  {
//
//                if request_to == .open {
//                    backgroundImage.image = UIImage(named: "bluebackground-halfround-fromtop")
//                    backgroundImage.cornerRadius = 0
//                }
//                else {
//                    backgroundImage.image = UIImage(named: "bluebackground-big")
//                    backgroundImage.cornerRadius = 8
//
//                }
//                backgroundImage.clipsToBounds = true
//
//            }
            
            if allCornersAlwaysRounded {
                
                myHeaderView?.contentView.roundCorners(corners: [.allCorners], radius: 8)
                
            } else{
                if request_to == .open {
                    myHeaderView?.contentView.roundCorners(corners: [.topRight, .topLeft], radius: 8)

                } else {
                    myHeaderView?.contentView.roundCorners(corners: [.allCorners], radius: 8)
                }
            }

            
            
            let stackView = mainView?.first?.subviews.compactMap{ $0 as? UIStackView }
            if stackView?.count ?? 0 > 0
            {
                
                let imageViews = stackView?.first?.subviews.compactMap { $0 as? UIImageView }
                
                if let dropUpDownImage = stackView?.first?.viewWithTag(1005) as? UIImageView  {
                    
                    if request_to == .open {
                        dropUpDownImage.image = getUpArrowImage()
                    }
                    else {
                        dropUpDownImage.image = getDownArrowImage()
                    }
                    
                }
                
                
                
                
                for imgView in imageViews! {
                
                    if imgView.tag == 1005
                    {
                        
                    }
                    else if imgView.tag == 99
                    {
                        /*
                        if request_to == .open {
                            imgView.image = UIImage(named: "bluebackground-halfround-fromtop")
                            imgView.cornerRadius = 0
                        }
                        else {
                            imgView.image = UIImage(named: "bluebackground-big")
                            imgView.cornerRadius = 8
                            
                        }
                        imgView.clipsToBounds = true*/
                    }
                    
                    
                    //
                }
            }
        }

    }
    
    
    func getUpArrowImage() -> UIImage? {
        return UIImage(named: "up-arrow-white")?
            .imageWithInsets(insets: UIEdgeInsets(top: 52, left: 36, bottom: 52, right: 36))
    }
    
    func getDownArrowImage() -> UIImage? {
        return UIImage(named: "down-arrow-white")?
        .imageWithInsets(insets: UIEdgeInsets(top: 52, left: 36, bottom: 52, right: 36))
    }
    
    func openUrlInBrowser(_ urlString: String?) {
        if let url = URL(string: urlString ?? "") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }else{
                view.hideAll_makeToast("Invalid Url")
            }
        } else{
            view.hideAll_makeToast("Invalid Url")
        }
    }

}
