
import UIKit
import Alamofire
import SwiftGifOrigin

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var viewDialogWindow: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var btnCrossOutlet: UIButton!
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var txtFieldEmailAddress: UITextField!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    
    // flags for textfield validations
    var emailFlag = false
    var Status = false
    

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK:- Custom Function
    func forgotPassword(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON, status) in
            if status {
                self.Status = false
                let JSON = JSON
                let isStatus = JSON["status"] as! String
                if isStatus == "success"{
                    self.Status = true
                    print("Data fetched")
                }else {
                    self.Status = false
                    print("Data didn't fetch")
                }
                completion(self.Status)
            }else {
                print("Internet is Unavailable")
            }
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK -  crossButtonPressed
    @IBAction func crossButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK -  submitButtonPressed
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        if textFieldFormatCheck() {
            let param = ["email":txtFieldEmailAddress.text!] as [String:Any]
            forgotPassword(url: forgotUrl, method: responseType.post.rawValue, param: param, header: nil) { (status) in
                if status{
                    self.showToast(message: "Password reset email sent")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.dismiss(animated: true, completion: nil)
                    }
                }else {
                    self.showToast(message: "Email doesn't exist in the system.")
//                    self.lblErrorMessage.isHidden = false
//                    self.lblErrorMessage.text = ConstantMessages.invalidEmailAddress
//                    self.txtFieldEmailAddress.borderColor = .red
                }
                self.loaderView.isHidden = true
            }
            print("Value inserted")
        }
        else{
            print("Empty Field")
        }
        print("Submit button pressed")
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - Dissmiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK -  viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

//        Api.sharedInstance.reachAbilityInitialize()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.txtFieldEmailAddress.delegate = self
        initialLayout()
        hideKeyboard()
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - shouldChangeCharactersIn
    // TextFiled Delegate Functions when change characters in textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtFieldEmailAddress {
            self.lblErrorMessage.isHidden = true
            self.txtFieldEmailAddress.borderColor = .darkGray
            
            let maxLength = 30
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else{
            return false
        }
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK -  initialLayout
    func initialLayout(){
        
        //Hide Error Labels
         self.lblErrorMessage.isHidden = true
        
        self.btnCrossOutlet.layer.cornerRadius = self.btnCrossOutlet.frame.size.width / 2
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - textFieldFormatCheck
    // textfield format Check function
    func textFieldFormatCheck() -> Bool {
        
        if self.txtFieldEmailAddress.text?.count == 0 || validateEmail(email: self.txtFieldEmailAddress.text!) == false{
            self.txtFieldEmailAddress.borderColor = .red
            if self.txtFieldEmailAddress.text?.count == 0 {
                self.lblErrorMessage.text = ConstantMessages.blankTextFiled
            }
            else{self.lblErrorMessage.text = ConstantMessages.invalidEmailAddress}
            self.lblErrorMessage.isHidden = false
            emailFlag = false
            
        }
        else{
            self.lblErrorMessage.isHidden = true
            emailFlag = true
        }
        
        if emailFlag == true {return true}
        else{return false}
    }
}

/*-----------------------------------------------------------------------------------------------------------------------------------*/
