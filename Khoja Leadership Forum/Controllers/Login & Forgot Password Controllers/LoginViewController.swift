
import UIKit
import Alamofire
import SwiftGifOrigin

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    //Outlets
    @IBOutlet weak var txtFieldUsername: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var btnLoginOutlet: UIButton!
    @IBOutlet weak var lblUsernameError: UILabel!
    @IBOutlet weak var lblPasswordError: UILabel!
    @IBOutlet weak var noInternetOutlet: UILabel!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var btnRemeber: UIButton!
    
    // flags for textfield validations
    var userFlag = false
    var passFlag = false
    var Status = false
    var logoutFlag = false
    var loginModel = LoginModel()
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK:- Custom Function
    func login (url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON, status) in
            if status {
                self.Status = false
                self.noInternetOutlet.isHidden = true
                let JSON = JSON
                guard  let json = JSON["status"] as? String else {
                    self.loaderView.isHidden = true
                    if let message = JSON["message"] as? String {
                        self.showToast(message: message)
                    }else {
                        self.showToast(message: "Server Error")
                    }
                    return
                }
                let isStatus = json // JSON["status"] as! String
                if isStatus == "success"{
                    let username = self.txtFieldUsername.text
                    let password = self.txtFieldPassword.text
                    self.Status = true
                    let data = JSON["data"] as! NSDictionary
                    self.loginModel = LoginModel(data: data)
                    print(data)
                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: data)
                    UserDefaults.standard.set(encodedData, forKey: "Login")
                    UserDefaults.standard.set(username, forKey: "username")
                    UserDefaults.standard.set(password, forKey: "passsword")
                    UserDefaults.standard.set(data["role"], forKey: Constants.User.role.rawValue)
                    print("Login data fetched")
                }else {
                    self.Status = false
                    print("login data didn't fetch")
                }
            }else {
                self.Status = false
                self.noInternetOutlet.isHidden = false
                self.loaderView.isHidden = true
                print("Internet is Unavailable")
            }
            completion(self.Status)
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - btnLogin
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if textFieldFormatCheck() {
            print("all values inserted without constraints")



            let fcmToken = UserDefaults.standard.value(forKey: Constants.Token.fcmToken) as? String ?? ""
            let param = ["email": txtFieldUsername.text!,"password": txtFieldPassword.text!, "fcm_token": fcmToken.escapeString(), "platform" : 1] as [String:Any]
            login(url: loginUrl, method: responseType.post.rawValue, param: param, header: nil) { (status) in
                if status{
                    UserDefaults.standard.set("True", forKey: "islogin")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContainerViewController") as! ContainerViewController
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    appdelegate.window!.rootViewController = vc
                    if self.logoutFlag{
                        NotificationCenter.default.post(name: NSNotification.Name("loginAgainNotification"), object:nil)
                    }
                    
                    if UserDefaults.standard.value(forKey: "IsRememberOn") as? Int == 1 {
                        UserDefaults.standard.set(self.txtFieldUsername.text, forKey: "rememberUserName")
                        UserDefaults.standard.set(self.txtFieldPassword.text, forKey: "rememberPassword")
                    }
                    
                     print("Login Successfully")
                }else {
                    UserDefaults.standard.set("False", forKey: "islogin")
                    self.showToast(message: "Please enter correct Email and Password.")
                }
                self.loaderView.isHidden = true
            }
        }
        else{
            print("one or more fields are empty.")
        }
        print("Login button pressed")
        
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @IBAction func btnRemeber(_ sender: UIButton) {
        if sender.isSelected {
            btnRemeber.isSelected = false
            UserDefaults.standard.set(0, forKey: "IsRememberOn")
        }else {
            btnRemeber.isSelected = true
            UserDefaults.standard.set(1, forKey: "IsRememberOn")
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    //MARK - btnForgotPassword
    @IBAction func btnForgotPassword(_ sender: UIButton) {
        print("Forgot Password button pressed")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.present(vc, animated: true, completion: nil)
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // txtFieldUsername.text = "" //"kladmin@koderlabs.com"
       // txtFieldPassword.text = ""
        
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.txtFieldUsername.delegate = self
        self.txtFieldPassword.delegate = self
        initialLayout()
        hideKeyboard()
        
        if UserDefaults.standard.value(forKey: "IsRememberOn") as? Int == nil {
            UserDefaults.standard.set(0, forKey: "IsRememberOn")
        }
        
        if UserDefaults.standard.value(forKey: "IsRememberOn") as? Int == 1 {
            btnRemeber.isSelected = true
            txtFieldUsername.text = UserDefaults.standard.value(forKey: "rememberUserName") as? String
            txtFieldPassword.text = UserDefaults.standard.value(forKey: "rememberPassword") as? String
        }
        
        
//        Api.sharedInstance.startMonitoring()

        NotificationCenter.default.addObserver(self, selector: #selector(logoutCustom), name: NSNotification.Name("logoutNotification"), object: nil)
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    override func viewWillAppear(_ animated: Bool) {
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    @objc func logoutCustom(_ notification:Notification) {
        print("logout notification received")
        logoutFlag = true
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - Dissmiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK -  initialLayout
    func initialLayout(){
        
        //Hide Error Labels
        self.lblUsernameError.isHidden = true
        self.lblPasswordError.isHidden = true
        self.noInternetOutlet.isHidden = true
        
        //Username textfield initial layout
        self.txtFieldUsername.leftViewMode = UITextField.ViewMode.always
        let leftImageView = UIImageView()
        leftImageView.frame = CGRect(x: 30, y: 0, width: 15, height: 15)
        
        let leftImage = UIImage(named: "username-icon.png")
        leftImageView.image = leftImage
        
        let leftView = UIView()
        leftView.frame = CGRect(x: 30, y: 0, width: 50, height: 15)
        leftView.addSubview(leftImageView)
        
        
        self.txtFieldUsername.leftView = leftView
        self.txtFieldUsername.attributedPlaceholder = NSAttributedString(string: "Email / Username",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        //Password textfield initial layout
        self.txtFieldPassword.leftViewMode = UITextField.ViewMode.always
        let leftImageViewPass = UIImageView()
        let leftImagePass = UIImage(named: "password-icon.png")
        leftImageViewPass.image = leftImagePass
        let leftViewPass = UIView()
        leftViewPass.addSubview(leftImageViewPass)
        leftViewPass.frame = CGRect(x: 30, y: 0, width: 50, height: 15)
        leftImageViewPass.frame = CGRect(x: 30, y: 0, width: 15, height: 15)
        self.txtFieldPassword.leftView = leftViewPass
        self.txtFieldPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
     
        //making password field secure
        self.txtFieldPassword.isSecureTextEntry = true
        
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - textFieldShouldReturn
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if textField == self.txtFieldUsername {
            self.txtFieldPassword .becomeFirstResponder()
        }
        else{
            
        }
        return true
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - shouldChangeCharactersIn
    // TextFiled Delegate Functions when change characters in textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtFieldUsername {
            self.lblUsernameError.isHidden = true
//            self.txtFieldUsername.background = UIImage (named: "text-field.png")
            self.txtFieldUsername.borderColor = .white
            
            let maxLength = 300
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else{
            self.lblPasswordError.isHidden = true
//             self.txtFieldPassword.background = UIImage (named: "text-field.png")
            self.txtFieldPassword.borderColor = .white
            let maxLength = 30
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
    
    }

 /*-----------------------------------------------------------------------------------------------------------------------------------*/
     //MARK - textFieldFormatCheck
    // textfield format Check function
    func textFieldFormatCheck() -> Bool {

        if self.txtFieldUsername.text?.count == 0 || validateEmail(email: self.txtFieldUsername.text!) == false{
//            self.txtFieldUsername.background = UIImage (named: "textfield-error.png")
            self.txtFieldUsername.borderColor = .red
            if self.txtFieldUsername.text?.count == 0 {
                self.lblUsernameError.text = ConstantMessages.blankTextFiled
            }
            else{self.lblUsernameError.text = ConstantMessages.invalidEmailAddress}
            self.lblUsernameError.isHidden = false
            userFlag = false
            
        }
        else{
                self.lblUsernameError.isHidden = true
                userFlag = true
        }

        if self.txtFieldPassword.text?.count == 0 || self.txtFieldPassword.text!.count < 3{
//            self.txtFieldPassword.background = UIImage (named: "textfield-error.png")
            self.txtFieldPassword.borderColor = .red
            if self.txtFieldPassword.text?.count == 0{
                self.lblPasswordError.text = ConstantMessages.blankTextFiled
            }
            else{self.lblPasswordError.text = ConstantMessages.shortPassword}
            self.lblPasswordError.isHidden = false
            passFlag = false
            
        }
        else{
                self.lblPasswordError.isHidden = true
                passFlag = true
            }
        
        if userFlag && passFlag == true {return true}
        else{return false}
    }
    
}

/*-----------------------------------------------------------------------------------------------------------------------------------*/


extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 25, y: self.view.frame.size.height - 100, width: self.view.frame.size.width-50, height: 35))
        toastLabel.backgroundColor = UIColor.black
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 5.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}
/*-----------------------------------------------------------------------------------------------------*/
