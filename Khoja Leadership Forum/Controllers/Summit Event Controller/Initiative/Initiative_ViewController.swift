//
//  Initiative_ViewController.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 4/22/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit
import WebKit

class Initiative_ViewController: BaseViewController {

    var userId: Int?
    var initiativeViewID: Int?
    var isEvent: Bool?
    @IBOutlet weak var webView: WKWebView!
//    @IBOutlet weak var progressBar: UIProgressView!
//    @IBOutlet weak var backButton: UIButton!
//    @IBOutlet weak var forwardButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareView()
        guard let _ = isEvent else { return }
        self.setNewDesignHeader()
        self.setupWireBg()
    }

    private func prepareView() {
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        self.userId = userData["id"] as? Int
        self.loadWebsite()
        self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }

    @IBAction func backButtonTapped(_ sender: UIButton) {
        webView.goBack()
    }

    @IBAction func forwardButtonTapped(_ sender: UIButton) {
        webView.goForward()
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        if keyPath == .estimatedProgress {
//            self.progressBar.progress = Float(webView.estimatedProgress)
//        }
    }

    private func loadWebsite() {
        self.webView.navigationDelegate = self
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        let role = userData["role"] as! String
        var roleURL = ""
        if role == "A" {
            roleURL = "admin"
        }else {
            roleURL = "user"
        }

        guard let url = URL(string: "\(initiativePledgeLink)\(roleURL)\(initiativeLink)\(self.initiativeViewID ?? 15)?forcelogin=\(userId ?? 0)") else { return }
        self.webView.load(URLRequest(url: url))
    }
}

extension Initiative_ViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
       // self.progressBar.isHidden = false
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        self.progressBar.isHidden = true
//        self.backButton.isHidden = webView.canGoBack ? false : true
//        self.forwardButton.isHidden = webView.canGoForward ? false : true
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        self.progressBar.isHidden = true
//        self.backButton.isHidden = webView.canGoBack ? false : true
//        self.forwardButton.isHidden = webView.canGoForward ? false : true
    }
}
