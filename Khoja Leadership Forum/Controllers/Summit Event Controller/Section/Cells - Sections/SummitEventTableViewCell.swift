//
//  SummitEventTableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/18/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

class SummitEventTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var lblSecondHeading: UILabel!
    @IBOutlet weak var lblFirstHeading: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
     

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        lblFirstHeading.textDropShadow()
        lblSecondHeading.textDropShadow()
        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()

    }
}
