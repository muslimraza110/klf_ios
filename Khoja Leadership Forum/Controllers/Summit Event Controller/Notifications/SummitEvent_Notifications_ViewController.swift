//
//  SummitEvent_Notifications_ViewController.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 1/25/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit

class SummitEvent_Notifications_ViewController: UIViewController {
    
    
    // MARK: Outlets and Variables
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    
    var apiClass = ApiCalling2()
    
    var blurEffectView: UIVisualEffectView?
    
    var onNotificationConrtollerDismiss: (() -> Void)?
    
    var summitEventNotifications: [SummitEventNotificationModel] = []
    
    
    var notificatoinCellIdentifier = "notificationCell"
    
    var previousVC: UIViewController?
    
    var thisEvent:SummitEventModel?
    var userId = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let userDefaults = UserDefaults.standard
        do {
            thisEvent = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        
        let loginData = userDefaults.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        
        setupBlurEffect()
        setUpViews()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "SummitEvent_Notification_TableViewCell", bundle: nil), forCellReuseIdentifier: notificatoinCellIdentifier)
        
        apiClass.delegate = self
        
        getNotifications()
    }
    
    
    func setupBlurEffect(){
        
        let blurEffect = UIBlurEffect(style: .light)
        
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView?.frame = self.view.bounds
        
        blurEffectView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.insertSubview(blurEffectView!, at: 0)
        
    }
    
    func setUpViews(){
        contentView.shadowColor = UIColor.black
        contentView.shadowOffset = CGSize(width: 0, height: 0.0)
        contentView.shadowOpacity = 0.2
        contentView.shadowRadius = 4
        contentView.layer.masksToBounds = false
        contentView.cornerRadius = 4
        tableView.cornerRadius = 4
        
    }
    
    func getNotifications(){
        var params = [:] as [String: Any]
        
        params["summit_event_id"] = thisEvent?.id ?? ""
        params["user_id"] = "\(userId)"
        
        apiClass.loadApiCall(identifier: summitEventNotificationsUrl, url: summitEventNotificationsUrl, param: params, method: responseType.get.rawValue, header: headerToken, view: self)
    }
    
    
    
    @IBAction func btnCloseDidTap(_ sender: UIButton) {
        onNotificationConrtollerDismiss?()
        self.dismiss(animated: true, completion: nil)
    }
    
}


// MARK: TableView Delegate, DataSource
extension SummitEvent_Notifications_ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return summitEventNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: notificatoinCellIdentifier, for: indexPath) as! SummitEvent_Notification_TableViewCell
        
        cell.lblDescription.text = summitEventNotifications[indexPath.row].description ?? ""
        cell.lblTime.text = summitEventNotifications[indexPath.row].date_time ?? ""
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let plannerVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_MyPlannerViewController") as! SummitEvent_MyPlannerViewController
        
        plannerVC.myPlannerVC_EventProgramDay = summitEventNotifications[indexPath.row].data?.event_day_index ?? 0
        
        previousVC?.navigationController?.pushViewController(plannerVC, animated: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

// MARK: API Delegate
extension SummitEvent_Notifications_ViewController: ApiDelegate {
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
        if(identifier == summitEventNotificationsUrl){
            
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventNotificationsBaseModel.self, from: data)
                
                summitEventNotifications = response.data ?? []
                self.tableView.reloadData()
                
                
                if(response.data == nil || response.data?.count == 0){
                    lblNoRecordsFound.text = response.message ?? ApiCallsMessages.NoRecordsFound.rawValue
                    lblNoRecordsFound.isHidden = false
                    tableView.isHidden = true
                }else{
                    lblNoRecordsFound.isHidden = true
                    tableView.isHidden = false
                }
                
            }
            catch {
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
            
        }
        
    }
    
    func failureApiCall(message: String, identifier: String) {
        self.view.hideAll_makeToast(message)
    }
}
