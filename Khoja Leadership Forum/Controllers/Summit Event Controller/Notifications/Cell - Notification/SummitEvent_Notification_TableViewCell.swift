//
//  SummitEvent_Notification_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 1/25/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit

class SummitEvent_Notification_TableViewCell: UITableViewCell {

    @IBOutlet weak var imgArrrow: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let degrees: Double = 180 //the value in degrees
        imgArrrow.transform = CGAffineTransform(rotationAngle: CGFloat(degrees * (Double.pi/180)))
        imgArrrow.image = UIImage(named: "grayBackArrow")?.withRenderingMode(.alwaysTemplate)
        imgArrrow.tintColor = UIColor.appLightGrey
//            .withTintColor(UIColor.appLightGrey ?? UIColor.darkGray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
