//
//  SCViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/4/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable

class SCViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    

    var textMessage = ""
    @IBOutlet weak var lblText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        //setNewDesignHeader()
        // Do any additional setup after loading the view.
        
       
        /*
        tableView.register(UINib(nibName: "SummitProgramMenusTableViewCell", bundle: nil), forCellReuseIdentifier: "cell-0")
        tableView.register(UINib(nibName: "SummitProgramCheckMyAvailabilityTableViewCell", bundle: nil), forCellReuseIdentifier: "cell-1")
        
        tableView.delegate = self
        tableView.dataSource = self
        */
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print(textMessage)
        print(lblText.text)
        lblText.text = textMessage
               
        
    }

    @IBOutlet weak var tableView: AccordionTableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-0") as! SummitProgramMenusTableViewCell
        
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1") as! SummitProgramCheckMyAvailabilityTableViewCell
            
            return cell
        }
        
    }
    

}

