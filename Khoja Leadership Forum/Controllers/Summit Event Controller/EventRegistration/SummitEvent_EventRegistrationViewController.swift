//
//  SummitEvent_EventRegistrationViewController.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 10/2/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD


class SummitEvent_EventRegistrationViewController: BaseViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var loader_ActualView: MBProgressHUD?
    
    var eventDetails: SummitEventModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNewDesignHeader()
        
        let userDefaults = UserDefaults.standard
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        
        
        loadWebView()
        

    }
    
    
    func loadWebView(){
        if let eventId = eventDetails?.id {
            loader_ActualView = showHideProgressHUD(progressHUD: nil, show: true, view: webView)
            let url = URL(string: registrationPageUrl)!
            webView.load( URLRequest(url: url) )
            webView.allowsBackForwardNavigationGestures = true
            webView.navigationDelegate = self
        }
    }
    


}

extension SummitEvent_EventRegistrationViewController: WKNavigationDelegate {

    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loader_ActualView?.hide(animated: true)
    }
}
