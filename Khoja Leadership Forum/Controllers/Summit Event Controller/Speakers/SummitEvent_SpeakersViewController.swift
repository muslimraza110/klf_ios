//
//  SummitEvent_SpeakersViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/21/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable


class SummitEvent_SpeakersViewController: BaseViewController {

    @IBOutlet weak var tableView: AccordionTableView!
    @IBOutlet weak var lblNoRecordsFound: UILabel!


    var tableViewData = [TableData]()
    
    let cellIdentifier = "TableCell"
    let headerIdentifier = "HeaderView"
    
    let recentlyAdded = "Recently Added"
    let allCountry = "All Countries"
    let allIndustry = "All Industries"
    
    var eventDetails: SummitEventModel?
    
    var userId = 0
    
    var searchTextHasChanged = false
    
    var searchBoxVc: SummitEvent_SearchBoxViewController?
    

    
    //var thisEventRequestMeeting:SummitEventRequestMeetingDataBaseModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNewDesignHeader()
        setupWireBg()
        
        userId = userData["id"] as! Int
        let userDefaults = UserDefaults.standard
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
            userDefaults.set(false, forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)

        } catch {
            print(error.localizedDescription)
        }
        
        
        
        apiClass.delegate = self
        tableView.accordionDelegate = self
        tableView.accordionDatasource = self
        
        tableView.register(UINib(nibName: "AccordianMenuHeader_TableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier)
        
        tableView.register(UINib(nibName: "SummitEvent_Speaker_TableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        
        getSpeakers(searchText: "")

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let meetingRequestSent = UserDefaults.standard.bool(forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)
        
        if meetingRequestSent {
            UserDefaults.standard.set(false, forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)
            searchBoxVc?.txtSearchBox.text = ""
            getSpeakers(searchText: "")
            searchTextHasChanged = false
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let abc = segue.identifier {
            if abc == "searchBar" {
                
                if let SearchBarVC = segue.destination as? SummitEvent_SearchBoxViewController {
                    searchBoxVc = SearchBarVC
                    searchBoxVc?.searchBoxDelegate = self
                    
                }
            }
        }
    }
    
    func getSpeakers(searchText: String){
        
        var params = [:] as [String: Any]
        
        params["summit_events_id"] = eventDetails?.id ?? ""

        params["user_id"] = "\(userId)"
        
        params["is_speaker"] = "1"
        
        if searchText.count > 0 {
            params["search"] = searchText
        }
        
        tableViewData = []
        reloadTableViewData()
        
        lblNoRecordsFound.isHidden = true


        apiClass.loadApiCall(identifier: getSummitEventSpeakerDelegates, url: getSummitEventSpeakerDelegates, param: params, method: responseType.get.rawValue, header: headerToken, view: self)
    }
    
    func reloadTableViewData(message: String = ApiCallsMessages.NoRecordsFound.rawValue){
        tableView.reloadData()
        
        if(tableViewData.isEmpty){
            lblNoRecordsFound.isHidden = false
        }else{
            lblNoRecordsFound.isHidden = true
        }
        lblNoRecordsFound.text = message
    }

}

extension SummitEvent_SpeakersViewController: ListingSearchBoxDelegate {
    func afterSearchButtonTapped(searchText: String) {
        print(searchText)
        if(searchTextHasChanged) {
            getSpeakers(searchText: searchText)
            searchTextHasChanged = false
        }

    }
    
    func onTextChanged(searchText: String) {
        searchTextHasChanged = true
    }
    
    
    
}

extension SummitEvent_SpeakersViewController: UITableViewDelegate, AccordionDelegate, AccordionDataSource {
    
    
    func tableView(_ tableView: AccordionTableView, dataForSection section: Int) -> TableData {
        return tableViewData[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRow row: Int, data: TableData) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SummitEvent_Speaker_TableViewCell
        
        cell.imgPerson.image = nil
        
        if let _data = data.rows[row] as? SummitEventPeopleDetailModel
        {

            if let user_image = _data.user_image {
                
                let imageUrl = URL(string: user_image)
                
                cell.imgPerson.sd_setImage(with: imageUrl, completed: { (image, err, cache, url) in
                    
                })
            }
            
            cell.lblPersonName.text = _data.name?.uppercased()
            cell.lblPersonBio.text = _data.short_description
            cell.lblPersonDesignation.text = _data.designation
            cell.lblDate.text = _data.speech_date
            cell.lblTime.text = _data.speech_time
            
            
            cell.onLinkedInTap = {
                if(_data.linkedin_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
                    self.openUrlInBrowser(_data.linkedin_link)
                }
            }
            
            cell.onFacebookTap = {
                if(_data.facebook_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
                    self.openUrlInBrowser(_data.facebook_link)
                }
            }
            
            
            if(_data.linkedin_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
                cell.btnLinkedIn.setImage(UIImage(named: ImageNames.tealLinkedIn), for: .normal)
                cell.btnLinkedIn.adjustsImageWhenHighlighted = true
            }else{
                cell.btnLinkedIn.setImage(UIImage(named: ImageNames.greyLinkedIn), for: .normal)
                cell.btnLinkedIn.adjustsImageWhenHighlighted = false
            }
            
            
            if(_data.facebook_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
                cell.btnFaceBook.setImage(UIImage(named: ImageNames.tealFacebook), for: .normal)
                cell.btnFaceBook.adjustsImageWhenHighlighted = true
            }else{
                cell.btnFaceBook.setImage(UIImage(named: ImageNames.greyFacebook), for: .normal)
                cell.btnFaceBook.adjustsImageWhenHighlighted = false
            }

        }
        
        
        cell.viewPersonDetails.layer.cornerRadius = 8
        cell.viewPersonDetails.clipsToBounds = false
        
        cell.viewPersonDetails.layer.shadowColor = UIColor.black.cgColor
        cell.viewPersonDetails.layer.shadowOffset = CGSize(width: 0, height: 0.0)
        cell.viewPersonDetails.layer.shadowOpacity = 0.2
        cell.viewPersonDetails.layer.shadowRadius = 2
        cell.viewPersonDetails.layer.backgroundColor = UIColor.white.cgColor
      
        
        cell.imgPerson.layer.cornerRadius = 8
        
        
        return cell
        
    }
    
    
    func enableDisableButtons(url: String?, imageName: String, button: UIButton){
        
        button.setImage(UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate), for: .normal)

        if(url?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ?? true == true){
            button.isEnabled = false
            button.tintColor = UIColor.appLightGrey
        }else{
            button.isEnabled = true
            button.tintColor = UIColor.appCyan
        }
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let data = tableViewData[section]
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! AccordianMenuHeader_TableViewCell
        header.section = section
        
        if  let title = data.header as? String {
            header.lblTime.text = title
        }
        
        header.imgBackground.image = UIImage(named: "multicolor-background")

        
        if data.status == .open {
            header.imgDropDown.image = getUpArrowImage()
        }
        else {
            header.imgDropDown.image = getDownArrowImage()
        }

//        if UIDevice().userInterfaceIdiom == .pad {
//            header.lblTime.font = .systemFont(ofSize: 22, weight: .semibold)
//        }
        
        header.btnCheckbox.reset = true
        header.btnCheckbox.isHidden = true
        
        
        header.toggleHideShowInputs(showHide: .hide)
        header.toggleHideShowInputs(showHide: .show, inputList: [.imgDropDown])
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        var _height = 40.0
//        if UIDevice().userInterfaceIdiom == .pad {
//            _height += 20.0
//        }
        return CGFloat(_height)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let personDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_RequestMeetingPeopleDetailViewController") as! SummitEvent_RequestMeetingPeopleDetailViewController
        
        if let data  = tableViewData[indexPath.section].rows[indexPath.row] as? SummitEventPeopleDetailModel{
            personDetailVC.personDetailModel = data
            
            self.navigationController?.pushViewController(personDetailVC, animated: true)
        }
        
        
    }
    
    
    func onOpenSection(section: Int) {
        toggleDropDown(section: section, request_to: .open, tableView: tableView)
    }
    
    func onCloseSection(section: Int) {
        toggleDropDown(section: section, request_to: .closed, tableView: tableView)
    }
    
    
    
}

extension SummitEvent_SpeakersViewController: ApiDelegate {
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
        
        if identifier == getSummitEventSpeakerDelegates {
            
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventDelegatePeoplesBaseModel.self, from: data )

                
                tableViewData = []
                
                let thisEventRequestMeeting = response
                
                if let tmpPeopleList = thisEventRequestMeeting.listingData
                {
                    if tmpPeopleList.recently_added?.count ?? 0 > 0 {
                        tableViewData.append(TableData(header: recentlyAdded, rows: tmpPeopleList.recently_added ?? [], status: .open))
                    }

                    if tmpPeopleList.all_country?.count ?? 0 > 0 {
                        tableViewData.append(TableData(header: allCountry, rows: tmpPeopleList.all_country ?? [],
                                                       status: tmpPeopleList.recently_added?.count ?? 0 == 0 ? .open : .closed))

                    }
                    
                    if tmpPeopleList.all_industry?.count ?? 0 > 0 {
                        tableViewData.append(TableData(header: allIndustry, rows: tmpPeopleList.all_industry ?? [],
                                                       status: tmpPeopleList.all_country?.count ?? 0 == 0 ? .open : .closed))
                    }

                }
                
                
                reloadTableViewData(message: response.message ?? ApiCallsMessages.NoRecordsFound.rawValue)
                
                
            }
            catch {
                print(error)
//                tableViewData = []
//                reloadTableViewData(message: ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
                searchTextHasChanged = true
            }
            
        }
        
    }
    
    func failureApiCall(message: String, identifier: String) {
//        tableViewData = []
//        reloadTableViewData(message: message)
        print(message)
        self.view.hideAll_makeToast(message)
        searchTextHasChanged = true
        
    }
    
    
    
}
