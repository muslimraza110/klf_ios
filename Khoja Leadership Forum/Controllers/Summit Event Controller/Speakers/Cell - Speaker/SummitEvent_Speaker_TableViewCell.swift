//
//  SummitEvent_Speaker_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/21/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

class SummitEvent_Speaker_TableViewCell: UITableViewCell {

    @IBOutlet weak var lblPersonBio: UILabel!
    @IBOutlet weak var lblPersonDesignation: UILabel!
    @IBOutlet weak var lblPersonName: UILabel!
    @IBOutlet weak var btnFaceBook: UIButton!
    @IBOutlet weak var btnLinkedIn: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewPersonDetails: UIView!
    @IBOutlet weak var imgPerson: UIImageView!
    
    var onFacebookTap: (()-> Void)?
    var onLinkedInTap: (()-> Void)?

    
    override func awakeFromNib() {
        super.awakeFromNib()
//        lblPersonBio.heightAnchor.constraint(equalToConstant: lblPersonBio.font.lineHeight * 2).isActive = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func btnTwtterTapped(_ sender: UIButton) {
        onLinkedInTap?()
    }
    
    
    @IBAction func btnFacebookTapped(_ sender: UIButton) {
        onFacebookTap?()
    }
    
    
}
