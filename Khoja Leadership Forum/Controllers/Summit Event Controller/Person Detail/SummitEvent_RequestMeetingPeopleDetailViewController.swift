//
//  SummitEvent_RequestMeetingPeopleDetailViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/12/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD

class SummitEvent_RequestMeetingPeopleDetailViewController: BaseViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnLinkedIn: UIButton!
    @IBOutlet weak var btnRequestMeeting: BlueBackgroundBtn!
    @IBOutlet weak var spacerViewBtnRequest: UIView!
    @IBOutlet weak var stackViewRight: NSLayoutConstraint!
    @IBOutlet weak var stackViewLeft: NSLayoutConstraint!
    
    var meetingRequestSent = false
    
    var timeSlotId: String?
    
    var thisEvent:SummitEventModel?
    
    var userId = 0
    
    var loader_ActualView:MBProgressHUD?
        
    var personDetailModel: SummitEventPeopleDetailModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNewDesignHeader()

        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        do {
            thisEvent = try UserDefaults.standard.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        
        
        setUpViews()
        
        loadWebView()
        
    }
    
    func setUpViews(){
        
        //  if("\(userId)" == personDetailModel?.id || thisEvent?.is_user_registered != true){
            
            btnRequestMeeting.isHidden = true
            spacerViewBtnRequest.isHidden = true
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                stackViewLeft.constant = 180
                stackViewRight.constant = 180
            }else{
                stackViewLeft.constant = 85
                stackViewRight.constant = 85
            }


      //  }
        
        if(personDetailModel?.twitter_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
            btnTwitter.setImage(UIImage(named: ImageNames.tealtwitter), for: .normal)
            btnTwitter.isUserInteractionEnabled = true
        }else{
            btnTwitter.setImage(UIImage(named: ImageNames.greytwitter), for: .normal)
            btnTwitter.isUserInteractionEnabled = false
        }
        
        if(personDetailModel?.facebook_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
            btnFacebook.setImage(UIImage(named: ImageNames.tealFacebook), for: .normal)
            btnFacebook.isUserInteractionEnabled = true
        }else{
            btnFacebook.setImage(UIImage(named: ImageNames.greyFacebook), for: .normal)
            btnFacebook.isUserInteractionEnabled = false
        }
        
        if(personDetailModel?.linkedin_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
            btnLinkedIn.setImage(UIImage(named: ImageNames.tealLinkedIn), for: .normal)
            btnLinkedIn.isUserInteractionEnabled = true
        }else{
            btnLinkedIn.setImage(UIImage(named: ImageNames.greyLinkedIn), for: .normal)
            btnLinkedIn.isUserInteractionEnabled = false
        }
        
    }
    
    
    func loadWebView(){
        if let personId = personDetailModel?.id {
            loader_ActualView = showHideProgressHUD(progressHUD: nil, show: true, view: webView)
            let url = URL(string: "\(profileDetailPageUrl)?faculty_id=\(personId)")!
            webView.load( URLRequest(url: url) )
            webView.allowsBackForwardNavigationGestures = true
            webView.navigationDelegate = self
            
        }
    }
    
    @IBAction func btnLinkedInTapped(_ sender: UIButton) {
        openUrlInBrowser(personDetailModel?.linkedin_link)
    }
    
    
    @IBAction func btnFbTapped(_ sender: UIButton) {
        openUrlInBrowser(personDetailModel?.facebook_link)
    }
    
    @IBAction func btnTwitterTapped(_ sender: UIButton) {
        openUrlInBrowser(personDetailModel?.twitter_link)
    }
    
    
    
    @IBAction func actionRequestMeeting(_ sender: Any) {
        
        
        if(timeSlotId != nil && meetingRequestSent){
            view.hideAll_makeToast("You have already sent this user a meeting request.")
            return
        }
        
        
        if(timeSlotId != nil || personDetailModel?.meeting_slots?.isEmpty == false) {
            
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_MeetingNotesViewController") as! SummitEvent_MeetingNotesViewController
            
                        
            VC.selectedSlotId = timeSlotId
            VC.requestedToUserId = personDetailModel?.id
            VC.meetingSlotData = personDetailModel?.meeting_slots
            VC.onMeetingRequestSent = { slotId, message in
                
                self.meetingRequestSent = true
                
                UserDefaults.standard.set(true, forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)
                self.view.hideAll_makeToast(message ?? "")
                
                self.personDetailModel?.meeting_slots?.forEach({day in
                    day.slots?.removeAll(where: {item in
                        item.program_id == slotId
                    })
                })
                
                self.personDetailModel?.meeting_slots?.removeAll(where: {day in
                    day.slots?.isEmpty ?? true == true
                })
                
                
            }
            
            self.present(VC, animated: true) {
                
            }
        } else{
            view.hideAll_makeToast("User is not available in any time slot.")
        }
        
        
    }
    
}


extension SummitEvent_RequestMeetingPeopleDetailViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loader_ActualView?.hide(animated: true)
    }
}

