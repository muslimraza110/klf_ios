//
//  SummitEvent_MyPlannerTabBarController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/7/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable


class SummitEvent_MyPlannerTabBarController: UITabBarController {
    
    
    var myPlannerTabBar_SelectedIndex = 0
    var myPlannerTabBar_EventProgramDay = 0
    var myPlannerTabBar_EventProgramTime = 0
    var myPlannerTabBar_IsComingFromMyItenary = false
    
    var timeSlotData = [[TimeSlotModel]]()
    var daysData = [DayData]()
    var dayCellTap: ((Int) -> Void)?
    
    var enableDropDowns: (() -> Void)?
    
    var requestMeetingCheckBoxTap: (((Int, Int) -> Void))?
    
    var plannerVC: SummitEvent_MyPlannerViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(myPlannerTabBar_SelectedIndex)
        
        UserDefaults.standard.set(false, forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)

        
        delegate = self
        
        if let VC = self.viewControllers?[0] as? SummitEvent_MyItenaryViewController
        {
            VC.myItenaryVC_EventProgramDay = myPlannerTabBar_EventProgramDay
            VC.myTabController = self
        }
        
        if let VC = self.viewControllers?[1] as? SummitEvent_RequestMeetingViewController
        {
            
            print(myPlannerTabBar_EventProgramDay)
            print(myPlannerTabBar_EventProgramTime)
            
//            VC.requestMeeting_EventProgramDay = myPlannerTabBar_EventProgramDay
//            VC.requestMeeting_EventProgramTime = myPlannerTabBar_EventProgramTime
            VC.requestMeeting_IsComingFromMyItenary = myPlannerTabBar_IsComingFromMyItenary
            VC.myTabController = self
        }
        
        self.selectedIndex = myPlannerTabBar_SelectedIndex
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        let meetingRequestSent = UserDefaults.standard.bool(forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)
        
        if meetingRequestSent {
            UserDefaults.standard.set(false, forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)
            plannerVC?.setSelectedButton(index: 0)
            selectedIndex = 0
            (viewControllers?[0] as? SummitEvent_MyItenaryViewController)?.getItineraryData()
        }

    }
    

    
     func switchToMyItinerary() {
        plannerVC?.setSelectedButton(index: 0)
        selectedIndex = 0
        (viewControllers?[0] as? SummitEvent_MyItenaryViewController)?.getItineraryData()
    }
    

    
}

extension SummitEvent_MyPlannerTabBarController: UITabBarControllerDelegate {
    
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let fromIndex = viewControllers?.firstIndex(of: fromVC) ?? -1
        let toIndex = viewControllers?.firstIndex(of: toVC) ?? -1
        
        var animated = true
        
        if toIndex == fromIndex {
            animated = false
        }
        
        var direction = TabBarSlideAnimatedTransitioning.SlideDirection.left
        
        if toIndex > fromIndex {
            direction = TabBarSlideAnimatedTransitioning.SlideDirection.right
        }
        
        return TabBarSlideAnimatedTransitioning(direction: direction, animated: animated)
    }
    
}




