//
//  SummitEvent_ProgrameViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/22/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable
import MBProgressHUD




class SummitEvent_MyPlannerViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionViewTopMenu: UICollectionView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    
    var TopMenus = [SummitEventMyPlannerTopMenusModel]()
    var defaultSelectedIndex = 0
    var myPlannerVC_EventProgramDay = 0
    var myPlannerVC_EventProgramTime = 0
    var myPlannerVC_IsComingFromMyItenary = false
    
    var isCheckAvailabilitySelected = false
    
    var myTabController: SummitEvent_MyPlannerTabBarController?
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        
        self.title = ""
        
        TopMenus.append(SummitEventMyPlannerTopMenusModel(heading: "My Itinerary", id: 0, selected: false))
        TopMenus.append(SummitEventMyPlannerTopMenusModel(heading: "Request Meeting", id: 1, selected: false))
        
        
        print(myPlannerVC_EventProgramDay)
        print(myPlannerVC_EventProgramTime)
        
        apiClass.delegate = self
        setNewDesignHeader()
        setupWireBg()
        setUpRefreshButton()
        
        

        for child in self.children{
            
            if let child = child as? SummitEvent_MyPlannerTabBarController
            {
                self.myTabController = child
            }
        }
        
        containerView.isHidden = true

        
        
        collectionViewTopMenu.delegate = self
        collectionViewTopMenu.dataSource = self
        
        
        if myPlannerVC_IsComingFromMyItenary {
            TopMenus.remove(at: 0)
        }
        
        
        
        let _dummyButton = UIButton()
        _dummyButton.tag = 0
        self.btnTopMenuTapped(sender: _dummyButton, firstCall: true)
        
        
        if UIDevice().userInterfaceIdiom == .pad {
         
            
        }
        
        
        collectionViewTopMenu.register(UINib(nibName: "MenuButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "mb-cell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        print(self.defaultSelectedIndex)
        print(defaultSelectedIndex)
        print("hello")
    }
    
    override func btnBackTapped(_ sender: UIButton) {
        if(myTabController?.selectedIndex == 0){
        
            if isCheckAvailabilitySelected {
                if let myItenaryViewController =  myTabController?.children[0] as? SummitEvent_MyItenaryViewController {
                    myItenaryViewController.btnCheckMyAvailabilityTapped(myItenaryViewController.btnCheckMyAvailability)
                }
            } else{
                super.btnBackTapped(sender)
            }
        }else{
            selectTab(position: 0)
        }
    }
    
    override func btnRefreshTapped(_ sender: UIButton) {
        if let myItenaryViewController =  myTabController?.children[0] as? SummitEvent_MyItenaryViewController {
            btnRefresh?.startRotating()
            myItenaryViewController.getItineraryData()
        }
    }
    
    
    func setSelectedButton(index: Int){
        if TopMenus.count > index {
            TopMenus.forEach({menuItem in
                menuItem.selected = false
                
            })
            
            TopMenus[index].selected = true
            
            collectionViewTopMenu.reloadData()
        }
    }
    
    //MARK:- Custom Functions
    func toggleMenuButton( selected: Bool, btn:UIButton )
    {
       if !selected
       {
           
           btn.setBackgroundImage(nil , for: .normal)
           btn.borderWidth = 2
           btn.borderColor = .lightGray
           btn.setTitleColor(.lightGray, for: .normal)
           btn.backgroundColor = .clear
           
       }
       else {
           
           btn.setBackgroundImage(UIImage(named: "bluegradient_bg") , for: .normal)
           btn.borderWidth = 2
           btn.borderColor = .clear
           btn.setTitleColor(.white, for: .normal)
           btn.backgroundColor = .clear
       }
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        print(segue.description)
        
        if let MI = segue.destination as? SummitEvent_MyPlannerTabBarController {
            
            MI.plannerVC = self
            
            
            MI.myPlannerTabBar_SelectedIndex = defaultSelectedIndex
            MI.myPlannerTabBar_EventProgramDay = myPlannerVC_EventProgramDay
            MI.myPlannerTabBar_EventProgramTime = myPlannerVC_EventProgramTime
            MI.myPlannerTabBar_IsComingFromMyItenary = myPlannerVC_IsComingFromMyItenary
            
            print(myPlannerVC_EventProgramDay)
            print(myPlannerVC_EventProgramTime)
            print("\(defaultSelectedIndex)")
            //do something here.
        }
        
        /*
        if let RM = segue.destination as? SummitEvent_RequestMeetingViewController {
            //do something here.
            for selfChildrens in self.children{
                
                if let selfChildren = selfChildrens as? SummitEvent_MyPlannerTabBarController
                {
                    RM.myTabController = selfChildren
                }
            }
        }
        */
    }
    
    
    //MARK:- Actions
    @objc func btnTopMenuTapped(  sender: UIButton, firstCall:Bool  )  {
        
        selectTab(position: sender.tag)
        
    }
    
    
    func selectTab(position: Int) {
        
        for i in 0..<TopMenus.count {
            TopMenus[i].selected = false
        }
        
        
        TopMenus[position].selected = true
        collectionViewTopMenu.reloadData()
        

                if let _id = TopMenus[position].id
                {
                    if let MyItenaryViewController =  myTabController?.children[_id] as? SummitEvent_MyItenaryViewController
                    {
                        myTabController?.selectedIndex = 0
                        defaultSelectedIndex = 0
                        //selfChildren.view.layoutIfNeeded()
                        //selfChildren.viewDidLoad()
                        
                        //MyItenaryViewController.viewDidLoad()
                    }
             
                    if let RequestMeetingViewController = myTabController?.children[_id] as? RequestMeeting_ViewController
                    {
                        myTabController?.selectedIndex = 1
                        defaultSelectedIndex = 1
                        //selfChildren.view.layoutIfNeeded()
                        //selfChildren.viewDidLoad()
                        
                        /*
                        RequestMeetingViewController.tableViewData = []
                        RequestMeetingViewController.viewDidLoad()
                        */
                        
                    }
          
                }

        
    }

}



extension SummitEvent_MyPlannerViewController: ApiDelegate
{
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
        do
        {
            
            
        }
        catch {
            
            view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
        }
        
    }
    
    func failureApiCall(message: String, identifier: String) {
        view.hideAll_makeToast(message)
    }
    
    
}

extension SummitEvent_MyPlannerTabBarController: buttonChangeDelegate {
    
    func didSelect() {
        
    }
    
    
}

extension SummitEvent_MyPlannerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TopMenus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mb-cell", for: indexPath) as! MenuButtonCollectionViewCell
        cell.btnDays.tag = indexPath.row
        
        //cell.selectButtonDelegate = self

        cell.btnDays.setTitle( TopMenus[indexPath.row].heading?.uppercased() , for: .normal)
        cell.btnDays.addTarget(self, action: #selector(btnTopMenuTapped), for: .touchUpInside)
        
        toggleMenuButton(selected: TopMenus[indexPath.row].selected ?? false, btn: cell.btnDays)
     
        return cell
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let padding: CGFloat =  5
         let collectionViewSize = collectionView.frame.size.width - padding

        print(collectionViewSize/2)
        
        var _width = 2.1
        if UIDevice().userInterfaceIdiom == .pad {
            _width = 2.05
        }
        
        if myPlannerVC_IsComingFromMyItenary {
            _width = 1
        }
        
        return CGSize(width: collectionViewSize / CGFloat( _width ), height: collectionView.frame.size.height - 5)
     }
    
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
}
