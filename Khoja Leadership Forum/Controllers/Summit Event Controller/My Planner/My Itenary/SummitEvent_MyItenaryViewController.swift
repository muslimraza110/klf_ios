//
//  SummitEvent_ProgrameViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/22/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable
import AAViewAnimator
import MBProgressHUD


class SummitEvent_MyItenaryViewController: BaseViewController {
    
    
    @IBOutlet weak var viewCheckMyAvailability: UIView!
    @IBOutlet weak var viewDateAndTable: UIView!
    @IBOutlet weak var tableView: AccordionTableView!
    @IBOutlet weak var collectionViewMenu: UICollectionView!
    
    @IBOutlet weak var lblProgramDate: UILabel!
    @IBOutlet weak var lblItineraryText: UILabel!
    
    @IBOutlet weak var btnCMABackArrow: UIButton!
    @IBOutlet weak var btnCheckMyAvailability: BlueBackgroundBtn!
    
    
    @IBOutlet weak var collectionViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewDateAndTable_Top_Constant: NSLayoutConstraint!
    @IBOutlet weak var btnDropDown: UIButton!
    
    
    var myTabController: SummitEvent_MyPlannerTabBarController?
    var tableViewData = [[TableData]]()
    var tableViewDayTimeData = [TableData]()
    
    var eventDetails: SummitEventModel?
    
    
    var clickedCheckMyAvailabilityItem: CheckAvailabilityItemModel?
    
    var clickedMeetingSection: TableData?
    
    private var cellHeightCache = [IndexPath: CGFloat]()
    
    
    
    let cellIdentifier = "TableCell"
    let cellIdentifier_RM_YesNo = "RM_YesNo"
    let cellIdentifier_MI_CMA = "MY_CMA"
    let cellIdentifier_MI_CMA_TOP = "MY_CMA_TOP"
    let headerIdentifier = "HeaderView"
    var thisEventProgram = [SummitEventProgramModel]()
    var myItenaryVC_EventProgramDay = 0
    var myItenaryVC_EventProgramTime = 0
    var tmpProgressHUD: MBProgressHUD?
    
    var userId = 0
    
    var isApiLoaded = false
    
    
    var DEFAULT_constraintCollectionView_Height:CGFloat = 50.0
    var DEFAULT_constraintViewDateAndTime_bottomSpacing:CGFloat = 8.0
    var DEFAULT_constraintLblDate_BottomSpacing:CGFloat = 8.0
    
    @IBOutlet weak var constraintCollectionView_Height: NSLayoutConstraint!
    @IBOutlet weak var constraintViewDateAndTime_bottomSpacing: NSLayoutConstraint!
    @IBOutlet weak var constraintLblDate_BottomSpacing: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblProgramDate.text = ""
        
        apiClass.delegate = self
                
        
        // To load RequestMeetingVC
        myTabController?.selectedIndex = 1
        myTabController?.selectedIndex = 0
        
        setUpButtonDropDown()
        
        let userDefaults = UserDefaults.standard
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        getItineraryData()
        
        
        collectionViewMenu.delegate = self
        collectionViewMenu.dataSource = self
        
        tableView.accordionDelegate = self
        tableView.accordionDatasource = self
        
        //        if UIDevice().userInterfaceIdiom == .pad {
        //
        //            lblProgramDate.font = .systemFont(ofSize: 25, weight: .medium)
        //        }
        
        
        btnCMABackArrow.alpha = 0
        toggleMenuButton(selected: false, btn: btnCheckMyAvailability)
        

        
        tableView.register(UINib(nibName: "AccordianMenuHeader_TableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier)
        
        tableView.register(UINib(nibName: "AccordianEventProgram_TableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        tableView.register(UINib(nibName: "SummitProgram_RequestMeeting_YesNo_TableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier_RM_YesNo)
        
        tableView.register(UINib(nibName: "SummitProgramCheckMyAvailability_Row_TableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier_MI_CMA)
        
        
        tableView.register(UINib(nibName: "SummitProgramCheckMyAvailability_Top_TableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier_MI_CMA_TOP)
        
        collectionViewMenu.register(UINib(nibName: "MenuButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "mb-cell")
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let myTabController = myTabController {
            myTabController.enableDropDowns?()
        }
        
    }
    
    func setUpButtonDropDown(){
        let image = UIImage(named: "up-arrow-white")?.withRenderingMode(.alwaysTemplate)
        btnDropDown.setImage(image, for: .normal)
        btnDropDown.tintColor = UIColor.appGrey
        btnDropDown.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    
    

    func getItineraryData(){
        if let eventId = eventDetails?.id {
            let param = ["summit_events_id": eventId, "user_id" : userId] as [String:Any]
                        
            apiClass.loadApiCall(identifier: eventProgramUrl, url: eventProgramUrl, param: param, method: responseType.get.rawValue, header: headerToken, view: myTabController?.plannerVC ?? self)
            isApiLoaded = true
        }

    }
    
    
    
    func updateMeetingStatus(status: MeetingStatus, programDetails: SummitEventProgramDetailsModel?, at index: Int) {
        
        let param = [
            "requested_by": programDetails?.meeting_request_details?.requested_by_id ?? "",
            "requested_to" : programDetails?.meeting_request_details?.requested_to_id ?? "",
            "summit_events_program_details_id": programDetails?.id ?? "",
            "note": programDetails?.meeting_request_details?.note ?? "",
            "status":status.rawValue] as [String:Any]
        
        
        self.apiClass.loadApiCall(identifier: setSummitEventMeetingRequestStatusUrl, url: setSummitEventMeetingRequestStatusUrl, param: param, method: responseType.post.rawValue, header: headerToken, view: self)
    }
    
    
    
    
    func updateCheckAvailabilityStatus(programDetailId: String, status: String){
        

        
        let param = ["user_id": self.userId, "summit_events_summit_events_program_details_id" : programDetailId, "is_available": status] as [String:Any]
        
        
        self.apiClass.loadApiCall(identifier: setSummitEventProgramDetailCheckMyAvailabilityUrl, url: setSummitEventProgramDetailCheckMyAvailabilityUrl, param: param, method: responseType.post.rawValue, header: headerToken, view: self.myTabController?.plannerVC ?? self)
    }

    
    func openPersonDetailVC(_ personDetail: SummitEventPeopleDetailModel){
        
        let personDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_RequestMeetingPeopleDetailViewController") as! SummitEvent_RequestMeetingPeopleDetailViewController
        
        personDetailVC.personDetailModel = personDetail
        
        self.navigationController?.pushViewController(personDetailVC, animated: true)
        
    }
    
    
    
    
    func openMeetingNotesVC(data: SummitEventProgramDetailsModel, isEdit: Bool, isUpdate: Bool, at index: Int){
    
        let meetingNotesVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_MeetingNotesViewController") as! SummitEvent_MeetingNotesViewController
        
        meetingNotesVC.isEdit = isEdit
        meetingNotesVC.isUpdate = isUpdate
        meetingNotesVC.meetingNotes = data.meeting_request_details?.note ?? ""
        meetingNotesVC.requestedByUserId = data.meeting_request_details?.requested_by_id ?? ""
        meetingNotesVC.requestedToUserId = data.meeting_request_details?.requested_to_id ?? ""
        meetingNotesVC.selectedSlotId = data.id
        meetingNotesVC.meetingStatus = data.meeting_request_details?.meeting_request_status?.first?.status ?? ""
        meetingNotesVC.onMeetingNotesUpdated = { notes, message in
            data.meeting_request_details?.note = notes
            self.view.hideAll_makeToast(message ?? "")
        }

        self.present(meetingNotesVC, animated: true) {
            
        }
    }
    
    
    
    
    @IBAction func btnDropDownTapped(_ sender: UIButton) {
        
        if(lblItineraryText.numberOfLines == 0){
            lblItineraryText.numberOfLines = 1
            let image = UIImage(named: "down-arrow-white")?.withRenderingMode(.alwaysTemplate)
            
            sender.setImage(image, for: .normal)

        }else{
            lblItineraryText.numberOfLines = 0
            let image = UIImage(named: "up-arrow-white")?.withRenderingMode(.alwaysTemplate)
            
            sender.setImage(image, for: .normal)
        }
        
    }
    

    
    
    @IBAction func btnCMABackArrowTapped(_ sender: UIButton) {
        
        btnCheckMyAvailabilityTapped(btnCheckMyAvailability)
    }
    
    
    @IBAction func btnCheckMyAvailabilityTapped(_ sender: UIButton) {
        
        if sender.isSelected {
            
            toggleMenuButton(selected: false, btn: sender)
            sender.isSelected = false
            btnCMABackArrow.aa_animate(animation: .toFade)
            
            constraintLblDate_BottomSpacing.constant = DEFAULT_constraintLblDate_BottomSpacing
            constraintCollectionView_Height.constant = DEFAULT_constraintCollectionView_Height
            constraintViewDateAndTime_bottomSpacing.constant = DEFAULT_constraintViewDateAndTime_bottomSpacing
            
            lblProgramDate.isHidden = false
            
            myTabController?.plannerVC?.isCheckAvailabilitySelected = false
            
            getItineraryData()
            
        }
        else {
            
            toggleMenuButton(selected: true, btn: sender)
            sender.isSelected = true
            btnCMABackArrow.aa_animate(animation: .fromFade)
            
            
            lblProgramDate.isHidden = true
            
            tmpProgressHUD = showHideProgressHUD(progressHUD: tmpProgressHUD, show: true, view: tableView, alpha: 0.6)
            
            DispatchQueue.global(qos: .default).async(execute: {
                sleep(UInt32(0.5))
                DispatchQueue.main.async(execute: {
                    self.tmpProgressHUD?.hide(animated: true)
                })
            })
            
            constraintLblDate_BottomSpacing.constant = -DEFAULT_constraintLblDate_BottomSpacing * 2
            constraintCollectionView_Height.constant = 0
            constraintViewDateAndTime_bottomSpacing.constant = 0
            
            myTabController?.plannerVC?.isCheckAvailabilitySelected = true

        }
        
        tableView.reloadData()
        
        //        UIView.animate(withDuration: 0.3) {
        //                self.view.layoutIfNeeded()
        //        }
        
        
    }
    
    //MARK:- Custom Functions
    func animateWithTransition(_ animator: AAViewAnimators) {
        
        tableView.aa_animate(duration: 1.2, springDamping: .slight, animation: animator) { (inAnimating, animView) in
            
            
        }
        
        
    }
    
    
    
    func toggleMenuButton( selected: Bool, btn:UIButton )
    {
        if !selected
        {
            btn.setBackgroundImage(nil , for: .normal)
            btn.borderWidth = 2
            btn.borderColor = .lightGray
            btn.setTitleColor(.lightGray, for: .normal)
            btn.backgroundColor = .clear
            
        }
        else {
            
            btn.setBackgroundImage(UIImage(named: "bluegradient_bg") , for: .normal)
            btn.borderWidth = 2
            btn.borderColor = .clear
            btn.setTitleColor(.white, for: .normal)
            btn.backgroundColor = .clear
        }
        
    }
    
    
    
    //MARK:- Actions
    @objc func btnProgramListTapped(  sender: UIButton  )  {
        
        if sender.tag >= thisEventProgram.count  {
            return
        }
        for i in 0..<thisEventProgram.count {
            thisEventProgram[i].selected = false
        }
        
        myItenaryVC_EventProgramDay = sender.tag
        
        thisEventProgram[sender.tag].selected = true
        
        if let myTabController = myTabController {
            
            let dayIndex = myTabController.daysData.firstIndex(where: { day in
                
                day.day ==  self.thisEventProgram[sender.tag].program_day
                
            })
            
            if let dayIndex = dayIndex {
                myTabController.dayCellTap?(dayIndex)
                
            }
            
        }
        
        collectionViewMenu.reloadData()
        
        if(isApiLoaded){
            collectionViewMenu.scrollToItem(at: IndexPath(row: myItenaryVC_EventProgramDay, section: 0), at: .right, animated: false)
            isApiLoaded = false
        }
        
        lblProgramDate.text = thisEventProgram[sender.tag].program_date
        
        tableView.reloadData()
        
    }
    
}

extension SummitEvent_MyItenaryViewController: ApiDelegate
{
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
        print(identifier)
        
        if  identifier == setSummitEventProgramDetailCheckMyAvailabilityUrl {
            
            
            do {
                
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(CheckAvailabilitySetModel.self, from: data)
                
                
                    
                var newStatus = ""
                
                if clickedCheckMyAvailabilityItem?.status == StringForOneZero.Zero.rawValue || clickedCheckMyAvailabilityItem?.status == nil{
                    newStatus = StringForOneZero.One.rawValue
                } else{
                    newStatus = StringForOneZero.Zero.rawValue
                }
                
                clickedCheckMyAvailabilityItem?.status = newStatus
                
                if let index = self.thisEventProgram.firstIndex(where: { program in
                    program.program_day == clickedCheckMyAvailabilityItem?.day
                    
                }) {
                    
                    self.thisEventProgram[index].program_list?.first(where: { item in
                        item.id == clickedCheckMyAvailabilityItem?.id
                    })?.check_my_availability_is_available = newStatus
                    
                }
                
                self.view.hideAll_makeToast(response.message ?? "")
                self.tableView.reloadData()
                resetDataForDayTimeDropDowns()
                    

                
            } catch  {
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
                self.tableView.reloadData()
            }
            
            
        }else if identifier == setSummitEventMeetingRequestStatusUrl{
                
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(RequestMeetingSetModel.self, from: data)
                
                
                if response.data?.is_error == false{
                    
                    // Update Meeting Slot
                    let model = (clickedMeetingSection?.rows.first as? SummitEventProgramDetailsModel)
                    model?.meeting_request_details?.meeting_request_status?.first?.status = response.data?.updated_status
                    
                    
                    //Update MyAvailability Data
                    tableViewDayTimeData.forEach({ tableData in
                            tableData.rows.forEach({ item in
                                if(item as? CheckAvailabilityItemModel)?.id == model?.id {
                                    (item as? CheckAvailabilityItemModel)?.lastMeetingStatus = response.data?.updated_status
                                }
                            })
                    })
                    
                    
                    
                    if(response.data?.updated_status == MeetingStatus.MEETING_ACCEPTED.rawValue ||
                        response.data?.updated_status == MeetingStatus.MEETING_REQUESTED.rawValue){
                        
                        clickedMeetingSection?.status = .open
                        
                    } else{
                        clickedMeetingSection?.status = .closed
                    }
                    
                    self.tableView.reloadData()
                    
                    resetDataForDayTimeDropDowns()

                }else{
                    self.view.hideAll_makeToast(response.message ?? "")
                }
                
            }
            catch {
                
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }
        else if identifier == eventProgramUrl{
            
            myTabController?.plannerVC?.btnRefresh?.stopRotating()
            
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventProgramBaseModel.self, from: data )
                
                self.thisEventProgram = response.programList?.programData ?? []
                
                
                tableViewData = []
                myTabController?.daysData = []
                myTabController?.timeSlotData = []
                
                for i in 0..<self.thisEventProgram.count {
                    
                    var tableData = [TableData]()
                    
                    var timeSlotListForSingleDay = [TimeSlotModel]()
                    
                    let day = DayData(day: thisEventProgram[i].program_day, date: thisEventProgram[i].program_date)
                    
                    for programList in self.thisEventProgram[i].program_list ?? [] {
                        

                        var status: Openable = .closed
                        if programList.can_request_a_meeting == RequestMeeting.YES.rawValue{
                            
                            
                            if let requestStatus = programList.meeting_request_details?.meeting_request_status?.first?.status{
                                if ((requestStatus == MeetingStatus.MEETING_ACCEPTED.rawValue ||
                                    requestStatus == MeetingStatus.MEETING_REQUESTED.rawValue) &&
                                    programList.check_my_availability_is_available == StringForOneZero.One.rawValue) {
                                    status = .open
                                    
                                }
                            }
                            
                            
                            let timeSlot = TimeSlotModel(slotId: programList.id, time: programList.start_end_time)
                            
                            if programList.check_my_availability_is_available == StringForOneZero.One.rawValue {
                                timeSlotListForSingleDay.append(timeSlot)
                            }
                            
                        }
                        
                        tableData.append(TableData(header: programList.start_end_time!, rows:  [programList], status: status))
                        
                    }
                    
                    tableViewData.append( tableData )
                    
                    if !timeSlotListForSingleDay.isEmpty{
                        myTabController?.timeSlotData.append(timeSlotListForSingleDay)
                        
                        myTabController?.daysData.append(day)
                    }
                    
                    
                }
                
                
                
                
                tableViewDayTimeData = []
                for i in 0..<self.thisEventProgram.count {
                    
                    var timeEntries = [CheckAvailabilityItemModel]()
                    
                    
                    for programList in self.thisEventProgram[i].program_list ?? [] {
                        
                        if programList.can_request_a_meeting == RequestMeeting.NO.rawValue {
                            
                            timeEntries.append(CheckAvailabilityItemModel(id: programList.id, day: thisEventProgram[i].program_day, dayId: thisEventProgram[i].id, slotId: programList.id, time: programList.start_end_time, status: CheckAvailabilityStatus.NOT_AVAILABLE.rawValue, lastMeetingStatus: programList.meeting_request_details?.meeting_request_status?.first?.status))
                            
                        } else{
                            
                            
                            timeEntries.append(CheckAvailabilityItemModel(id: programList.id, day: thisEventProgram[i].program_day, dayId: thisEventProgram[i].id, slotId: programList.id, time: programList.start_end_time, status: programList.check_my_availability_is_available, lastMeetingStatus: programList.meeting_request_details?.meeting_request_status?.first?.status))
                            
                        }
                        
                    }
                    
                    
                    // Model for headerView which shows {Yes, No, N/A}
                    timeEntries.insert(CheckAvailabilityItemModel(id: nil, day: nil, dayId: nil, slotId: nil, time: nil, status: CheckAvailabilityStatus.TOP_HEADER.rawValue,lastMeetingStatus: nil), at: 0)
                    
                    
                    let TD = TableData(header: "\(self.thisEventProgram[i].program_day ?? "") - \(self.thisEventProgram[i].program_date ?? "")", rows: timeEntries, status: .open)
                    tableViewDayTimeData.append( TD )
                    
                }
                
                
                if let requestmeetingVC = myTabController?.children[1] as? SummitEvent_RequestMeetingViewController {
                    requestmeetingVC.resetChooseMeetingDayDataSource()
                }
                
                let _dummyButton = UIButton()
                _dummyButton.tag = myItenaryVC_EventProgramDay
                self.btnProgramListTapped(sender: _dummyButton)
                
                
                
                if !(myTabController?.daysData.isEmpty ?? true ) {
                    
                    if thisEventProgram[0].program_day != myTabController?.daysData[0].day {
                        myTabController?.dayCellTap?(0)
                    }
                    
                }
                
                if(response.programList == nil){
//                    self.myTabController?.plannerVC?.view.hideAll_makeToast(response.message ?? ApiCallsMessages.NoRecordsFound.rawValue)
                    myTabController?.plannerVC?.lblNoRecordsFound.isHidden = false
                    myTabController?.plannerVC?.containerView.isHidden = true
                }else{
                    myTabController?.plannerVC?.lblNoRecordsFound.isHidden = true
                    myTabController?.plannerVC?.containerView.isHidden = false
                }
                
                
                print(self)
                
            }
                        catch let DecodingError.typeMismatch(type, context)  {
                           print("Type '\(type)' mismatch:", context.debugDescription)
                           print("codingPath:", context.codingPath)
                        }
            catch {
                myTabController?.plannerVC?.btnRefresh?.stopRotating()
                self.myTabController?.plannerVC?.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }
        
        
        
    } // API Success Method End
    
    func failureApiCall(message: String, identifier: String) {
        
        myTabController?.plannerVC?.btnRefresh?.stopRotating()

        if(identifier == setSummitEventProgramDetailCheckMyAvailabilityUrl){
            tableView.reloadData()
        }
        
        if(identifier == eventProgramUrl){
            self.myTabController?.plannerVC?.view.hideAll_makeToast(message)
        }

    }
    
    func resetDataForDayTimeDropDowns(){
        
        
        myTabController?.daysData = []
        myTabController?.timeSlotData = []
        
        for i in 0..<self.thisEventProgram.count {
            
            var timeSlotListForSingleDay = [TimeSlotModel]()
            
            let day = DayData(day: thisEventProgram[i].program_day, date: thisEventProgram[i].program_date)
            
            for programList in self.thisEventProgram[i].program_list ?? [] {
                
                if programList.can_request_a_meeting == RequestMeeting.YES.rawValue{
                    
                    let timeSlot = TimeSlotModel(slotId: programList.id, time: programList.start_end_time)
                    
                    if programList.check_my_availability_is_available == StringForOneZero.One.rawValue {
                        timeSlotListForSingleDay.append(timeSlot)
                    }
                }
            }
            
            if !timeSlotListForSingleDay.isEmpty{
                myTabController?.timeSlotData.append(timeSlotListForSingleDay)
                
                myTabController?.daysData.append(day)
            }
            
            
            
        }
        
        if let requestmeetingVC = myTabController?.children[1] as? SummitEvent_RequestMeetingViewController {
            requestmeetingVC.resetChooseMeetingDayDataSource()
        }
        
        
        if !(myTabController?.daysData.isEmpty ?? true ) {
            
            myTabController?.dayCellTap?(0)
            
        }
    }
    
    
}



extension SummitEvent_MyItenaryViewController: UITableViewDelegate, AccordionDelegate, AccordionDataSource, AccordianHeaderCheckboxDelegate
{
    func beforeCheckboxSelect(button: UIButton, sectionIndex: Int) {
        
        myItenaryVC_EventProgramTime = sectionIndex
        if button.tag == 1
        {
            /*
             print(myItenaryVC_EventProgramDay)
             print(myItenaryVC_EventProgramTime)
             
             let gotoDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_MyPlannerViewController") as! SummitEvent_MyPlannerViewController
             
             gotoDetailVC.myPlannerVC_EventProgramDay = myItenaryVC_EventProgramDay
             gotoDetailVC.myPlannerVC_EventProgramTime = myItenaryVC_EventProgramTime
             gotoDetailVC.myPlannerVC_IsComingFromMyItenary = true
             
             gotoDetailVC.defaultSelectedIndex = 1
             
             
             self.navigationController?.pushViewController(gotoDetailVC, animated: true)
             */
            
            
            /*
             
             
             if let mtc = myTabController {
             mtc.selectedIndex = 1
             }*/
        }
        
    }
    
    func afterCheckboxSelect(button: UIButton, sectionIndex: Int) {
        
        
        if let myTabController = myTabController {
            
            
            if let dayIndex = myTabController.daysData.firstIndex(where: { day in
                day.day == thisEventProgram[myItenaryVC_EventProgramDay].program_day
            }){
                
                let programItemId = self.thisEventProgram[myItenaryVC_EventProgramDay].program_list?[sectionIndex].id
                
                let timeSlotIndex: Int? = myTabController.timeSlotData[dayIndex].firstIndex(where: { timeSlotModel in
                    timeSlotModel.slotId == programItemId
                })
                
                if let timeSlotIndex = timeSlotIndex{
                    myTabController.requestMeetingCheckBoxTap?(dayIndex,timeSlotIndex)
                    
                    myTabController.selectedIndex = 1
                    
                    myTabController.plannerVC?.setSelectedButton(index: 1)
                    
                }
                
            }
            
        }
        
    }
    
    
    // Fix for tableview jumping when opening/closing section
    // Start
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeightCache[indexPath] ?? 44
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightCache[indexPath] = cell.bounds.height
    }
    // End
    
    func tableView(_ tableView: AccordionTableView, dataForSection section: Int) -> TableData {
        
        print(btnCheckMyAvailability.isSelected)
        
        if btnCheckMyAvailability.isSelected {
            
            return tableViewDayTimeData[section]
        }
        
        return tableViewData[myItenaryVC_EventProgramDay][section]
    }
    
    
    func tableView(_ tableView: UITableView, cellForRow row: Int, data: TableData) -> UITableViewCell {
        
        
        if btnCheckMyAvailability.isSelected {
            
            let checkAvailabilityCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_MI_CMA) as! SummitProgramCheckMyAvailability_Row_TableViewCell
            
            
            if let checkAvailabilityModel = data.rows[row] as? CheckAvailabilityItemModel {
                
                
                if(checkAvailabilityModel.status == CheckAvailabilityStatus.TOP_HEADER.rawValue){
                    
                    return tableView.dequeueReusableCell(withIdentifier: cellIdentifier_MI_CMA_TOP) as! SummitProgramCheckMyAvailability_Top_TableViewCell
                    
                    
                } else{
                    
                    checkAvailabilityCell.CMA_Detail = checkAvailabilityModel
                    checkAvailabilityCell.lblTime.text = checkAvailabilityModel.time
                    checkAvailabilityCell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
                    
                    if checkAvailabilityModel.status == CheckAvailabilityStatus.NOT_AVAILABLE.rawValue {
                        checkAvailabilityCell.btnCheckbox.allowedValues = [.line]
                        checkAvailabilityCell.btnCheckbox.defaultTag = KLCheckbox.KLCheckboxTypes.line.rawValue
                        checkAvailabilityCell.btnCheckbox.isEnabled = true
                        checkAvailabilityCell.btnCheckbox.alpha = 1
                    }
                    else
                    {
                        checkAvailabilityCell.btnCheckbox.allowedValues = [.cross, .tick]
                        
                        if checkAvailabilityModel.status == StringForOneZero.One.rawValue {
                            checkAvailabilityCell.btnCheckbox.defaultTag = KLCheckbox.KLCheckboxTypes.tick.rawValue
                            
                            if (checkAvailabilityModel.lastMeetingStatus == MeetingStatus.MEETING_REQUESTED.rawValue || checkAvailabilityModel.lastMeetingStatus == MeetingStatus.MEETING_ACCEPTED.rawValue){
                                
                                checkAvailabilityCell.btnCheckbox.isEnabled = false
                                checkAvailabilityCell.btnCheckbox.alpha = 0.5
                                
                            }else{
                                checkAvailabilityCell.btnCheckbox.isEnabled = true
                                checkAvailabilityCell.btnCheckbox.alpha = 1
                            }

                        }
                        else if checkAvailabilityModel.status == StringForOneZero.Zero.rawValue {
                            checkAvailabilityCell.btnCheckbox.defaultTag = KLCheckbox.KLCheckboxTypes.cross.rawValue
                            checkAvailabilityCell.btnCheckbox.isEnabled = true
                            checkAvailabilityCell.btnCheckbox.alpha = 1
                        }else{
                            checkAvailabilityCell.btnCheckbox.defaultTag = KLCheckbox.KLCheckboxTypes.empty.rawValue
                            checkAvailabilityCell.btnCheckbox.isEnabled = true
                            checkAvailabilityCell.btnCheckbox.alpha = 1
                        }
                        //checkAvailabilityCell.btnCheckbox.defaultTag = KLCheckbox.KLCheckboxTypes.tick.rawValue
                    }
                    
                    
                    checkAvailabilityCell.onCheckboxTap = { checkBoxStatus, CMA_Detail in
                        
                        self.clickedCheckMyAvailabilityItem = checkAvailabilityModel
                        
                        
                        if checkBoxStatus == .tick {
                            
                            
                            self.updateCheckAvailabilityStatus(programDetailId: CMA_Detail.id!, status: "1")
                            
                            
                        } else if checkBoxStatus == .cross {
                            
                            
                            self.updateCheckAvailabilityStatus(programDetailId: CMA_Detail.id!, status: "0")
                            

                            
                        }
                        
                    }
                    
                }
                
                
                
            }
            
            
            return checkAvailabilityCell
            
        }else{
            
            
            if let programDetails = data.rows.first as? SummitEventProgramDetailsModel {
                
                if programDetails.can_request_a_meeting == RequestMeeting.YES.rawValue {
                    
                    let requestMeetingCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_RM_YesNo) as! SummitProgram_RequestMeeting_YesNo_TableViewCell
                    
                    
                    requestMeetingCell.data = programDetails
                    requestMeetingCell.onBtnAcceptTap = { details in
                        
                        self.clickedMeetingSection = data

                        self.updateMeetingStatus(status: MeetingStatus.MEETING_ACCEPTED, programDetails: details, at: row)
                        

                    }
                    
                    requestMeetingCell.onBtnRejectTap = { details in
                        self.clickedMeetingSection = data


                        self.updateMeetingStatus(status: MeetingStatus.MEETING_REJECTED, programDetails: details, at: row)
                        
                    }
                    
                    //Callback for chat icon used previously for updating meeting notes
                    
                    
//                    requestMeetingCell.onBtnMeetingNotesTap = { details in
//                        print(details?.id)
//                        if let details = details{
//                            self.openMeetingNotesVC(data: details, isEdit: true, isUpdate: true)
//                        }
//                    }
                    
                    requestMeetingCell.onBtnCancelMeetingTap = { details in
                        
                        self.clickedMeetingSection = data
//                        self.clickedProgramDetailsMeeting = details
                        self.updateMeetingStatus(status: MeetingStatus.MEETING_HOST_CANCELED, programDetails: details, at: row)
                        
                    }
                    
                    requestMeetingCell.toggleHideShowInputs(showHide: .hide)
                    
                    if let requestStatus = programDetails.meeting_request_details?.meeting_request_status?.first?.status {
                        
                        if requestStatus == MeetingStatus.MEETING_REQUESTED.rawValue {
                            if programDetails.meeting_request_details?.is_received == true{
                                
                                requestMeetingCell.toggleHideShowInputs(showHide: .show, inputList: [.acceptRejectStack])
                                
                            } else{
                                requestMeetingCell.toggleHideShowInputs(showHide: .show, inputList: [.cancelNotesStack])
                            }
                            
                            
                        } else if requestStatus == MeetingStatus.MEETING_ACCEPTED.rawValue {
                            requestMeetingCell.toggleHideShowInputs(showHide: .show, inputList: [.cancelNotesStack])
                        }
                        
                        if programDetails.meeting_request_details?.requested_by_id == "\(userId)" {
                            
                            requestMeetingCell.lblName?.text = programDetails.meeting_request_details?.requested_to_details?.name ?? ""
                            requestMeetingCell.lblDesignation?.text = programDetails.meeting_request_details?.requested_to_details?.designation ?? ""
                        } else{
                            requestMeetingCell.lblName?.text = programDetails.meeting_request_details?.requested_by_details?.name ?? ""
                            requestMeetingCell.lblDesignation?.text = programDetails.meeting_request_details?.requested_by_details?.designation ?? ""
                        }
                    }else{
                        
                    }
                    
                    
                    
                    requestMeetingCell.includeRectCorner = [ .bottomLeft, .bottomRight ]
                    
                    return requestMeetingCell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AccordianEventProgram_TableViewCell
                
                    
                    cell.selectionStyle = .none
                    
                    
                    
                    
                    if let item = data.rows[row] as? String
                    {
                        cell.lblProgramList.text = "\(item)"
                    }
                    
                    cell.textView.text = ""
                    cell.textView.attributer =  "".black
                    cell.textView.isEditable = false
                    cell.textView.isSelectable = false
                    cell.textView.textContainerInset = UIEdgeInsets.zero
                    cell.textView.textContainer.lineFragmentPadding = 0
                    cell.textView.attributer.linkColor = UIColor.appCyan
                    
                    
                    var _increase_height        = 0
                    
                    if let _description = programDetails.description
                    {
                        cell.lblProgramList.text = "\(_description)"
                    }
                    
                    if let _speakers = programDetails.speakers {
                        
                        if(_speakers.count > 0) {
                            
                            
                            cell.textView.attributer = cell.textView.attributer
                                //                               .appendHtml("<br><br>")
                                .append("Confirmed Speakers: ").black.font(.boldSystemFont(ofSize: 12))
                                .appendHtml("<br><br>")
                            
                            _increase_height    += 40
                            
                            for speaker in _speakers {
                                
                                
                                cell.textView.attributer = cell.textView.attributer.append(speaker.name!).underline.makeInteract { (st) in
                                    print("CALL Speaker \(st) \(String(describing: speaker.id))")
                                    
                                    self.openPersonDetailVC(speaker)
                                }
                                .append(speaker.designation == "" || speaker.designation == nil ? "" : ", \(speaker.designation!).")
                                .appendHtml("<br>")
                                
                                _increase_height += 10
                            }
                        }
                    }
                    
                    
                    if let _delegates = programDetails.delegates  {
                        
                        if(_delegates.count > 0) {
                            
                            if cell.textView.attributedText.length > 0
                            {
                                cell.textView.attributer =  cell.textView.attributer
                                    .appendHtml("<br>")
                                
                                _increase_height        += 10
                            }
                            
                            cell.textView.attributer =  cell.textView.attributer
                                .append("Confirmed Delegates: ").black
                                .appendHtml("<br><br>")
                            _increase_height        += 20
                            
                            for delegate in _delegates {
                                
                                cell.textView.attributer =  cell.textView.attributer.append(delegate.name!).underline.makeInteract { ( st ) in
                                    print("CALL Delegate \(st)")
                                    self.openPersonDetailVC(delegate)
                                }
                                .append(delegate.designation == "" || delegate.designation == nil ? "" : ", \(delegate.designation!).")
                                .appendHtml("<br>")
                                
                                _increase_height        += 10
                            }
                        }
                    }
                    
                    
                    
                    cell.textView.attributer = cell.textView.attributer.all.font(UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 12))
                    
                    
                    print(cell.lblProgramList.calculateMaxLines())
                    
                    //                      if UIDevice().userInterfaceIdiom == .pad {
                    //
                    //                          cell.lblProgramList.font = .systemFont(ofSize: 20, weight: .regular)
                    //
                    //                      }
                    
                    
                    
                    let cc = cell.textView.contentSize
                    cell.textView.isScrollEnabled = false
                    cell.textView.frame.size.height = cc.height
                    
                    
                    
                    return cell
                }
            }
            
            
            return tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AccordianEventProgram_TableViewCell
            
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        if btnCheckMyAvailability.isSelected {
            
            return tableViewDayTimeData.count
        }
        else {
            if thisEventProgram.count > 0
            {
                return thisEventProgram[myItenaryVC_EventProgramDay].program_list?.count ?? 0
            }
            return  0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! AccordianMenuHeader_TableViewCell
        
        header.delegate = self
        header.checkboxDelegate = self
        header.section = section
        
        header.toggleHideShowInputs(showHide: .hide)
        
        
        
        var data:TableData
        
        if btnCheckMyAvailability.isSelected {
            data = tableViewDayTimeData[section]
            
            header.toggleHideShowInputs(showHide: .hide)
            //header.toggleHideShowInputs(showHide: .show, inputList: [.imgDropDown, .viewRightSpacer])
            //header.constraintlblTitle_Trailing.constant = 10
            header.imgBackground.image = UIImage(named: "multicolor-background")
            
            
            if let dateString = data.header as? String {
                header.lblTime.text = dateString
            }
            
        }
        else {
            data = tableViewData[myItenaryVC_EventProgramDay][section]
            
            if let programDetails = (data.rows as? [SummitEventProgramDetailsModel])?.first{
                
                header.lblTime.text = programDetails.start_end_time
                header.lblTitle.text = programDetails.title
                
                header.data = programDetails
                
//                header.onBtnCheckboxTap = { details in
//                    print(details?.id)
//                }
                
                header.onBtnMeetingNotesTap = { details in
                    //print(details?.id)
                    if let details = details {
                        if(details.meeting_request_details?.requested_by_id == "\(self.userId)"){
                            self.openMeetingNotesVC(data: details, isEdit: true, isUpdate: true, at: section)
                        }else{
                            self.openMeetingNotesVC(data: details, isEdit: false, isUpdate: false, at: section)
                        }
                    }
                    
                }
                
                if let canRequestMeeting = programDetails.can_request_a_meeting {
                    switch canRequestMeeting {
                    case RequestMeeting.YES.rawValue, RequestMeeting.NO.rawValue:
                        
                        header.toggleHideShowInputs(showHide: .hide)
                        
                        if programDetails.event_session_type == Constants.MyItinerary.plenary {
                            header.toggleHideShowInputs(showHide: .show, inputList: [ .viewRightSpacer, .lblRequestMeeting])
                            header.lblRequestMeeting.text = Constants.MyItinerary.moreInfo
                            header.isPlenary = false
                            header.btnCheckbox.allowedValues = [.empty]
                            header.imgBackground.image = UIImage(named: "bluebackground-big")
                        } else {
                            header.toggleHideShowInputs(showHide: .show, inputList: [ .viewRightSpacer, .lblRequestMeeting])
                            header.isPlenary = true
                            debugPrint("Status of Session \(programDetails)")
                            for index in 0..<(programDetails.session_details?.count ?? 0){
                                if (programDetails.session_details?[index].i_am_enrolled ?? false) {
                                    header.lblRequestMeeting.text = Constants.MyItinerary.changeSession
                                    break
                                } else {
                                    header.lblRequestMeeting.text = Constants.MyItinerary.selectSession
                                }
                            }

                            header.btnCheckbox.allowedValues = [.empty]
                            header.imgBackground.image = UIImage(named: "bluebackground-big")
                        }

//                        if programDetails.check_my_availability_is_available == StringForOneZero.One.rawValue {
//
//                            if let requestStatus = programDetails.meeting_request_details?.meeting_request_status?.first?.status {
//                                if (requestStatus == MeetingStatus.MEETING_CANCELED.rawValue ||
//                                    requestStatus == MeetingStatus.MEETING_REJECTED.rawValue){
//
//                                    header.toggleHideShowInputs(showHide: .show, inputList: [.btnCheckbox, .viewRightSpacer, .lblRequestMeeting])
//                                    header.btnCheckbox.isEnabled = true
//                                    header.btnCheckbox.backgroundColor = UIColor.white
//                                    header.btnCheckbox.allowedValues = [.empty]
//                                    header.imgBackground.image = UIImage(named: "bluebackground-big")
//
//                                }
//
//                                if requestStatus == MeetingStatus.MEETING_REQUESTED.rawValue {
//
//                                    if programDetails.meeting_request_details?.is_received == true{
//
//                                        header.toggleHideShowInputs(showHide: .show, inputList: [.btnMeetingRequest, .btnMeetingNotes, .dividerView])
//                                        header.imgBackground.image = UIImage(named: "gradient up")
//
//                                    }else{
//
//                                        header.toggleHideShowInputs(showHide: .show, inputList: [.btnAwaitingResponse, .btnMeetingNotes, .dividerView])
//                                        header.imgBackground.image = UIImage(named: "gradient up")
//
//                                    }
//                                }
//
//                                if requestStatus == MeetingStatus.MEETING_ACCEPTED.rawValue {
//
//                                    header.toggleHideShowInputs(showHide: .show, inputList: [.btnMeetingScheduled, .btnMeetingNotes, .dividerView])
//                                    header.imgBackground.image = UIImage(named: "gradient up")
//
//                                }
//
//
//                            } else{
//                                // second call for more info ▲ ▼
//                                // .btnCheckbox will add in input list if needed.
//                                header.toggleHideShowInputs(showHide: .show, inputList: [ .viewRightSpacer, .lblRequestMeeting, .imgDropDown])
//                                header.lblRequestMeeting.text = Constants.MyItinerary.moreInfo
//                                header.btnCheckbox.isEnabled = true
//                                header.isPlenary = false
//                                header.btnCheckbox.backgroundColor = UIColor.white
//                                header.btnCheckbox.allowedValues = [.empty]
//                                header.imgBackground.image = UIImage(named: "bluebackground-big")
//                            }
//
//                        } else{
//                            // here is first call for change session
//                            // .btnCheckbox will add in input list if needed.
//                            header.toggleHideShowInputs(showHide: .show, inputList: [ .viewRightSpacer, .lblRequestMeeting, .imgDropDown])
//                            header.isPlenary = true
//                            header.lblRequestMeeting.text = Constants.MyItinerary.selectSession
//                            header.btnCheckbox.isEnabled = false
//                            header.btnCheckbox.backgroundColor = UIColor.appCheckBoxDisabled
//
//                            header.btnCheckbox.allowedValues = [.empty]
//                            header.imgBackground.image = UIImage(named: "bluebackground-big")
//
//                        }
                        header.data = self.thisEventProgram[myItenaryVC_EventProgramDay].program_list?[section]
                        
                        break
//                    case RequestMeeting.NO.rawValue:
//
//                        header.toggleHideShowInputs(showHide: .hide)
//                        header.toggleHideShowInputs(showHide: .show, inputList: [.imgDropDown, .lblTitle])
//                        header.imgBackground.image = UIImage(named: "bluebackground-big")
                        
                      //  break
                    default:
                        print(canRequestMeeting)
                        break
                    }
                }
                                
            }
        }
        
        if data.status == .open {
            header.imgDropDown.image = getUpArrowImage()
            
            
            if btnCheckMyAvailability.isSelected {
                header.contentView.roundCorners(corners: [.allCorners], radius: 8)
            } else{
                header.contentView.roundCorners(corners: [.topRight, .topLeft], radius: 8)
            }
        }
        else {
            header.imgDropDown.image = getDownArrowImage()
            
            
            header.contentView.roundCorners(corners: [.allCorners], radius: 8)
        }

        //        if UIDevice().userInterfaceIdiom == .pad {
        //            header.lblTime.font = .systemFont(ofSize: 22, weight: .semibold)
        //        }
        
        header.btnCheckbox.reset = true
        
        
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    /*
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
     var _height = 45.0
     if UIDevice().userInterfaceIdiom == .pad {
     _height += 20.0
     }
     return CGFloat(_height)
     }
     */
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let _height = 40.0
        //        if UIDevice().userInterfaceIdiom == .pad {
        //            _height += 20.0
        //        }
        return CGFloat(_height)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
        
    }

    func onOpenSection(section: Int) {
        toggleDropDown(section: section, request_to: .open, tableView: tableView, allCornersAlwaysRounded: false)
    }
    
    func onCloseSection(section: Int) {
        toggleDropDown(section: section, request_to: .closed, tableView: tableView, allCornersAlwaysRounded: false)
    }
}



extension SummitEvent_MyItenaryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return thisEventProgram.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mb-cell", for: indexPath) as! MenuButtonCollectionViewCell
        cell.btnDays.tag = indexPath.row
        cell.btnDays.clipsToBounds = true
        cell.btnDays.cornerRadius = 8
        cell.btnDays.setTitle(thisEventProgram[indexPath.row].program_day?.uppercased() , for: .normal)
        cell.btnDays.addTarget(self, action: #selector(btnProgramListTapped), for: .touchUpInside)
        toggleMenuButton(selected: thisEventProgram[indexPath.row].selected ?? false, btn: cell.btnDays)
        return cell
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  5
        let collectionViewSize = collectionView.frame.size.width - padding
        print(collectionViewSize/2)
        var _width = 2.3
        if UIDevice().userInterfaceIdiom == .pad {
            _width = 2.35
        }
        return CGSize(width: collectionViewSize / CGFloat( _width ), height: collectionView.frame.size.height - 5)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }


}

extension SummitEvent_MyItenaryViewController: AccordianMenuHeader_TableViewCellDelegate {
    func didTitleTapped(isPlenary: Bool, programDetails: SummitEventProgramDetailsModel?) {
        debugPrint("Plenary : \(isPlenary)")
        guard let showItineraryViewController = UIStoryboard.init(name: Constants.StoryBoard.summitEvent, bundle: Bundle.main).instantiateViewController(withIdentifier: Constants.ViewControllers.showItineraryViewController) as? ShowItineraryViewController else {
            return
        }
        showItineraryViewController.changeSessionDelegate = self
        showItineraryViewController.programDetails = programDetails
        showItineraryViewController.modalPresentationStyle = .overFullScreen
        self.present(showItineraryViewController, animated: false, completion: nil)
    }
}

extension SummitEvent_MyItenaryViewController: ShowItineraryViewControllerForChangeSession {
    func sessionJoind(sessionID: String, isJoin: Bool) {
        self.getItineraryData()
    }
}
