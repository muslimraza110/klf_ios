//
//  SummitProgram_RequestMeeting_YesNo_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/14/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

enum CellViews{
    case cancelNotesStack
    case acceptRejectStack
    case dividerCancelNotesStack
    case btnMeetingNotes
}


class SummitProgram_RequestMeeting_YesNo_TableViewCell: UITableViewCell {

    @IBOutlet weak var imageBlueBackground: UIImageView!
    var includeRectCorner: UIRectCorner?
    @IBOutlet weak var acceptRejectStack: UIStackView!
    @IBOutlet weak var cancelNotesStack: UIStackView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnCancelMeeting: UIButton!
    @IBOutlet weak var dividerCancelNotesStack: UIView!
    @IBOutlet weak var btnMeetingNotes: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    
    var data: SummitEventProgramDetailsModel?
    var onBtnAcceptTap: ((SummitEventProgramDetailsModel?) -> ())?
    var onBtnRejectTap: ((SummitEventProgramDetailsModel?) -> ())?
    var onBtnCancelMeetingTap: ((SummitEventProgramDetailsModel?) -> ())?
    var onBtnMeetingNotesTap: ((SummitEventProgramDetailsModel?) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnAccept.tintColor = .white
        btnReject.tintColor = .white
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        if let includeRectCorner = includeRectCorner {
            print(includeRectCorner)
            imageBlueBackground.roundCorners(corners: includeRectCorner , radius: 8)
            imageBlueBackground.clipsToBounds = true
        }
    }
    
    
    @IBAction func btnAcceptTapped(_ sender: UIButton) {
        onBtnAcceptTap?(data)
    }
    
    
    
    @IBAction func btnRejectTapped(_ sender: UIButton) {
        onBtnRejectTap?(data)
    }
    
    
    @IBAction func btnCancelMeetingTapped(_ sender: UIButton) {
        onBtnCancelMeetingTap?(data)
    }
    
    @IBAction func btnMeetingNotesTapped(_ sender: UIButton) {
        onBtnMeetingNotesTap?(data)
    }
    
    
    func toggleHideShowInputs( showHide:toggleHideShow )
    {
        if showHide == .hide {
            cancelNotesStack.isHidden = true
            acceptRejectStack.isHidden = true
            dividerCancelNotesStack.isHidden = true
            btnMeetingNotes.isHidden = true
        }
        else {
            cancelNotesStack.isHidden = false
            acceptRejectStack.isHidden = false
            dividerCancelNotesStack.isHidden = false
            btnMeetingNotes.isHidden = false
        }
    }
    
    func toggleHideShowInputs( showHide:toggleHideShow, inputList:[CellViews] )
    {
        
        var isHide = false
        
        if showHide == .hide {
            isHide = true
        }
        else {
            isHide = false
        }
        
        for iL in inputList {
            if iL == .cancelNotesStack {
                cancelNotesStack.isHidden = isHide
            }

            if iL == .acceptRejectStack {
                acceptRejectStack.isHidden = isHide
            }

            if iL == .dividerCancelNotesStack {
                dividerCancelNotesStack.isHidden = isHide
            }

            if iL == .btnMeetingNotes {
                btnMeetingNotes.isHidden = isHide
            }

        }
        
    }
    
}


