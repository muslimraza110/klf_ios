//
//  ShowItineraryTableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/3/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import AttributedTextView
import UIKit

protocol  ShowItineraryTableViewCellDelegate: AnyObject {
    func didJoinButtonTapped(isJoin: Bool, title: String, selectedSession: SessionDetails?)
    func openPersonDetailVC(_ personDetail: SummitEventPeopleDetailModel?)
    func didLinkLongPressed(linkText: String, recognizer: UITapGestureRecognizer)
}

class ShowItineraryTableViewCell: UITableViewCell {

    weak var delegate: ShowItineraryTableViewCellDelegate?
    @IBOutlet weak private var joinButton: UIButton!
    @IBOutlet weak private var linkLabel: CopyableLabel!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var textView: AttributedTextView!
    @IBOutlet weak private var availableSeatsLabel: UILabel!
    @IBOutlet weak private var zoomMeetingLabel: UILabel!
    @IBOutlet weak private var roomLimitLabel: UILabel!
    var isPlenary: Bool?
    private var zoomLink: String = ""

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var sessionDetails: SessionDetails? {
        didSet {
            self.setProgramDetails()
        }
    }

    @IBAction func joinButtonTapped(_ sender: UIButton) {
        guard let sessionDetails = self.sessionDetails else { return }
        self.delegate?.didJoinButtonTapped(isJoin: !sender.isSelected, title: sender.titleLabel?.text ?? "",  selectedSession: sessionDetails)
        sender.isSelected.toggle()
    }

    private func setJoinButtonData(isJoind: Bool = false) {
        self.joinButton.setTitle( isJoind ? Constants.MyItinerary.leave : Constants.MyItinerary.join, for: .normal)
        self.joinButton.isSelected = isJoind
        self.joinButton.backgroundColor = isJoind ? .appRedColor : .appBlueColor
    }

    private func setProgramDetails() {
        guard let sessionDetails = self.sessionDetails, let isPlenary = self.isPlenary else { return }
        self.titleLabel.text = sessionDetails.title

        self.roomLimitLabel.text = .roomLimit + "\(sessionDetails.room_limit ?? 0)"
        self.availableSeatsLabel.text = .availableSeats + "\((sessionDetails.room_limit ?? 0) - (sessionDetails.room_occupied ?? 0))"
        self.joinButton.isHidden = isPlenary
        self.roomLimitLabel.isHidden = isPlenary
        self.availableSeatsLabel.isHidden = isPlenary
        self.setJoinButtonData(isJoind: sessionDetails.i_am_enrolled ?? false)

        if (sessionDetails.sessionFaculties?.speakers?.count ?? 0) > 0 {
            self.textView.attributer = "".all
            if let speakers = sessionDetails.sessionFaculties?.speakers {
                self.textView.attributer = self.textView.attributer
                    .append(sessionDetails.description ?? "").color(UIColor.appGreyColor).fontName(AppFonts.PoppinsRegular.rawValue).size(13)
                    .appendHtml("<br><br>")
                self.textView.attributer = self.textView.attributer
                    .append(.confirmedSpeakers).color(UIColor.appGreyColor).fontName(AppFonts.PoppinsSemiBold.rawValue).size(13)
                    .appendHtml("<br><br>")
                for speaker in speakers {
                    let  person = speaker.summitEventsSessionFacultiesDetail
                    self.textView.attributer = self.textView.attributer.append(person?.name ?? "").color(UIColor.appCyan ?? .black).fontName(AppFonts.PoppinsRegular.rawValue).size(13)
                        .underline.makeInteract { (st) in
                            self.delegate?.openPersonDetailVC(person)
                        }
                        .append(person?.designation == "" || ((person?.designation) == nil) ?  "" : ", \(person?.designation ?? "").").color(UIColor.appGreyColor).fontName(AppFonts.PoppinsRegular.rawValue).size(13)
                        .appendHtml("<br>")
                }
            }
        } else {
            self.textView.textColor = UIColor.appGrey
            self.textView.font = UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 13)
            self.textView.text = sessionDetails.description
        }
        self.textView.textContainerInset =  UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        if (sessionDetails.zoomLink?.isEmpty ?? false) {
            self.zoomMeetingLabel.isHidden = true
            self.linkLabel.isHidden = true
        }else {
            self.setZoomLink(link: sessionDetails.zoomLink ?? "")
        }
    }

    private func setZoomLink(link: String) {
        self.zoomLink = link
        self.linkLabel.text = link
        self.linkLabel.underLineText = link
        self.linkLabel.customFont = UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 16)
        self.linkLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleLongPress)))
    }
    
    @objc private func handleLongPress(recognizer: UITapGestureRecognizer) {
        self.delegate?.didLinkLongPressed(linkText: zoomLink, recognizer: recognizer)
    }
}
