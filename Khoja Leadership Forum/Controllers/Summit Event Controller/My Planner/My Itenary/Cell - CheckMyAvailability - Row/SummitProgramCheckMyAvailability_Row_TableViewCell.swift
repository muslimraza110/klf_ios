//
//  SummitProgramCheckMyAvailability_Row_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/19/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

class SummitProgramCheckMyAvailability_Row_TableViewCell: UITableViewCell {

    @IBOutlet weak var btnCheckbox: KLCheckbox!
    @IBOutlet weak var lblTime: UILabel!
    
    var CMA_Detail: CheckAvailabilityItemModel?
    
    var onCheckboxTap: ((KLCheckbox.KLCheckboxTypes, CheckAvailabilityItemModel) -> ())?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        print(btnCheckbox.allowedValues)
        btnCheckbox.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
      

        // Configure the view for the selected state
    }
    
    @IBAction func btnCheckboxTapped(_ sender: KLCheckbox) {
        
        
    }
    
}

extension SummitProgramCheckMyAvailability_Row_TableViewCell: KLCheckboxDelegate {
    
    func KL_afterSelect(button: UIButton) {
        
        print(button.tag)
        print("after:")
        
        

    }
    
    func KL_beforeSelect(button: UIButton) {
        
        print(button.tag)
        print("before:")
        
        if let status = KLCheckbox.KLCheckboxTypes.init(rawValue: button.tag) {
            self.onCheckboxTap?(status, CMA_Detail!)
        }
    }
    
    
}
