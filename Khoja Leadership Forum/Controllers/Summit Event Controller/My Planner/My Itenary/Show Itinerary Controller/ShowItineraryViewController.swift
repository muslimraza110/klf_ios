//
//  ShowItineraryViewController.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/3/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit

protocol  ShowItineraryViewControllerDelegate: AnyObject {
    func didJoin(isJoin: Bool)
}

protocol  ShowItineraryViewControllerForChangeSession: AnyObject{
    func sessionJoind(sessionID: String, isJoin: Bool)
}
class ShowItineraryViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var titleLabel: UILabel!
    weak var delegate: ShowItineraryViewControllerDelegate?
    weak var changeSessionDelegate: ShowItineraryViewControllerForChangeSession?
    internal var programDetails: SummitEventProgramDetailsModel?
    private var clickedSessionModel: SessionDetails?
    private var eventDetails: SummitEventModel?
    private var apiClass = ApiCalling2()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
    }

    private func prepareView() {
        self.apiClass.delegate = self
        self.titleLabel.text = self.programDetails?.title
        let userDefaults = UserDefaults.standard
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        self.setBackgroundBlurred()
        self.tableView.register(UINib(nibName: Constants.MyItineraryTableCell.showItineraryTableViewCell, bundle: nil), forCellReuseIdentifier:  Constants.MyItineraryTableCell.showItineraryTableViewCell)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300

    }

    // MARK: - Action
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension ShowItineraryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = programDetails?.session_details?.count else {
            return 0
        }
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier:  Constants.MyItineraryTableCell.showItineraryTableViewCell, for: indexPath) as? ShowItineraryTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.isPlenary = !(self.programDetails?.event_session_type == Constants.MyItinerary.breakout)
        cell.sessionDetails = self.programDetails?.session_details?[indexPath.row]
        return cell
    }
}

extension ShowItineraryViewController: ShowItineraryTableViewCellDelegate {

    func didLinkLongPressed(linkText: String, recognizer: UITapGestureRecognizer) {
        guard let url = URL(string: linkText) else { return }
        UIApplication.shared.open(url)
    }

    func didJoinButtonTapped(isJoin: Bool, title: String, selectedSession: SessionDetails?) {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure to want to \(title) this session", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: .yes, style: .default, handler: { (action: UIAlertAction!) in
            self.clickedSessionModel = selectedSession
            self.join(isJoin: isJoin, sessionID: selectedSession?.id)
        }))
        refreshAlert.addAction(UIAlertAction(title: .no, style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        present(refreshAlert, animated: true, completion: nil)
    }

    // MARK: - API Call
    private func join(isJoin: Bool, sessionID: String?) {
        let param =
            ["summit_events_faculty_id": eventDetails?.summit_events_faculty_id ?? "",
             "summit_events_program_details_sessions_id" : sessionID ?? "",
             "enrolled": isJoin ?  "1" : "0"] as [String:Any]
        apiClass.loadApiCall(identifier: summitEventsSet_summit_event_breakout_session_registrationURL, url: summitEventsSet_summit_event_breakout_session_registrationURL, param: param, method: responseType.get.rawValue, header: headerToken, view: self)
    }

    func openPersonDetailVC(_ personDetail: SummitEventPeopleDetailModel?){
        let personDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.summitEvent_RequestMeetingPeopleDetailViewController) as! SummitEvent_RequestMeetingPeopleDetailViewController
        personDetailVC.personDetailModel = personDetail
        // self.navigationController?.pushViewController(personDetailVC, animated: true)
        self.present(personDetailVC, animated: true, completion: nil)
    }
}

// MARK: - API Delegates
extension ShowItineraryViewController: ApiDelegate {
    func successApiCall(data: Data, identifier: String, status: Bool) {
        if identifier == summitEventsSet_summit_event_breakout_session_registrationURL {
            do {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(BaseModelClass.self, from: data)

                let previouslyJoinedSessionId = programDetails?.session_details?.first(where: { item in
                    item.i_am_enrolled == true
                })?.id
                if(previouslyJoinedSessionId != nil){
                    if (previouslyJoinedSessionId == clickedSessionModel?.id) {
                        // MARK: -  Leaving a Joined Session
                        programDetails?.session_details?.forEach { item in
                            if (item.id == clickedSessionModel?.id) {
                                item.room_occupied = (item.room_occupied ?? 1) - 1
                            }
                        }
                    }else{
                        // MARK: -  Leaving and Joining a new Session
                        programDetails?.session_details?.forEach { item in
                            if (item.id == clickedSessionModel?.id) {
                                item.room_occupied = (item.room_occupied ?? 0) + 1
                            } else if (item.id == previouslyJoinedSessionId) {
                                item.room_occupied = (item.room_occupied ?? 1) - 1
                            }
                        }
                    }

                }else{

                    // MARK: -  Joining a new session
                    programDetails?.session_details?.forEach { item in
                        if (item.id == clickedSessionModel?.id) {
                            item.room_occupied = (item.room_occupied ?? 0) + 1
                        }
                    }
                }
                programDetails?.session_details?.forEach { item in
                    if (item.id == clickedSessionModel?.id) {
                        item.i_am_enrolled = !(clickedSessionModel?.i_am_enrolled ?? true)
                    } else {
                        item.i_am_enrolled = false
                    }
                }
                self.changeSessionDelegate?.sessionJoind(sessionID: programDetails?.id ?? "", isJoin: true)
                self.tableView.reloadData()
                self.view.makeToast(response.message)
            } catch {
                self.view.makeToast(Constants.Errors.networkError)
            }
        }
    }

    func failureApiCall(message: String, identifier: String) {
        self.view.makeToast(message)
    }
}
