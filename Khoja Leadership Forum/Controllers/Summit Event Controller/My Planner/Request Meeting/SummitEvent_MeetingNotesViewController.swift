//
//  SummitEvent_MeetingNotesViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/13/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import DropDown
import MBProgressHUD

protocol blurEffectDelegate {
    func afterDimiss()
}

protocol SummitEvent_MeetingNotesViewControllerDelegate: AnyObject {
    func notesUpdatedSuccessfully(updatedNotes: String, meetingChildID: String)
}

class SummitEvent_MeetingNotesViewController: UIViewController {
    
    @IBOutlet weak var btnSendNotes: BlueBackgroundBtn!
    weak var delegate: SummitEvent_MeetingNotesViewControllerDelegate?
    @IBOutlet weak var btnMeetingTime: UIButton!
    @IBOutlet weak var btnMeetingDay: UIButton!
    @IBOutlet weak var txtViewMeetingNotes: UITextView!
    @IBOutlet weak var imgBackgroundMN: UIImageView!
    @IBOutlet weak var viewSendNotes: UIView!
    @IBOutlet weak var viewMeetingNotes: UIView!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    
    var blurEffectView: UIVisualEffectView?
    
    // Indicates whether the txtViewMeetingNotes is Edittable and btnSendNotes is visible
    var isEdit = true
    
    var isUpdate = false
    
    var requestedToUserId: String?
    
    var requestedByUserId: String?
    
    var userId = 0
    
    var meetingNotes = ""
    
    var meetingStatus = MeetingStatus.MEETING_REQUESTED.rawValue
    
    var meetingSlotData: [SummitEventDayAvailabilityModel]?
    
    var onMeetingRequestSent: ((_ slotId: String?,_ message: String?) -> ())?
    
    var selectedSlotId: String?
    
    var onMeetingNotesUpdated: ((_ meetingNotes: String,_ message: String?) -> ())?
    
    
    var timeSlotData = [[TimeSlotModel]]()
    var daysData = [DayData]()
    
    var apiClass = ApiCalling2()
    
    
    var requestMeeting_EventProgramDay = 0
    var requestMeeting_EventProgramTime = 0
    
    let chooseMeetingDay = DropDown()
    let chooseMeetingTime = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtViewMeetingNotes.text = meetingNotes
        
        apiClass.delegate = self
        
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        if !isUpdate {
            requestedByUserId = "\(userId)"
        }
        
        setupBlueEffect()
        
        viewMeetingNotes.shadowColor = UIColor.black
        viewMeetingNotes.shadowOffset = CGSize(width: 0, height: 0.0)
        viewMeetingNotes.shadowOpacity = 0.2
        viewMeetingNotes.shadowRadius = 4
        viewMeetingNotes.layer.masksToBounds = false
        
        btnMeetingDay.titleLabel?.adjustsFontSizeToFitWidth = true
        btnMeetingTime.titleLabel?.adjustsFontSizeToFitWidth = true
        
        
        imgBackgroundMN.roundCorners(corners: [.topLeft, .topRight], radius: 8.0)
        
        setUpViews()
        
    }
    
    
    
    func loadDummyData(){
        daysData = [
            DayData(day: "Day 01", date: "thursday"),
            DayData(day: "Day 02", date: "friday"),
            DayData(day: "Day 03", date: "saturday")
        ]
        
        timeSlotData = [
            [
                TimeSlotModel(slotId: "dsfawdf", time: "01:00 - 02:00"),
                TimeSlotModel(slotId: "dsfawdsf", time: "02:00 - 03:00"),
                TimeSlotModel(slotId: "dsfdawdf", time: "03:00 - 04:00"),
                TimeSlotModel(slotId: "dsfdgsfawdf", time: "04:00 - 05:00")
            ],
            [
                TimeSlotModel(slotId: "dsfawsdf", time: "01:00 - 02:00"),
                TimeSlotModel(slotId: "dsfdawddf", time: "03:00 - 04:00"),
                TimeSlotModel(slotId: "dsfdsgsfawdf", time: "04:00 - 05:00")
            ],
            [
                TimeSlotModel(slotId: "dsfdawddf", time: "03:00 - 04:00"),
                TimeSlotModel(slotId: "dsfdsgsfawdf", time: "04:00 - 05:00")
            ]
        ]
    }
    
    
    func loadMeetingSlots() {
        
        meetingSlotData?.forEach({ item in
            if(item.slots?.isEmpty == false) {
                daysData.append(DayData(day: item.program_day, date: item.program_date))
                let timeSlots = item.slots?.map({ slot in
                    TimeSlotModel(slotId: slot.program_id, time: slot.time)
                })
                timeSlotData.append(timeSlots ?? [])
            }
            
        })
        
    }
    
    func setupBlueEffect(){
        
        let blurEffect = UIBlurEffect(style: .light)
        
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView?.frame = self.view.bounds
        
        blurEffectView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.insertSubview(blurEffectView!, at: 0)
        
    }
    
    
    
    func setUpViews(){
        
        let image = UIImage(named: "black-crossicon")?.withRenderingMode(.alwaysTemplate)
        
        btnCancel.setImage(image, for: .normal)
        btnCancel.tintColor = .white
        
        //Fix for spacing between button icon and title
        if (UIDevice.current.userInterfaceIdiom == .pad){
            btnMeetingDay.titleEdgeInsets.right = 15
            btnMeetingTime.titleEdgeInsets.right = 15
        }
        
        
        if isEdit {
            
            txtViewMeetingNotes.isEditable = true
            btnSendNotes.isHidden = false
            
            
            if isUpdate {
                
                btnMeetingDay.isHidden = true
                btnMeetingTime.isHidden = true
                btnSendNotes.setTitle("UPDATE NOTES", for: .normal)
                
            } else if selectedSlotId != nil {
                
                btnMeetingTime.isHidden = true
                btnMeetingDay.isHidden = true
                
            } else {
                
                loadMeetingSlots()
                setupMeetingTimeDropDown()
                setupMeetingDayDropDown()
                
            }
            
        }else{
            txtViewMeetingNotes.isEditable = false
            btnSendNotes.isHidden = true
            btnMeetingDay.isHidden = true
            btnMeetingTime.isHidden = true
            dividerView.isHidden = true
            
        }
    }
    
    func selectMeetingTime( _SelectDayIndex:Int, _SelectTimeIndex:Int )
    {
        self.chooseMeetingTime.dataSource = timeSlotData[_SelectDayIndex].map({ timeModel in
            timeModel.time ?? ""
        })
        
        if(!chooseMeetingTime.dataSource.isEmpty){
            self.btnMeetingTime.setTitle(chooseMeetingTime.dataSource[_SelectTimeIndex], for: .normal)
            self.chooseMeetingTime.selectRow(at: _SelectTimeIndex)
            chooseMeetingTime.selectionAction?(_SelectTimeIndex,chooseMeetingTime.dataSource[_SelectTimeIndex])
            
        } else{
            self.btnMeetingTime.setTitle("", for: .normal)
        }
        
    }
    
    
    @IBAction func btnMeetingDayTapped(_ sender: Any) {
        chooseMeetingDay.show()
    }
    
    @IBAction func btnMeetingTimeTapped(_ sender: Any) {
        chooseMeetingTime.show()
    }
    
    @IBAction func btnCancelTapped(_ sender: Any) {
        
        self.dismiss(animated: true)
        
    }
    
    
    @IBAction func btnSendNotesTapped(_ sender: Any) {
        
        
        if let requestedToUserId = requestedToUserId{
//            let param =
//                ["requested_by": requestedByUserId,
//                    "requested_to" : requestedToUserId,
//                    "summit_events_program_details_id": selectedSlotId,
//                    "note": txtViewMeetingNotes.text ?? "",
//                    "status": meetingStatus] as [String:Any]
//
//            print(param)
//            apiClass.loadApiCall(identifier: setSummitEventMeetingRequestStatusUrl, url: setSummitEventMeetingRequestStatusUrl, param: param, method: responseType.post.rawValue, header: headerToken, view: self)
                        let param =
                            ["user_id": userId,
                                "summit_events_meeting_request_parent_id" : requestedToUserId,
                                "notes": txtViewMeetingNotes.text ?? ""] as [String:Any]

                        print(param)
                        apiClass.loadApiCall(identifier: updateNotesUrl, url: updateNotesUrl, param: param, method: responseType.post.rawValue, header: headerToken, view: self)
        }
        
        
        
        
    }
    
    
    func setupMeetingTimeDropDown() {
        
        chooseMeetingTime.anchorView = btnMeetingTime
        chooseMeetingTime.backgroundColor = UIColor(hexString: "#F0F0F0")
        chooseMeetingTime.setupCornerRadius(10)
        
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseMeetingTime.bottomOffset = CGPoint(x: 0, y: btnMeetingTime.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        
        // Action triggered on selection
        chooseMeetingTime.selectionAction = { [weak self] (index, item) in
            self?.btnMeetingTime.setTitle(item, for: .normal)
            if let dayIndex = self?.requestMeeting_EventProgramDay {
                print(self?.timeSlotData[dayIndex][index].time! as Any)
                self?.selectedSlotId = self?.timeSlotData[dayIndex][index].slotId
            }
        }
    }
    
    func setupMeetingDayDropDown() {
        
        
        chooseMeetingDay.anchorView = btnMeetingDay
        chooseMeetingDay.backgroundColor = UIColor(hexString: "#F0F0F0")
        chooseMeetingDay.setupCornerRadius(10)
        
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseMeetingDay.bottomOffset = CGPoint(x: 0, y: btnMeetingDay.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseMeetingDay.dataSource = daysData.map({ day in
            day.date ?? ""
        })
        
        // Action triggered on selection
        chooseMeetingDay.selectionAction = { [weak self] (index, item) in
            self?.btnMeetingDay.setTitle(item, for: .normal)
            self?.requestMeeting_EventProgramDay = index
            
            self?.selectMeetingTime(_SelectDayIndex: index, _SelectTimeIndex: 0)
            
        }
        
        if !chooseMeetingDay.dataSource.isEmpty {
            chooseMeetingDay.selectRow(at: 0)
            chooseMeetingDay.selectionAction?(0, chooseMeetingDay.dataSource[0])
        }
    }
    
    
    
}

extension SummitEvent_MeetingNotesViewController: ApiDelegate {
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
       // if identifier == setSummitEventMeetingRequestStatusUrl{
        if identifier == updateNotesUrl {
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(RequestMeetingSetModel.self, from: data)
                self.delegate?.notesUpdatedSuccessfully(updatedNotes: txtViewMeetingNotes.text ?? "", meetingChildID: self.requestedToUserId ?? "")
                
                if response.data?.is_error == false{
                    
                    if isUpdate {
                        onMeetingNotesUpdated?(txtViewMeetingNotes.text ?? "", response.message)
                    }else{
                        onMeetingRequestSent?(selectedSlotId, response.message)
                    }
                    
                    self.dismiss(animated: true, completion: nil)
                    
                } else{
                    self.dismiss(animated: true, completion: nil)
                    self.view.hideAll_makeToast(response.message ?? ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
                }
            }
            catch {
                print(error)
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }
        
    }
    
    func failureApiCall(message: String, identifier: String) {
        view.hideAll_makeToast(message)
    }
    
    
}
