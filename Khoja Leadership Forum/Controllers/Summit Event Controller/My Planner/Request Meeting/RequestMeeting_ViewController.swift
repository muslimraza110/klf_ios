//
//  RequestMeeting_ViewController.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/8/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//


import AccordionTable
import DropDown
import MBProgressHUD
import WSTagsField
import UIKit

class RequestMeeting_ViewController: BaseViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var tagsField: WSTagsField!
    var myTabController: SummitEvent_MyPlannerTabBarController?
    var thisEventRequestMeeting:SummitEventRequestMeetingDataBaseModel?
    var allDelegates: [SummitEventPeopleDetailModel]?
    var tableViewData = [TableData]()
    let chooseMeetingDay = DropDown()
    let chooseMeetingTime = DropDown()
    let recentlyAdded = "Recently Added"
    let allCountry = "All Countries"
    let allIndustry = "All Industries"
    var requestMeeting_EventProgramDay = 0
    var requestMeeting_EventProgramTime = 0
    var requestMeeting_IsComingFromMyItenary = false
    var selectedSlotId = ""
    var eventDetails: SummitEventModel?
    var shouldPreventItemSelcetionOfChooseMeetingDayDropDown = false
    var searchBoxVc: SummitEvent_SearchBoxViewController?
    var userId = 0
    @IBOutlet weak var btnMeetingDay: UIButton!
    @IBOutlet weak var btnMeetingTime: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var delegatesTextField: UITextField!
    var meetingNotesPlaceHolderText = "Enter Meeting Notes"
    var selectedSlotProgramDetailsId = ""
    let maxHeightForTagsField = CGFloat(120)
    @IBOutlet weak var tagsFieldHeightContraint: NSLayoutConstraint!

    // MARK:- Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        userId = userData["id"] as! Int
        let userDefaults = UserDefaults.standard
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        apiClass.delegate = self
        print(requestMeeting_EventProgramDay)
        print(requestMeeting_EventProgramTime)
        myTabController?.dayCellTap = { index in
            self.chooseMeetingDay.selectRow(at: index)
            if let myTabController = self.myTabController {
                if myTabController.timeSlotData[index].count > 0 {
                    self.chooseMeetingDay.selectionAction?(index, self.chooseMeetingDay.dataSource[index])
                }
            }
        }
        myTabController?.requestMeetingCheckBoxTap = { dayIndex, timeSLotIndex in
            self.enableDisableDropdowns(enable: false)
            if self.chooseMeetingDay.selectedItem == self.myTabController?.daysData[dayIndex].date {
                self.chooseMeetingTime.selectRow(at: timeSLotIndex)
                self.chooseMeetingTime.selectionAction?(timeSLotIndex, self.chooseMeetingTime.dataSource[timeSLotIndex])
            } else{
                self.shouldPreventItemSelcetionOfChooseMeetingDayDropDown = true
                self.chooseMeetingDay.selectRow(at: dayIndex)
                self.chooseMeetingDay.selectionAction?(dayIndex, self.chooseMeetingDay.dataSource[dayIndex])
                self.selectMeetingTime(_SelectDayIndex: dayIndex, _SelectTimeIndex: timeSLotIndex)
            }
        }
        self.btnMeetingDay.isHidden = true
        self.btnMeetingTime.isHidden = true
        var _rmp_index = requestMeeting_EventProgramTime
        if _rmp_index > 1
        {
            _rmp_index = 1
        }
        setupMeetingTimeDropDown()
        setupMeetingDayDropDown()
        myTabController?.enableDropDowns = { [weak self] in
            self?.enableDisableDropdowns(enable: true)
        }
        if requestMeeting_IsComingFromMyItenary {
            enableDisableDropdowns(enable: false)
        }
        self.getSepakersAndDelegates(slotId: "")
        self.setupFacultyTagsField()
        self.resetChooseMeetingDayDataSource()
    }

    func setupFacultyTagsField() {
        tagsField.layoutMargins = UIEdgeInsets(top: 2, left: 6, bottom: 2, right: 6)
        tagsField.contentInset = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        tagsField.spaceBetweenLines = 10.0
        tagsField.spaceBetweenTags = 10.0
        tagsField.font = UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 18)
        tagsField.backgroundColor = .clear

        tagsField.textColor = .white
        tagsField.textField.textColor = .white
        tagsField.selectedColor = .clear
        tagsField.selectedTextColor = .clear
        tagsField.isScrollEnabled = true
        tagsField.enableScrolling = true
        tagsField.numberOfLines = 3
        tagsField.placeholderColor = UIColor.appGrey
        tagsField.placeholder = "Select Delegates"


        tagsFieldHeightContraint.constant = tagsField.intrinsicContentSize.height

        tagsField.onDidChangeHeightTo = { tagField, height in
            if(height < self.maxHeightForTagsField){
                self.tagsFieldHeightContraint.constant = height
            }
        }

        tagsField.onDidAddTag = { wsTagsField, wsTagView in
            wsTagsField.tagViews.forEach({ wsTagView in
                wsTagView.isUserInteractionEnabled = false
                wsTagView.backgroundColor = .appBlueColor
            })
        }
        tagsField.textField.isUserInteractionEnabled = false
        tagsField.tintColor = .appOrangeColor


        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tagsFieldTapped(sender: )))
        tagsField.addGestureRecognizer(tapGesture)

    }

    @objc func tagsFieldTapped(sender: UITapGestureRecognizer){
        if let showDelegatesVC = storyboard?.instantiateViewController(withIdentifier: "ShowDelegates_ViewController") as? ShowDelegates_ViewController{
            showDelegatesVC.alldelegates = allDelegates?.map({ item in
                item.copy() as! SummitEventPeopleDetailModel

            })
            showDelegatesVC.delegate = self
            self.present(showDelegatesVC, animated: true) {}
        }
    }

    func updateTagsField(){
        tagsField.removeTags()
        allDelegates?.forEach({item in
            if(item.isSelected){
                let tag = WSTag(item.name ?? "")
                tagsField.addTag(tag)
            }
        })
        tagsField.tagViews.forEach({tagsView in
            tagsView.isUserInteractionEnabled = false
            (tagsView as UIView).cornerRadius = 5
        })

    }

    func enableDisableDropdowns(enable: Bool){
        if enable {
            btnMeetingDay.isEnabled = true
            btnMeetingTime.isEnabled = true

            btnMeetingTime.setImage(UIImage(named: "dropdown"), for: .normal)
            btnMeetingDay.setImage(UIImage(named: "dropdown"), for: .normal)
        } else {
            btnMeetingDay.isEnabled = false
            btnMeetingTime.isEnabled = false

            btnMeetingTime.setImage(nil, for: .normal)
            btnMeetingDay.setImage(nil, for: .normal)
        }

    }


    func resetChooseMeetingDayDataSource(){

        if let myTabController = myTabController {

            selectedSlotId  = ""
            // You can also use localizationKeysDataSource instead. Check the docs.
            chooseMeetingDay.dataSource = myTabController.daysData.map({ day in
                day.date ?? ""
            })
            if myTabController.daysData.isEmpty {
                self.btnMeetingDay.setTitle("Day", for: .normal)
                self.btnMeetingTime.setTitle("Time", for: .normal)
                self.btnMeetingDay.isHidden = true
                self.btnMeetingTime.isHidden = true
                chooseMeetingTime.dataSource = []
            }else{
                self.btnMeetingDay.isHidden = false
                self.btnMeetingTime.isHidden = false
            }
        }
    }

    //MARK:- Actions
    @IBAction func actionMeetingTime(_ sender: Any) {

        print(requestMeeting_IsComingFromMyItenary)

        chooseMeetingTime.show()
    }

    @IBAction func actionMeetingDay(_ sender: Any) {

        print(requestMeeting_IsComingFromMyItenary)


        chooseMeetingDay.show()
    }

    @IBAction func submitButtonTapped(_ sender: UIButton) {
        let summitedID = self.allDelegates?.filter({ ($0.isSelected )
        }).map({
            return $0.summit_events_faculty_id ?? ""
        })
        if (summitedID?.count ?? 0) > 0 {
        if let requestedByUserId = eventDetails?.summit_events_faculty_id{
            let param =
                ["requested_by": requestedByUserId,
                 "requested_to" : summitedID?.joined(separator: ",") ?? "",
                    "summit_events_program_details_id": selectedSlotId,
                    "note": self.textView.text ?? "",
                    "user_id": "\(userId)",
                    "is_update": false,
                    "status": MeetingStatus.MEETING_REQUESTED.rawValue] as [String:Any]

            print(param)
            apiClass.loadApiCall(identifier: setSummitEventMeetingRequestStatusUrl, url: setSummitEventMeetingRequestStatusUrl, param: param, method: responseType.post.rawValue, header: headerToken, view: self)
        }
        } else {
            view.hideAll_makeToast("Please select delegates.")
        }
    }

    func getSepakersAndDelegates(slotId: String){
        var params = [:] as [String: Any]
        params["summit_events_id"] = eventDetails?.id ?? ""
        params["user_id"] = "\(userId)"
        print(params)
        apiClass.loadApiCall(identifier: getSummitEventSpeakerDelegates, url: getSummitEventSpeakerDelegates, param: params, method: responseType.get.rawValue, header: headerToken, view: self)
    }

    //MARK:- DropDown Function
    func setupMeetingTimeDropDown() {

        chooseMeetingTime.anchorView = btnMeetingTime
        chooseMeetingTime.backgroundColor = UIColor(hexString: "#F0F0F0")
        chooseMeetingTime.setupCornerRadius(10)


        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseMeetingTime.bottomOffset = CGPoint(x: 0, y: btnMeetingTime.bounds.height)

        // You can also use localizationKeysDataSource instead. Check the docs.

        // Action triggered on selection
        chooseMeetingTime.selectionAction = { [weak self] (index, item) in
            self?.btnMeetingTime.setTitle(item, for: .normal)
            self?.requestMeeting_EventProgramTime = index
            if let myTabController = self?.myTabController {
                if let dayIndex = self?.requestMeeting_EventProgramDay {
                    if let timeIndex = self?.requestMeeting_EventProgramTime {
                        let slotId = myTabController.timeSlotData[dayIndex][timeIndex].slotId ?? ""
                        if slotId != self?.selectedSlotId {
                            print(item)
                            self?.searchBoxVc?.txtSearchBox.text = ""
                            self?.getSepakersAndDelegates(slotId: slotId)
                        }
                        self?.selectedSlotId = slotId
                    }
                }
            }
        }
        // DataSource is Set when something is selected from chooseMeetingDay DropDown
    }

    func setupMeetingDayDropDown() {
        chooseMeetingDay.anchorView = btnMeetingDay
        chooseMeetingDay.backgroundColor = UIColor(hexString: "#F0F0F0")
        chooseMeetingDay.setupCornerRadius(10)

        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseMeetingDay.bottomOffset = CGPoint(x: 0, y: btnMeetingDay.bounds.height)
        chooseMeetingDay.selectionAction = { [weak self] (index, item) in
            self?.btnMeetingDay.setTitle(item, for: .normal)
            self?.requestMeeting_EventProgramDay = index
            if !(self?.shouldPreventItemSelcetionOfChooseMeetingDayDropDown ?? false){
                self?.selectMeetingTime(_SelectDayIndex: index, _SelectTimeIndex: 0)
            }
            self?.shouldPreventItemSelcetionOfChooseMeetingDayDropDown = false
        }
        // DataSource set in func resetChooseMeetingDayDataSource()
    }


    //MARK:- Custom Functions
    func selectMeetingTime( _SelectDayIndex:Int, _SelectTimeIndex:Int )
    {
        if let myTabController = myTabController
        {
            self.chooseMeetingTime.dataSource = myTabController.timeSlotData[_SelectDayIndex].map({ timeModel in
                timeModel.time ?? ""
            })
            if(!chooseMeetingTime.dataSource.isEmpty){
                self.btnMeetingTime.setTitle(chooseMeetingTime.dataSource[_SelectTimeIndex], for: .normal)
                self.chooseMeetingTime.selectRow(at: _SelectTimeIndex)
                chooseMeetingTime.selectionAction?(_SelectTimeIndex,chooseMeetingTime.dataSource[_SelectTimeIndex])
            } else{
                self.btnMeetingTime.setTitle("", for: .normal)
            }
        }

    }

}

//MARK:- APIDelegate
extension RequestMeeting_ViewController: ApiDelegate {

    func successApiCall(data: Data, identifier: String, status: Bool) {
        let jsonDecoder = JSONDecoder()
        if identifier == getSummitEventSpeakerDelegates{

            do
            {
                self.btnMeetingDay.isHidden = false
                self.btnMeetingTime.isHidden = false
                let response = try jsonDecoder.decode(SummitEventDelegatePeoplesBaseModel.self, from: data )
                let thisEventRequestMeeting = response
                if let tmpPeopleList = thisEventRequestMeeting.listingData
                {
                    // Removing those speakers/delegates from the list who already have a meeting placed at this slot
                    tmpPeopleList.recently_added?.forEach({ item in
                        if(item.meeting_slots == nil) {
                            tmpPeopleList.recently_added?.removeAll(where: { person in
                                person.id == item.id
                            })
                        }

                    })
                    tmpPeopleList.all_country?.forEach({ item in

                        if(item.meeting_slots == nil) {
                            tmpPeopleList.all_country?.removeAll(where: { person in
                                person.id == item.id
                            })
                        }
                    })
                    tmpPeopleList.all_industry?.forEach({ item in
                        if(item.meeting_slots == nil) {
                            tmpPeopleList.all_industry?.removeAll(where: { person in
                                person.id == item.id
                            })
                        }
                    })

                    if tmpPeopleList.all_users_by_type?.count ?? 0 > 0 {
                        debugPrint("Number of users : \(tmpPeopleList.all_users_by_type?.count ?? 0)")
                        self.allDelegates = tmpPeopleList.all_users_by_type
                    }
                }
                var message = response.message

                if(message == nil || message?.isEmpty == true){
                    message = ApiCallsMessages.NoRecordsFound.rawValue
                }
            }
            catch {
                print(error)
                view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }

        if identifier == setSummitEventMeetingRequestStatusUrl {
            do {
            let response = try jsonDecoder.decode(RequestMeetingSetModel.self, from: data )
                view.makeToast(response.message)
            }
            catch let err {
                print(err)
                view.makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }
    }

    func failureApiCall(message: String, identifier: String) {
        if identifier == setSummitEventMeetingRequestStatusUrl {
            let refreshAlert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
            refreshAlert.addAction(UIAlertAction(title: .ok, style: .default, handler: { (action: UIAlertAction!) in
            }))
            present(refreshAlert, animated: true, completion: nil)
        }else {
            print(message)
            view.hideAll_makeToast(message)
        }
    }
}

extension RequestMeeting_ViewController: ShowDelegates_ViewControllerDelegate {
    func didOKTapped(allDelegates: [SummitEventPeopleDetailModel]?) {
        self.allDelegates = allDelegates
        self.updateTagsField()
    }
}
