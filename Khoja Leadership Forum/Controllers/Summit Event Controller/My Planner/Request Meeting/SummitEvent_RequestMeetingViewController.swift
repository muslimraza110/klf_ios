//
//  SummitEvent_RequestMeetingViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/7/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable
import DropDown
import MBProgressHUD

class SummitEvent_RequestMeetingViewController: BaseViewController {
    
    //MARK:- Variables & Outlets
    var myTabController: SummitEvent_MyPlannerTabBarController?
    var thisEventRequestMeeting:SummitEventRequestMeetingDataBaseModel?
    var tableViewData = [TableData]()
    let cellIdentifier = "TableCell"
    let headerIdentifier = "HeaderView"
    
    let chooseMeetingDay = DropDown()
    let chooseMeetingTime = DropDown()
    
    
    let recentlyAdded = "Recently Added"
    let allCountry = "All Countries"
    let allIndustry = "All Industries"
    
    
    //    var dataMeetingTime = [String]()
    //    var dataMeetingDay = [String]()
    
    var requestMeeting_EventProgramDay = 0
    var requestMeeting_EventProgramTime = 0
    var requestMeeting_IsComingFromMyItenary = false
    
    var selectedSlotId = ""
    
    var eventDetails: SummitEventModel?
    
    var shouldPreventItemSelcetionOfChooseMeetingDayDropDown = false
    
    var searchBoxVc: SummitEvent_SearchBoxViewController?
    
    var userId = 0
    
    var searchTextHasChanged = false
    
    
    @IBOutlet weak var btnMeetingDay: UIButton!
    @IBOutlet weak var btnMeetingTime: UIButton!
    
    @IBOutlet weak var tableViewPeoples: AccordionTableView!
    @IBOutlet weak var lblEventDescription: UILabel!
    @IBOutlet weak var lblNoRecordsFound: UILabel!

    
    
    
    
    //MARK:- Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        

        userId = userData["id"] as! Int
        let userDefaults = UserDefaults.standard
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        
        lblEventDescription.text = eventDetails?.venue_address?.replacingOccurrences(of: "\r\n", with: "").replacingOccurrences(of: "\n", with: "")
        
        apiClass.delegate = self
        
        
        print(requestMeeting_EventProgramDay)
        print(requestMeeting_EventProgramTime)
        
        
        myTabController?.dayCellTap = { index in
            
            self.chooseMeetingDay.selectRow(at: index)
            
            if let myTabController = self.myTabController {
                
                if myTabController.timeSlotData[index].count > 0 {
                    self.chooseMeetingDay.selectionAction?(index, self.chooseMeetingDay.dataSource[index])
                }
                //                else{
                //                    // clear delegates list and reload tableview
                //                }
            }
            
        }
        
        myTabController?.requestMeetingCheckBoxTap = { dayIndex, timeSLotIndex in
            
            self.enableDisableDropdowns(enable: false)
            
            if self.chooseMeetingDay.selectedItem == self.myTabController?.daysData[dayIndex].date {
                self.chooseMeetingTime.selectRow(at: timeSLotIndex)
                self.chooseMeetingTime.selectionAction?(timeSLotIndex, self.chooseMeetingTime.dataSource[timeSLotIndex])
            } else{
                
                self.shouldPreventItemSelcetionOfChooseMeetingDayDropDown = true
                
                self.chooseMeetingDay.selectRow(at: dayIndex)
                self.chooseMeetingDay.selectionAction?(dayIndex, self.chooseMeetingDay.dataSource[dayIndex])
                
                self.selectMeetingTime(_SelectDayIndex: dayIndex, _SelectTimeIndex: timeSLotIndex)
                
            }
            
        }
        
        
        tableViewPeoples.accordionDelegate = self
        tableViewPeoples.accordionDatasource = self
        
        tableViewPeoples.register(UINib(nibName: "AccordianMenuHeader_TableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier)
        
        tableViewPeoples.register(UINib(nibName: "AccordianRequestMeetingPeople_TableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        
        
        self.btnMeetingDay.isHidden = true
        self.btnMeetingTime.isHidden = true
        
        
        var _rmp_index = requestMeeting_EventProgramTime
        if _rmp_index > 1
        {
            _rmp_index = 1
        }

        
        
        setupMeetingTimeDropDown()
        setupMeetingDayDropDown()
        
        myTabController?.enableDropDowns = { [weak self] in
            self?.enableDisableDropdowns(enable: true)
        }
        
        if requestMeeting_IsComingFromMyItenary {
            
            enableDisableDropdowns(enable: false)
            
        }
        
    }
    
    
    
    
    func enableDisableDropdowns(enable: Bool){
        
        if enable{
            btnMeetingDay.isEnabled = true
            btnMeetingTime.isEnabled = true
            
            btnMeetingTime.setImage(UIImage(named: "dropdown"), for: .normal)
            btnMeetingDay.setImage(UIImage(named: "dropdown"), for: .normal)
        } else{
            btnMeetingDay.isEnabled = false
            btnMeetingTime.isEnabled = false
            
            btnMeetingTime.setImage(nil, for: .normal)
            btnMeetingDay.setImage(nil, for: .normal)
        }
        
    }
    
    
    func resetChooseMeetingDayDataSource(){
        
        if let myTabController = myTabController {
            
            if(myTabController.daysData.isEmpty){
                tableViewData = []
                reloadTableViewData()
                
            }
            
            selectedSlotId  = ""
            // You can also use localizationKeysDataSource instead. Check the docs.
            chooseMeetingDay.dataSource = myTabController.daysData.map({ day in
                day.date ?? ""
            })
            if myTabController.daysData.isEmpty {
                self.btnMeetingDay.setTitle("Day", for: .normal)
                self.btnMeetingTime.setTitle("Time", for: .normal)
                self.btnMeetingDay.isHidden = true
                self.btnMeetingTime.isHidden = true
                chooseMeetingTime.dataSource = []
            }else{
                self.btnMeetingDay.isHidden = false
                self.btnMeetingTime.isHidden = false
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let abc = segue.identifier {
            if abc == "searchBar" {
                
                if let SearchBarVC = segue.destination as? SummitEvent_SearchBoxViewController {
                    searchBoxVc = SearchBarVC
                    searchBoxVc?.searchBoxDelegate = self
                }
            }
        }
        

    }
    
    
    //MARK:- Actions
    @IBAction func actionMeetingTime(_ sender: Any) {
        
        print(requestMeeting_IsComingFromMyItenary)
        
        chooseMeetingTime.show()
    }
    
    @IBAction func actionMeetingDay(_ sender: Any) {
        
        print(requestMeeting_IsComingFromMyItenary)
        
        
        chooseMeetingDay.show()
    }
    
    func getSepakersAndDelegates(searchText: String, slotId: String){
        
        if !slotId.isEmpty {
            var params = [:] as [String: Any]
            params["summit_events_id"] = eventDetails?.id ?? ""
            params["summit_events_program_details_id"] = slotId
            params["check_attendee_availibility"] = true
            params["user_id"] = "\(userId)"
            
            if searchText.count > 0 {
                params["search"] = searchText
            }
            
            tableViewData = []
            tableViewPeoples.reloadData()
            
            lblNoRecordsFound.isHidden = true
            
            print(params)

            apiClass.loadApiCall(identifier: getSummitEventSpeakerDelegates, url: getSummitEventSpeakerDelegates, param: params, method: responseType.get.rawValue, header: headerToken, view: self)
        }

    }
    
    func reloadTableViewData(message: String = ApiCallsMessages.NoRecordsFound.rawValue){
        tableViewPeoples.reloadData()
        
        if(tableViewData.isEmpty){
            lblNoRecordsFound.isHidden = false
        }else{
            lblNoRecordsFound.isHidden = true
        }
        lblNoRecordsFound.text = message
    }
    
    
    //MARK:- DropDown Function
    func setupMeetingTimeDropDown() {
        
        chooseMeetingTime.anchorView = btnMeetingTime
        chooseMeetingTime.backgroundColor = UIColor(hexString: "#F0F0F0")
        chooseMeetingTime.setupCornerRadius(10)
        
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseMeetingTime.bottomOffset = CGPoint(x: 0, y: btnMeetingTime.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        
        // Action triggered on selection
        chooseMeetingTime.selectionAction = { [weak self] (index, item) in
            self?.btnMeetingTime.setTitle(item, for: .normal)
            self?.requestMeeting_EventProgramTime = index
            if let myTabController = self?.myTabController {
                if let dayIndex = self?.requestMeeting_EventProgramDay {
                    if let timeIndex = self?.requestMeeting_EventProgramTime {
                        let slotId = myTabController.timeSlotData[dayIndex][timeIndex].slotId ?? ""
                        
                        if slotId != self?.selectedSlotId {
                            print(item)
                            
                            self?.searchBoxVc?.txtSearchBox.text = ""
                            self?.getSepakersAndDelegates(searchText: "", slotId: slotId)
                            
                        }
                        self?.selectedSlotId = slotId
                    }
                }
            }
        }
        
        // DataSource is Set when something is selected from chooseMeetingDay DropDown
    }
    
    func setupMeetingDayDropDown() {
        
        
        chooseMeetingDay.anchorView = btnMeetingDay
        chooseMeetingDay.backgroundColor = UIColor(hexString: "#F0F0F0")
        chooseMeetingDay.setupCornerRadius(10)
        
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseMeetingDay.bottomOffset = CGPoint(x: 0, y: btnMeetingDay.bounds.height)
        
        
        
        chooseMeetingDay.selectionAction = { [weak self] (index, item) in
            self?.btnMeetingDay.setTitle(item, for: .normal)
            self?.requestMeeting_EventProgramDay = index
            
            if !(self?.shouldPreventItemSelcetionOfChooseMeetingDayDropDown ?? false){
                self?.selectMeetingTime(_SelectDayIndex: index, _SelectTimeIndex: 0)
            }
            self?.shouldPreventItemSelcetionOfChooseMeetingDayDropDown = false
            
        }
        
        // DataSource set in func resetChooseMeetingDayDataSource()
        
    }
    
    
    //MARK:- Custom Functions
    func selectMeetingTime( _SelectDayIndex:Int, _SelectTimeIndex:Int )
    {
        if let myTabController = myTabController
        {
            
            self.chooseMeetingTime.dataSource = myTabController.timeSlotData[_SelectDayIndex].map({ timeModel in
                timeModel.time ?? ""
            })
            
            if(!chooseMeetingTime.dataSource.isEmpty){
                self.btnMeetingTime.setTitle(chooseMeetingTime.dataSource[_SelectTimeIndex], for: .normal)
                self.chooseMeetingTime.selectRow(at: _SelectTimeIndex)
                chooseMeetingTime.selectionAction?(_SelectTimeIndex,chooseMeetingTime.dataSource[_SelectTimeIndex])
                
            } else{
                self.btnMeetingTime.setTitle("", for: .normal)
                
            }
            
            
        }
        
    }
    
}


extension SummitEvent_RequestMeetingViewController: ListingSearchBoxDelegate {
    func afterSearchButtonTapped(searchText: String) {
        print(searchText)
        if searchTextHasChanged {
            getSepakersAndDelegates(searchText: searchText, slotId: selectedSlotId)
            searchTextHasChanged = false
        }
    }
    
    func onTextChanged(searchText: String) {
        searchTextHasChanged = true
    }
    
    
    
}



//MARK:- APIDelegate
extension SummitEvent_RequestMeetingViewController: ApiDelegate {
    
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
        if identifier == getSummitEventSpeakerDelegates{
            
            do
            {
                self.btnMeetingDay.isHidden = false
                self.btnMeetingTime.isHidden = false
                  
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventDelegatePeoplesBaseModel.self, from: data )
                
                tableViewData = []
                
                let thisEventRequestMeeting = response
                
                if let tmpPeopleList = thisEventRequestMeeting.listingData
                {
                    
                    // Removing those speakers/delegates from the list who already have a meeting placed at this slot
                
                    tmpPeopleList.recently_added?.forEach({ item in
                        
                        if(item.meeting_slots == nil) {
                            tmpPeopleList.recently_added?.removeAll(where: { person in
                                person.id == item.id
                            })
                        }
                        
                    })
                    tmpPeopleList.all_country?.forEach({ item in
                        
                        if(item.meeting_slots == nil) {
                            tmpPeopleList.all_country?.removeAll(where: { person in
                                person.id == item.id
                            })
                        }
                        
                    })
                    
                    tmpPeopleList.all_industry?.forEach({ item in
                        
                        if(item.meeting_slots == nil) {
                            tmpPeopleList.all_industry?.removeAll(where: { person in
                                person.id == item.id
                            })
                        }
                        
                    })
                    
                    if tmpPeopleList.recently_added?.count ?? 0 > 0 {
                        tableViewData.append(TableData(header: recentlyAdded, rows: tmpPeopleList.recently_added ?? [], status: .open))
                    }

                    if tmpPeopleList.all_country?.count ?? 0 > 0 {
                        tableViewData.append(TableData(header: allCountry, rows: tmpPeopleList.all_country ?? [],
                                                       status: tmpPeopleList.recently_added?.count ?? 0 == 0 ? .open : .closed))

                    }
                    
                    if tmpPeopleList.all_industry?.count ?? 0 > 0 {
                        tableViewData.append(TableData(header: allIndustry, rows: tmpPeopleList.all_industry ?? [],
                                                       status: tmpPeopleList.all_country?.count ?? 0 == 0 ? .open : .closed))
                    }

                }
                
                var message = response.message
                
                if(message == nil || message?.isEmpty == true){
                    message = ApiCallsMessages.NoRecordsFound.rawValue
                }
                
                reloadTableViewData(message: message!)
            }
            catch {
                print(error)
                view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
                searchTextHasChanged = true
            }
            
        }
        
        
        
    }
    
    func failureApiCall(message: String, identifier: String) {
        print(message)
        view.hideAll_makeToast(message)
        searchTextHasChanged = true
    }
    
    
    
}


//MARK:- AccordionDelegate, TableViewDelegate
extension SummitEvent_RequestMeetingViewController: UITableViewDelegate, AccordionDelegate, AccordionDataSource {
    
    func tableView(_ tableView: AccordionTableView, dataForSection section: Int) -> TableData {
        return tableViewData[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRow row: Int, data: TableData) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AccordianRequestMeetingPeople_TableViewCell
        
        
        if let _data = data.rows[row] as? SummitEventPeopleDetailModel
        {
            print(_data)
            print(_data.designation)
            
            
            
            if let user_image = _data.user_image {
                
                let imageUrl = URL(string: user_image)
                
                cell.imgPeopleImage.sd_setImage(with: imageUrl, completed: { (image, err, cache, url) in
                    
                })
            }
            
            
            
            cell.lblPeopleName.text = _data.name?.uppercased()
            cell.lblPeopleBio.text = _data.short_description
            cell.lblPeopleDesignation.text = _data.designation
            
            cell.onLinkedInTap = {
                if(_data.linkedin_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
                    self.openUrlInBrowser(_data.linkedin_link)
                }
            }
            
            cell.onFacebookTap = {
                if(_data.facebook_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
                    self.openUrlInBrowser(_data.facebook_link)
                }
            }
            
            if(_data.linkedin_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
                cell.btnLinkedIn.setImage(UIImage(named: ImageNames.tealLinkedIn), for: .normal)
                cell.btnLinkedIn.adjustsImageWhenHighlighted = true
            }else{
                cell.btnLinkedIn.setImage(UIImage(named: ImageNames.greyLinkedIn), for: .normal)
                cell.btnLinkedIn.adjustsImageWhenHighlighted = false
            }


            if(_data.facebook_link?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0){
                cell.btnFacebook.setImage(UIImage(named: ImageNames.tealFacebook), for: .normal)
                cell.btnFacebook.adjustsImageWhenHighlighted = true
            }else{
                cell.btnFacebook.setImage(UIImage(named: ImageNames.greyFacebook), for: .normal)
                cell.btnFacebook.adjustsImageWhenHighlighted = false
            }
            
            
        }
        
        
        
        cell.layer.masksToBounds = false
        // set the shadow properties
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0.0)
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowRadius = 2
        cell.backgroundColor = .clear // very important
        cell.contentView.layer.cornerRadius = 8
        cell.contentView.backgroundColor = .white
        
        return cell
        
    }
    

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let data = tableViewData[section]
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! AccordianMenuHeader_TableViewCell
        header.section = section
        
        if  let title = data.header as? String {
            header.lblTime.text = title
        }
        
        header.imgBackground.image = UIImage(named: "multicolor-background")
        
        
        if data.status == .open {
            header.imgDropDown.image = getUpArrowImage()
        }
        else {
            header.imgDropDown.image = getDownArrowImage()
        }
        
//        if UIDevice().userInterfaceIdiom == .pad {
//            header.lblTime.font = .systemFont(ofSize: 22, weight: .semibold)
//        }
        
        header.btnCheckbox.reset = true
        header.btnCheckbox.isHidden = true
        
        
        header.toggleHideShowInputs(showHide: .hide)
        header.toggleHideShowInputs(showHide: .show, inputList: [.imgDropDown])
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        var _height = 40.0
//        if UIDevice().userInterfaceIdiom == .pad {
//            _height += 20.0
//        }
        return CGFloat(_height)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let gotoPersonVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_RequestMeetingPeopleDetailViewController") as! SummitEvent_RequestMeetingPeopleDetailViewController
        
        if let data  = tableViewData[indexPath.section].rows[indexPath.row] as? SummitEventPeopleDetailModel{
            gotoPersonVC.personDetailModel = data
        }
        
        if let myTabController = myTabController {
            gotoPersonVC.timeSlotId = myTabController.timeSlotData[requestMeeting_EventProgramDay][requestMeeting_EventProgramTime].slotId
        }
        
        self.navigationController?.pushViewController(gotoPersonVC, animated: true)
    }
    
    
    func onOpenSection(section: Int) {
        toggleDropDown(section: section, request_to: .open, tableView: tableViewPeoples)
    }
    
    func onCloseSection(section: Int) {
        toggleDropDown(section: section, request_to: .closed, tableView: tableViewPeoples)
    }
    
    
}
