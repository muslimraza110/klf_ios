//
//  AccordianRequestMeetingPeople_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/7/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

class AccordianRequestMeetingPeople_TableViewCell: UITableViewCell {

    @IBOutlet weak var imgPeopleImage: UIImageView!
    @IBOutlet weak var lblPeopleName: UILabel!
    @IBOutlet weak var lblPeopleDesignation: UILabel!
    @IBOutlet weak var lblPeopleBio: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnLinkedIn: UIButton!
    
    
    var onFacebookTap: (()-> Void)?
    var onLinkedInTap: (()-> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
        let margins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        contentView.frame = contentView.frame.inset(by: margins)
        
    }

    
    @IBAction func btnFacebookTapped(_ sender: UIButton) {
        onFacebookTap?()
    }
    
    @IBAction func btnTwitterTapped(_ sender: UIButton) {
        onLinkedInTap?()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
