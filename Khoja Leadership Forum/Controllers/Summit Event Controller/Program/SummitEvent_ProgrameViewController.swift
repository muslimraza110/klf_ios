//
//  SummitEvent_ProgrameViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/22/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable
import AttributedTextView


class SummitEvent_ProgrameViewController: BaseViewController {
    
    @IBOutlet weak var viewCheckMyAvailability: UIView!
    @IBOutlet weak var viewDateAndTable: UIView!
    @IBOutlet weak var tableView: AccordionTableView!
    @IBOutlet weak var collectionViewMenu: UICollectionView!
    
    @IBOutlet weak var lblProgramDate: UILabel!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblProgramDesc: UILabel!
    
    @IBOutlet weak var collectionViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewDateAndTable_Top_Constant: NSLayoutConstraint!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    
    private var cellHeightCache = [IndexPath: CGFloat]()
    
    var userId = 0
    
    
    var cellHeights = [Int: [Int: CGFloat]]()
    var tableViewData = [[TableData]]()
    let cellIdentifier = "TableCell"
    let headerIdentifier = "HeaderView"
    var thisEventProgram = [SummitEventProgramModel]()
    var selectedEventProgram = 0
    var currentSectionSelected:Int?
    
    var eventDetails: SummitEventModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = UserDefaults.standard
        
        let loginData = userDefaults.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        
        lblProgramDate.text = ""
        
        apiClass.delegate = self
        setNewDesignHeader()
        setupWireBg()
        
        
        
        collectionViewMenu.delegate = self
        collectionViewMenu.dataSource = self
        
        
        
        tableView.accordionDelegate = self
        tableView.accordionDatasource = self
        
        
        
        let param = ["summit_events_id":eventDetails?.id ?? "", "section_name" :"program", "user_id": userId] as [String:Any]
        apiClass.loadApiCall(identifier: "", url: eventProgramUrl, param: param, method: responseType.get.rawValue, header: headerToken, view: self)
        
        
        
        if UIDevice().userInterfaceIdiom == .pad {
            
            //            lblProgramDate.font = .systemFont(ofSize: 25, weight: .medium)
        }
        
        
        
        tableView.register(UINib(nibName: "AccordianMenuHeader_TableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier)
        
        tableView.register(UINib(nibName: "AccordianEventProgram_TableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        collectionViewMenu.register(UINib(nibName: "MenuButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "mb-cell")
    }
    
    
    //MARK:- Custom Functions
    func toggleMenuButton( selected: Bool, btn:UIButton )
    {
        if !selected
        {
            
            btn.setBackgroundImage(nil , for: .normal)
            btn.borderWidth = 2
            btn.borderColor = .lightGray
            btn.setTitleColor(.lightGray, for: .normal)
            btn.backgroundColor = .clear
            
        }
        else {
            
            btn.setBackgroundImage(UIImage(named: "bluegradient_bg") , for: .normal)
            btn.borderWidth = 0
            btn.borderColor = .clear
            btn.setTitleColor(.white, for: .normal)
            btn.backgroundColor = .clear
        }
    }
    
    
    
    //MARK:- Actions
    @objc func btnProgramListTapped(  sender: UIButton  )  {
        
        if thisEventProgram.isEmpty {
            return
        }
        
        for i in 0..<thisEventProgram.count {
            thisEventProgram[i].selected = false
        }
        
        selectedEventProgram = sender.tag
        thisEventProgram[sender.tag].selected = true
        lblProgramDate.text = thisEventProgram[sender.tag].program_date
        collectionViewMenu.reloadData()
        tableView.reloadData()
        
    }
    
}

extension SummitEvent_ProgrameViewController: ApiDelegate
{
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
        do
        {
            let jsonDecoder = JSONDecoder()
            let response = try jsonDecoder.decode(SummitEventProgramBaseModel.self, from: data )
            
            
            
            self.thisEventProgram = response.programList?.programData ?? []
            
            for i in 0..<self.thisEventProgram.count {
                
                var tableData = [TableData]()
                
                for programList in self.thisEventProgram[i].program_list ?? [] {
                    
                    
                    var details = [Any]()
                    details.append(programList)                    
                    
                    tableData.append(TableData(header: programList.start_end_time, rows: details, status: .closed))
                    
                }
                
                tableViewData.append( tableData )
                
            }
            
            self.collectionViewMenu.reloadData()
            
            let _dummyButton = UIButton()
            _dummyButton.tag = 0
            self.btnProgramListTapped(sender: _dummyButton)
            
            if(response.programList == nil){
                lblNoRecordsFound.text = response.message ?? ApiCallsMessages.NoRecordsFound.rawValue
                lblNoRecordsFound.isHidden = false
                tableView.isHidden = true
            }else{
                lblNoRecordsFound.isHidden = true
                tableView.isHidden = false
            }
            
            //            self.removeSpinner()
            
        }
        catch {
            
            self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
        }
        
    }
    
    func failureApiCall(message: String, identifier: String) {
        self.view.hideAll_makeToast(message)
    }
    
    
}



extension SummitEvent_ProgrameViewController: UITableViewDelegate, AccordionDelegate, AccordionDataSource
{
    
    // Fix for tableview jumping when opening/closing section
    // Start
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeightCache[indexPath] ?? 44
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightCache[indexPath] = cell.bounds.height
    }
    // End
    
    func tableView(_ tableView: AccordionTableView, dataForSection section: Int) -> TableData {
        
        currentSectionSelected = section
        return tableViewData[selectedEventProgram][section]
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRow row: Int, data: TableData) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AccordianEventProgram_TableViewCell
        
        
        cell.selectionStyle = .none
        
        
        let _details = self.thisEventProgram[selectedEventProgram].program_list
        print(_details?.count)
        
        if let item = data.rows[row] as? String
        {

            cell.lblProgramList.text = "\(item)"
        }
        
        cell.textView.text = ""
        cell.textView.attributer =  "".black
        cell.textView.isEditable = false
        cell.textView.isSelectable = false
        cell.textView.textContainerInset = UIEdgeInsets.zero
        cell.textView.textContainer.lineFragmentPadding = 0
        cell.textView.attributer.linkColor = UIColor.appCyan
        
        
        var _increase_height = 0
        if let model = data.rows[row] as? SummitEventProgramDetailsModel {
            
            if let _description = model.description
            {
                cell.lblProgramList.text = "\(_description)"
            }
            
            if let _speakers = model.speakers {
                
                if(_speakers.count > 0){
                    
                    
                    print(_speakers)
                    
                    cell.textView.attributer = cell.textView.attributer
                        //                    .appendHtml("<br><br>")
                        .append("Confirmed Speakers: ").black.font(.boldSystemFont(ofSize: 12))
                        .appendHtml("<br><br>")
                    
                    _increase_height    += 40
                    
                    for speaker in _speakers {
                        
                        cell.textView.attributer = cell.textView.attributer.append(speaker.name!).underline.makeInteract { (st) in
                            print("CALL Speaker \(st) \(String(describing: speaker.id))")
                            self.openPersonDetailVC(speaker)
                        }
                        .append(speaker.designation == "" || speaker.designation == nil ? "" : ", \(speaker.designation!).")
                        .appendHtml("<br>")
                        
                        _increase_height    += 10
                    }
                }
                
            }
            
            
            if let _delegates = model.delegates  {
                
                if(_delegates.count > 0) {
                    
                    
                    if cell.textView.attributedText.length > 0
                    {
                        cell.textView.attributer =  cell.textView.attributer
                            .appendHtml("<br>")
                        
                        _increase_height        += 10
                    }
                    
                    cell.textView.attributer =  cell.textView.attributer
                        .append("Confirmed Delegates: ").black
                        .appendHtml("<br><br>")
                    _increase_height        += 20
                    
                    for delegate in _delegates {
                        
                        cell.textView.attributer =  cell.textView.attributer.append(delegate.name!).underline.makeInteract { ( st ) in
                            print("CALL Delegate \(st)")
                            self.openPersonDetailVC(delegate)
                        }
                        .append(delegate.designation == "" || delegate.designation == nil ? "" : ", \(delegate.designation!).")
                        .appendHtml("<br>")
                        
                        _increase_height        += 10
                    }
                }
            }
            
        }
        
        cell.textView.attributer = cell.textView.attributer.all.font(UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 12))
        
        
        print(cell.lblProgramList.calculateMaxLines())
        
        //        if UIDevice().userInterfaceIdiom == .pad {
        
        //            cell.lblProgramList.font = .systemFont(ofSize: 20, weight: .regular)
        
        //        }
        
        
        
        let cc = cell.textView.contentSize
        cell.textView.isScrollEnabled = false
        cell.textView.frame.size.height = cc.height
        
        
        //        cell.constant_tableViewHeight.constant = cc.height
        //            + CGFloat(_increase_height)
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if thisEventProgram.count > 0
        {
            return thisEventProgram[selectedEventProgram].program_list?.count ?? 0
        }
        return  0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let data = tableViewData[selectedEventProgram][section]
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! AccordianMenuHeader_TableViewCell
        header.section = section
        
        
        if let programDetails = data.rows.first as? SummitEventProgramDetailsModel {
            header.lblTime.text = programDetails.start_end_time
            header.lblTitle.text = programDetails.title
        }
        
        
        if data.status == .open {
            header.imgDropDown.image = getUpArrowImage()
            header.contentView.roundCorners(corners: [.topRight, .topLeft], radius: 8)
        }
        else {
            header.imgDropDown.image = getDownArrowImage()
            header.contentView.roundCorners(corners: [.allCorners], radius: 8)
        }
        
        //        if UIDevice().userInterfaceIdiom == .pad {
        //            header.lblTime.font = .systemFont(ofSize: 22, weight: .semibold)
        //        }
        
        header.btnCheckbox.reset = true
        
        
        header.toggleHideShowInputs(showHide: .hide)
        header.toggleHideShowInputs(showHide: .show, inputList: [.imgDropDown, .lblTitle])
        
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        var _height = 40.0
        
        //        if UIDevice().userInterfaceIdiom == .pad {
        //            _height += 20.0
        //        }
        return CGFloat(_height)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
        
    }
    
    
    func onOpenSection(section: Int) {
        toggleDropDown(section: section, request_to: .open, tableView: tableView, allCornersAlwaysRounded: false)
    }
    
    func onCloseSection(section: Int) {
        toggleDropDown(section: section, request_to: .closed, tableView: tableView, allCornersAlwaysRounded: false)
    }
    
    
    func openPersonDetailVC(_ personDetail: SummitEventPeopleDetailModel){
        
        let personDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_RequestMeetingPeopleDetailViewController") as! SummitEvent_RequestMeetingPeopleDetailViewController
        
        personDetailVC.personDetailModel = personDetail
        
        self.navigationController?.pushViewController(personDetailVC, animated: true)
        
    }
}



extension SummitEvent_ProgrameViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(thisEventProgram.count)
        return thisEventProgram.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mb-cell", for: indexPath) as! MenuButtonCollectionViewCell
        cell.btnDays.clipsToBounds = true
        
        cell.btnDays.cornerRadius = 8
        cell.btnDays.setTitle(thisEventProgram[indexPath.row].program_day?.uppercased() , for: .normal)
        cell.btnDays.tag = indexPath.row
        cell.btnDays.addTarget(self, action: #selector(btnProgramListTapped), for: .touchUpInside)
        
        toggleMenuButton(selected: thisEventProgram[indexPath.row].selected ?? false, btn: cell.btnDays)
        
        return cell
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  5
        let collectionViewSize = collectionView.frame.size.width - padding
        
        print(collectionViewSize/2)
        
        var _width = 2.1
        if UIDevice().userInterfaceIdiom == .pad {
            _width = 2.05
        }
        
        return CGSize(width: collectionViewSize / CGFloat( _width ), height: collectionView.frame.size.height - 5)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
}
