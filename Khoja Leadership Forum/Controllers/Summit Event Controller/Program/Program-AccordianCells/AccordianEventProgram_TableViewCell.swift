//
//  AccordianEventProgram_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/29/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable
import AttributedTextView

class AccordianEventProgram_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var textView: AttributedTextView!

    @IBOutlet weak var tableViewSpeakerDelegates: AccordionTableView!
    @IBOutlet weak var lblProgramList: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        layer.masksToBounds = false
        // set the shadow properties
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0.0)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 2
        backgroundColor = .clear // very important
        contentView.backgroundColor = .white
                

        textView.delegate = self
        
        // To hide the text selction highlight effect on double tap
        textView.tintColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.1)

        
        
        if let gestureRecognizers = textView.gestureRecognizers {
            for gestureRecognizer in gestureRecognizers {
                if gestureRecognizer is UILongPressGestureRecognizer{
                    gestureRecognizer.isEnabled = false
                }
            }
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
}


extension AccordianEventProgram_TableViewCell: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        print(textView.text)
        print(URL)
        return true
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if !NSEqualRanges(textView.selectedRange, NSMakeRange(0, 0)) {
            textView.selectedRange = NSMakeRange(0, 0)
        }
    }
}
