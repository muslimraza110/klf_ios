//
//  MeetignDetails_TabBarController.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/11/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable

class MeetingDetails_TabBarController: UITabBarController {

    var myPlannerTabBar_SelectedIndex = 0
    var timeSlotData = [[TimeSlotModel]]()
    var daysData = [DayData]()
    var dayCellTap: ((Int) -> Void)?
    var enableDropDowns: (() -> Void)?
    var requestMeetingCheckBoxTap: (((Int, Int) -> Void))?
    var plannerVC: MeetingDetails_ViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(false, forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)
        delegate = self
        if let VC = self.viewControllers?[0] as? MeetingsViewController
        {
            VC.tabController = self
        }
        if let VC = self.viewControllers?[1] as? RequestMeetingViewController
        {
            VC.myTabController = self
        }
        self.selectedIndex = myPlannerTabBar_SelectedIndex
    }

    override func viewDidAppear(_ animated: Bool) {
        let meetingRequestSent = UserDefaults.standard.bool(forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)
        if meetingRequestSent {
            UserDefaults.standard.set(false, forKey: KEY_SUMMIT_EVENT_REQUEST_SENT)
            plannerVC?.setSelectedButton(index: 0)
            selectedIndex = 0
         //   (viewControllers?[0] as? MeetingsViewController)?.getAPIData()
        }
    }

    func switchToMyItinerary() {
        plannerVC?.setSelectedButton(index: 0)
        selectedIndex = 0
      //  (viewControllers?[0] as? MeetingsViewController)?.getAPIData()
    }
}

extension MeetingDetails_TabBarController: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let fromIndex = viewControllers?.firstIndex(of: fromVC) ?? -1
        let toIndex = viewControllers?.firstIndex(of: toVC) ?? -1
        var animated = true
        if toIndex == fromIndex {
            animated = false
        }
        var direction = TabBarSlideAnimatedTransitioning.SlideDirection.left
        if toIndex > fromIndex {
            direction = TabBarSlideAnimatedTransitioning.SlideDirection.right
        }
        return TabBarSlideAnimatedTransitioning(direction: direction, animated: animated)
    }
}
