//
//  Meetings_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/12/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable

protocol Meetings_TableViewCellDelegate: AnyObject {
    func updateSingleMeetingStatus(status: String, meetingDetail:MeetingItemUIModel)
}

class Meetings_TableViewCell: UITableViewCell {

    @IBOutlet weak var acceptRejectView: UIView!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var meetingStatusButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lineViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stackViewheight: NSLayoutConstraint!
    weak var delegate: Meetings_TableViewCellDelegate?
    var data: SummitEventProgramDetailsModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.lineViewHeight.constant = 0.3
        self.stackViewheight.constant = self.cancelButton.isHidden ? 170 : 162
    }

    var requestedDetails: MeetingItemUIModel? {
        didSet {
            setDelegatesData()
        }
    }


    private func setDelegatesData() {
        guard let requestedDetail = requestedDetails else {
            self.makeCellEmpty()
            return
        }

        self.cancelButton.setTitle( requestedDetail.iAmHost ?? false ? "Remove Attendee" : "Cancel", for: .normal)
        self.nameLabel.text = requestedDetail.name
        self.descriptionLabel.text = requestedDetail.designation
        if requestedDetail.requestedBy == requestedDetail.requestedTo {
            self.setCell(text: .meeting + .host, color: UIColor.appCyan ?? .white)
//            self.acceptRejectView.isHidden = true
//            self.cancelButton.isHidden = true
            self.hideShowButtons()
        } else if requestedDetail.iAmHost ?? false {
            self.setStatus(status: requestedDetail.meetingStatus ?? "")
//            self.acceptRejectView.isHidden = true
//            self.cancelButton.isHidden = true
            self.hideShowButtons()
            if (requestedDetail.meetingStatus != MeetingStatus.MEETING_HOST_CANCELED.rawValue &&
                    requestedDetail.meetingStatus != MeetingStatus.MEETING_REJECTED.rawValue) {
//                self.cancelButton.isHidden = false
                self.hideShowButtons( isCanncelled: false)
            }
        } else if requestedDetail.requestedTo == requestedDetail.myFacultyId {
//            self.acceptRejectView.isHidden = true
//            self.cancelButton.isHidden = true
            self.hideShowButtons()
            self.setStatus(status: requestedDetail.meetingStatus ?? "")
            if(requestedDetail.meetingStatus == MeetingStatus.MEETING_REQUESTED.rawValue){
//                self.acceptRejectView.isHidden = false
                self.hideShowButtons(isAcceptReject: false)
            }else if(requestedDetail.meetingStatus == MeetingStatus.MEETING_ACCEPTED.rawValue){
                //self.cancelButton.isHidden = false
                self.hideShowButtons(isCanncelled: false)
            }
        } else {
            self.setStatus(status: requestedDetail.meetingStatus ?? "")
//            self.acceptRejectView.isHidden = true
//            self.cancelButton.isHidden = true
            self.hideShowButtons()
        }



    }

    private func setCell(text: String, color: UIColor) {
        self.meetingStatusButton.backgroundColor = color
        self.meetingStatusButton.setTitle(text, for: UIControl.State.normal)
    }

    private func hideShowButtons(isAcceptReject: Bool = true, isCanncelled: Bool = true) {
          self.acceptRejectView.isHidden = isAcceptReject
         self.cancelButton.isHidden = isCanncelled
    }

    private func setStatus(status: String) {
        switch status {
        case MeetingStatus.MEETING_REQUESTED.rawValue:
            self.setCell(text: .awaitingResponse, color: UIColor.appOrange ?? .white)
        case MeetingStatus.MEETING_ACCEPTED.rawValue:
            self.setCell(text: .meeting + .accepted, color: UIColor.appDullGreen ?? .white)
        case MeetingStatus.MEETING_REJECTED.rawValue:
            self.setCell(text: .attendeeRemoved, color: UIColor.appRed ?? .white)
        case MeetingStatus.MEETING_HOST_CANCELED.rawValue:
            self.setCell(text: .removeAttendee , color: UIColor.appLightGrey ?? .white)
        default:
            return
        }
    }

    private func makeCellEmpty() {
        self.nameLabel.text = ""
        self.descriptionLabel.text = ""
//        self.acceptRejectView.isHidden = true
//        self.cancelButton.isHidden = true
        self.hideShowButtons()
    }

    @IBAction func cancelMeetingButtonTapped(_ sender: UIButton) {
        guard let detail = requestedDetails else { return }

        var tmp_meeting_constant = MeetingStatus.MEETING_SELF_CANCELED.rawValue
        if detail.iAmHost ?? false {
            tmp_meeting_constant =  MeetingStatus.MEETING_HOST_CANCELED.rawValue
        }

        detail.meetingStatus = tmp_meeting_constant
        self.delegate?.updateSingleMeetingStatus(status: tmp_meeting_constant, meetingDetail: detail)
    }
    @IBAction func acceptButtonTapped(_ sender: UIButton) {
        guard let detail = requestedDetails else { return }
        detail.meetingStatus = MeetingStatus.MEETING_ACCEPTED.rawValue
        self.delegate?.updateSingleMeetingStatus(status: MeetingStatus.MEETING_ACCEPTED.rawValue, meetingDetail: detail)
    }
    @IBAction func rejectButtonTapped(_ sender: UIButton) {
        guard let detail = requestedDetails else { return }
        detail.meetingStatus = MeetingStatus.MEETING_REJECTED.rawValue
        self.delegate?.updateSingleMeetingStatus(status: MeetingStatus.MEETING_REJECTED.rawValue, meetingDetail: detail)
    }
}

