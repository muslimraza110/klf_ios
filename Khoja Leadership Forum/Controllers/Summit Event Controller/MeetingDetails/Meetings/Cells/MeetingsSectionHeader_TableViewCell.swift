//
//  MeetingsSectionHeader_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/16/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable

protocol MeetingsSectionHeader_TableViewCellDelegate: AnyObject {
    func deleteMeeting(hostDetails: MeetingHeaderItem)
    func editMeeting(hostDetails: MeetingHeaderItem, index: Int)
}

class MeetingsSectionHeader_TableViewCell: AccordionTableHeaderFooterView {


    var DEFAULT_constraintlblTitle_Trailing: CGFloat = 153.0
    @IBOutlet weak var editMeetingButton: UIButton!
    @IBOutlet weak var deleteMeetingButton: UIButton!
    @IBOutlet weak var constraintlblTitle_Trailing: NSLayoutConstraint!
    weak var delegate: MeetingsSectionHeader_TableViewCellDelegate?
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var viewRightSpacer: UIView!
    @IBOutlet weak var stackViewItems: UIStackView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    var index = 0
    var status: Openable? = .open
    @IBOutlet weak var btnMeetingNotes: UIButton!
    lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(expandCollapse))
        gesture.numberOfTapsRequired = 1
        return gesture
    }()
    var listIndex = 0
    var data: MeetingHeaderItem?
    var onBtnMeetingNotesTap: ((MeetingHeaderItem?) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        if let recognizers = self.gestureRecognizers {
            for recognizer in recognizers {
                self.removeGestureRecognizer(recognizer)
            }
        }
        self.imgDropDown.isUserInteractionEnabled = true
        self.imgDropDown.addGestureRecognizer(tapGesture)
    }

    @IBAction func btnMeetingNotesTapped(_ sender: UIButton) {
        onBtnMeetingNotesTap?(data)
    }

    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        guard let data = data else { return }
        self.delegate?.deleteMeeting(hostDetails: data)
    }

    @IBAction func editMeetingButtonTapped(_ sender: UIButton) {
        guard let meetingData = self.data else { return }
        self.delegate?.editMeeting(hostDetails: meetingData, index: self.index)
    }
    
    
    @objc func expandCollapse( anything:Any ) {
    //    self.delegate?.didOpenSection(status: self.status ?? .open)
        debugPrint("collapse pressed")
        self.contentView.isUserInteractionEnabled = false

        (self.superview as? AccordionTableView)?.contentInset.bottom = 50

        (self.superview as? AccordionTableView)?.toogleSection(section: self.section)

        (self.superview as? AccordionTableView)?.contentInset.bottom = 0

        self.contentView.isUserInteractionEnabled = true
    }
}
