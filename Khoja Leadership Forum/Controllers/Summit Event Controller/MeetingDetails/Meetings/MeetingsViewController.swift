//
//  MeetingsViewController.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/11/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable
import AAViewAnimator
import MBProgressHUD

class MeetingsViewController: BaseViewController {

    // MARK: - Properties and Outlets
    @IBOutlet weak var tableView: AccordionTableView!
    var tabController: MeetingDetails_TabBarController?
    var tableViewData = [TableData]()
    var status: String? = ""
    var clickedMeetingUIModel: MeetingItemUIModel?
    var eventDetails: SummitEventModel?
    private var cellHeightCache = [IndexPath: CGFloat]()
    let cellIdentifier = "TableCell"
    let meetings_TableViewCell = "Meetings_TableViewCell"
    let headerIdentifier = "HeaderView"
    var meetingDetailsModel = [MeetingDetailsModel]()
    var tmpProgressHUD: MBProgressHUD?
    var userId = 0
    var meeting_parent_id: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
    }

    private func prepareView() {
        self.apiClass.delegate = self
        self.tabController?.selectedIndex = 1
        self.tabController?.selectedIndex = 0
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        let userDefaults = UserDefaults.standard
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        self.userId = userData["id"] as! Int
        self.getAPIData()
        self.tableView.accordionDelegate = self
        self.tableView.accordionDatasource = self
        self.tableView.register(UINib(nibName: "MeetingsSectionHeader_TableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier)
        tableView.register(UINib(nibName: "Meetings_TableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }

    func getAPIData() {
        if let eventId = eventDetails?.id {
            let param = ["summit_events_id": eventId, "user_id" : userId, "user_faculty_id": eventDetails?.summit_events_faculty_id ?? ""] as [String:Any]
            apiClass.loadApiCall(identifier: getSummitEventMeetingRequestToDelegates, url: getSummitEventMeetingRequestToDelegates, param: param, method: responseType.get.rawValue, header: headerToken, view: tabController?.plannerVC ?? self)
        }
    }

    func openMeetingNotesVC(data: MeetingHeaderItem, isEdit: Bool, isUpdate: Bool, at index: Int) {
        let meetingNotesVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_MeetingNotesViewController") as! SummitEvent_MeetingNotesViewController
        meetingNotesVC.isEdit = isEdit
        meetingNotesVC.isUpdate = isUpdate
        meetingNotesVC.meetingNotes = data.meetingNotes ?? ""
        meetingNotesVC.requestedToUserId = data.meetingId
        meetingNotesVC.delegate = self
        meetingNotesVC.onMeetingNotesUpdated = { notes, message in
            data.meetingNotes = notes
            self.view.hideAll_makeToast(message ?? "")
        }
        self.present(meetingNotesVC, animated: true) {
        }
    }

    //MARK:- Custom Functions
    func animateWithTransition(_ animator: AAViewAnimators) {
        tableView.aa_animate(duration: 1.2, springDamping: .slight, animation: animator) { (inAnimating, animView) in
        }
    }

    private func deleteMeetingFromServer(hostDetails: MeetingHeaderItem) {
        let param =
            ["summit_events_meeting_request_parent_id": hostDetails.meetingId ?? "",
             "user_id": "\(userId)"] as [String : Any]
        apiClass.loadApiCall(identifier: deleteMeetingUrl, url: deleteMeetingUrl, param: param , method: responseType.post.rawValue, header: headerToken, view: self)
    }

    private func updateStatus(details: MeetingItemUIModel) {
        let param =
            ["summit_events_meeting_request_child_id": details.meetingChildId ?? "",
             "user_id": "\(userId)",
             "new_status": status ?? ""] as [String : Any]
        apiClass.loadApiCall(identifier: updateStatusUrl, url: updateStatusUrl, param: param , method: responseType.post.rawValue, header: headerToken, view: self)
    }
}

// MARK: - API Response Delegates
extension MeetingsViewController: ApiDelegate
{
    func successApiCall(data: Data, identifier: String, status: Bool) {
        let jsonDecoder = JSONDecoder()

        if identifier == getSummitEventMeetingRequestToDelegates {
            tabController?.plannerVC?.btnRefresh?.stopRotating()
            do
            {
                let response = try jsonDecoder.decode(MeetingDetailsBaseModel.self, from: data )
                self.meetingDetailsModel = response.data ?? []
                print(self.meetingDetailsModel.count)
                tableViewData = []
                var selectedIndex: Int?
                for index in 0..<self.meetingDetailsModel.count {
                    var status: Openable = .closed
                    var tableData = [TableData]()
                    var meetingList = [MeetingItemUIModel]()
                    meetingList.append(MeetingItemUIModel(meetingChildId: self.meetingDetailsModel[index].hostDetails?.id ?? "", name: self.meetingDetailsModel[index].hostDetails?.requestedByDetails?.name ?? "", designation: self.meetingDetailsModel[index].hostDetails?.requestedByDetails?.designation ?? "", requestedBy: self.meetingDetailsModel[index].hostDetails?.requestedBy ?? "", requestedTo: self.meetingDetailsModel[index].hostDetails?.requestedBy ?? "", meetingStatus: self.meetingDetailsModel[index].hostDetails?.status ?? "", myFacultyId: eventDetails?.summit_events_faculty_id ?? "", isLast: false, iAmHost: self.meetingDetailsModel[index].hostDetails?.iAmHost ?? false))
                    if let list = self.meetingDetailsModel[index].meetingDetails {
                        for (subIndex, item) in list.enumerated() {
                            debugPrint("\(item.requestedToDetails?.name ?? "")")
                            if let meeting_parent_id =  self.meeting_parent_id {
                                if meeting_parent_id == self.meetingDetailsModel[index].meetingDetails?[subIndex].id ?? "" {
                                    self.meeting_parent_id = nil
                                    selectedIndex = index
                                    status = .open
                                }
                            }
                            meetingList.append(MeetingItemUIModel(meetingChildId: item.id ?? "", name: item.requestedToDetails?.name ?? "", designation: item.requestedToDetails?.designation ?? "", requestedBy: self.meetingDetailsModel[index].hostDetails?.requestedBy ?? "", requestedTo: item.requestedTo ?? "", meetingStatus: item.status ?? "", myFacultyId: eventDetails?.summit_events_faculty_id ?? "", isLast: subIndex == list.count - 1, iAmHost: self.meetingDetailsModel[index].hostDetails?.iAmHost ?? false))
                        }
                    }
                    tableData.append(TableData(header:MeetingHeaderItem(meetingId: self.meetingDetailsModel[index].hostDetails?.id ?? "", dateTime: "\(self.meetingDetailsModel[index].hostDetails?.meetingDate ?? ""), \(self.meetingDetailsModel[index].hostDetails?.meetingTime ?? "")", meetingNotes: self.meetingDetailsModel[index].hostDetails?.notes ?? "", isOpened: false, startTime: self.meetingDetailsModel[index].hostDetails?.meetingStartTime ?? "", endTime: self.meetingDetailsModel[index].hostDetails?.meetingEndTime ?? "", simpleDate: self.meetingDetailsModel[index].hostDetails?.meetingDate ?? "", iAmHost: self.meetingDetailsModel[index].hostDetails?.iAmHost ?? false) , rows:  meetingList, status: status))
                    self.tableViewData.append(contentsOf: tableData)
                }
                self.tableView.reloadData()
                guard let unwrappedSelectedIndex = selectedIndex else { return }
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: unwrappedSelectedIndex), at: .none, animated: true)
            }
            catch let DecodingError.typeMismatch(type, context)  {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            }
            catch {
                tabController?.plannerVC?.btnRefresh?.stopRotating()
                self.tabController?.plannerVC?.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        } else if identifier == deleteMeetingUrl || (identifier == updateStatusUrl && self.status == MeetingStatus.MEETING_SELF_CANCELED.rawValue) {
            do {
                let response = try jsonDecoder.decode(SummitEventDelegatePeoplesBaseModel.self, from: data )
                self.getAPIData()
                view.hideAll_makeToast(response.message)
                self.tableView.reloadData()
                var message = response.message
                if(message == nil || message?.isEmpty == true){
                    message = ApiCallsMessages.NoRecordsFound.rawValue
                }
            } catch {
                print(error)
                view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        } else if identifier == updateStatusUrl {
            do {
                let response = try jsonDecoder.decode(SummitEventDelegatePeoplesBaseModel.self, from: data )
                view.hideAll_makeToast(response.message)
                for index in 0..<(self.tableViewData.count) {
                    for subIndex in 0..<(tableViewData[index].rows.count - 1){
                        if let data = (tableViewData[index].rows[subIndex] as? MeetingItemUIModel) {
                            if data.meetingChildId == clickedMeetingUIModel?.meetingChildId ?? "" {
                              //  (tableViewData[index].rows[subIndex] as? MeetingItemUIModel)?.isLast = self.status ?? ""
                                if self.status == MeetingStatus.MEETING_REJECTED.rawValue {
                                    tableViewData[index].rows.remove(at: subIndex)
                                } else {
                                      (tableViewData[index].rows[subIndex] as? MeetingItemUIModel)?.meetingStatus = self.status ?? ""
                                }
                            }
                        }
                    }
                }
                self.tableView.reloadData()
                var message = response.message
                if(message == nil || message?.isEmpty == true){
                    message = ApiCallsMessages.NoRecordsFound.rawValue
                }
            } catch {
                print(error)
                view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }
    }

    func failureApiCall(message: String, identifier: String) {
        tabController?.plannerVC?.btnRefresh?.stopRotating()
        if(identifier == setSummitEventProgramDetailCheckMyAvailabilityUrl){
            tableView.reloadData()
        }
        if(identifier == eventProgramUrl){
            self.tabController?.plannerVC?.view.hideAll_makeToast(message)
        }
    }
}

// MARK: - Accordion TableView Delegates and DataSource
extension MeetingsViewController: UITableViewDelegate, AccordionDelegate, AccordionDataSource
{
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeightCache[indexPath] ?? 44
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightCache[indexPath] = cell.bounds.height
    }

    func tableView(_ tableView: AccordionTableView, dataForSection section: Int) -> TableData {
        return tableViewData[section]
    }

    func tableView(_ tableView: UITableView, cellForRow row: Int, data: TableData) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! Meetings_TableViewCell
        if let requestedData = (data.rows[row] as? MeetingItemUIModel) {
            cell.delegate = self
            cell.requestedDetails = requestedData
            if requestedData.isLast ?? false {
                cell.contentView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 8)
                cell.lineView.isHidden = true
            } else {
                cell.contentView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 0)
                cell.lineView.isHidden = false
            }
        }
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return  self.tableViewData.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! MeetingsSectionHeader_TableViewCell
        let data = tableViewData[section]
        if let meetingDetails = (data.header as? MeetingHeaderItem) {
            header.lblTime.text = "\(meetingDetails.dateTime ?? "")"
            header.data = meetingDetails
            header.editMeetingButton.isHidden = !(meetingDetails.iAmHost ?? false)
            header.deleteMeetingButton.isHidden =  !(meetingDetails.iAmHost ?? false)
        }

        header.index = section
        header.delegate = self
        header.onBtnMeetingNotesTap = { details in
            if let details = details{
                self.openMeetingNotesVC(data: details, isEdit: true, isUpdate: true, at: section)
            }
        }
        if data.status == .open {
            header.imgDropDown.image = getUpArrowImage()
            header.contentView.roundCorners(corners: [.topRight, .topLeft], radius: 8)
        }
        else {
            header.imgDropDown.image = getDownArrowImage()
            header.contentView.roundCorners(corners: [.allCorners], radius: 8)
        }
        header.section = section
        return header
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let _height = 45.0
        return CGFloat(_height)
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }

    func onOpenSection(section: Int) {
        toggleDropDown(section: section, request_to: .open, tableView: tableView, allCornersAlwaysRounded: false)
    }

    func onCloseSection(section: Int) {
        toggleDropDown(section: section, request_to: .closed, tableView: tableView, allCornersAlwaysRounded: false)
    }
}

extension MeetingsViewController: MeetingsSectionHeader_TableViewCellDelegate {
    func editMeeting(hostDetails: MeetingHeaderItem, index: Int) {
       // debugPrint("Meeting Details: \(hostDetails) \(self.meetingDetailsModel[index].meetingDetails)")
        if let requestViewController = (self.tabController?.children[1] as? RequestMeetingViewController) {
            requestViewController.updateMeetingData = hostDetails
            requestViewController.updateMeetingDetails = self.meetingDetailsModel[index].meetingDetails
            requestViewController.isUpdateMeeting = true
            self.tabController?.plannerVC?.selectTab(position: 1)
        }
    }

    func deleteMeeting(hostDetails: MeetingHeaderItem) {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure to want to cancel this meeting?", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: .yes, style: .default, handler: { (action: UIAlertAction!) in
            self.deleteMeetingFromServer(hostDetails: hostDetails)
        }))
        refreshAlert.addAction(UIAlertAction(title: .no, style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        present(refreshAlert, animated: true, completion: nil)

    }
}

// MARK: - Meetings Delegate
extension MeetingsViewController: Meetings_TableViewCellDelegate {
    
    func updateSingleMeetingStatus(status: String, meetingDetail: MeetingItemUIModel) {
        var alertText = "cancel"
        if status == "2" {
            alertText = "accept"
        }
        let refreshAlert = UIAlertController(title: "", message: "Are you sure want to \(alertText) the meeting invitation for \(meetingDetail.name ?? "")?", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: .yes, style: .default, handler: { (action: UIAlertAction!) in
            self.clickedMeetingUIModel = meetingDetail
            self.status = status
            self.updateStatus(details: meetingDetail)
        }))
        refreshAlert.addAction(UIAlertAction(title: .no, style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
}

// MARK: - Notes Delegate
extension MeetingsViewController: SummitEvent_MeetingNotesViewControllerDelegate {
    func notesUpdatedSuccessfully(updatedNotes: String, meetingChildID: String) {
        for index in 0..<(self.tableViewData.count) {
            if let data = (tableViewData[index].header as? MeetingHeaderItem) {
                if data.meetingId == meetingChildID {
                    (tableViewData[index].header as? MeetingHeaderItem)?.meetingNotes = updatedNotes
                }
            }
        }
        self.tableView.reloadData()
    }
}
