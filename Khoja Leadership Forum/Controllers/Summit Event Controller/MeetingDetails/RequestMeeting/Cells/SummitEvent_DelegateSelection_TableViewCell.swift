//
//  SummitEvent_DelegateSelection_TableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 2/24/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit

class SummitEvent_DelegateSelection_TableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    var cellTapped: (()-> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cellTapped(sender:)))
        contentView.addGestureRecognizer(tapGesture)
    }
    
    @objc func cellTapped(sender: UITapGestureRecognizer){
        cellTapped?()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
