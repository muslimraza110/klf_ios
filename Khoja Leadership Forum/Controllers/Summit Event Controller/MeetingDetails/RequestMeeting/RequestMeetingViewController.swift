//
//  RequestMeetingViewController.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/11/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import AccordionTable
import DropDown
import MBProgressHUD
import WSTagsField
import UIKit

class RequestMeetingViewController: BaseViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var selectStartTimeTextField: UITextField!
    @IBOutlet weak var submitMeetingRequestButton: UIButton!
    @IBOutlet weak var selectEndTimeTextField: UITextField!
    @IBOutlet weak var selectDateTextField: UITextField!
    var datePicker: UIDatePicker?
    @IBOutlet weak var tagsField: WSTagsField!
    var myTabController: MeetingDetails_TabBarController?
    var thisEventRequestMeeting:SummitEventRequestMeetingDataBaseModel?
    var allDelegates: [SummitEventPeopleDetailModel]?
    var updatedAllDelegates = [SummitEventPeopleDetailModel]()
    var eventDetails: SummitEventModel?
    var updateMeetingDetails: [MeetingDetail]?
    var updateMeetingData: MeetingHeaderItem?
    var userId = 0
    var isUpdateMeeting: Bool? {
        didSet {
            self.setUpdateConfiguration()
        }
    }
    @IBOutlet weak var textView: UITextView!
    let maxHeightForTagsField = CGFloat(120)
    @IBOutlet weak var tagsFieldHeightContraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getSepakersAndDelegates()
      //  self.setUpdateConfiguration()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
      //  self.isUpdateMeeting = false
        self.makeFieldsEmpty()
      //  self.isUpdateMeeting = nil
//        self.updatedAllDelegates = []
     //   self.setUpdateConfiguration()
       // self.makeFieldsEmpty()
    }

    private func prepareView() {
        userId = userData["id"] as! Int
        let userDefaults = UserDefaults.standard
        do {
            eventDetails = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        apiClass.delegate = self
       // self.getSepakersAndDelegates()
        self.setupFacultyTagsField()
        self.datePicker = UIDatePicker()
        self.selectDateTextField.delegate = self
        self.selectStartTimeTextField.delegate = self
        self.selectEndTimeTextField.delegate = self
        self.textView.text = .textViewPlaceHolderText
        self.textView.textColor = UIColor.appLightGrey
        self.textView.delegate = self

    }

    private func setUpdateConfiguration() {
        self.submitMeetingRequestButton.setTitle((self.isUpdateMeeting ?? false) ? "UPDATE" : "SUBMIT", for: .normal)
        guard let isUpdateMeeting = self.isUpdateMeeting else { return }
        if isUpdateMeeting {
            if (self.updateMeetingDetails?.count ?? 0) > 0 {
                for delegatesIndex in 0..<(self.allDelegates?.count ?? 0) {
                    self.allDelegates?[delegatesIndex].isSelected = false
                    for selectedDelegatesIndex in 0..<(self.updateMeetingDetails?.count ?? 0) {
                        if  self.allDelegates?[delegatesIndex].id == self.updateMeetingDetails?[selectedDelegatesIndex].requestedToDetails?.id &&  self.updateMeetingDetails?[selectedDelegatesIndex].status != MeetingStatus.MEETING_HOST_CANCELED.rawValue {
                            self.allDelegates?[delegatesIndex].isSelected = true
                        }
                    }
                }
            }
        self.selectStartTimeTextField.text = convertTime24To12(dateString: self.updateMeetingData?.startTime ?? "")
        self.selectEndTimeTextField.text = convertTime24To12(dateString: self.updateMeetingData?.endTime ?? "")
            let _ = self.setUpdatedDate(dateTextString: "\(String(describing: self.updateMeetingData?.simpleDate ?? ""))")
        self.updateTagsField()
        self.textView.text = self.updateMeetingData?.meetingNotes
        }
            self.updateTagsField()
    }

    private func convertTime24To12(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: String(dateString.dropLast(3)))
        dateFormatter.dateFormat = "h:mm a"
        let convertedTime24To12 = dateFormatter.string(from: date!)
        return  convertedTime24To12
    }

    private func setUpdatedDate(dateTextString: String) -> Date {
        var dateString = dateTextString
        if let stSubrange = dateString.range(of:"st") {
            dateString.removeSubrange(stSubrange)
        } else if let ndSubrange = dateString.range(of:"nd") {
                dateString.removeSubrange(ndSubrange)
        } else if let rdSubrange = dateString.range(of:"rd") {
                dateString.removeSubrange(rdSubrange)
        }  else if let thSubrange = dateString.range(of:"th") {
                dateString.removeSubrange(thSubrange)
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy"
        let date = dateFormatter.date(from: dateString )!

        self.datePicker?.setDate(date , animated: false)


        if let date = datePicker?.date {


        let _ = datePicker?.date.dateFormatWithSuffix(dateFormat: "yyyy-MM-dd")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = date.dateFormatWithSuffix()
            self.selectDateTextField.text = dateFormatter.string(from: date)
        }

        return date
    }

    // MARK: - Tags Field Setup
    func setupFacultyTagsField() {
        tagsField.layoutMargins = UIEdgeInsets(top: 2, left: 6, bottom: 2, right: 6)
        tagsField.contentInset = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        tagsField.spaceBetweenLines = 10.0
        tagsField.spaceBetweenTags = 10.0
        tagsField.font = UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 14)
        tagsField.backgroundColor = .clear
        tagsField.textColor = .white
        tagsField.textField.textColor = .white
        tagsField.selectedColor = .clear
        tagsField.selectedTextColor = .clear
        tagsField.isScrollEnabled = true
        tagsField.enableScrolling = true
        tagsField.numberOfLines = 3
        tagsField.placeholderColor = UIColor.appLightGrey
        tagsField.placeholder = "Select Delegates"
        tagsFieldHeightContraint.constant = tagsField.intrinsicContentSize.height
        tagsField.onDidChangeHeightTo = { tagField, height in
            if(height < self.maxHeightForTagsField){
                self.tagsFieldHeightContraint.constant = height
            }
        }
        tagsField.onDidAddTag = { wsTagsField, wsTagView in
            wsTagsField.tagViews.forEach({ wsTagView in
                wsTagView.isUserInteractionEnabled = false
                wsTagView.backgroundColor = .appBlueColor
            })
        }
        tagsField.textField.isUserInteractionEnabled = false
        tagsField.tintColor = .appOrangeColor
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tagsFieldTapped(sender: )))
        tagsField.addGestureRecognizer(tapGesture)
    }

    // MARK: - Action on TagsField
    @objc func tagsFieldTapped(sender: UITapGestureRecognizer){
        if let showDelegatesVC = storyboard?.instantiateViewController(withIdentifier: "ShowDelegates_ViewController") as? ShowDelegates_ViewController {
                showDelegatesVC.alldelegates = allDelegates?.map({ item in
                    item.copy() as! SummitEventPeopleDetailModel
                })
            showDelegatesVC.delegate = self
            self.present(showDelegatesVC, animated: true) {}
        }
    }

    func updateTagsField() {
        tagsField.removeTags()
            allDelegates?.forEach({item in
                if(item.isSelected){
                    let tag = WSTag(item.name ?? "")
                    tagsField.addTag(tag)
                }
            })
        tagsField.tagViews.forEach({tagsView in
            tagsView.isUserInteractionEnabled = false
            (tagsView as UIView).cornerRadius = 5
        })
    }

    // MARK: - Actions
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        let summitedID = self.allDelegates?.filter({ ($0.isSelected )
        }).map({
            return $0.summit_events_faculty_id ?? ""
        })
        guard let _ = self.selectDateTextField.text, !self.selectDateTextField.text!.isEmpty else {
            view.hideAll_makeToast(Constants.Errors.emptyFieldError + .date)
            return
        }

        guard let startTime = self.selectStartTimeTextField.text, !self.selectStartTimeTextField.text!.isEmpty else {
            view.hideAll_makeToast(Constants.Errors.emptyFieldError + .startTime)
            return
        }

        guard let endTime = self.selectEndTimeTextField.text, !self.selectEndTimeTextField.text!.isEmpty else {
            view.hideAll_makeToast(Constants.Errors.emptyFieldError + .endTime)
            return
        }

        if self.textView.text == .textViewPlaceHolderText || self.textView.text.isEmpty {
            view.hideAll_makeToast(Constants.Errors.emptyFieldError + .meetingNotes)
            return
        }
        if (summitedID?.count ?? 0) > 0 {
            if let requestedByUserId = eventDetails?.summit_events_faculty_id{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = datePicker?.date.dateFormatWithSuffix(dateFormat: "yyyy-MM-dd")
                let dateString = dateFormatter.string(from: datePicker!.date)

                var param =
                    ["requested_by": requestedByUserId,
                     "requested_to" : summitedID?.joined(separator:.comma) ?? "",
                     "summit_events_id": eventDetails?.id ?? "",
                     "meeting_date":dateString,
                     "meeting_start_time": startTime,
                     "meeting_end_time": endTime,
                     "meeting_venue":".",
                     "notes": self.textView.text ?? "",
                     "user_id": "\(userId)"]
                if isUpdateMeeting ?? false {
                    param["event_meeting_id"] = self.updateMeetingData?.meetingId
                    self.updateEvent(param: param)
                }else {
                    apiClass.loadApiCall(identifier: setSummitEventMeetingRequestToDelegatesUrl, url: setSummitEventMeetingRequestToDelegatesUrl, param: param, method: responseType.post.rawValue, header: headerToken, view: self)
                }
            }
        } else {
            view.hideAll_makeToast(Constants.Errors.emptyFieldError  + .delegates)
        }
    }

    private func updateEvent(param: [String:Any]?) {
        apiClass.loadApiCall(identifier: setSummitEventMeetingRequestToDelegatesUrl, url: updateMeetingEvent, param: param, method: responseType.post.rawValue, header: headerToken, view: self)
    }

    // MARK: -  Get Updated Speakers and Delegates
    func getSepakersAndDelegates() {
        var params = [:] as [String: Any]
        params["summit_events_id"] = eventDetails?.id ?? ""
        params["user_id"] = "\(userId)"
        apiClass.loadApiCall(identifier: getSummitEventSpeakerDelegates, url: getSummitEventSpeakerDelegates, param: params, method: responseType.get.rawValue, header: headerToken, view: self)
    }

    // MARK: - Dismiss DatePicker
    @objc func dismissPicker() {
        if let viewWithTag = self.view.viewWithTag(1110) {
            viewWithTag.removeFromSuperview()
        }
        if self.selectDateTextField.isFirstResponder{
            if let date = datePicker?.date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = date.dateFormatWithSuffix()
                self.selectDateTextField.text = dateFormatter.string(from: date)
            }
        }
        if self.selectStartTimeTextField.isFirstResponder {
            if let date = datePicker?.date {
                let strTime = date.dateStringWith(strFormat: "hh:mm a")
                self.selectStartTimeTextField.text = strTime
            }
        }
        if self.selectEndTimeTextField.isFirstResponder {
            if let date = datePicker?.date {
                let strTime = date.dateStringWith(strFormat: "hh:mm a")
                self.selectEndTimeTextField.text = strTime
            }
        }
        view.endEditing(true)
    }

    // MARK: - DatePicker Setup
    private func setDatePicker(textField: UITextField, datePickerMode: UIDatePicker.Mode) {
        if let viewWithTag = self.view.viewWithTag(1110) {
            viewWithTag.removeFromSuperview()
        }
        self.datePicker?.datePickerMode = datePickerMode
        if #available(iOS 13.4, *) {
            self.datePicker?.preferredDatePickerStyle = .wheels
        }
    //    self.datePicker?.frame = CGRect(x: 0, y: view.frame.height - 400, width: view.frame.width, height: 400)
        guard  let datePicker = self.datePicker else { return }
        self.datePicker?.tag = 1110
        self.datePicker?.minimumDate = Date()
        if  datePicker.datePickerMode == .date { // (isUpdateMeeting ?? true) &&
            let endDate = self.setUpdatedDate(dateTextString: eventDetails?.end_date ?? "")
            let startDate = self.setUpdatedDate(dateTextString: eventDetails?.start_date ?? "")
          //  let nextDate = Calendar.current.date(byAdding: .day, value: 5, to: alreadyselectedDateFromUser)
            self.datePicker?.minimumDate = startDate
            self.datePicker?.maximumDate = endDate
//        } else {
//             let dateFormatter = DateFormatter()
//             dateFormatter.dateFormat =  "EEE, dd MMM yyyy HH:mm"
//            let date = dateFormatter.date(from: String( textField.text ?? "12:00 PM"))
//             self.datePicker?.date = date ?? Date()
        }
     //   self.view.addSubview(datePicker)
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dismissPicker))
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textField.inputAccessoryView = toolBar
        textField.inputView = self.datePicker
    }

    private func setOldDate(timeString: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
       let date = dateFormatter.date(from: String( timeString.split(separator: " ")[0]))
        self.datePicker?.date = date ?? Date()
    }

    func makeFieldsEmpty() {
        self.selectDateTextField.text = ""
        self.selectStartTimeTextField.text = ""
        self.selectEndTimeTextField.text = ""
        self.textView.text = ""
        self.allDelegates = nil
        self.isUpdateMeeting = nil
        self.updatedAllDelegates = []
        self.updateTagsField()
    }
}

// MARK: - APIDelegate
extension RequestMeetingViewController: ApiDelegate {

    func successApiCall(data: Data, identifier: String, status: Bool) {
        let jsonDecoder = JSONDecoder()
        if identifier == getSummitEventSpeakerDelegates{
            do
            {
                let response = try jsonDecoder.decode(SummitEventDelegatePeoplesBaseModel.self, from: data )
                let thisEventRequestMeeting = response
                if let tmpPeopleList = thisEventRequestMeeting.listingData
                {
                    if tmpPeopleList.all_users_by_type?.count ?? 0 > 0 {
                        debugPrint("Number of users : \(tmpPeopleList.all_users_by_type?.count ?? 0)")
                        self.allDelegates = tmpPeopleList.all_users_by_type?.filter({
                            $0.id != "\(userId)"
                        })
                        if isUpdateMeeting ?? false {
                            self.setUpdateConfiguration()
                        }
                    }
                }
                var message = response.message
                if(message == nil || message?.isEmpty == true){
                    message = ApiCallsMessages.NoRecordsFound.rawValue
                }
            }
            catch {
                print(error)
                view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }

        if identifier == setSummitEventMeetingRequestToDelegatesUrl || identifier == updateMeetingEvent {
            do {
                self.makeFieldsEmpty()
                let response = try jsonDecoder.decode(MeetingRequestBaseModel.self, from: data )
                view.makeToast(response.message)
                myTabController?.plannerVC?.setSelectedButton(index: 0)
                myTabController?.selectedIndex = 0
                if let myItenaryViewController =  myTabController?.children[0] as? MeetingsViewController {
                    btnRefresh?.startRotating()
                    myItenaryViewController.getAPIData()
                }
            }
            catch let err {
                print(err)
                view.makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }
    }

    func failureApiCall(message: String, identifier: String) {
        if identifier == setSummitEventMeetingRequestStatusUrl {
            let refreshAlert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
            refreshAlert.addAction(UIAlertAction(title: .ok, style: .default, handler: { (action: UIAlertAction!) in
            }))
        } else if identifier == setSummitEventMeetingRequestToDelegatesUrl {
            let refreshAlert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
            refreshAlert.addAction(UIAlertAction(title: .ok, style: .default, handler: { (action: UIAlertAction!) in
            }))
            present(refreshAlert, animated: true, completion: nil)
        } else {
            print(message)
            view.hideAll_makeToast(message)
        }
    }
}

// MARK: - ShowDelegates Delegate
extension RequestMeetingViewController: ShowDelegates_ViewControllerDelegate {
    func didOKTapped(allDelegates: [SummitEventPeopleDetailModel]?) {
        //guard let isUpdateMeeting = self.isUpdateMeeting else {
            self.allDelegates = allDelegates
            self.updateTagsField()
        //    return
      //  }
//        if isUpdateMeeting {
//            self.updatedAllDelegates = allDelegates ?? []
//            self.updateTagsField()
//        }
    }
}

// MARK: - TextField Delegate
extension RequestMeetingViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case self.selectDateTextField:
            self.setDatePicker(textField: textField, datePickerMode: .date)
        case self.selectStartTimeTextField:
            self.setDatePicker(textField: textField, datePickerMode: .time)
//           let selectedTime = self.convertTime24To12(dateString: textField.text ?? "12:00:00")
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat =  "HH:mm"
//
//            let date = dateFormatter.date(from: String(selectedTime.split(separator: " ")[0]))
//            self.datePicker?.date = date ?? Date()
        case self.selectEndTimeTextField:
            self.setDatePicker(textField: textField, datePickerMode: .time)
        default:
            return
        }
    }
}

// MARK: - TextView Delegate
extension RequestMeetingViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.appLightGrey {
            textView.text = nil
            textView.textColor = UIColor.appBlue
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = .textViewPlaceHolderText
            textView.textColor = UIColor.appLightGrey
        }
    }
}
