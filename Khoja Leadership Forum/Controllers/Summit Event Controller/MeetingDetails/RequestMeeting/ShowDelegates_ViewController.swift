//
//  ShowDelegates_ViewController.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/8/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit

protocol ShowDelegates_ViewControllerDelegate: AnyObject {
    func didOKTapped(allDelegates: [SummitEventPeopleDetailModel]?)
}

class ShowDelegates_ViewController: UIViewController {

    // MARK: - Properties and Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var emptyRecordLabel: UILabel!
    weak var delegate: ShowDelegates_ViewControllerDelegate?
    var alldelegates: [SummitEventPeopleDetailModel]?
    var tempDelegates: [SummitEventPeopleDetailModel]?
    var searchString: String?
    var isSearching: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBackgroundBlurred()
        self.perpareView()
    }

    private func perpareView() {
        self.tableView.register(UINib(nibName: Constants.Nibs.summitEvent_DelegateSelection_TableViewCell, bundle: nil), forCellReuseIdentifier: Constants.TableCells.delegateSelectionReuseIdentifier)
        self.tableView.keyboardDismissMode = .onDrag
        self.searchBar.delegate = self
    }

    // MARK: - Actions
    @IBAction func okButtonTapped(_ sender: UIButton) {
        self.delegate?.didOKTapped(allDelegates: self.alldelegates)
        dismiss(animated: true, completion: nil)
    }

    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - TableView Data Source
extension ShowDelegates_ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView.isHidden = false
        guard let count = alldelegates?.count  else {
            return 0
        }
        if isSearching {
            if tempDelegates?.count == 0 {
                self.tableView.isHidden = true
                return 0
            }else {
                self.tableView.isHidden = false
                return tempDelegates?.count ?? 0
            }
        }else {
            return count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableCells.delegateSelectionReuseIdentifier, for: indexPath) as! SummitEvent_DelegateSelection_TableViewCell
        let model = self.isSearching ? tempDelegates?[indexPath.row] :  alldelegates?[indexPath.row]
        cell.lblName.text = model?.name ?? ""
        cell.lblDesignation.text = model?.designation ?? ""
        if(model?.isSelected ?? false) {
            cell.contentView.backgroundColor = UIColor.systemBlue.withAlphaComponent(0.3)
        } else {
            cell.contentView.backgroundColor = UIColor.white
        }
        cell.cellTapped = {
            model?.isSelected = !(model?.isSelected ?? false)
            tableView.beginUpdates()
            tableView.reloadRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
        return cell
    }
}
 // MARK: - Search Bar Delegate
extension ShowDelegates_ViewController: UISearchBarDelegate {

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
         if searchBar.text?.count  == 0 {
             self.isSearching = false
            searchBar.endEditing(true)
             self.tableView.reloadData()
         }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count != 0 {
            self.tempDelegates = self.alldelegates?.filter({($0.name?.lowercased().contains(searchBar.text?.lowercased() ?? ""))!})
            self.isSearching = true
            self.tableView.reloadData()
        } else {
            searchBar.endEditing(true)
            self.isSearching = false
            self.tableView.reloadData()
        }
    }
}
