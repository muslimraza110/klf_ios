//
//  MeetingDetails_ViewController.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/11/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit
import AccordionTable
import MBProgressHUD

class MeetingDetails_ViewController: BaseViewController {

    // MARK: - Properties
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionViewTopMenu: UICollectionView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    var TopMenus = [SummitEventMyPlannerTopMenusModel]()
    var defaultSelectedIndex = 0
    var myTabController: MeetingDetails_TabBarController?
    var meeting_parent_id: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        self.TopMenus.append(SummitEventMyPlannerTopMenusModel(heading: Constants.MeetingDetails.meetings, id: 0, selected: false))
        self.TopMenus.append(SummitEventMyPlannerTopMenusModel(heading: Constants.MeetingDetails.requestMeeting, id: 1, selected: false))
        self.setNewDesignHeader()
        self.setupWireBg()
        self.setUpRefreshButton()
        for child in self.children{
            if let child = child as? MeetingDetails_TabBarController
            {
                self.myTabController = child
            }
        }
//        self.containerView.isHidden = true
        self.collectionViewTopMenu.delegate = self
        self.collectionViewTopMenu.dataSource = self
        let _dummyButton = UIButton()
        _dummyButton.tag = 0
        self.btnTopMenuTapped(sender: _dummyButton, firstCall: true)
        self.collectionViewTopMenu.register(UINib(nibName: "MenuButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "mb-cell")
        (myTabController?.children[0] as? MeetingsViewController)?.meeting_parent_id = self.meeting_parent_id ?? ""
    }

    override func btnBackTapped(_ sender: UIButton) {
        if(myTabController?.selectedIndex == 0){
                super.btnBackTapped(sender)
        }else{
            selectTab(position: 0)
        }
    }

    override func btnRefreshTapped(_ sender: UIButton) {
        if let myItenaryViewController =  myTabController?.children[0] as? MeetingsViewController {
            btnRefresh?.startRotating()
            myItenaryViewController.getAPIData()
        }
    }

    func setSelectedButton(index: Int){
        if TopMenus.count > index {
            TopMenus.forEach({menuItem in
                menuItem.selected = false
            })
            TopMenus[index].selected = true
            collectionViewTopMenu.reloadData()
        }
    }

    //MARK:- Custom Functions
    func toggleMenuButton( selected: Bool, btn:UIButton )
    {
            btn.setBackgroundImage(selected ? UIImage(named: "bluegradient_bg") : nil , for: .normal)
            btn.borderWidth = 2
            btn.borderColor = selected ? .clear : .lightGray
            btn.setTitleColor(selected ? .white : .lightGray, for: .normal)
            btn.backgroundColor = .clear
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.description)
        if let meetingSegue = segue.destination as? MeetingDetails_TabBarController {
            meetingSegue.plannerVC = self
            meetingSegue.myPlannerTabBar_SelectedIndex = defaultSelectedIndex
        }
    }

    //MARK:- Actions
    @objc func btnTopMenuTapped(  sender: UIButton, firstCall:Bool  )  {
        selectTab(position: sender.tag)
    }

    func selectTab(position: Int) {
        for i in 0..<TopMenus.count {
            TopMenus[i].selected = false
        }
        TopMenus[position].selected = true
        collectionViewTopMenu.reloadData()
        if let _id = TopMenus[position].id
        {
            if (myTabController?.children[_id] as? MeetingsViewController) != nil
            {
                myTabController?.selectedIndex = 0

                defaultSelectedIndex = 0
            }
            if (myTabController?.children[_id] as? RequestMeetingViewController) != nil
            {
                myTabController?.selectedIndex = 1
                defaultSelectedIndex = 1
            }
        }
    }

}

// MARK: - CollectionView Delegates and DataSource
extension MeetingDetails_ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TopMenus.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mb-cell", for: indexPath) as! MenuButtonCollectionViewCell
        cell.btnDays.tag = indexPath.row
        cell.btnDays.setTitle( TopMenus[indexPath.row].heading?.uppercased() , for: .normal)
        cell.btnDays.addTarget(self, action: #selector(btnTopMenuTapped), for: .touchUpInside)
        toggleMenuButton(selected: TopMenus[indexPath.row].selected ?? false, btn: cell.btnDays)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  5
        let collectionViewSize = collectionView.frame.size.width - padding
        print(collectionViewSize/2)
        var _width = 2.1
        if UIDevice().userInterfaceIdiom == .pad {
            _width = 2.05
        }
        return CGSize(width: collectionViewSize / CGFloat( _width ), height: collectionView.frame.size.height - 5)
    }
}
