//
//  SummitEventViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 3/24/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

class SummitEvent_SectionsViewController: BaseViewController {
    
    
    enum EventSectionsTags: String {
        case programe
        case myplanner
        case delegates
        case speakers
        case meetingdetails
        case initiative
    }
    
    
    //MARK:- Outlets & Variables
    @IBOutlet weak var tableViewEventProgramList: UITableView!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    
    var eventSections = [SummitEventSectionModel]()
    var thisEvent:SummitEventModel?
    var userId = 0
    
    
    var onNotificationConrtollerDismiss: (() -> Void)?
    
    

    //MARK:- Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
                
        let userDefaults = UserDefaults.standard
        do {
            thisEvent = try userDefaults.getObject(forKey: KEY_EVENT_DETAILS, castTo: SummitEventModel.self)
        } catch {
            print(error.localizedDescription)
        }
        
        let loginData = userDefaults.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        
        
        apiClass.delegate = self
        
        setNewDesignHeader()
        setupWireBg()
        
        
        
        if(thisEvent?.is_user_registered == true) {
            toggleNotificationButton(visible: true)
        }
        
        onNotificationConrtollerDismiss = {
            if(self.thisEvent?.is_user_registered == true) {
                self.getNotifications()
            }
        }
        
        
        
        tableViewEventProgramList.delegate = self
        tableViewEventProgramList.dataSource = self
        
        let textFieldCell = UINib(nibName: "SummitEventTableViewCell",
                                  bundle: nil)
        self.tableViewEventProgramList.register(textFieldCell,forCellReuseIdentifier: "cell")
        
        
        let param = ["summit_event_id":thisEvent?.id ?? ""] as [String:Any]
        apiClass.loadApiCall(identifier: eventSectionUrl, url: eventSectionUrl, param: param, method: responseType.get.rawValue, header: headerToken, view: self)
        
    }


    override func viewDidAppear(_ animated: Bool) {
        if(self.thisEvent?.is_user_registered == true) {
            self.getNotifications()
        }
    }
    
    override func btnNotificationTapped(_ sender: UIButton) {
        let notificationVC = storyboard?.instantiateViewController(withIdentifier: "SummitEvent_Notifications_ViewController") as! SummitEvent_Notifications_ViewController
        
        notificationVC.onNotificationConrtollerDismiss = self.onNotificationConrtollerDismiss
        notificationVC.previousVC = self
        
        self.present(notificationVC, animated: true) {}
    }
    
    
    
    
    @IBAction func btnRegisterPressed(_ sender: UIButton) {
        
        
        let summitEventID = self.thisEvent?.id ?? ""
        
        if  !summitEventID.isEmpty {
            
            apiClass.loadApiCall(identifier: setSummitEventsRegistration, url: setSummitEventsRegistration, param: ["user_id": "\(userId)", "summit_event_id":  summitEventID], method: responseType.post.rawValue, header: headerToken, view: self)
        }
        else {
            
            ShowErrorAlert(message: "No Current Event Found")
            
        }
        
        /*
        let eventRegistrationVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_EventRegistrationViewController") as? SummitEvent_EventRegistrationViewController
        
        if let eventRegistrationVC = eventRegistrationVC {
            self.navigationController?.pushViewController(eventRegistrationVC, animated: true)
        }*/
        
    }
    
    
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "SummitEvent", bundle: Bundle.main).instantiateViewController(withIdentifier: "SummitEventViewController") as? SummitEvent_SectionsViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    func getNotifications(){
        var params = [:] as [String: Any]
            
        params["summit_event_id"] = thisEvent?.id ?? ""
        params["user_id"] = "\(userId)"
            
    //apiClass.loadApiCall(identifier: summitEventNotificationsUrl, url: summitEventNotificationsUrl, param: params, method: responseType.get.rawValue, header: headerToken, view: self, showProgress: false)
    }
    
    
}


//MARK:- TableView Deleagtes
extension SummitEvent_SectionsViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return eventSections.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .pad {
            return 200
        }
        return 166
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SummitEventTableViewCell
        
        let _details = eventSections[indexPath.row]
        
        cell.lblFirstHeading.text = _details.title
        cell.lblSecondHeading.text = EventSectionsTags(rawValue: _details.tag ?? "" ) == .initiative ? "" :  _details.short_description 
        
        
        if UIDevice().userInterfaceIdiom == .pad {
            
            cell.lblFirstHeading.font = UIFont(name: AppFonts.PoppinsSemiBold.rawValue, size: 25)
            cell.lblSecondHeading.font = UIFont(name: AppFonts.PoppinsMedium.rawValue, size: 15)
        }
        
        print(_details.tag!)
        switch _details.tag! {
        case EventSectionsTags.delegates.rawValue:
            cell.imageBackground.image = UIImage(named: "image-\(EventSectionsTags.delegates.rawValue)")
            break
        case EventSectionsTags.initiative.rawValue:
            cell.imageBackground.image = UIImage(named: "image-\(EventSectionsTags.speakers.rawValue)")
            break
        case EventSectionsTags.myplanner.rawValue:
            cell.imageBackground.image = UIImage(named: "image-\(EventSectionsTags.myplanner.rawValue)")
            break
        case EventSectionsTags.speakers.rawValue:
            cell.imageBackground.image = UIImage(named: "image-\(EventSectionsTags.speakers.rawValue)")
            break
        case EventSectionsTags.programe.rawValue:
            cell.imageBackground.image = UIImage(named: "image-\(EventSectionsTags.programe.rawValue)")
            break

        case EventSectionsTags.meetingdetails.rawValue:
            cell.imageBackground.image = UIImage(named: "image-\(EventSectionsTags.delegates.rawValue)")
            break
            
        default:
            break
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let _details = eventSections[indexPath.row]
        
        if let section = EventSectionsTags(rawValue: _details.tag ?? "" )
        {
            if section == .programe
            {
                let gotoDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_ProgrameViewController") as! SummitEvent_ProgrameViewController
                self.navigationController?.pushViewController(gotoDetailVC, animated: true)
            }
            else if section == .myplanner
            {
                if(thisEvent?.is_user_registered == true){
                    let gotoDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_MyPlannerViewController") as! SummitEvent_MyPlannerViewController
                    self.navigationController?.pushViewController(gotoDetailVC, animated: true)
                }else{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: .youAreNotRegisteredForThisEvent(), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
            else if section == .delegates {
                let gotoDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_DelegatesViewController") as! SummitEvent_DelegatesViewController
                self.navigationController?.pushViewController(gotoDetailVC, animated: true)
            }
            else if section  == .speakers {
                let gotoDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_SpeakersViewController") as! SummitEvent_SpeakersViewController
                self.navigationController?.pushViewController(gotoDetailVC, animated: true)
            }
            else if section == .meetingdetails {

                if(thisEvent?.is_user_registered == true) {
                    let gotoDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MeetingDetails_ViewController") as! MeetingDetails_ViewController
                    self.navigationController?.pushViewController(gotoDetailVC, animated: true)
                }else{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: .youAreNotRegisteredForThisEvent(), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }

            }
            else if section == .initiative {
              //  let gotoInitiativeVC = self.storyboard?.instantiateViewController(withIdentifier: "Initiative_ViewController") as! Initiative_ViewController
              //  gotoInitiativeVC.isEvent = true
              //  self.navigationController?.pushViewController(gotoInitiativeVC, animated: true)

                //  if let initiativeID = Int(eventSections[indexPath.row].short_description ?? "-") {
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "InitiativeViewController") as! InitiativeViewController
              //  vc.navigationController?.navigationItem.setHidesBackButton(true, animated: true)
//                    vc.initiativeId = "\(initiativeID)"
//                    vc.initiativeTitle = section.rawValue
                   vc.isEventInitiative = true
                    self.navigationController?.pushViewController(vc, animated: true)
                //} else {
                  //  showToast(message: "Something went wrong!")
               // }

            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationItem.hidesBackButton = true
      //  self.navigationController?.isNavigationBarHidden = 
    }
}


//MARK:- Api Delegate
extension SummitEvent_SectionsViewController:ApiDelegate
{
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
        
        if identifier == setSummitEventsRegistration {
            
           
            
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventsFacultyBaseModel.self, from: data )
                
                
                let summitEventRegistrationLink = self.thisEvent?.registration_link ?? ""
                
                if summitEventRegistrationLink.isEmpty{
                    
                    ShowErrorAlert(message: "No Registration Link Found.")
                }
                else {
                    
                    if let url = URL(string: summitEventRegistrationLink) {
                        UIApplication.shared.open(url)
                    }
                    
                }
                
            }
            catch {
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)

            }
            
        }
        
        else if(identifier == eventSectionUrl){
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventSectionBaseModel.self, from: data)
                
                //            if(thisEvent?.is_user_registered ?? false == false){
                //                response.sections?.removeAll(where: {item in
                //                    item.tag == EventSectionsTags.myplanner.rawValue
                //                })
                //            }
                
                self.eventSections = response.sections ?? []
                self.tableViewEventProgramList.reloadData()
                
                
                
                
                if(response.sections == nil){
                    lblNoRecordsFound.text = response.message ?? ApiCallsMessages.NoRecordsFound.rawValue
                    lblNoRecordsFound.isHidden = false
                    tableViewEventProgramList.isHidden = true
                    //                self.view.hideAll_makeToast(response.message ?? ApiCallsMessages.NoRecordsFound.rawValue)
                }else{
                    lblNoRecordsFound.isHidden = true
                    tableViewEventProgramList.isHidden = false
                }
                
            }
            catch {
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
        }
        if(identifier == summitEventNotificationsUrl){
            
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventNotificationsBaseModel.self, from: data)
            
                if(response.data?.count ?? 0 > 0){
                    lblNotificationCount?.isHidden = true
                    lblNotificationCount?.text = "\(response.data?.count ?? 0)"
                }else{
                    lblNotificationCount?.isHidden = true
                }
                
            }
            catch {
//                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
            }
            
        }
              
    }
    
    func failureApiCall(message: String, identifier: String) {
        self.view.hideAll_makeToast(message)
    }
    
}
