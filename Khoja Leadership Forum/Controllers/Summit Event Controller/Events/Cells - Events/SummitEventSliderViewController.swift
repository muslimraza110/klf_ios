//
//  SummitEventSliderViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/21/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import FSPagerView

class SummitEventSliderViewController: FSPagerViewCell {

    @IBOutlet weak var viewPriceBackground: UIView!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var btnViewDetails: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgEventLeading: NSLayoutConstraint!
    @IBOutlet weak var imgEventTrailing: NSLayoutConstraint!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var stackLblAddressTop: NSLayoutConstraint!
    
    var onViewDetailsTap: (() -> ())?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        
        if UIDevice().userInterfaceIdiom == .pad {
            imgEventTrailing.constant = 70
            imgEventLeading.constant = 70
            lblDateTime.font = UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 16)
            btnViewDetails.titleLabel?.font = UIFont(name: AppFonts.PoppinsSemiBold.rawValue, size: 22)
            lblCity.font = UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 16)
            lblPrice.font = UIFont(name: AppFonts.PoppinsSemiBold.rawValue, size: 18)
            lblAddress.font = UIFont(name: AppFonts.PoppinsRegular.rawValue, size: 25)
            stackLblAddressTop.constant = 12

        }
    }
    
    
    @IBAction func btnViewDetailsTapped(_ sender: UIButton) {
        print("View Details Tapped")
        onViewDetailsTap?()
    }


}
