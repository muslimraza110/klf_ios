//
//  SummitEvent_CurrentEvents_ViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/21/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit
import FSPagerView

class SummitEvent_Lists_ViewController: BaseViewController {
    
    //MARK:- Outlets & Variables
    fileprivate var eventLists = [SummitEventModel]()
    var eventSelectedInSlider:SummitEventModel?
    var isCurrentEvent = true
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblPreviousCurrentEvent: UILabel!
    @IBOutlet weak var viewEventsList: FSPagerView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContainerView: UIView!
    @IBOutlet weak var scrollContainerHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak var btnRegisterHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak var viewEventsListLeading: NSLayoutConstraint!
    @IBOutlet weak var viewEventsListTrailing: NSLayoutConstraint!
    @IBOutlet weak var lblNoEventsFound: UILabel!
    
    
    var userId = 0

    
    //MARK:- Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        
        if UIDevice().userInterfaceIdiom == .pad {
            viewEventsListLeading.constant = 80
            viewEventsListTrailing.constant = 80
        }
        
        calculateScrollViewContentHeight()
        
        apiClass.delegate = self
        setNewDesignHeader()
        setupWireBg()
        
        viewEventsList.delegate = self
        viewEventsList.dataSource = self
        
        viewEventsList.register(UINib(nibName: "SummitEventSliderViewController", bundle: nil), forCellWithReuseIdentifier: "cell")
        ///self.viewEventsList.register(SummitEventSliderViewController.self, forCellWithReuseIdentifier: "cell")
        self.viewEventsList.itemSize = FSPagerView.automaticSize
        
        
        
        
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped ))
        self.lblPreviousCurrentEvent.isUserInteractionEnabled = true
        self.lblPreviousCurrentEvent.addGestureRecognizer(labelTap)
        if isCurrentEvent {
            self.lblPreviousCurrentEvent.text = "<< Previous Event"
            self.lblHeading.text = "Upcoming Event"
        }
        else {
            self.lblPreviousCurrentEvent.text = "<< Upcoming Event"
            self.lblHeading.text = "Previous Event"
            self.btnRegisterHeightAnchor.constant = 0
        }
        
        lblPreviousCurrentEvent.isHidden = true
        self.pageControl.isHidden = true
        
        getEvents(isCurrentEvent: isCurrentEvent)
        
    }
    
    
    
    func getEvents(isCurrentEvent: Bool){
        
        if(isCurrentEvent){
            apiClass.loadApiCall(identifier: currentEventsUrl, url: currentEventsUrl, param: ["user_id": "\(userId)"], method: responseType.get.rawValue, header: headerToken, view: self)
        } else{
            apiClass.loadApiCall(identifier: previousEventsUrl, url: previousEventsUrl, param: ["user_id": "\(userId)"], method: responseType.get.rawValue, header: headerToken, view: self)
        }
        
    }
    
    func calculateScrollViewContentHeight(){
        
        scrollContainerHeightAnchor.isActive = false
        
        
        var height = lblHeading.frame.height
            //            + btnDetails.frame.height
            + lblPreviousCurrentEvent.frame.height
        if UIDevice().userInterfaceIdiom == .pad {
            height += 700
        }else{
            height += viewEventsList.frame.height
        }
        scrollContainerView.heightAnchor.constraint(equalToConstant: height).isActive = true
        
        
    }
    
    @objc func labelTapped()
    {
        
        if !isCurrentEvent {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            
            let VC = storyboard?.instantiateViewController(withIdentifier: "SummitEvent_Lists_ViewController") as! SummitEvent_Lists_ViewController
            
            
            VC.isCurrentEvent = false
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    
    //MARK:- Actions
    
    @IBAction func btnRegisterTapped(_ sender: Any) {
        
        let summitEventID = self.eventLists.first?.id ?? ""
        
        if  !summitEventID.isEmpty {
            
            apiClass.loadApiCall(identifier: setSummitEventsRegistration, url: setSummitEventsRegistration, param: ["user_id": "\(userId)", "summit_event_id":  summitEventID], method: responseType.post.rawValue, header: headerToken, view: self)
        }
        else {
            
            ShowErrorAlert(message: "No Current Event Found")
            
        }
        
       
        
        /*
        let VC = storyboard?.instantiateViewController(withIdentifier: "SummitEvent_EventRegistrationViewController") as! SummitEvent_EventRegistrationViewController

        if viewEventsList.currentIndex < eventLists.count {
            let event = eventLists[viewEventsList.currentIndex]

            let userDefaults = UserDefaults.standard

            do {
                try userDefaults.setObject(event, forKey: KEY_EVENT_DETAILS)
            } catch {
                print(error.localizedDescription)
            }

            self.navigationController?.pushViewController(VC, animated: true)
        }*/
    }
    
    
    func showEventSectionVC(){
        if let eventSelectedInSlider = eventSelectedInSlider
        {
            let userDefaults = UserDefaults.standard
            
            do {
                try userDefaults.setObject(eventSelectedInSlider, forKey: KEY_EVENT_DETAILS)
            } catch {
                print(error.localizedDescription)
            }
            
            let gotoEventSectionVC = self.storyboard?.instantiateViewController(withIdentifier: "SummitEvent_SectionsViewController") as! SummitEvent_SectionsViewController
            
            gotoEventSectionVC.thisEvent = eventSelectedInSlider
            self.navigationController?.pushViewController(gotoEventSectionVC, animated: true)
        }
    }
    
}

extension SummitEvent_Lists_ViewController: blurEffectDelegate
{
    func afterDimiss() {
        
    }
}
//MARK:- FSPager Delegate
extension SummitEvent_Lists_ViewController: FSPagerViewDataSource,FSPagerViewDelegate
{
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! SummitEventSliderViewController
        
        if !isCurrentEvent {
            cell.btnViewDetails.isHidden = true
        }
        
        let _currentRecord = eventLists[index]
        
        eventSelectedInSlider = _currentRecord
        
        
        cell.lblDateTime.text = _currentRecord.start_date
        
        if let event_images = _currentRecord.event_images {
            let imageUrl = URL(string:( event_images[0] ) )
            cell.imgEvent.sd_setImage(with: imageUrl, placeholderImage: nil, context: nil)
        }
        
        
        cell.lblCity.text = _currentRecord.venue_city
        
        
        
        let priceRangeStart = _currentRecord.price_range_start ?? "0"
        let priceRangeEnd = _currentRecord.price_range_end ?? "0"
      
        cell.lblPrice.isHidden = false
        cell.viewPriceBackground.isHidden = false
        
        if priceRangeStart != "0" && priceRangeEnd == "0" {
            cell.lblPrice.text = "$\(_currentRecord.price_range_start ?? "")"
        }
        else  if priceRangeEnd != "0" && priceRangeStart == "0" {
            cell.lblPrice.text = "$\(_currentRecord.price_range_end ?? "")"
        }
        else  if priceRangeEnd != "0" && priceRangeStart != "0" {
            cell.lblPrice.text = "$\(_currentRecord.price_range_start ?? "") - $\(_currentRecord.price_range_end ?? "")"
        }
        else {
            cell.lblPrice.isHidden = true
            cell.viewPriceBackground.isHidden = true
            cell.lblPrice.text = "$0"
        }
        
        
        var lblAddressFontSize: CGFloat = 15
        if(UIDevice.current.userInterfaceIdiom == .pad){
            lblAddressFontSize = 25
        }
        
        let completeString = NSMutableAttributedString()
        if(_currentRecord.name?.isEmpty == false){
            let attrs = [NSAttributedString.Key.font : UIFont(name: AppFonts.PoppinsSemiBold.rawValue, size: lblAddressFontSize + 1)]
            let nameAttributedString = NSMutableAttributedString(string: "\(_currentRecord.name ?? "")\n", attributes: attrs as [NSAttributedString.Key : Any])
            completeString.append(nameAttributedString)
        }
        
        if(_currentRecord.venue_address?.isEmpty == false){
            let attrs = [NSAttributedString.Key.font : UIFont(name: AppFonts.PoppinsRegular.rawValue, size: lblAddressFontSize)]
            let addressAttributedString = NSMutableAttributedString(string:_currentRecord.venue_address ?? "", attributes: attrs as [NSAttributedString.Key : Any])
            completeString.append(addressAttributedString)
        }
        
        cell.lblAddress.attributedText = completeString

        
        
        cell.contentView.isUserInteractionEnabled = false
        
        if(cell.onViewDetailsTap == nil){
            cell.onViewDetailsTap = {
                self.showEventSectionVC()
            }
        }
        
        return cell
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        print(eventLists.count)
        return self.eventLists.count
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
}

//MARK:- Api Delegate
extension SummitEvent_Lists_ViewController: ApiDelegate
{
    func successApiCall(data: Data, identifier: String, status: Bool) {
        
        lblPreviousCurrentEvent.isHidden = false
        
        if identifier == setSummitEventsRegistration {
            
           
            
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventsFacultyBaseModel.self, from: data )
                
                
                let summitEventRegistrationLink = self.eventLists.first?.registration_link ?? ""
                
                if summitEventRegistrationLink.isEmpty{
                    
                    ShowErrorAlert(message: "No Registration Link Found.")
                }
                else {
                    
                    if let url = URL(string: summitEventRegistrationLink) {
                        UIApplication.shared.open(url)
                    }
                    
                }
                
            }
            catch {
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)

            }
            
        }
        else if identifier == currentEventsUrl || identifier == previousEventsUrl {
            
            do
            {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(SummitEventBaseModel.self, from: data )
                
                
                if isCurrentEvent {
                    
                    if let firstEvent = response.eventLists?.first {
                        self.eventLists.append(firstEvent)
                    }
                }
                else {
                    self.eventLists = response.eventLists ?? []
                    self.pageControl.numberOfPages = self.eventLists.count
                    self.pageControl.contentHorizontalAlignment = .right
                    self.pageControl.isHidden = false
                }
                
                if self.eventLists.isEmpty{
                    self.lblNoEventsFound.isHidden = false
                    self.lblNoEventsFound.text = response.message ?? ApiCallsMessages.NoRecordsFound.rawValue
                    self.viewEventsList.isHidden = true
                }else{
                    self.lblNoEventsFound.isHidden = true
                    self.viewEventsList.isHidden = false
                }
                
                self.viewEventsList.reloadData()
                
                
                
            }
            catch {
                self.view.hideAll_makeToast(ApiCallsMessages.SomethingWentWrongWithResult.rawValue)
//                self.lblNoEventsFound.isHidden = false
//                self.viewEventsList.isHidden = true
            }
            
        }
        
        
    }
    
    func failureApiCall(message: String, identifier: String) {
        self.view.hideAll_makeToast(message)
        lblPreviousCurrentEvent.isHidden = false
//        self.lblNoEventsFound.isHidden = false
//        self.lblNoEventsFound.text = message
//        self.viewEventsList.isHidden = true
    }
}
