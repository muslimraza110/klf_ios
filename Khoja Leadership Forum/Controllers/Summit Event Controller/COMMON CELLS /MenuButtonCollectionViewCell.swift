//
//  MenuButtonCollectionViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 4/22/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

class BlueBackgroundBtn: UIButton {

    required public init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.lineBreakMode = .byClipping
        self.titleEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        self.cornerRadius = 8
        self.clipsToBounds = true
        
        
        self.setBackgroundImage(UIImage(named: "bluegradient_bg"), for: .normal)
        
        
        
        self.titleLabel?.font = .systemFont(ofSize: 17, weight: .bold)
        self.setTitleColor(.white, for: .normal)
        
        

    }
    
}

protocol buttonChangeDelegate {
    func didSelect()
}

class MenuButtonCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var btnDays: BlueBackgroundBtn!
    var selectButtonDelegate:buttonChangeDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnDays.titleLabel?.adjustsFontSizeToFitWidth = true
        btnDays.titleLabel?.lineBreakMode = .byClipping
        btnDays.titleEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        btnDays.cornerRadius = 8
        
        
        
    }

}
