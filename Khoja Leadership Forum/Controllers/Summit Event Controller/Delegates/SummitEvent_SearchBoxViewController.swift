//
//  SummitEvent_SearchBoxViewController.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/21/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

protocol ListingSearchBoxDelegate {
    
    func afterSearchButtonTapped( searchText:String )
    
    func onTextChanged(searchText:String)
    
    
}

class SummitEvent_SearchBoxViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var viewSearchBox: UIView!
    @IBOutlet weak var btnSearchBox: UIButton!
    @IBOutlet weak var txtSearchBox: UITextField!
    var searchBoxDelegate:ListingSearchBoxDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSearchBox.imageView?.contentMode = .scaleAspectFit
        btnSearchBox.setImage(UIImage(named: "magnify"), for: .normal)
        btnSearchBox.contentVerticalAlignment = .fill
        btnSearchBox.contentHorizontalAlignment = .fill
        btnSearchBox.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
//        btnSearchBox.tintColor = UIColor.appLightGrey
        
        
        txtSearchBox.delegate = self
        
        txtSearchBox.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @IBAction func btnSearchBoxTapped(_ sender: UIButton) {
        textFieldDidChange(txtSearchBox)
        txtSearchBox.resignFirstResponder()
    }
    
    @objc func textFieldDidChange(_ sender: UITextField)  {
        searchBoxDelegate?.onTextChanged(searchText: sender.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldDidChange(txtSearchBox)
        txtSearchBox.resignFirstResponder()
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchBoxDelegate?.afterSearchButtonTapped(searchText: txtSearchBox.text ?? "")
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
//        searchBoxDelegate?.afterSearchButtonTapped(searchText: "")
        return true
        
    }
    

}
