//
//  SummitProgramMenusTableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 5/4/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit

class SummitProgramMenusTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(UINib(nibName: "MenuButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "mb-cell")
    }

   
}


extension SummitProgramMenusTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mb-cell", for: indexPath) as! MenuButtonCollectionViewCell
        
        cell.btnDays.cornerRadius = 5
        cell.btnDays.setTitle(String(describing: indexPath.row)  , for: .normal)
        cell.btnDays.tag = indexPath.row
      
        return cell
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let padding: CGFloat =  5
         let collectionViewSize = collectionView.frame.size.width - padding

        print(collectionViewSize/2)
        
        var _width = 2.1
        if UIDevice().userInterfaceIdiom == .pad {
            _width = 2.05
        }
        
        print(collectionView.frame.size.height)
        
        return CGSize(width: collectionViewSize / CGFloat( _width ), height: 55 - 5)
     }
    
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
}
