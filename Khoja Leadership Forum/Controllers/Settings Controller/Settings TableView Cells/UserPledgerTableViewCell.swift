//
//  UserPledgerTableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 11/10/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import UIKit
class UserPledgerTableViewCell:UITableViewCell {
    
    @IBOutlet weak var lblPledgerName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
}
