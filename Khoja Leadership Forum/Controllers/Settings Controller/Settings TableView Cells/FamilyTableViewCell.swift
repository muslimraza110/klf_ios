
import UIKit

class FamilyTableViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var lblRelationship: UILabel!
    @IBOutlet weak var lblRelationshipName: UILabel!
    @IBOutlet weak var imgFamily: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
