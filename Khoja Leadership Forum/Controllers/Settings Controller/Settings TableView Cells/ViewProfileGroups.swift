//
//  ViewProfileGroups.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 14/10/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import UIKit
class ViewProfileGroupsTableViewCell:UITableViewCell {
    
    @IBOutlet weak var title: ContentLabel!
    @IBOutlet weak var name: ContentLabel!
    @IBOutlet weak var groupImageView: UIImageView!
}
