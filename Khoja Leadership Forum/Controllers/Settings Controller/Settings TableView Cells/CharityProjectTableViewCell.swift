//
//  CharityProjectTableViewCell.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 17/10/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import UIKit

class CharityProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var projectName: ContentLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
