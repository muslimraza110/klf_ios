
import UIKit
import RichEditorView
import Alamofire
import SwiftGifOrigin
import SDWebImage

class EditProfileViewController: UIViewController, UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {

    //MARK:- Btn Outlets
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var lblProfileName: ContentLabel!
    @IBOutlet weak var txtFieldTitle: TextFieldPadding!
    @IBOutlet weak var txtFieldFirstName: TextFieldPadding!
    @IBOutlet weak var txtFieldMiddleName: TextFieldPadding!
    @IBOutlet weak var txtFieldLastName: TextFieldPadding!
    @IBOutlet weak var txtFieldGender: TextFieldPadding!
    @IBOutlet weak var txtFieldPrimaryEmail: TextFieldPadding!
    @IBOutlet weak var txtFieldSecondaryEmail: TextFieldPadding!
    @IBOutlet weak var txtFieldWebsite: UITextField!
    @IBOutlet weak var txtFieldFaceBook: TextFieldPadding!
    @IBOutlet weak var txtFieldLinkdIn: TextFieldPadding!
    @IBOutlet weak var txtFieldBusinessTitle: TextFieldPadding!
    @IBOutlet weak var txtFieldDescription: TextFieldPadding!
    @IBOutlet weak var txtFieldAddress: TextFieldPadding!
    @IBOutlet weak var txtFieldCity: TextFieldPadding!
    @IBOutlet weak var txtFieldPostalCode: TextFieldPadding!
    @IBOutlet weak var txtFieldProvince: TextFieldPadding!
    @IBOutlet weak var txtFieldCountry: TextFieldPadding!
    @IBOutlet weak var txtFieldPrimaryPhone: TextFieldPadding!
    @IBOutlet weak var txtFieldSecondaryPhone: TextFieldPadding!
    @IBOutlet weak var privateBioEditorView: RichEditorView!
    @IBOutlet weak var aboutUsEditorView: RichEditorView!
    @IBOutlet weak var commentsOutlet: UITextView!
    @IBOutlet weak var familyTableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var dropDown: UIView!
    @IBOutlet weak var genderDropDown: UIView!
    
    //MARK:- Custom Variables
    var userId = 0
    var gender = ""
    var profileImage = ""
    var Status = false
    var userInfoModel = UserinfoModel()
    var userCommentModel = [UserCommentModel]()
    var familyModel = [FamilyModel]()
    var relatedId = [String]()
    var relation = [String]()
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    lazy var aboutToolBar: RichEditorToolbar = {
        let aboutToolBar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        aboutToolBar.options = RichEditorDefaultOption.all
        return aboutToolBar
    }()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        
        super.viewDidLoad()

        privateBioEditorView.delegate = self
        aboutUsEditorView.delegate = self
        privateBioEditorView.inputAccessoryView = toolbar
        aboutUsEditorView.inputAccessoryView = aboutToolBar
        toolbar.delegate = self
        aboutToolBar.delegate = self
        toolbar.editor = privateBioEditorView
        aboutToolBar.editor = aboutUsEditorView
        
        genderDropDown.isHidden = true
        dropDown.isHidden = true
        hideKeyboard()

        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        let itemDone = RichEditorOptionItem(image: nil, title: "Done") { (toolBar) in
            self.view.endEditing(true)
        }
       
        var options = toolbar.options
        options.append(item)
        options.append(itemDone)
        toolbar.options = options
        
        var options1 = aboutToolBar.options
        options1.append(item)
        options1.append(itemDone)
        aboutToolBar.options = options1
        
        self.navigationController?.isNavigationBarHidden = true
       
        registerTableViewCell()
       
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        profileImage = userData["profile_img"] as! String
       
        self.profileImageView.layer.cornerRadius = profileImageView.frame.width/2
        let url = URL(string: profileImage)!
        let imgData = try? Data(contentsOf: url)
        if let imageData = imgData {
            self.profileImageView.image = UIImage(data: imageData)
        }
       
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        headerHeight()
        
        let param = ["user_id":userId] as [String:Any]
        viewProfileApi(url: viewProfileUrl, method: responseType.get.rawValue, param: param, header: headerToken) {    (status) in
            if status{
                self.privateBioEditorView.html = self.userInfoModel.userDetailsPrivateBio ?? ""
                self.aboutUsEditorView.html = self.userInfoModel.userDetailsBio ?? ""
                self.loadData()
                self.familyTableView.reloadData()
                print("viewProfile data fetched")
            }else {
                print("viewProfile data couldn't fetch")
            }
            self.loaderView.isHidden = true
        }
       
   }
    //MARK:- Button Actions
    @IBAction func editTitlePictureButtonPressed(_ sender: UIButton) {
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func editProfilePictureButtonPressed(_ sender: UIButton) {
    }
    
 /*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func familyButtonPressed(_ sender: UIButton) {
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func categoryTriggerBtn(_ sender: Any) {
        dropDown.isHidden = true
        genderDropDown.isHidden = false
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func CategoryButton(_ sender: UIButton) {
        if sender.tag == 1{
            gender = "M"
            txtFieldGender.text = "Male"
            genderDropDown.isHidden = true
            
        }else if sender.tag == 2 {
            gender = "F"
            txtFieldGender.text = "Female"
            genderDropDown.isHidden = true
            
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func saveButtonPressed(_ sender: UIButton) {
        let relatedUserId = relatedId.joined(separator: ",")
        let userRelation = relation.joined(separator: ",")
        var copywriteId = ""
        if userCommentModel.count != 0 {
            copywriteId = userCommentModel[0].userCommentsCopywriterUserID!
        }else {
            copywriteId = ""
        }
        
        let param = ["user_id":userId, "description":txtFieldDescription.text ?? "","private_bio":privateBioEditorView.contentHTML,"address":txtFieldAddress.text ?? "","address2": userInfoModel.userDetailsAddress2!,"title":txtFieldTitle.text!, "city":txtFieldCity.text!,"country":txtFieldCountry.text!,"region":txtFieldProvince.text!,"postal_code":txtFieldPostalCode.text!, "email2":txtFieldSecondaryEmail.text!,"website":txtFieldWebsite.text!, "facebook":txtFieldFaceBook.text!,"linkedin":txtFieldLinkdIn.text!,"phone":txtFieldPrimaryPhone.text!,"phone2":txtFieldSecondaryPhone.text!,"first_name":txtFieldFirstName.text!,"middle_name":txtFieldMiddleName.text!, "last_name":txtFieldLastName.text!,"gender":gender,"email":txtFieldPrimaryEmail.text!,"business_title":"PMDC","bio":aboutUsEditorView.contentHTML,"copywriter_user_id":copywriteId,"comment":commentsOutlet.text!, "status":userInfoModel.usersStatus!,"related_user_id":relatedUserId ,"relation":userRelation] as [String:Any]
        
        editProfileApi(url: editProfileUrl, param: param, method: responseType.post.rawValue, headers: nil) { (status) in
            if status {
                print("edited")
                self.navigationController?.popViewController(animated: true)
            }else {
                self.showToast(message: "Profile couldn't edit try again later.")
                 print("couldn't edit")
            }
            self.loaderView.isHidden = true
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func categoryHideBtn(_ sender: Any) {
        genderDropDown.isHidden = true
        dropDown.isHidden = false
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func categorySelect(_ sender: UIButton) {
        if sender.tag == 1 {
            txtFieldTitle.text = "Dr."
            dropDown.isHidden = true
        }else if  sender.tag == 2 {
            txtFieldTitle.text = "Mr."
            dropDown.isHidden = true
        }
        else if  sender.tag == 3 {
            txtFieldTitle.text = "Mrs."
            dropDown.isHidden = true
        }
        else if  sender.tag == 4 {
            txtFieldTitle.text = "Ms."
            dropDown.isHidden = true
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @IBAction func backBtn(_ sender: Any) {
       self.navigationController?.isNavigationBarHidden = false
       self.navigationController?.popViewController(animated: true)
   }
   
    
    //MARK:- TextField Delegate Method
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.becomeFirstResponder() {
            dropDown.isHidden = true
        }
    }
    
    //MARK:- Custom Function
    func loadData() {
        
        txtFieldCity.text = userInfoModel.userDetailsCity
        txtFieldTitle.text = userInfoModel.usersTitle
        txtFieldAddress.text = userInfoModel.userDetailsAddress
        txtFieldCountry.text = userInfoModel.userDetailsCountry
        txtFieldLinkdIn.text = userInfoModel.userDetailsLinkedin
        txtFieldWebsite.text = userInfoModel.userDetailsWebsite
        txtFieldFaceBook.text = userInfoModel.userDetailsFacebook
        txtFieldLastName.text = userInfoModel.usersLastName
        txtFieldProvince.text = userInfoModel.userDetailsRegion
        txtFieldFirstName.text = userInfoModel.usersFirstName
        txtFieldMiddleName.text = userInfoModel.usersMiddleName
        txtFieldPostalCode.text = userInfoModel.userDetailsPostalCode
        txtFieldDescription.text = userInfoModel.userDetailsDescription
        txtFieldPrimaryEmail.text = userInfoModel.usersEmail
        txtFieldPrimaryPhone.text = userInfoModel.userDetailsPhone
        txtFieldBusinessTitle.text = userInfoModel.userDetailsCity
        txtFieldSecondaryEmail.text = userInfoModel.userDetailsEmail2
        txtFieldSecondaryPhone.text = userInfoModel.userDetailsPhone2
        lblProfileName.text = "\(userInfoModel.usersFirstName ?? "") \(userInfoModel.usersLastName ?? "")"
        
        for i in familyModel {
            relatedId.append(i.userFamiliesRelatedUserID ?? "")
            relation.append(i.userFamiliesRelation ?? "")
        }
        
        if userInfoModel.usersGender == "M" {
            txtFieldGender.text = "Male"
            gender = "M"
        }else {
            txtFieldGender.text = "Female"
            gender = "F"
        }
       

        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func viewProfileApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        userCommentModel.removeAll()
        familyModel.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                    if let data = JSON["userinfo1"] as? [[String:Any]] {
                        var infoDetail = [UserinfoModel]()
                        for i in data {
                            infoDetail.append(UserinfoModel(data: [i]))
                        }
                        self.userInfoModel = infoDetail[0]
                        print("Userinfo fetched")
                    }
                    if let data = JSON["families"] as? [[String:Any]] {
                        for i in data {
                            self.familyModel.append(FamilyModel(data: [i]))
                        }
                        print("family data fetched")
                    }
                    if let data = JSON["usercomments"] as? [[String:Any]] {
                        for i in data {
                            self.userCommentModel.append(UserCommentModel(data: [i]))
                        }
                        print("userComment data fetched")
                    }
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func editProfileApi(url:String,param:[String:Any],method:HTTPMethod,headers:HTTPHeaders?,completion:@escaping (_ status:Bool) -> Void){
        self.loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status) in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func registerTableViewCell ()
    {
        self.familyTableView.register(UINib(nibName: "FamilyTableViewCell", bundle: nil), forCellReuseIdentifier: "FamilyCell")
        
    }
  
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return familyModel.count
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = familyTableView.dequeueReusableCell(withIdentifier: "FamilyCell", for: indexPath) as! FamilyTableViewCell
        cell.lblRelationship.text = familyModel[indexPath.row].userFamiliesRelation
        cell.lblRelationshipName.text = "\(familyModel[indexPath.row].relatedUsersFirstName ?? "") \(familyModel[indexPath.row].relatedUsersLastName ?? "")"
    
        cell.imgFamily.layer.cornerRadius = cell.imgFamily.frame.width/2
        let remoteImageUrlString = familyModel[indexPath.row].profileImgName ?? ""
        let imageUrl = URL(string:remoteImageUrlString)
        cell.imgFamily.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            print("image \(indexPath.row) loaded")
        })
    
        return cell
    }
}

//MARK:- RichEditorDelegate Extensions
extension EditProfileViewController: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            //         htmlTextView.text = "HTML Preview"
        } else {
            print(content)
        }
    }
    
    
}

//MARK:- RichEditorToolbarDelegate Extensions
extension EditProfileViewController: RichEditorToolbarDelegate {
        
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if ((toolbar.editor?.hasRangeSelection) != nil) == true {
            toolbar.editor?.insertLink(href: "http://github.com/cjwirth/RichEditorView", text: "Github Link")
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
}
