
import UIKit
import Alamofire
import SwiftGifOrigin
import SDWebImage
import MessageUI


class ViewProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {

    //MARK:- Btn Outlets
    @IBOutlet weak var lblTitleName: BlueViewLabel!
    @IBOutlet weak var btnEmail: AdaptiveButton!
    @IBOutlet weak var lblPhone: ContentLabel!
    @IBOutlet weak var lblSecondaryPhone: UILabel!
    @IBOutlet weak var lblFB: ContentLabel!
    @IBOutlet weak var lblWebsite: ContentLabel!
    @IBOutlet weak var lblAddress: ContentLabel!
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var tableViewPledger: UITableView!
    @IBOutlet weak var tableViewBusiness: UITableView!
    @IBOutlet weak var tableViewCharities: UITableView!
    @IBOutlet weak var tableViewBusinessProjects: UITableView!
    @IBOutlet weak var tableViewCharityProjects: UITableView!
    @IBOutlet weak var tableViewFamily: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var isBusinessRecordFound: UILabel!
    @IBOutlet weak var isCharityRecordFound: UILabel!
    @IBOutlet weak var isFamilyRecordFound: UILabel!
    @IBOutlet weak var isPledgerFound: UILabel!
    @IBOutlet weak var isbusinessProjectFound: UILabel!
    @IBOutlet weak var isCharityProjectFound: UILabel!

    //MARK:- Custom Variables
    var businessCounter:Int = 0
    var charityCounter:Int = 0
    var email = ""
    var Status = false
    var isLoginMember = true
    var profileImage = ""
    var userId = 0
    var dropImage = UIImage.init(named: "dropdown.png")
    var businessSectionDataArray = [ExpandableViewProfile]()
    var businessHeadersTitleArray = [GroupsProjectModel]()
    var charitySectionDataArray = [ExpandableViewProfile]()
    var charityHeaderTitleArray = [GroupsProjectModel]()
    var userInfoModel = UserinfoModel()
    var businessGroupModel = [BusinessAndCharityGroupModel]()
    var charityGroupModel = [BusinessAndCharityGroupModel]()
    var groupProjectsModel = [GroupsProjectModel]()
    var familyModel = [FamilyModel]()
    var userBusinessProjectModel = [UserProjectModel]()
    var userCharityProjectModel = [UserProjectModel]()
    var userPledgeInfo = [UserPledgeInfo]()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        headerHeight()
        
        if isLoginMember {
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        }
                
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.navigationController?.isNavigationBarHidden = true
        
        self.tableViewFamily.delegate = self
        self.tableViewBusiness.delegate = self
        self.tableViewCharities.delegate = self
        self.tableViewBusinessProjects.delegate = self
        self.tableViewCharityProjects.delegate = self
        
        self.tableViewFamily.dataSource = self
        self.tableViewBusiness.dataSource = self
        self.tableViewCharities.dataSource = self
        self.tableViewBusinessProjects.dataSource = self
        self.tableViewCharityProjects.dataSource = self
        
        registerTableViewCell()
        loadApiOnScreen(userId: userId)
       
    }
    
    //MARK:- Btn Actions
    @IBAction func btnEmailPressed(_ sender: Any) {
        let mailComposeViewController = self.configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
           self.showSendMailErrorAlert()
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
        
    @IBAction func addProjectButtonPressed(_ sender: UIButton) {
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
        
    //MARK - detectSectionHeaderClick
    @objc func detectBusinessSectionHeaderClick(_ sender:UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewGroupViewController") as! ViewGroupViewController
        let section = sender.tag
        if String(userId) == businessHeadersTitleArray[section].groupsUsersUserID {
            vc.isGroupOwnwe = true
        }else {
            vc.isGroupOwnwe = false
        }
        vc.groupsId = businessHeadersTitleArray[section].groupID ?? ""
        vc.aboutUs = businessHeadersTitleArray[section].groupsBio ?? ""
        vc.address = businessHeadersTitleArray[section].groupsAddress ?? ""
        vc.email = businessHeadersTitleArray[section].groupsEmail ?? ""
        vc.phoneNumber = businessHeadersTitleArray[section].groupsPostalCode ?? ""
        vc.groupName = businessHeadersTitleArray[section].groupsName ?? ""
        vc.groupDescriptionName = businessHeadersTitleArray[section].groupsDescription ?? ""
        vc.profileImage = businessHeadersTitleArray[section].groupURL ?? ""

        self.navigationController?.pushViewController(vc, animated: true)

    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    //MARK - detectSectionHeaderClick
    @objc func detectCharitySectionHeaderClick(_ sender:UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewGroupViewController") as! ViewGroupViewController
        let section = sender.tag
        if String(userId) == charityHeaderTitleArray[section].groupsUsersUserID {
            vc.isGroupOwnwe = true
        }else {
            vc.isGroupOwnwe = false
        }
         vc.groupsId = charityHeaderTitleArray[section].groupID ?? ""
       vc.aboutUs = charityHeaderTitleArray[section].groupsBio ?? ""
       vc.address = charityHeaderTitleArray[section].groupsAddress ?? ""
       vc.email = charityHeaderTitleArray[section].groupsEmail ?? ""
       vc.phoneNumber = charityHeaderTitleArray[section].groupsPostalCode ?? ""
       vc.groupName = charityHeaderTitleArray[section].groupsName ?? ""
       vc.groupDescriptionName = charityHeaderTitleArray[section].groupsDescription ?? ""
       vc.profileImage = charityHeaderTitleArray[section].groupURL ?? ""

       self.navigationController?.pushViewController(vc, animated: true)

    }
                
/*-----------------------------------------------------------------------------------------------------------------------------------*/
        
    @objc func ExpandCloseForBusinessProject (button: UIButton) {
        
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in businessSectionDataArray[section].names.indices{
            let indexpath = IndexPath(row: row, section: section)
            indexPaths.append(indexpath)
        }
        
        let isExpanded = businessSectionDataArray[section].isExpanded
        businessSectionDataArray[section].isExpanded = !isExpanded
        if isExpanded {
            button.isSelected = false
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
            
            button.setImage(dropImage, for: .normal)
            tableViewBusinessProjects.deleteRows(at: indexPaths, with: .top)
        }
        else{
            button.isSelected = true
            dropImage = UIImage.init(named: "of-admin-dropup.png")
            
            button.setImage(dropImage, for: .normal)
            tableViewBusinessProjects.insertRows(at: indexPaths, with: .bottom)
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    @objc func ExpandCloseForCharityProject (button: UIButton) {
           
           let section = button.tag
           var indexPaths = [IndexPath]()
           for row in charitySectionDataArray[section].names.indices{
               let indexpath = IndexPath(row: row, section: section)
               indexPaths.append(indexpath)
           }
           
           let isExpanded = charitySectionDataArray[section].isExpanded
           charitySectionDataArray[section].isExpanded = !isExpanded
           if isExpanded {
               button.isSelected = false
               dropImage = UIImage.init(named: "of-admin-dropdown.png")
               
               button.setImage(dropImage, for: .normal)
               tableViewCharityProjects.deleteRows(at: indexPaths, with: .top)
           }
           else{
               button.isSelected = true
               dropImage = UIImage.init(named: "of-admin-dropup.png")
               
               button.setImage(dropImage, for: .normal)
               tableViewCharityProjects.insertRows(at: indexPaths, with: .bottom)
           }
           
       }
    
    //MARK:- Custom Function
    func loadApiOnScreen(userId:Int) {
         isBusinessRecordFound.isHidden = true
         isCharityRecordFound.isHidden = true
         isFamilyRecordFound.isHidden = true
         isPledgerFound.isHidden = true
         isbusinessProjectFound.isHidden = true
         isCharityProjectFound.isHidden = true
             
         let param = ["user_id":userId] as [String:Any]
        viewProfileApi(url: viewProfileUrl, method: responseType.get.rawValue, param: param, header: headerToken) { (status) in
            if status{
                self.tableViewFamily.reloadData()
                self.tableViewBusiness.reloadData()
                self.tableViewBusinessProjects.reloadData()
                self.tableViewCharityProjects.reloadData()
                self.tableViewCharities.reloadData()
                self.tableViewPledger.reloadData()

                if self.businessGroupModel.count ==  0 {
                    self.isBusinessRecordFound.isHidden = false
                }
                
                if self.charityGroupModel.count ==  0 {
                    self.isCharityRecordFound.isHidden = false
                }
                
                if self.familyModel.count ==  0 {
                    self.isFamilyRecordFound.isHidden = false
                }
                
                if self.userPledgeInfo.count == 0 {
                    self.isPledgerFound.isHidden = false
                }
                
                if self.businessSectionDataArray.count == 0 {
                    self.isbusinessProjectFound.isHidden = false
                }
                if self.charitySectionDataArray.count == 0 {
                    self.isCharityProjectFound.isHidden = false
                }
                
                
                self.lblPhone.text = self.userInfoModel.userDetailsPhone
                self.lblTitleName.text = "\(self.userInfoModel.usersFirstName ?? "") \(self.userInfoModel.usersLastName ?? "")"
                
                if self.userInfoModel.userDetailsFacebook == "" {
                    self.lblFB.text = "Not Available"
                }else {
                    self.lblFB.text = self.userInfoModel.userDetailsFacebook ?? "Not Available"
                }
                
                if self.userInfoModel.userDetailsAddress == "" && self.userInfoModel.userDetailsAddress2 == "" {
                    self.lblAddress.text = "Not Available"
                }else {
                    self.lblAddress.text = "\(self.userInfoModel.userDetailsAddress ?? "Not Available") \(self.userInfoModel.userDetailsAddress2 ?? "") \(self.userInfoModel.userDetailsCity ?? "") \(self.userInfoModel.userDetailsRegion ?? "") \(self.userInfoModel.userDetailsPostalCode ?? "") \(self.userInfoModel.userDetailsCountry ?? "")"
                }
                
                if self.userInfoModel.userDetailsWebsite == "" {
                    self.lblWebsite.text = "Not Available"
                }else {
                    self.lblWebsite.text = self.userInfoModel.userDetailsWebsite ?? "Not Available"
                }
                
                if self.userInfoModel.userDetailsPhone2 == "" {
                    self.lblSecondaryPhone.text = "Not Available"
                }else {
                    self.lblSecondaryPhone.text = self.userInfoModel.userDetailsPhone2 ?? ""
                }
                
                
                self.email = self.userInfoModel.usersEmail ?? ""
                self.btnEmail.setTitle(self.email, for: .normal)
//                self.lblEmailAddress.text = self.userInfoModel.usersEmail
                
                self.profileImage = self.userInfoModel.profileImgName ?? ""
                self.imgViewProfile.layer.cornerRadius = self.imgViewProfile.frame.width/2
                let url = URL(string: self.profileImage)!
                let imgData = try? Data(contentsOf: url)
                if let imageData = imgData {
                   self.imgViewProfile.image = UIImage(data: imageData)
                }
                
                print("viewProfile data fetched")
            }else {
                print("viewProfile data couldn't fetch")
            }
            self.loaderView.isHidden = true

        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func registerTableViewCell ()
    {
        self.tableViewFamily.register(UINib(nibName: "FamilyTableViewCell", bundle: nil), forCellReuseIdentifier: "FamilyCell")
        self.tableViewBusinessProjects.register(UINib(nibName: "GroupProjectTableViewCell", bundle: nil), forCellReuseIdentifier: "GProjCell")
        self.tableViewCharityProjects.register(UINib(nibName: "CharityProjectTableViewCell", bundle: nil), forCellReuseIdentifier: "CharityProjectTableViewCell")
        self.tableViewBusiness.register(UINib(nibName: "ViewProfileGroupsTableViewCell", bundle: nil), forCellReuseIdentifier: "ViewProfileGroupsTableViewCell")
        self.tableViewCharities.register(UINib(nibName: "ViewProfileGroupsTableViewCell", bundle: nil), forCellReuseIdentifier: "ViewProfileGroupsTableViewCell")
        self.tableViewPledger.register(UINib(nibName: "UserPledgerTableViewCell", bundle: nil), forCellReuseIdentifier: "UserPledgerTableViewCell")
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func businessAndCharitySectionData(projectModel:[UserProjectModel]) {
        var groupData = [String]()
        
        for i in projectModel {
            groupData.append(i.groupID ?? "")
        }
        
        for i in groupProjectsModel {
            for a in groupData {
                if a == i.groupID {
                    if i.groupsCategoryID == "2" {
                        for found in businessHeadersTitleArray{
                            if found.groupID == i.groupID{
                               businessCounter += 1
                            }
                            else{}
                        }
                        if businessCounter == 0{
                        businessHeadersTitleArray.append(i)
                        }
                        else{}
                    }else {
                        for found in charityHeaderTitleArray{
                            if found.groupID == i.groupID{
                               charityCounter += 1
                            }
                            else{}
                        }
                        if charityCounter == 0{
                        charityHeaderTitleArray.append(i)
                        }
                        else{}
                        
                    }
                }
            }
        }
        
        
        for i in businessHeadersTitleArray {
            var businessArr = [UserProjectModel]()
            for a in projectModel {
                if i.groupID == a.groupID {
                    businessArr.append(a)
                }
            }
            businessSectionDataArray.append(ExpandableViewProfile(isExpanded: false, names: businessArr))
        }
        
        for i in charityHeaderTitleArray {
            var charityArr = [UserProjectModel]()
            for a in projectModel {
                if i.groupID == a.groupID {
                    charityArr.append(a)
                }
            }
            charitySectionDataArray.append(ExpandableViewProfile(isExpanded: false, names: charityArr))
        }
  
    }
        
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func expandableSectionData (projectModel:[UserProjectModel]) {
        
        var business = [UserProjectModel]()
        var charity = [UserProjectModel]()
        
        //Data Appending with duplications
        for groupProject in projectModel {
            for groupId in self.groupProjectsModel {
                if groupProject.groupID == groupId.groupID {
                    if groupId.groupsCategoryID == "2" {
                        business.append(groupProject)
                        
                    }else {
                        charity.append(groupProject)
                    }
                }
            }
        }
        
        var businessDuplicateChk = [String]()
        var charityDuplicateChk = [String]()
        
        //Data Appending without duplications
        for i in business {
            if self.userBusinessProjectModel.count != 0 {
                for a in self.userBusinessProjectModel {
                    if i.projectID != a.projectID {
                        if !businessDuplicateChk.contains(i.projectID!) {
                            self.userBusinessProjectModel.append(i)
                            businessDuplicateChk.append(i.projectID!)
                        }
                    }
                }
            }else {
                self.userBusinessProjectModel.append(i)
                businessDuplicateChk.append(i.projectID!)
            }
        }
        
        for i in charity {
            if self.userCharityProjectModel.count != 0 {
                for a in self.userCharityProjectModel {
                    if i.projectID != a.projectID {
                        if !charityDuplicateChk.contains(i.projectID!) {
                            self.userCharityProjectModel.append(i)
                            charityDuplicateChk.append(i.projectID!)
                        }
                    }
                }
            }else {
                self.userCharityProjectModel.append(i)
                charityDuplicateChk.append(i.projectID!)
            }
        }
        
        //Data Appending in SectionDataArray after remove duplications
        self.businessSectionDataArray.append(ExpandableViewProfile(isExpanded: false, names: self.userBusinessProjectModel))
        self.businessSectionDataArray.append(ExpandableViewProfile(isExpanded: false, names: self.userCharityProjectModel))
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func viewProfileApi(url:String,method:HTTPMethod,param:[String:Any]?,header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        businessGroupModel.removeAll()
        charityGroupModel.removeAll()
        groupProjectsModel.removeAll()
        familyModel.removeAll()
        userPledgeInfo.removeAll()
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: headerToken) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.businessSectionDataArray.removeAll()
                    self.Status = true
                    if let data = JSON["userinfo1"] as? [[String:Any]] {
                        var infoDetail = [UserinfoModel]()
                        for i in data {
                            infoDetail.append(UserinfoModel(data: [i]))
                        }
                        if infoDetail.count != 0 {
                            self.userInfoModel = infoDetail[0]
                        }
                        
                        print("Userinfo fetched")
                    }
                    if let data = JSON["pledges"] as? [[String:Any]] {
                        for i in data {
                            self.userPledgeInfo.append(UserPledgeInfo(data: [i]))
                        }
                        
                        print("Userinfo pledge fetched")
                    }
                    if let data = JSON["businessandprojects"] as? [[String:Any]] {
                        var businessDetail = [BusinessAndCharityGroupModel]()
                        for i in data {
                            businessDetail.append(BusinessAndCharityGroupModel(data: [i]))
                        }
                        for i in businessDetail {
                            if i.groupsCategoryID == "2" {
                                self.businessGroupModel.append(i)
                            }
                        }
                        print("business data fetched")
                    }
                    if let data = JSON["charitiesandprojects"] as? [[String:Any]] {
                        var charityDetail = [BusinessAndCharityGroupModel]()
                        for i in data {
                            charityDetail.append(BusinessAndCharityGroupModel(data: [i]))
                        }
                        for i in charityDetail {
                            if i.groupsCategoryID == "3" {
                                self.charityGroupModel.append(i)
                            }
                        }
                        print("charityGroup data fetched")
                    }
                   

                    if let data = JSON["groups"] as? [[String:Any]] {
                        for i in data {
                            self.groupProjectsModel.append(GroupsProjectModel(data: [i]))
                        }
                        print("groupProjects data fetched")
                    }
                    
                    var projectModel = [UserProjectModel]()
                    if let data = JSON["groupsproject"] as? [[String:Any]] {
                        for i in data {
                            projectModel.append(UserProjectModel(data: [i]))
                        }
                        print("groupProjects data fetched")
                    }
                    
                    if let data = JSON["families"] as? [[String:Any]] {
                        for i in data {
                            self.familyModel.append(FamilyModel(data: [i]))
                        }
                        print("family data fetched")
                    }
                    
//                    self.expandableSectionData(projectModel: projectModel)
                    self.businessAndCharitySectionData(projectModel: projectModel)
                    
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func showSendMailErrorAlert() {
        self.ShowErrorAlert(message: "Could Not Send Email", AlertTitle: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
    }
    
    //MARK:- Mail Composer Function
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([email])
        return mailComposerVC
     }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableViewBusinessProjects  {
            return businessHeadersTitleArray.count
        }else if tableView == tableViewCharityProjects  {
            return charityHeaderTitleArray.count
        }
        return 1
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            if tableView == tableViewBusiness || tableView == tableViewCharities {
                return 70
            }
            return 40
        }else {
            if tableView == tableViewBusiness || tableView == tableViewCharities {
                return 90
            }
            return 60
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableViewBusinessProjects{
            if !businessSectionDataArray[section].isExpanded {
                return 0
            }
            return self.businessSectionDataArray[section].names.count
        }else if tableView == tableViewCharityProjects {
            if !charitySectionDataArray[section].isExpanded {
                return 0
            }
            return self.charitySectionDataArray[section].names.count
        }else if tableView == tableViewCharities{
            return charityGroupModel.count
        }
        else if tableView == tableViewBusiness{
            return businessGroupModel.count
        }
        else if tableView == tableViewPledger{
            return userPledgeInfo.count
        }
        else{
            return familyModel.count
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableViewBusiness{
            let cell = self.tableViewBusiness.dequeueReusableCell(withIdentifier: "ViewProfileGroupsTableViewCell", for: indexPath) as! ViewProfileGroupsTableViewCell
            cell.name.attributedText = NSAttributedString(string: "\(businessGroupModel[indexPath.row].groupsName ?? "")", attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            if businessGroupModel[indexPath.row].groupsUsersUserAlias == "" || businessGroupModel[indexPath.row].groupsUsersUserAlias == nil {
                cell.title.text = "Not Available"
            }else {
                cell.title.text = businessGroupModel[indexPath.row].groupsUsersUserAlias
            }
            cell.groupImageView.layer.cornerRadius = cell.groupImageView.frame.width/2
            let remoteImageUrlString = businessGroupModel[indexPath.row].groupImage ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.groupImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
            
            return cell
        }
        else if tableView == self.tableViewCharities{
            let cell = self.tableViewCharities.dequeueReusableCell(withIdentifier: "ViewProfileGroupsTableViewCell", for: indexPath) as! ViewProfileGroupsTableViewCell
            cell.name.attributedText = NSAttributedString(string: "\(charityGroupModel[indexPath.row].groupsName ?? "")", attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            if charityGroupModel[indexPath.row].groupsUsersUserAlias == "" || charityGroupModel[indexPath.row].groupsUsersUserAlias == nil {
                cell.title.text = "Not Available"
            }else {
                cell.title.text = charityGroupModel[indexPath.row].groupsUsersUserAlias
            }
            
            
            cell.groupImageView.layer.cornerRadius = cell.groupImageView.frame.width/2
            let remoteImageUrlString = charityGroupModel[indexPath.row].groupImage ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.groupImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
            
            return cell
        }
        else if tableView == self.tableViewBusinessProjects{
            let cell = self.tableViewBusinessProjects.dequeueReusableCell(withIdentifier: "GProjCell", for: indexPath) as! GroupProjectTableViewCell
            let name = businessSectionDataArray[indexPath.section].names[indexPath.row]
            cell.projectName.text = name.projectsName
            
            return cell
        }
        else if tableView == self.tableViewCharityProjects{
            let cell = self.tableViewCharityProjects.dequeueReusableCell(withIdentifier: "CharityProjectTableViewCell", for: indexPath) as! CharityProjectTableViewCell
            let name = charitySectionDataArray[indexPath.section].names[indexPath.row]
            cell.projectName.text = name.projectsName
            return cell
        }
        else if tableView == self.tableViewPledger{
            let cell = self.tableViewPledger.dequeueReusableCell(withIdentifier: "UserPledgerTableViewCell", for: indexPath) as! UserPledgerTableViewCell
            cell.lblPledgerName.attributedText = NSAttributedString(string: userPledgeInfo[indexPath.row].pledgeName ?? "", attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            cell.lblAmount.text = userPledgeInfo[indexPath.row].pledgeAmount
            return cell
        }
        else {
            let cell = self.tableViewFamily.dequeueReusableCell(withIdentifier: "FamilyCell", for: indexPath) as! FamilyTableViewCell
            cell.lblRelationship.text = "\(familyModel[indexPath.row].userFamiliesRelation ?? "") of"
            cell.lblRelationshipName.attributedText = NSAttributedString(string: "\(familyModel[indexPath.row].relatedUsersFirstName ?? "") \(familyModel[indexPath.row].relatedUsersLastName ?? "")", attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            
            cell.imgFamily.layer.cornerRadius = cell.imgFamily.frame.width/2
            let remoteImageUrlString = familyModel[indexPath.row].profileImgName ?? ""
            let imageUrl = URL(string:remoteImageUrlString)
            cell.imgFamily.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("image \(indexPath.row) loaded")
            })
            
            return cell
        }
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView == tableViewBusinessProjects {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProjectViewController") as! ViewProjectViewController
            vc.projectId = businessSectionDataArray[indexPath.section].names[indexPath.row].projectID ?? ""
            self.navigationController?.pushViewController(vc, animated: true)

        }else if tableView == tableViewCharityProjects {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewProjectViewController") as! ViewProjectViewController
            vc.projectId = charitySectionDataArray[indexPath.section].names[indexPath.row].projectID ?? ""
            self.navigationController?.pushViewController(vc, animated: true)

        }else if tableView == tableViewBusiness {
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewGroupViewController") as! ViewGroupViewController
            vc.groupsId = businessGroupModel[indexPath.row].groupsID ?? ""
            vc.aboutUs = businessGroupModel[indexPath.row].groupsBio ?? ""
            vc.address = "\(businessGroupModel[indexPath.row].groupsAddress ?? "") \(businessGroupModel[indexPath.row].groupsAddress2 ?? ""), \(businessGroupModel[indexPath.row].groupsCity ?? "") \(businessGroupModel[indexPath.row].groupsRegion ?? ""), \(businessGroupModel[indexPath.row].groupsCountry ?? "")"
            vc.email = businessGroupModel[indexPath.row].groupsEmail ?? ""
            vc.phoneNumber = businessGroupModel[indexPath.row].groupsPhone ?? ""
            vc.groupName = businessGroupModel[indexPath.row].groupsName ?? ""
            vc.groupDescriptionName = businessGroupModel[indexPath.row].groupsDescription ?? ""
            vc.profileImage = businessGroupModel[indexPath.row].groupImage ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if tableView == tableViewCharities {
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewGroupViewController") as! ViewGroupViewController
            vc.groupsId = charityGroupModel[indexPath.row].groupsID ?? ""
            vc.aboutUs = charityGroupModel[indexPath.row].groupsBio ?? ""
            vc.address = "\(charityGroupModel[indexPath.row].groupsAddress ?? "") \(charityGroupModel[indexPath.row].groupsAddress2 ?? ""), \(charityGroupModel[indexPath.row].groupsCity ?? "") \(charityGroupModel[indexPath.row].groupsRegion ?? ""), \(charityGroupModel[indexPath.row].groupsCountry ?? "")"
            vc.email = charityGroupModel[indexPath.row].groupsEmail ?? ""
            vc.phoneNumber = charityGroupModel[indexPath.row].groupsPhone ?? ""
            vc.groupName = charityGroupModel[indexPath.row].groupsName ?? ""
            vc.groupDescriptionName = charityGroupModel[indexPath.row].groupsDescription ?? ""
            vc.profileImage = charityGroupModel[indexPath.row].groupImage ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if tableView == tableViewFamily {
            
            let familyId = familyModel[indexPath.row].relatedUsersID
            loadApiOnScreen(userId: Int(familyId ?? "0") ?? 0)
            
        }else if tableView == tableViewPledger {
            if userPledgeInfo[indexPath.row].isDeleted == 0 {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "InitiativeDetailViewController") as! InitiativeDetailViewController
                vc.initiativeId = userPledgeInfo[indexPath.row].pledgeInitiativeId ?? "0"
                vc.initiativeTitle = userPledgeInfo[indexPath.row].pledgeName ?? ""
                vc.userId = userId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }

/*-----------------------------------------------------------------------------------------------------------------------------------*/
       
    // Header for a section settings
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if tableView == tableViewBusinessProjects{
            
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableViewBusinessProjects.frame.width, height: 150))
            
            // code for adding centered title
            headerView.backgroundColor = UIColor.white
            let headerLabel = UILabel(frame: CGRect(x: 15, y: 5, width:tableViewBusinessProjects.frame.width-80, height: 30))
            headerLabel.textColor = UIColor.black
            if businessHeadersTitleArray.count != 0 {
                headerLabel.text = businessHeadersTitleArray[section].groupsName
            }
            if UIDevice().userInterfaceIdiom == .phone {
                headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
            }else {
                headerLabel.font = UIFont.boldSystemFont(ofSize: 18)
            }
            headerLabel.textAlignment = .left
            headerLabel.contentMode = .scaleAspectFit
            
            
            //Custom Button to detect the Header section tap event
           let buttonWithTag = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width-80, height: 100))
           buttonWithTag.tag = section
           buttonWithTag.addTarget(self, action: #selector(self.detectBusinessSectionHeaderClick(_:)), for: .touchUpInside)
           headerView.addSubview(buttonWithTag)

            headerView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            headerLabel.textColor = UIColor.white
            headerView.addSubview(headerLabel)
            
            let arrowButton = UIButton(frame: CGRect(x:headerView.frame.size.width - 80, y:0, width:60, height:30))
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
            
            arrowButton.setImage(dropImage, for: .normal)
            arrowButton.setTitleColor(.black, for: .normal)
            arrowButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
            arrowButton.backgroundColor = UIColor.clear
            arrowButton.addTarget(self, action: #selector(ExpandCloseForBusinessProject), for: .touchUpInside)
            arrowButton.tag = section
            arrowButton.imageView?.contentMode = .scaleAspectFit
            
    //        let countValue = self.sectionDataArray[section].names.count
    //        if countValue != 0{
                headerView.addSubview(arrowButton)
    //        }
            //constraint for dropdown arrow
            arrowButton.translatesAutoresizingMaskIntoConstraints = false
            arrowButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
            arrowButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
            arrowButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
            arrowButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
            
            arrowButton.setNeedsLayout()
            arrowButton.layoutIfNeeded()
            arrowButton.clipsToBounds = true
            headerView.layer.cornerRadius = 10
            return headerView
            
        }else if tableView == tableViewCharityProjects {
            
             let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableViewCharityProjects.frame.width, height: 150))
                    
                    // code for adding centered title
            headerView.backgroundColor = UIColor.white
            let headerLabel = UILabel(frame: CGRect(x: 15, y: 5, width:tableViewCharityProjects.frame.width-80, height: 30))
            headerLabel.textColor = UIColor.black
            if charityHeaderTitleArray.count != 0 {
                headerLabel.text = charityHeaderTitleArray[section].groupsName
            }
            if UIDevice().userInterfaceIdiom == .phone {
                headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
            }else {
                headerLabel.font = UIFont.boldSystemFont(ofSize: 18)
            }
            headerLabel.textAlignment = .left
            headerLabel.contentMode = .scaleAspectFit
            
            //Custom Button to detect the Header section tap event
           let buttonWithTag = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width-80, height: 100))
           buttonWithTag.tag = section
           buttonWithTag.addTarget(self, action: #selector(self.detectCharitySectionHeaderClick(_:)), for: .touchUpInside)
           headerView.addSubview(buttonWithTag)

            
            headerView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            headerLabel.textColor = UIColor.white
            headerView.addSubview(headerLabel)
            
            let arrowButton = UIButton(frame: CGRect(x:headerView.frame.size.width - 80, y:0, width:60, height:30))
            dropImage = UIImage.init(named: "of-admin-dropdown.png")
            
            arrowButton.setImage(dropImage, for: .normal)
            arrowButton.setTitleColor(.black, for: .normal)
            arrowButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
            arrowButton.backgroundColor = UIColor.clear
            arrowButton.addTarget(self, action: #selector(ExpandCloseForCharityProject), for: .touchUpInside)
            arrowButton.tag = section
            arrowButton.imageView?.contentMode = .scaleAspectFit
            
    //        let countValue = self.sectionDataArray[section].names.count
    //        if countValue != 0{
                headerView.addSubview(arrowButton)
    //        }
            //constraint for dropdown arrow
            arrowButton.translatesAutoresizingMaskIntoConstraints = false
            arrowButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
            arrowButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
            arrowButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
            arrowButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
            
            arrowButton.setNeedsLayout()
            arrowButton.layoutIfNeeded()
            arrowButton.clipsToBounds = true
            headerView.layer.cornerRadius = 10
            return headerView
            
        }
        else{
            let emptyView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            return emptyView
            
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            if tableView == tableViewBusinessProjects || tableView == tableViewCharityProjects {
                return 40
            }
            else{
                return 0
            }
        }else {
            if tableView == tableViewBusinessProjects || tableView == tableViewCharityProjects {
                return 50
            }
            else{
                return 0
            }
        }
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == tableViewBusinessProjects || tableView == tableViewCharityProjects {
            return 5
        }
        else{
            return 0
        }
    }
    
    
}
