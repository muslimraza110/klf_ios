
import UIKit
import Alamofire
import SwiftGifOrigin

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {

    //MARK:- Btn Outlets
    @IBOutlet weak var txtFieldPassword: TextFieldPadding!
    @IBOutlet weak var txtFieldConfirmPassword: TextFieldPadding!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    @IBOutlet weak var header: NSLayoutConstraint!
    
    //MARK:- Custom Variable
    var confirmPassFlag = false
    var passFlag = false
    var Status = false
    var userId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.txtFieldPassword.delegate = self
        self.txtFieldConfirmPassword.delegate = self
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
    }
    
    //MARK:- Button Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if textFieldFormatCheck() {
            if isValidPassword() {
                print("all values inserted without constraints")
                let param = ["user_id":userId,"newpassword":txtFieldPassword.text!,"confirmpassword":txtFieldConfirmPassword.text!] as [String:Any]
                changePasswordApi(url: changePasswordUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                    if status {
                        print("password changed")
                        //Removing all user defaults
                        let domain = Bundle.main.bundleIdentifier!
                        UserDefaults.standard.removePersistentDomain(forName: domain)
                        UserDefaults.standard.synchronize()
                        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                        
                        self.showToast(message: "Password changed successfully")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            appdelegate.window!.rootViewController = vc
                        }
                        //setting login as root
                        
                        NotificationCenter.default.post(name: NSNotification.Name("logoutNotification"), object:nil)
                    }else {
                        self.showToast(message: "Try again password couldn't change")
                        print("password couldn't change")
                    }
                    self.txtFieldConfirmPassword.text = ""
                    self.txtFieldPassword.text = ""
                    self.txtFieldConfirmPassword.resignFirstResponder()
                    self.txtFieldPassword.resignFirstResponder()
                    self.loaderView.isHidden = true
                }
            }else {
                self.showToast(message: "Password should be match with validation")
            }
           
        }else {
            print("one or more fields are empty.")
            if self.txtFieldPassword.text!.count == 0 || self.txtFieldConfirmPassword.text!.count == 0{
                showToast(message: "Password can't be blank")
            }
            else if self.txtFieldPassword.text != self.txtFieldConfirmPassword.text && (self.txtFieldPassword.text!.count > 6 && self.txtFieldConfirmPassword.text!.count > 6){
                showToast(message: "Password doesn't match")
            }
            else if self.txtFieldPassword.text!.count < 6 || self.txtFieldConfirmPassword.text!.count < 6{
                showToast(message: "Password too short")
            }
            
        }
        
    }
        
    //MARK:- TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if textField == self.txtFieldPassword {
            self.txtFieldConfirmPassword .becomeFirstResponder()
        }
        else{
            
        }
        return true
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtFieldPassword {
    
            self.txtFieldPassword.borderColor = .darkGray
            
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else{
            self.txtFieldConfirmPassword.borderColor = .darkGray
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
    }
    
    //MARK:- Custom Function
    func isValidPassword() -> Bool {

        let passwordRegex = "^(?=.*[A-Z])(?=.*[0-9]).{6,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: txtFieldPassword.text)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
        
    func changePasswordApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func textFieldFormatCheck() -> Bool {
        
        if self.txtFieldPassword.text!.count < 6 {
            self.txtFieldPassword.borderColor = .red
            passFlag = false
            
        }
        else{
            passFlag = true
        }
        
        let matchValue = self.txtFieldPassword.text == self.txtFieldConfirmPassword.text ? true : false

        if self.txtFieldConfirmPassword.text!.count < 6 || matchValue == false {

            self.txtFieldConfirmPassword.borderColor = .red
            confirmPassFlag = false
        }
        else{
            confirmPassFlag = true
        }
        
        if passFlag && confirmPassFlag == true {return true}
        else{return false}
    }
    
/*-----------------------------------------------------------------------------------------------------*/
}
