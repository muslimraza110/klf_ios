
import UIKit
import Alamofire
import SwiftGifOrigin


class FeedbackViewController: UIViewController,UITextFieldDelegate, UITextViewDelegate {

    //MARK:- Btn Outlets
    @IBOutlet weak var txtFieldTitle: TextFieldPadding!
    @IBOutlet weak var txtViewContent: UITextView!
    @IBOutlet weak var header: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderImg: UIImageView!
    
    //MARK:- Custom Variables
    var titleFlag:Bool = false
    var contentFlag:Bool = false
    var Status = false
    var userId = 0
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        headerHeight()
        loaderView.isHidden = true
        loaderImg.image = UIImage.gif(name: "loader_gif")
        self.navigationController?.isNavigationBarHidden = true
        
        self.txtFieldTitle.delegate = self
        self.txtViewContent.delegate = self
        
        let loginData = UserDefaults.standard.object(forKey: "Login") as? NSData
        let userData = NSKeyedUnarchiver.unarchiveObject(with: loginData! as Data) as! NSDictionary
        userId = userData["id"] as! Int
        
        
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK:- Button Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
        if textFormatCheck() {
            print("all values inserted without constraints")
            let param = ["user_id":userId,"name":txtFieldTitle.text!,"description":txtViewContent.text!] as [String : Any]
            feedbackApi(url: feedBackUrl, method: responseType.post.rawValue, param: param, header: headerToken) { (status) in
                if status {
                    print("feedback updated")
                    self.txtFieldTitle.text = ""
                    self.txtViewContent.text = ""
                    self.txtViewContent.resignFirstResponder()
                    self.showToast(message: "Feedback has been sent")
                }else {
                    self.showToast(message: "Feedback couldn't send try again")
                    print("feedback couldn't update")
                }
                self.loaderView.isHidden = true
            }
        }
        else{
            showToast(message: "All fields are mandatory")
            print("some value is missing")
        }
    }
        
    //MARK: - Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
            self.txtFieldTitle.borderColor = .darkGray
            
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        self.txtViewContent.borderColor = .darkGray
        
        let maxLength = 1000
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
   
    //MARK:- Custom Functions
    func headerHeight(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                header.constant += 24
            case 2688:
                header.constant += 24
            case 1792:
                header.constant += 24
            default:
                print("Unknown")
            }
        }
        if UIDevice().userInterfaceIdiom == .pad {
            header.constant += 4
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func textFormatCheck() -> Bool {
        
        if self.txtFieldTitle.text?.count == 0 {

            self.txtFieldTitle.borderColor = .red
            titleFlag = false
            
        }
        else{
            titleFlag = true
        }
        
        if self.txtViewContent.text?.count == 0 {

            self.txtViewContent.borderColor = .red
            contentFlag = false
            
        }
        else{
            contentFlag = true
        }
        
        if titleFlag && contentFlag == true {return true}
        else{return false}
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    
    func feedbackApi(url:String,method:HTTPMethod,param:[String:Any],header:HTTPHeaders?,completion: @escaping (_ status:Bool) -> Void) {
        loaderView.isHidden = false
        Api.sharedInstance.sendData(url: url, param: param, method: method, header: header) { (JSON,status)  in
            if status {
                self.Status = false
                let JSON = JSON
                let status = JSON["status"] as? String
                if status == "success" {
                    self.Status = true
                }else {
                    self.Status = false
                }
                completion(self.Status)
            }else {
                self.loaderView.isHidden = true
                print("internet is unavailable")
            }
        }
    }
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/

    
}
