
import UIKit

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}





var vSpinner : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        
        vSpinner = spinnerView
        print(vSpinner)

    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            print(vSpinner)
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }

    func setBackgroundBlurred() {
        if !UIAccessibility.isReduceTransparencyEnabled {
            let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurEffectView.tag = 765
            blurEffectView.alpha = 0
            view.insertSubview(blurEffectView, at: 0)
            _ = view.constrainToEdges(blurEffectView)
            UIView.animate(withDuration: 0.5) { [weak blurEffectView] in
                blurEffectView?.alpha = 1
            }
        } else {
            view.backgroundColor = .black
        }
    }

    func removeBackgroundBlurred() {
        if !UIAccessibility.isReduceTransparencyEnabled {
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                self?.view.subviews.first(where: { $0.tag == 765 })?.alpha = 0
            }, completion: { [weak self] _ in
                self?.view.subviews.first(where: { $0.tag == 765 })?.removeFromSuperview()
            })
        } else {
            view.backgroundColor = .clear
        }
    }

    /// Make the navigation bar background clear
    func navigationBarTransparent() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
}

enum ColorCodes: String {
    
    case Gray_Background = "#F5F5F5"
}

extension String {
    func escapeString() -> String {
        var newString = self.replacingOccurrences(of: "\"", with:  "\"\"")
        if newString.contains(",") || newString.contains("\n") {
            newString = String(format: "\"%@\"", newString)
        }

        return newString
    }
}
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
    
    func getColorCodes( colorCode: ColorCodes ) -> UIColor
    {
        switch colorCode.rawValue  {
        case ColorCodes.Gray_Background.rawValue:
            return UIColor(hexString: colorCode.rawValue)
            
            
        default:
            return .clear
            
        }
        
    }
    
}


extension UIColor {
    
    static var appBlue: UIColor? {
        get{
            return UIColor(named: "AppBlue")
        }
    }
    
    static var appCyan: UIColor? {
        get{
            return UIColor(named: "AppCyan")
        }
    }
    static var appGrey: UIColor? {
        get{
            return UIColor(named: "AppGrey")
        }
    }
    
    static var appLightGrey: UIColor? {
        get{
            return UIColor(named: "AppLightGrey")
        }
    }
    
    static var appCheckBoxDisabled: UIColor? {
        get{
            return UIColor(named: "AppCheckboxDisabled")
        }
    }
    
    static var appRed: UIColor? {
        get{
            return UIColor(named: "AppRed")
        }
    }
    static var appDullGreen: UIColor? {
        get{
            return UIColor(named: "AppDullGreen")
        }
    }
    
    static var appOrange: UIColor? {
        get{
            return UIColor(named: "AppOrange")
        }
    }
    
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }

    var htmlToAttributedStringWithFont: NSAttributedString? {
            var attribStr = NSMutableAttributedString()
        guard let data = data(using: .utf8) else { return nil }
            do { //, allowLossyConversion: true
                attribStr = try NSMutableAttributedString(data: data, options:  [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)

                let textRangeForFont : NSRange = NSMakeRange(0, attribStr.length)
                attribStr.addAttributes([NSAttributedString.Key.font : UIFont(name: "Avenir", size: UIDevice.current.userInterfaceIdiom == .phone ? 14 : 20)!], range: textRangeForFont)

            } catch {
                print(error)
            }

            return attribStr
        }
}

enum AppFonts : String {
    case PoppinsLight = "Poppins-Light"
    case PoppinsRegular = "Poppins-Regular"
    case PoppinsMedium = "Poppins-Medium"
    case PoppinsSemiBold = "Poppins-SemiBold"
}

extension UILabel {
    func textDropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
    }

    static func createCustomLabel() -> UILabel {
        let label = UILabel()
        label.textDropShadow()
        return label
    }
    
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}

extension UIView {


    func rotate(degrees: CGFloat) {

        let degreesToRadians: (CGFloat) -> CGFloat = { (degrees: CGFloat) in
            return degrees / 180.0 * CGFloat.pi
        }
        self.transform =  CGAffineTransform(rotationAngle: degreesToRadians(degrees))

        // If you like to use layer you can uncomment the following line
        //layer.transform = CATransform3DMakeRotation(degreesToRadians(degrees), 0.0, 0.0, 1.0)
    }

    /// Attaches constraints to it's child view provided in parameter as `subview` and
    /// return all the constrain in tupple as`(top , bottom, leading, trailing)`
    /// - Parameters:
    ///   - subview: child view
    ///   - insets: UIEdgeInsets - default -> UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    func constrainToEdges(_ subview: UIView, insets: UIEdgeInsets =
                            UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)) ->
    (NSLayoutConstraint,
     NSLayoutConstraint,
     NSLayoutConstraint,
     NSLayoutConstraint) {

        subview.translatesAutoresizingMaskIntoConstraints = false

        let topContraint = NSLayoutConstraint(
            item: subview,
            attribute: .top,
            relatedBy: .equal,
            toItem: self,
            attribute: .top,
            multiplier: 1.0,
            constant: insets.top)

        let bottomConstraint = NSLayoutConstraint(
            item: subview,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: self,
            attribute: .bottom,
            multiplier: 1.0,
            constant: -insets.bottom)

        let leadingContraint = NSLayoutConstraint(
            item: subview,
            attribute: .leading,
            relatedBy: .equal,
            toItem: self,
            attribute: .leading,
            multiplier: 1.0,
            constant: insets.left)

        let trailingContraint = NSLayoutConstraint(
            item: subview,
            attribute: .trailing,
            relatedBy: .equal,
            toItem: self,
            attribute: .trailing,
            multiplier: 1.0,
            constant: -insets.right)

        addConstraints([
                        topContraint,
                        bottomConstraint,
                        leadingContraint,
                        trailingContraint])
        return (topContraint, bottomConstraint, leadingContraint, trailingContraint)
    }
}


extension UIView {
//    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        layer.mask = mask
//    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
          clipsToBounds = true
          layer.cornerRadius = radius
          layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat, opacity: Float) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.opacity = opacity
        print(self.frame.size.width)
        
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addRightBorderWithColor(color: UIColor, width: CGFloat, opacity: Float) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.opacity = opacity
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }

    func addBottomBorderWithColor(color: UIColor, width: CGFloat, opacity: Float) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.opacity = opacity
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addLeftBorderWithColor(color: UIColor, width: CGFloat, opacity: Float) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.opacity = opacity
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}


extension UIView {
    private static let kRotationAnimationKey = "rotationanimationkey"

    func startRotating(duration: Double = 1) {
        if layer.animation(forKey: UIView.kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")

            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float.pi * 2.0
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = Float.infinity

            layer.add(rotationAnimation, forKey: UIView.kRotationAnimationKey)
        }
    }

    func stopRotating() {
        if layer.animation(forKey: UIView.kRotationAnimationKey) != nil {
            layer.removeAnimation(forKey: UIView.kRotationAnimationKey)
        }
    }
}


extension UIImage {
    func imageWithInsets(insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let _ = UIGraphicsGetCurrentContext()
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }
}
