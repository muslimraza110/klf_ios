
import Foundation
import MBProgressHUD
/*
func showAlert( selfView:UIViewController,  title:String, message:String, okCallback:@escaping () -> Void )
{
    let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    refreshAlert.addAction(UIAlertAction(title: .ok, style: .default, handler: { (action: UIAlertAction!) in
            
            okCallback()
            
    }))
    
    selfView.present(refreshAlert, animated: true, completion: nil)
    
}
*/
// Email Vaidation Code
func validateEmail(email:String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailResult = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailResult.evaluate(with: email)
    
}


func showHideProgressHUD( progressHUD:MBProgressHUD?, show:Bool, view:UIView, alpha:CGFloat ) -> MBProgressHUD?
{
    var _defaultProgressBar:MBProgressHUD?
    
    if show {
        _defaultProgressBar = MBProgressHUD.showAdded(to: view, animated: true)
        _defaultProgressBar?.backgroundView.style = .solidColor
        _defaultProgressBar?.backgroundView.color = UIColor(hexString: "#F5F5F5")
        _defaultProgressBar?.backgroundView.alpha = alpha
        return _defaultProgressBar
    }
    else{
        
        _defaultProgressBar = progressHUD
        
        if let _defaultProgressBar = _defaultProgressBar {
            
            _defaultProgressBar.hide(animated: true)
           
        }
        
        return _defaultProgressBar
    }
}

func showHideProgressHUD( progressHUD:MBProgressHUD?, show:Bool, view:UIView ) -> MBProgressHUD?
{
    return showHideProgressHUD(progressHUD: progressHUD, show: show, view: view, alpha: 0.6)
}

func convert_DICTIONARY_to_DATA( json:NSDictionary ) -> Data?
{
    var _data:Data?
    do
    {
        let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
        let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!
        return jsonString.data(using: .utf8) ?? _data
    }
    catch
    {
        return _data
    }
}
