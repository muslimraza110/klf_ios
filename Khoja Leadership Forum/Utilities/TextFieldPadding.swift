
import Foundation
import UIKit

@IBDesignable
class TextFieldPadding: UITextField {
    @IBInspectable var leftPadding: CGFloat = 20
    var padding: UIEdgeInsets?
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        padding = UIEdgeInsets(top: 0, left:  leftPadding, bottom: 0, right: 5)
        return bounds.inset(by: padding!)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        padding = UIEdgeInsets(top: 0, left:  leftPadding, bottom: 0, right: 5)
        return bounds.inset(by: padding!)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        padding = UIEdgeInsets(top: 0, left:  leftPadding, bottom: 0, right: 5)
        return bounds.inset(by: padding!)
    }
    
    required init(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)!
           // titleLabel Required due to property of Uibutton
           let deviceWidth = UIScreen.main.bounds.width
           var value : CGFloat = 27
           if UIDevice().userInterfaceIdiom == .phone {
               value = 30
           }else{
               value = 40
           }
           let sizeOfFont = deviceWidth / value
           self.font = UIFont(name: "Avenir-Medium", size: sizeOfFont)

       }
    

}
