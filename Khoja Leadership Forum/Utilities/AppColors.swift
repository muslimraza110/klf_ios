//
//  AppColors.swift
//  Enqueue
//
//  Created by Moosa Baloch on 28/12/2020.
//

import UIKit

extension UIColor {
    /// return UIColor(named: "AppBlue")
    open class var appBlueColor: UIColor {
        return UIColor(named: "AppBlue") ?? .white
    }
    
    /// return UIColor(named: "AppRed")
    open class var appRedColor: UIColor {
        return UIColor(named: "AppRed") ?? .white
    }
    
    /// return UIColor(named: "AppOrange")
    open class var appOrangeColor: UIColor {
        return UIColor(named: "AppOrange") ?? .white
    }
    
    /// return UIColor(named: "contentBackground")
    open class var contentBackground: UIColor {
        return UIColor(named: "contentBackground") ?? .white
    }
    
    /// return UIColor(named: "textColorRegular")
    open class var textColorRegular: UIColor {
        return UIColor(named: "textColorRegular") ?? .white
    }
    
    /// return UIColor(named: "secondaryTextColorRegular")
    open class var secondaryTextColorRegular: UIColor {
        return UIColor(named: "secondaryTextColorRegular") ?? .white
    }
    
    /// return UIColor(named: "appGrey")
    open class var appGreyColor: UIColor {
        return UIColor(named: "AppGrey") ?? .white
    }
    
    /// return UIColor(named: "textColorLight")
    open class var textColorLight: UIColor {
        return UIColor(named: "textColorLight") ?? .white
    }

    /// return UIColor(named: "AppCyan")
    open class var appCyanColor: UIColor {
        return UIColor(named: "AppCyan") ?? .white
    }
    
    /// return UIColor(named: "textColorDark")
    open class var textColorDark: UIColor {
        return UIColor(named: "textColorDark") ?? .white
    }
}
