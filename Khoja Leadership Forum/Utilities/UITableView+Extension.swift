////
////  UITableView+Extension.swift
////  Khoja Leadership Forum
////
////  Created by Aijaz Ali on 3/16/21.
////  Copyright © 2021 Adeel ilyas. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//protocol Reusable {
//    static var reuseIdentifier: String { get }
//}
//
//extension Reusable {
//    static var reuseIdentifier: String {
//        return String(describing: self)
//    }
//}
//
//extension UITableViewCell: Reusable { }
//
//extension UITableView {
//    func register<T: UITableViewCell>(_ class: T.Type) {
//     //   register('class', forCellReuseIdentifier: T.reuseIdentifier)
//    //    register('class', forCellReuseIdentifier:  T.reuseIdentifier)
//    }
//
//    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
//        return dequeueReusableCell(withIdentifier: T.reuseIdentifier!, for: indexPath) as! T
//    }
//}
