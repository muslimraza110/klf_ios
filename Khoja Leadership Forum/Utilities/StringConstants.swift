//
//  String.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz ALi on 05/02/2021.
//

import Foundation

extension String {
    ///  return "Yes"
    static let yes = { return "Yes" }()

    ///  return "OK"
    static let ok = { return "OK" }()

    ///  return "No"
    static let no = { return "No" }()

    ///  return "Room Limit: "
    static let roomLimit = { return "Room Limit: " }()

    ///  return "Available Seats: "
    static let availableSeats = { return "Available Seats: " }()

    ///  return " Confirmed Speakers: "
    static let  confirmedSpeakers = { return "Confirmed Speakers: " }()

    ///  return "date"
    static let date = { return "date" }()

    ///  return "start time"
    static let startTime = { return "start time" }()

    ///  return "end time"
    static let endTime = { return "end time" }()

    ///  return "delegates"
    static let delegates = { return "delegates" }()

    ///  return "meeting notes"
    static let meetingNotes = { return "meeting notes" }()


    ///  return ","
    static let comma = { return "," }()

    ///  return "Meeting "
    static let meeting = { return "Meeting " }()

    ///  return "Host "
    static let host = { return "Host" }()

    ///  return "Accepted "
    static let accepted = { return "Accepted" }()

    ///  return "Rejected "
    static let attendeeRemoved = { return "Meeting Rejected" }()

    ///  return "Cancelled "
    static let removeAttendee = { return "Attendee Removed" }()

    ///  return "Awaiting Response "
    static let awaitingResponse = { return "Awaiting Response" }()


    ///  return "Please type in meeting details, venue and other description"
    static let  textViewPlaceHolderText = { return "Please type in meeting details, venue and other description" }()
    
    static let youAreNotRegisteredForThisEvent = { return "You are not registered in this Event. Please get yourself registered to access this section." }
}
