
import Foundation

//Constant Messages Definition
struct ConstantMessages {
    static let blankTextFiled = "This field is mandatory"
    static let invalidEmailAddress = "Email address not valid"
    static let shortPassword = "Password too short"
    static var appearingFirstTime: Bool = true
    static var appearingAfterReLogin: Bool = false
}
