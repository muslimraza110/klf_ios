//
//  ImageNames.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 12/30/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import Foundation


struct ImageNames {
    
    static let tealFacebook = "teal-facebookicon"
    static let greyFacebook = "grey-facebookicon"
    static let tealtwitter = "teal-twittericon"
    static let greytwitter = "grey-twittericon"
    static let tealLinkedIn = "teal-linkedinicon"
    static let greyLinkedIn = "grey-linkedinicon"

    
}
