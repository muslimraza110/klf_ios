//
//  Constants.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/3/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

enum Constants {

    enum MyItinerary {
        static let changeSession = "Change Session"
        static let requestMeeting = "Request Meeting"
        static let moreInfo = "More Info"
        static let selectSession = "Select Session"
        static let join = "Join"
        static let cancel = "Cancel"
        static let leave = "Leave"
        static let plenary = "plenary"
        static let breakout = "breakout"
    }

    enum User: String {
        case role
    }

    enum StoryBoard {
        static let summitEvent = "SummitEvent"
    }

    enum ViewControllers {
        static let showItineraryViewController = "ShowItineraryViewController"
        static let summitEvent_RequestMeetingPeopleDetailViewController = "SummitEvent_RequestMeetingPeopleDetailViewController"
        static let requestMeetingViewController = "RequestMeetingViewController"
    }

    enum MyItineraryTableCell {
        static let showItineraryTableViewCell = "ShowItineraryTableViewCell"
    }

    enum TableCells {
        static let delegateSelectionReuseIdentifier = "delegateSelectionReuseIdentifier"
    }

    enum Nibs {
        static let summitEvent_DelegateSelection_TableViewCell = "SummitEvent_DelegateSelection_TableViewCell"
    }

    enum APIModels {
        static let set_summit_event_breakout_session_registration = "set_summit_event_breakout_session_registration"
    }

    enum  Errors {
        static let networkError = "Something went wrong"
        static let emptyFieldError = "Please select "
    }

    enum MeetingDetails {
        static let meetings = "Meetings"
        static let requestMeeting = "Request Meeting"
    }

    enum DateFormats {
        static let dateFormatLocal = "en_US_POSIX"
        static let dateFormatForAPI = "yyyy-MM-dd"
    }

    enum Token {
        static let fcmToken = "fcm_token"
    }
}
