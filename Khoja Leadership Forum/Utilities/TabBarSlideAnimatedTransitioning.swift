//
//  TabBarSlideAnimatedTransitioning.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 10/6/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import UIKit


final class TabBarSlideAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    let direction: SlideDirection
    
    let animated: Bool
    
    
    
    init(direction: SlideDirection, animated: Bool) {
        self.direction = direction
        self.animated = animated
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toView = transitionContext.view(forKey: UITransitionContextViewKey.to), let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)
            else { return }
        
        if !animated{
            return
        }
        
        fromView.superview!.addSubview(toView)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width
        let offset = (direction == .right ? screenWidth : -screenWidth)
        toView.center = CGPoint(x: fromView.center.x + offset, y: toView.center.y)
        
        
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            // Slide the views by -offset
            fromView.center = CGPoint(x: fromView.center.x - offset, y: fromView.center.y)
            toView.center   = CGPoint(x: toView.center.x - offset, y: toView.center.y)
            
        }, completion: { finished in
            
            // Remove the old view from the super view.
            fromView.removeFromSuperview()
            
            transitionContext.completeTransition(true)
        })
        
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.30
    }
    
    
    enum SlideDirection {
        case left
        case right
    }
    
}
