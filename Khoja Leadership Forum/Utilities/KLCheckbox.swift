import UIKit


protocol KLCheckboxDelegate {
    
    func KL_afterSelect( button:UIButton )
    func KL_beforeSelect( button:UIButton )
}

protocol AccordianHeaderCheckboxDelegate {
    
    func beforeCheckboxSelect( button:UIButton, sectionIndex:Int)
    func afterCheckboxSelect( button:UIButton, sectionIndex:Int )
}



class KLCheckbox : UIButton
{
    
    var delegate: KLCheckboxDelegate?
    enum KLCheckboxTypes:Int{
        
        case tick = 1
        case cross = 2
        case line = 3
        case empty = 4
    }
    
    var allowedValues:[KLCheckboxTypes] = [KLCheckboxTypes.tick, KLCheckboxTypes.cross, KLCheckboxTypes.line, KLCheckboxTypes.empty ]
    var reset: Bool = false {
        
        didSet{
            self.defaultTag = allowedValues.first?.rawValue ?? KLCheckboxTypes.empty.rawValue
            //tapKLButton(self)
        }
    }
    
    var defaultTag: Int?  {
        didSet {
            if let defaultTag = defaultTag {
                self.tag = defaultTag
                tapKLButton(self, avoidDelegateMethods: true)
            }
           
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
        self.tag = allowedValues.first?.rawValue ?? KLCheckboxTypes.empty.rawValue
        self.addTarget(self, action: #selector(tapKLButton), for: .touchUpInside)
    }

    @objc func tapKLButton( _ sender: UIButton, avoidDelegateMethods: Bool )
    {
        
        print(avoidDelegateMethods)
        if avoidDelegateMethods {
            
        }
        else {
            delegate?.KL_beforeSelect(button: self)
        }
        
        switch sender.tag {
            case KLCheckboxTypes.empty.rawValue:
                sender.setImage(nil, for: .normal)
                sender.borderColor = .black
                break
            
            case KLCheckboxTypes.tick.rawValue:
                sender.setImage(UIImage(named: "check_tick_green"), for: .normal)
                sender.borderColor = .clear
                break
            
            case KLCheckboxTypes.cross.rawValue:
                sender.setImage(UIImage(named: "check_cross_red"), for: .normal)
                sender.borderColor = .clear
                break
            
            case KLCheckboxTypes.line.rawValue:
                sender.setImage(UIImage(named: "check_minus_black"), for: .normal)
                sender.borderColor = .clear
                break
            
            default:
                sender.setImage(nil, for: .normal)
                sender.borderColor = .black
                break
        }
        
    
        
        let value = KLCheckboxTypes.init(rawValue: sender.tag)
        let getIndex = allowedValues.firstIndex { (kl) -> Bool in
            if kl == value {
                return true
            }
            return false
        }
        
        
        
        let ifNextIndexAvailable = allowedValues.indices.contains( (getIndex ?? 0) + 1 )
        let ifZeroIndexAvailable = allowedValues.indices.contains( 0 )
        
        
        if ifNextIndexAvailable {
            
            let getKey = allowedValues[(getIndex ?? 0) + 1]
            sender.tag = getKey.rawValue
        }
        else if ifZeroIndexAvailable {
            let getKey = allowedValues[0]
            sender.tag = getKey.rawValue
        }
        
        if avoidDelegateMethods {
            
        }
        else
        {
            delegate?.KL_afterSelect(button: sender)
        }
    }

    
    

}
