
import UIKit

class NavigationLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 22
        if UIDevice().userInterfaceIdiom == .phone {
            value = 22
        }else{
            value = 29
        }
        let sizeOfFont = deviceWidth / value
        let font = UIFont(name: "Avenir-Heavy", size: sizeOfFont)
        self.font = font
        print("font value",sizeOfFont)
      
    }
}


class BlueViewLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 27
        if UIDevice().userInterfaceIdiom == .phone {
            value = 27
        }else{
            value = 34
        }
        let sizeOfFont = deviceWidth / value
        let font = UIFont(name: "Avenir-Roman", size: sizeOfFont)
        self.font = font
        print("font value",sizeOfFont)
        
    }
}

class GroupCellLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 30
        if UIDevice().userInterfaceIdiom == .phone {
            value = 30
        }else{
            value = 44
        }
        let sizeOfFont = deviceWidth / value
        let font = UIFont(name: "Avenir-Roman", size: sizeOfFont)
        self.font = font
        print("font value of cell ",sizeOfFont)
        
    }
}

class ContentLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 34
        if UIDevice().userInterfaceIdiom == .phone {
            value = 34
        }else{
            value = 41
        }
        let sizeOfFont = deviceWidth / value
        let font = UIFont(name: "Avenir-Medium", size: sizeOfFont)
        self.font = font
        print("font value",sizeOfFont)
        
    }
}

class ContentSmallLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 38
        if UIDevice().userInterfaceIdiom == .phone {
            value = 38
        }else{
            value = 45
        }
        let sizeOfFont = deviceWidth / value
        let font = UIFont(name: "Avenir-Medium", size: sizeOfFont)
        self.font = font
        print("font value",sizeOfFont)
        
    }
}


class ContentxsLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 40
        if UIDevice().userInterfaceIdiom == .phone {
            value = 40
        }else{
            value = 47
        }
        let sizeOfFont = deviceWidth / value
        let font = UIFont(name: "Avenir-Medium", size: sizeOfFont)
        self.font = font
        print("font value",sizeOfFont)
        
    }
}

class ContentLabelPost: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 40
        if UIDevice().userInterfaceIdiom == .phone {
            value = 47
        }else{
            value = 53
        }
        let sizeOfFont = deviceWidth / value
        let font = UIFont(name: "Avenir-Medium", size: sizeOfFont)
        self.font = font
        print("font value",sizeOfFont)
        
    }
}
