
import UIKit

class AdaptiveButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 26
        if UIDevice().userInterfaceIdiom == .phone {
            value = 26
        }else{
            value = 33
        }
        let sizeOfFont = deviceWidth / value
        self.titleLabel?.font = UIFont(name: "Avenir-Medium", size: sizeOfFont)
    
    }
}

class SmallButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 30
        if UIDevice().userInterfaceIdiom == .phone {
            value = 30
        }else{
            value = 37
        }
        let sizeOfFont = deviceWidth / value
        self.titleLabel?.font = UIFont(name: "Avenir-Medium", size: sizeOfFont)
        
    }
}

class ESmallButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 34
        if UIDevice().userInterfaceIdiom == .phone {
            value = 34
        }else{
            value = 41
        }
        let sizeOfFont = deviceWidth / value
        self.titleLabel?.font = UIFont(name: "Avenir-Medium", size: sizeOfFont)
        
    }
}

class AdaptiveTextField: UITextField {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 27
        if UIDevice().userInterfaceIdiom == .phone {
            value = 30
        }else{
            value = 40
        }
        let sizeOfFont = deviceWidth / value
        self.font = UIFont(name: "Avenir-Medium", size: sizeOfFont)

    }
    
}


class AdaptiveSegmentedButton: UISegmentedControl {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // titleLabel Required due to property of Uibutton
        let deviceWidth = UIScreen.main.bounds.width
        var value : CGFloat = 28.84
        if UIDevice().userInterfaceIdiom == .phone {
            value = 28.64
        }else{
            value = 35.64
        }
        let sizeOfFont = deviceWidth / value
        let font = UIFont.systemFont(ofSize: sizeOfFont)
        self.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)

    }
    
}
