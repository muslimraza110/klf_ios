//
//  CopyableLabel.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/9/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit

class CopyableLabel: UILabel {

    var customFont: UIFont? {
        didSet {
            self.setFont()
        }
    }

    var underLineText: String? {
        didSet {
            self.setUnderLine()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.sharedInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.sharedInit()
    }

    func sharedInit() {
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(self.showMenu)))
    }

    @objc func showMenu(sender: AnyObject?) {
        self.becomeFirstResponder()
        let board = UIPasteboard.general
      //  let link =  underLineText?.split(separator: " ")
        board.string = underLineText ?? "" // String(link?[1] ?? "")
        makeToast("Link copied!")
    }

    private func setFont() {
        self.font = self.customFont
    }

    func setUnderLine() {
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: underLineText ?? "", attributes: underlineAttribute)
        attributedText = underlineAttributedString
    }

    override func copy(_ sender: Any?) { }

    override var canBecomeFirstResponder: Bool {
        return true
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(UIResponderStandardEditActions.copy)
    }
}
