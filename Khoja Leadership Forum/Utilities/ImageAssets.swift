//
//  ImageAssets.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/3/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit

extension UIImage {

    /// return UIImage(named: "bluegradient_bg")
    public static var bluegradient_bg: UIImage? = {
        return UIImage(named: "bluegradient_bg")
    }()

    /// return UIImage(named: "up-arrow-white")
    public static var arrowUp: UIImage? = {
        return UIImage(named: "up-arrow-white")
    }()

    /// return UIImage(named: "up-arrow-white")
    public static var arrowDown: UIImage? = {
        return UIImage(named: "down-arrow-white")
    }()
}
