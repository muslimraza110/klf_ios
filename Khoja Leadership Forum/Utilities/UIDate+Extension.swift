//
//  UIDate+Extension.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/24/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import UIKit

extension Date {
    func dateStringWith(strFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Calendar.current.locale
        dateFormatter.dateFormat = strFormat
        return dateFormatter.string(from: self)
    }

    func dateFormatWithSuffix(dateFormat: String = "") -> String {

        return dateFormat.isEmpty ? "E, dd'\(self.daySuffix())' MMM yyyy" : dateFormat
      }

      func daySuffix() -> String {
          let calendar = Calendar.current
          let components = (calendar as NSCalendar).components(.day, from: self)
          let dayOfMonth = components.day
          switch dayOfMonth {
          case 1, 21, 31:
              return "st"
          case 2, 22:
              return "nd"
          case 3, 23:
              return "rd"
          default:
              return "th"
          }
      }
}
