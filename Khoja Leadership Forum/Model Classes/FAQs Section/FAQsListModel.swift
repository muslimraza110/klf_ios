
import Foundation
class FAQsListModel {
    var id, userID: String?
    var parentID: String?
    var lft, rght, level, title: String?
    var datumDescription, status, position, modified: String?
    var created: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.id = i["id"] as? String
            self.userID = i["user_id"] as? String
            self.parentID = i["parent_id"] as? String
            self.lft = i["lft"] as? String
            self.rght = i["rght"] as? String
            self.level = i["level"] as? String
            self.title = i["title"] as? String
            self.datumDescription = i["description"] as? String
            self.status = i["status"] as? String
            self.position = i["position"] as? String
            self.modified = i["modified"] as? String
            self.created = i["created"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

