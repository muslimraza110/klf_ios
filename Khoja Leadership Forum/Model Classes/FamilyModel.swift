//
//  FamilyModel.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 02/09/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import Foundation
class FamilyModel {
    var userFamiliesID, userFamiliesUserID, userFamiliesRelatedUserID, userFamiliesRelation: String?
    var relatedUsersID, relatedUsersRole, relatedUsersEmail, relatedUsersPassword: String?
    var relatedUsersTitle, relatedUsersFirstName, relatedUsersMiddleName, relatedUsersLastName: String?
    var relatedUsersGender, relatedUsersStatus, relatedUsersChairPerson, relatedUsersChatStatus: String?
    var relatedUsersLastOnline: String?
    var relatedUsersLastLogin: String?
    var relatedUsersExpirationDate: String?
    var relatedUsersPending, relatedUsersVoteDecision, relatedUsersVoteDecisionDate, relatedUsersVoteAdminDecision: String?
    var relatedUsersTerms, relatedUsersCreated, relatedUsersModified, relatedUsersResetKey: String?
    var profileImgID, profileImgModel, profileImgModelID, profileImgAssociation: String?
    var profileImgName, profileImgAlt, profileImgType, profileImgSize: String?
    var profileImgWidth, profileImgHeight, profileImgCropX, profileImgCropY: String?
    var profileImgCropWidth, profileImgCropHeight, profileImgCreated, profileImgModified: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.userFamiliesID = i["UserFamilies__id"] as? String
            self.userFamiliesUserID = i["UserFamilies__user_id"] as? String
            self.userFamiliesRelatedUserID = i["UserFamilies__related_user_id"] as? String
            self.userFamiliesRelation = i["UserFamilies__relation"] as? String
            self.relatedUsersID = i["RelatedUsers__id"] as? String
            self.relatedUsersRole = i["RelatedUsers__role"] as? String
            self.relatedUsersEmail = i["RelatedUsers__email"] as? String
            self.relatedUsersPassword = i["RelatedUsers__password"] as? String
            self.relatedUsersTitle = i["RelatedUsers__title"] as? String
            self.relatedUsersFirstName = i["RelatedUsers__first_name"] as? String
            self.relatedUsersMiddleName = i["RelatedUsers__middle_name"] as? String
            self.relatedUsersLastName = i["RelatedUsers__last_name"] as? String
            self.relatedUsersGender = i["RelatedUsers__gender"] as? String
            self.relatedUsersStatus = i["RelatedUsers__status"] as? String
            self.relatedUsersChairPerson = i["RelatedUsers__chair_person"] as? String
            self.relatedUsersChatStatus = i["RelatedUsers__chat_status"] as? String
            self.relatedUsersLastOnline = i["RelatedUsers__last_online"] as? String
            self.relatedUsersLastLogin = i["RelatedUsers__last_login"] as? String
            self.relatedUsersExpirationDate = i["RelatedUsers__expiration_date"] as? String
            self.relatedUsersPending = i["RelatedUsers__pending"] as? String
            self.relatedUsersVoteDecision = i["RelatedUsers__vote_decision"] as? String
            self.relatedUsersVoteDecisionDate = i["RelatedUsers__vote_decision_date"] as? String
            self.relatedUsersVoteAdminDecision = i["RelatedUsers__vote_admin_decision"] as? String
            self.relatedUsersTerms = i["RelatedUsers__terms"] as? String
            self.relatedUsersCreated = i["RelatedUsers__created"] as? String
            self.relatedUsersModified = i["RelatedUsers__modified"] as? String
            self.relatedUsersResetKey = i["RelatedUsers__reset_key"] as? String
            self.profileImgID = i["ProfileImg__id"] as? String
            self.profileImgModel = i["ProfileImg__model"] as? String
            self.profileImgModelID = i["ProfileImg__model_id"] as? String
            self.profileImgAssociation = i["ProfileImg__association"] as? String
            self.profileImgName = i["ProfileImg__name"] as? String
            self.profileImgAlt = i["ProfileImg__alt"] as? String
            self.profileImgType = i["ProfileImg__type"] as? String
            self.profileImgSize = i["ProfileImg__size"] as? String
            self.profileImgWidth = i["ProfileImg__width"] as? String
            self.profileImgHeight = i["ProfileImg__height"] as? String
            self.profileImgCropX = i["ProfileImg__crop_x"] as? String
            self.profileImgCropY = i["ProfileImg__crop_y"] as? String
            self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
            self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
            self.profileImgCreated = i["ProfileImg__created"] as? String
            self.profileImgModified = i["ProfileImg__modified"] as? String
        }
       
    }
/*-----------------------------------------------------------------------------------------------------*/
}
