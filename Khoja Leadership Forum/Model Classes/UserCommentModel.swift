//
//  UserCommentModel.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 03/09/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import Foundation
class UserCommentModel {
    var userCommentsID, userCommentsUserID, userCommentsCopywriterUserID, userCommentsComment: String?
    var userCommentsStatus, userCommentsCreated, userCommentsModified: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.userCommentsID = i["UserComments__id"] as? String
            self.userCommentsUserID = i["UserComments__user_id"] as? String
            self.userCommentsCopywriterUserID = i["UserComments__copywriter_user_id"] as? String
            self.userCommentsComment = i["UserComments__comment"] as? String
            self.userCommentsStatus = i["UserComments__status"] as? String
            self.userCommentsCreated = i["UserComments__created"] as? String
            self.userCommentsModified = i["UserComments__modified"] as? String
        }
       
    }
/*-----------------------------------------------------------------------------------------------------*/
}
