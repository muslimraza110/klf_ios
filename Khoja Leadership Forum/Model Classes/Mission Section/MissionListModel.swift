
import Foundation

class MissionListModel {
    var pagesID, pagesTitle, pagesContent, pagesCreated: String?
    var pagesModified: String?
    
    init() {
    }
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.pagesID = i["Pages__id"] as? String
            self.pagesTitle = i["Pages__title"] as? String
            self.pagesContent = i["Pages__content"] as? String
            self.pagesCreated = i["Pages__created"] as? String
            self.pagesModified = i["Pages__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

