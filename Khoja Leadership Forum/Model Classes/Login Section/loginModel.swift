
import Foundation
class LoginModel {
    var id : Int?
    var userRole : String?
    var firstName : String?
    var lastName : String?
    var profileImage : String?
    
    init() {}
/*-----------------------------------------------------------------------------------------------------*/
    init(data : NSDictionary) {
        id = data["id"] as? Int
        userRole = data["role"] as? String
        firstName = data["first_name"] as? String
        lastName = data["last_name"] as? String
        profileImage = data["profile_img"] as? String
    }
/*-----------------------------------------------------------------------------------------------------*/
}
