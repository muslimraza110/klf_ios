//
//  UserInfoModel.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 02/09/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import Foundation
class UserinfoModel {
    var usersID, usersRole, usersEmail, usersTitle: String?
    var usersFirstName, usersMiddleName, usersLastName, usersGender: String?
    var usersStatus, usersChairPerson, usersChatStatus: String?
    var usersLastOnline: String?
    var usersLastLogin: String?
    var usersExpirationDate: String?
    var usersPending, usersVoteDecision, usersVoteDecisionDate, usersVoteAdminDecision: String?
    var usersTerms, usersCreated, usersModified, usersResetKey: String?
    var userDetailsID, userDetailsUserID, userDetailsTitle, userDetailsDescription: String?
    var userDetailsBio, userDetailsPrivateBio, userDetailsAddress, userDetailsAddress2: String?
    var userDetailsCity, userDetailsRegion, userDetailsCountry, userDetailsPostalCode: String?
    var userDetailsEmail2, userDetailsWebsite, userDetailsFacebook, userDetailsLinkedin: String?
    var userDetailsPhone, userDetailsPhone2, userDetailsCreated, userDetailsModified: String?
    var userDetailsStatus: String?
    var profileImgID, profileImgModel, profileImgModelID, profileImgAssociation: String?
    var profileImgName, profileImgAlt, profileImgType, profileImgSize: String?
    var profileImgWidth, profileImgHeight, profileImgCropX, profileImgCropY: String?
    var profileImgCropWidth, profileImgCropHeight, profileImgCreated, profileImgModified: String?
    
    init() {
    }
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.usersID = i["Users__id"] as? String
            self.usersRole = i["Users__role"] as? String
            self.usersEmail = i["Users__email"] as? String
            self.usersTitle = i["Users__title"] as? String
            self.usersFirstName = i["Users__first_name"] as? String
            self.usersMiddleName = i["Users__middle_name"] as? String
            self.usersLastName = i["Users__last_name"] as? String
            self.usersGender = i["Users__gender"] as? String
            self.usersStatus = i["Users__status"] as? String
            self.usersChairPerson = i["Users__chair_person"] as? String
            self.usersChatStatus = i["Users__chat_status"] as? String
            self.usersLastOnline = i["Users__last_online"] as? String
            self.usersLastLogin = i["Users__last_login"] as? String
            self.usersExpirationDate = i["Users__expiration_date"] as? String
            self.usersPending = i["Users__pending"] as? String
            self.usersVoteDecision = i["Users__vote_decision"] as? String
            self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.usersTerms = i["Users__terms"] as? String
            self.usersCreated = i["Users__created"] as? String
            self.usersModified = i["Users__modified"] as? String
            self.usersResetKey = i["Users__reset_key"] as? String
            self.userDetailsID = i["UserDetails__id"] as? String
            self.userDetailsUserID = i["UserDetails__user_id"] as? String
            self.userDetailsTitle = i["UserDetails__title"] as? String
            self.userDetailsDescription = i["UserDetails__description"] as? String
            self.userDetailsBio = i["UserDetails__bio"] as? String
            self.userDetailsPrivateBio = i["UserDetails__private_bio"] as? String
            self.userDetailsAddress = i["UserDetails__address"] as? String
            self.userDetailsAddress2 = i["UserDetails__address2"] as? String
            self.userDetailsCity = i["UserDetails__city"] as? String
            self.userDetailsRegion = i["UserDetails__region"] as? String
            self.userDetailsCountry = i["UserDetails__country"] as? String
            self.userDetailsPostalCode = i["UserDetails__postal_code"] as? String
            self.userDetailsEmail2 = i["UserDetails__email2"] as? String
            self.userDetailsWebsite = i["UserDetails__website"] as? String
            self.userDetailsFacebook = i["UserDetails__facebook"] as? String
            self.userDetailsLinkedin = i["UserDetails__linkedin"] as? String
            self.userDetailsPhone = i["UserDetails__phone"] as? String
            self.userDetailsPhone2 = i["UserDetails__phone2"] as? String
            self.userDetailsCreated = i["UserDetails__created"] as? String
            self.userDetailsModified = i["UserDetails__modified"] as? String
            self.userDetailsStatus = i["UserDetails__status"] as? String
            self.profileImgID = i["ProfileImg__id"] as? String
            self.profileImgModel = i["ProfileImg__model"] as? String
            self.profileImgModelID = i["ProfileImg__model_id"] as? String
            self.profileImgAssociation = i["ProfileImg__association"] as? String
            self.profileImgName = i["ProfileImg__name"] as? String
            self.profileImgAlt = i["ProfileImg__alt"] as? String
            self.profileImgType = i["ProfileImg__type"] as? String
            self.profileImgSize = i["ProfileImg__size"] as? String
            self.profileImgWidth = i["ProfileImg__width"] as? String
            self.profileImgHeight = i["ProfileImg__height"] as? String
            self.profileImgCropX = i["ProfileImg__crop_x"] as? String
            self.profileImgCropY = i["ProfileImg__crop_y"] as? String
            self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
            self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
            self.profileImgCreated = i["ProfileImg__created"] as? String
            self.profileImgModified = i["ProfileImg__modified"] as? String
        }
       
    }
}
/*-----------------------------------------------------------------------------------------------------*/
