
import Foundation
class ListOfGroupMemberModel{
    var groupsUsersUserID, groupsUsersID, groupsUsersGroupID: String?
    var groupsUsersUserAlias: String?
    var groupsUsersAdmin, usersID, usersRole, usersEmail: String?
    var usersPassword, usersTitle, usersFirstName, usersMiddleName: String?
    var usersLastName, usersGender, usersStatus, usersChairPerson: String?
    var usersChatStatus: String?
    var usersLastOnline: String?
    var usersLastLogin: String?
    var usersExpirationDate: String?
    var usersPending, usersVoteDecision, usersVoteDecisionDate, usersVoteAdminDecision: String?
    var usersTerms, usersCreated, usersModified, usersResetKey: String?
    var profileImgID, profileImgModel, profileImgModelID, profileImgAssociation: String?
    var profileImgName, profileImgAlt, profileImgType, profileImgSize: String?
    var profileImgWidth, profileImgHeight, profileImgCropX, profileImgCropY: String?
    var profileImgCropWidth, profileImgCropHeight, profileImgCreated, profileImgModified: String?
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
        self.groupsUsersUserID = i["GroupsUsers__user_id"] as? String
        self.groupsUsersID = i["GroupsUsers__id"] as? String
        self.groupsUsersGroupID = i["GroupsUsers__group_id"] as? String
        self.groupsUsersUserAlias = i["GroupsUsers__user_alias"] as? String
        self.groupsUsersAdmin = i["GroupsUsers__admin"] as? String
        self.usersID = i["Users__id"] as? String
        self.usersRole = i["Users__role"] as? String
        self.usersEmail = i["Users__email"] as? String
        self.usersPassword = i["Users__password"] as? String
        self.usersTitle = i["Users__title"] as? String
        self.usersFirstName = i["Users__first_name"] as? String
        self.usersMiddleName = i["Users__middle_name"] as? String
        self.usersLastName = i["Users__last_name"] as? String
        self.usersGender = i["Users__gender"] as? String
        self.usersStatus = i["Users__status"] as? String
        self.usersChairPerson = i["Users__chair_person"] as? String
        self.usersChatStatus = i["Users__chat_status"] as? String
        self.usersLastOnline = i["Users__last_online"] as? String
        self.usersLastLogin = i["Users__last_login"] as? String
        self.usersExpirationDate = i["Users__expiration_date"] as? String
        self.usersPending = i["Users__pending"] as? String
        self.usersVoteDecision = i["Users__vote_decision"] as? String
        self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
        self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
        self.usersTerms = i["Users__terms"] as? String
        self.usersCreated = i["Users__created"] as? String
        self.usersModified = i["Users__modified"] as? String
        self.usersResetKey = i["Users__reset_key"] as? String
        self.profileImgID = i["ProfileImg__id"] as? String
        self.profileImgModel = i["ProfileImg__model"] as? String
        self.profileImgModelID = i["ProfileImg__model_id"] as? String
        self.profileImgAssociation = i["ProfileImg__association"] as? String
        self.profileImgName = i["ProfileImg__name"] as? String
        self.profileImgAlt = i["ProfileImg__alt"] as? String
        self.profileImgType = i["ProfileImg__type"] as? String
        self.profileImgSize = i["ProfileImg__size"] as? String
        self.profileImgWidth = i["ProfileImg__width"] as? String
        self.profileImgHeight = i["ProfileImg__height"] as? String
        self.profileImgCropX = i["ProfileImg__crop_x"] as? String
        self.profileImgCropY = i["ProfileImg__crop_y"] as? String
        self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
        self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
        self.profileImgCreated = i["ProfileImg__created"] as? String
        self.profileImgModified = i["ProfileImg__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
