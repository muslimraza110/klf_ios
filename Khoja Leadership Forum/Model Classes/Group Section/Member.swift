
import Foundation
import UIKit

class Member {
    
    var createdByUserID: String?
    var memberID: String?
    var memberName: String?
    var createdBy: String?
    var city: String?
/*-----------------------------------------------------------------------------------------------------*/
    //Initialization
    init(memberDictionary: Dictionary<String, String>) {
        
        self.createdByUserID = memberDictionary["createdByUserID"]
        self.memberID = memberDictionary["memberID"]
        self.memberName = memberDictionary["memberName"]
        self.createdBy = memberDictionary["createdBy"]
        self.city = memberDictionary["City"]

        
    }
/*-----------------------------------------------------------------------------------------------------*/
}
