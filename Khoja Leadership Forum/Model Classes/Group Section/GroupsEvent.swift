
import Foundation
class GroupsEventModel {
    
    var id, groupID, projectID, userID: String?
    var name, startDate, endDate, allDay: String?
    var description, city, country, created: String?
    var modified: String?
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
        self.id = i["id"] as? String
        self.groupID = i["group_id"] as? String
        self.projectID = i["project_id"] as? String
        self.userID = i["user_id"] as? String
        self.name = i["name"] as? String
        self.startDate = i["start_date"] as? String
        self.endDate = i["end_date"] as? String
        self.allDay = i["all_day"] as? String
        self.description = i["description"] as? String
        self.city = i["city"] as? String
        self.country = i["country"] as? String
        self.created = i["created"] as? String
        self.modified = i["modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
