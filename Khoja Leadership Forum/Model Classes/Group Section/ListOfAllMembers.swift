
import Foundation
// MARK: - Datum
class listOfAllMemberModel {
    
    var role, title, firstName, middleName: String?
    var lastName, gender, chairPerson, id: String?
    var email, groupID, profileImage: String?
    var ismember: Int?
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
        self.role = i["role"] as? String
        self.title = i["title"] as? String
        self.firstName = i["first_name"] as? String
        self.middleName = i["middle_name"] as? String
        self.lastName = i["last_name"] as? String
        self.gender = i["gender"] as? String
        self.chairPerson = i["chair_person"] as? String
        self.id = i["id"] as? String
        self.email = i["email"] as? String
        self.groupID = i["group_id"] as? String
        self.ismember = i["Ismember"] as? Int
        self.profileImage = i["profile_image"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
