
import Foundation
class GroupProjectListModel {
    
    var projectsGroupsProjectID, projectsGroupsID, projectsGroupsGroupID, projectsGroupsPercent: String?
    var projectsID, projectsCreatorID, projectsKhojaCareCategoryID, projectsName: String?
    var projectsWebsite, projectsDescription, projectsStartDate, projectsEstCompletionDate: String?
    var projectsFundraisingTarget: String?
    var projectsCity: String?
    var projectsRegion: String?
    var projectsCountry, projectsApprovalStamp, projectsStatus, projectsCreated: String?
    var projectsModified: String?
    
    init() {
        
    }
/*-----------------------------------------------------------------------------------------------------*/
    init(data:NSDictionary) {
       
            self.projectsGroupsProjectID = data["ProjectsGroups__project_id"] as? String
            self.projectsGroupsID = data["ProjectsGroups__id"] as? String
            self.projectsGroupsGroupID = data["ProjectsGroups__group_id"] as? String
            self.projectsGroupsPercent = data["ProjectsGroups__percent"] as? String
            self.projectsID = data["Projects__id"] as? String
            self.projectsCreatorID = data["Projects__creator_id"] as? String
            self.projectsKhojaCareCategoryID = data["Projects__khoja_care_category_id"] as? String
            self.projectsName = data["Projects__name"] as? String
            self.projectsWebsite = data["Projects__website"] as? String
            self.projectsDescription = data["Projects__description"] as? String
            self.projectsStartDate = data["Projects__start_date"] as? String
            self.projectsEstCompletionDate = data["Projects__est_completion_date"] as? String
            self.projectsFundraisingTarget = data["Projects__fundraising_target"] as? String
            self.projectsCity = data["Projects__city"] as? String
            self.projectsRegion = data["Projects__region"] as? String
            self.projectsCountry = data["Projects__country"] as? String
            self.projectsApprovalStamp = data["Projects__approval_stamp"] as? String
            self.projectsStatus = data["Projects__status"] as? String
            self.projectsCreated = data["Projects__created"] as? String
            self.projectsModified = data["Projects__modified"] as? String
    }
/*-----------------------------------------------------------------------------------------------------*/
}



class GroupProjectListMemberModel {
    
    var projectName, memberName, memberId, profileImgName : String?
    
    init(data:[[String:Any]]) {
        for i in data {
            projectName = i["Project__name"] as? String
            memberName = i["Member__name"] as? String
            memberId = i["Member_id"] as? String
            profileImgName = i["ProfileImg__name"] as? String
            
        }
    }
    
}



class CombineGroupProjectMemberModel {
    
    var project = GroupProjectListModel()
    var projectMemberModel = [GroupProjectListMemberModel]()
    
    init(data:[[String:Any]]) {
        for i in data {
            if let projectData = i["project"] as? NSDictionary {
                project = GroupProjectListModel(data: projectData)
            }
            if let projectMember = i["members"] as? [[String:Any]] {
                for i in projectMember {
                    projectMemberModel.append(GroupProjectListMemberModel(data: [i]))
                }
            }
            
        }
    }
    
}
