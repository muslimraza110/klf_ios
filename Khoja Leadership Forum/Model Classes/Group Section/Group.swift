
import Foundation
import UIKit

class GroupModel {
        var createdByUserID, groupsID, groupsCategoryID, groupsName: String?
        var groupsDescription, groupsEmail, groupsPhone, groupsAddress: String?
        var groupsAddress2, groupsWebsite, groupsCity, groupsRegion: String?
        var groupsCountry, groupsPostalCode, groupsBio, groupsHidden: String?
        var groupsPending, groupsVoteDecision, groupsVoteDecisionDate, groupsCreated: String?
        var groupsStatus, groupsDonations, groupsPassive: String?
        var profileImgID, profileImgModel, profileImgModelID, profileImgAssociation: String?
        var profileImgName, profileImgAlt, profileImgType, profileImgSize: String?
        var profileImgWidth, profileImgHeight, profileImgCropX, profileImgCropY: String?
        var profileImgCropWidth, profileImgCropHeight, profileImgCreated, profileImgModified: String?
        var categoriesID, categoriesModel, categoriesName, categoriesDescription, groupImage: String?
    
    init() {}
/*-----------------------------------------------------------------------------------------------------*/
    
    init(data:[[String:Any]]) {
        for i in data {
            self.createdByUserID = i["createdByUserID"] as? String
            self.groupsID = i["Groups__id"] as? String
            self.groupsCategoryID = i["Groups__category_id"] as? String
            self.groupsName = i["Groups__name"] as? String
            self.groupsDescription = i["Groups__description"] as? String
            self.groupsEmail = i["Groups__email"] as? String
            self.groupsPhone = i["Groups__phone"] as? String
            self.groupsAddress = i["Groups__address"] as? String
            self.groupsAddress2 = i["Groups__address2"] as? String
            self.groupsWebsite = i["Groups__website"] as? String
            self.groupsCity = i["Groups__city"] as? String
            self.groupsRegion = i["Groups__region"] as? String
            self.groupsCountry = i["Groups__country"] as? String
            self.groupsPostalCode = i["Groups__postal_code"] as? String
            self.groupsBio = i["Groups__bio"] as? String
            self.groupsHidden = i["Groups__hidden"] as? String
            self.groupsPending = i["Groups__pending"] as? String
            self.groupsVoteDecision = i["Groups__vote_decision"] as? String
            self.groupsVoteDecisionDate = i["Groups__vote_decision_date"] as? String
            self.groupsCreated = i["Groups__created"] as? String
            self.groupsStatus = i["Groups__status"] as? String
            self.groupsDonations = i["Groups__donations"] as? String
            self.groupsPassive = i["Groups__passive"] as? String
            self.profileImgID = i["ProfileImg__id"] as? String
            self.profileImgModel = i["ProfileImg__model"] as? String
            self.profileImgModelID = i["ProfileImg__model_id"] as? String
            self.profileImgAssociation = i["ProfileImg__association"] as? String
            self.profileImgName = i["ProfileImg__name"] as? String
            self.profileImgAlt = i["ProfileImg__alt"] as? String
            self.profileImgType = i["ProfileImg__type"] as? String
            self.profileImgSize = i["ProfileImg__size"] as? String
            self.profileImgWidth = i["ProfileImg__width"] as? String
            self.profileImgHeight = i["ProfileImg__height"] as? String
            self.profileImgCropX = i["ProfileImg__crop_x"] as? String
            self.profileImgCropY = i["ProfileImg__crop_y"] as? String
            self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
            self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
            self.profileImgCreated = i["ProfileImg__created"] as? String
            self.profileImgModified = i["ProfileImg__modified"] as? String
            self.categoriesID = i["Categories__id"] as? String
            self.categoriesModel = i["Categories__model"] as? String
            self.categoriesName = i["Categories__name"] as? String
            self.categoriesDescription = i["Categories__description"] as? String
            self.groupImage = i["groupimage"] as? String
        }
        
        }
/*-----------------------------------------------------------------------------------------------------*/
    }



