
import Foundation

class GroupForumPostModel {
    
    var forumPostsGroupsForumPostID, forumPostsGroupsID, forumPostsGroupsGroupID, forumPostsID: String?
    var forumPostsUserID: String?
    var forumPostsParentID: String?
    var forumPostsSubforumID, forumPostsLft, forumPostsRght, forumPostsTopic: String?
    var forumPostsContent, forumPostsPending, forumPostsModerated, forumPostsPinned: String?
    var forumPostsSod, forumPostsNotifyUsers: String?
    var forumPostsVisibility: String?
    var forumPostsPostVisibility, forumPostsStatus, forumPostsEblast, forumPostsCreated: String?
    var forumPostsModified, usersID, usersRole, usersEmail: String?
    var usersPassword, usersTitle, usersFirstName, usersMiddleName: String?
    var usersLastName, usersGender, usersStatus, usersChairPerson: String?
    var usersChatStatus: String?
    var usersLastOnline: String?
    var usersLastLogin: String?
    var usersExpirationDate: String?
    var usersPending, usersVoteDecision, usersVoteDecisionDate, usersVoteAdminDecision: String?
    var usersTerms, usersCreated, usersModified, usersResetKey: String?
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.forumPostsGroupsForumPostID = i["ForumPostsGroups__forum_post_id"] as? String
            self.forumPostsGroupsID = i["ForumPostsGroups__id"] as? String
            self.forumPostsGroupsGroupID = i["ForumPostsGroups__group_id"] as? String
            self.forumPostsID = i["ForumPosts__id"] as? String
            self.forumPostsUserID = i["ForumPosts__user_id"] as? String
            self.forumPostsParentID = i["ForumPosts__parent_id"] as? String
            self.forumPostsSubforumID = i["ForumPosts__subforum_id"] as? String
            self.forumPostsLft = i["ForumPosts__lft"] as? String
            self.forumPostsRght = i["ForumPosts__rght"] as? String
            self.forumPostsTopic = i["ForumPosts__topic"] as? String
            self.forumPostsContent = i["ForumPosts__content"] as? String
            self.forumPostsPending = i["ForumPosts__pending"] as? String
            self.forumPostsModerated = i["ForumPosts__moderated"] as? String
            self.forumPostsPinned = i["ForumPosts__pinned"] as? String
            self.forumPostsSod = i["ForumPosts__sod"] as? String
            self.forumPostsNotifyUsers = i["ForumPosts__notify_users"] as? String
            self.forumPostsVisibility = i["ForumPosts__visibility"] as? String
            self.forumPostsPostVisibility = i["ForumPosts__post_visibility"] as? String
            self.forumPostsStatus = i["ForumPosts__status"] as? String
            self.forumPostsEblast = i["ForumPosts__eblast"] as? String
            self.forumPostsCreated = i["ForumPosts__created"] as? String
            self.forumPostsModified = i["ForumPosts__modified"] as? String
            self.usersID = i["Users__id"] as? String
            self.usersRole = i["Users__role"] as? String
            self.usersEmail = i["Users__email"] as? String
            self.usersPassword = i["Users__password"] as? String
            self.usersTitle = i["Users__title"] as? String
            self.usersFirstName = i["Users__first_name"] as? String
            self.usersMiddleName = i["Users__middle_name"] as? String
            self.usersLastName = i["Users__last_name"] as? String
            self.usersGender = i["Users__gender"] as? String
            self.usersStatus = i["Users__status"] as? String
            self.usersChairPerson = i["Users__chair_person"] as? String
            self.usersChatStatus = i["Users__chat_status"] as? String
            self.usersLastOnline = i["Users__last_online"] as? String
            self.usersLastLogin = i["Users__last_login"] as? String
            self.usersExpirationDate = i["Users__expiration_date"] as? String
            self.usersPending = i["Users__pending"] as? String
            self.usersVoteDecision = i["Users__vote_decision"] as? String
            self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.usersTerms = i["Users__terms"] as? String
            self.usersCreated = i["Users__created"] as? String
            self.usersModified = i["Users__modified"] as? String
            self.usersResetKey = i["Users__reset_key"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
