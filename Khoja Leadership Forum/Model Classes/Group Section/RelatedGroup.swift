
import Foundation
class RelatedGroupModel {
    
   var GroupsUsers__group_id,
    GroupsUsers__id,
    GroupsUsers__user_id,
    GroupsUsers__user_alias,
    GroupsUsers__admin,
    Groups__id,
    Groups__category_id,
    Groups__name,
    Groups__description,
    Groups__email,
    Groups__phone,
    Groups__address,
    Groups__address2,
    Groups__website,
    Groups__city,
    Groups__region,
    Groups__country,
    Groups__postal_code,
    Groups__bio,
    Groups__hidden,
    Groups__pending,
    Groups__vote_decision,
    Groups__vote_decision_date,
    Groups__created,
    Groups__status,
    Groups__donations,
    Groups__passive,
    Groups__image: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
          GroupsUsers__group_id = i["GroupsUsers__group_id"] as? String
          GroupsUsers__id = i["GroupsUsers__id"] as? String
          GroupsUsers__user_id = i["GroupsUsers__user_id"] as? String
          GroupsUsers__user_alias = i["GroupsUsers__user_alias"] as? String
          GroupsUsers__admin = i["GroupsUsers__admin"] as? String
          Groups__id = i["id"] as? String
          Groups__category_id = i["category_id"] as? String
          Groups__name = i["name"] as? String
          Groups__description = i["description"] as? String
          Groups__email  = i["email"] as? String
          Groups__phone = i["phone"] as? String
          Groups__address = i["address"] as? String
          Groups__address2 = i["address2"] as? String
          Groups__website = i["website"] as? String
          Groups__city = i["city"] as? String
          Groups__region = i["region"] as? String
          Groups__country = i["country"] as? String
          Groups__postal_code = i["postal_code"] as? String
          Groups__bio = i["bio"] as? String
          Groups__hidden = i["hidden"] as? String
          Groups__pending = i["pending"] as? String
          Groups__vote_decision = i["vote_decision"] as? String
          Groups__vote_decision_date = i["vote_decision_date"] as? String
          Groups__created = i["created"] as? String
          Groups__status = i["status"] as? String
          Groups__donations = i["donations"] as? String
          Groups__passive = i["passive"] as? String
          Groups__image = i["group_image"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
