//
//  UserPledgeInfo.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 11/10/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import Foundation
class UserPledgeInfo {
    var pledgeName, pledgeAmount, pledgeInitiativeId : String?
    var isDeleted : Int?
    
    init(data:[[String:Any]]) {
        for i in data {
            pledgeName = i["Pledge__name"] as? String
            pledgeAmount = i["Pledge__amount"] as? String
            pledgeInitiativeId = i["Pledge__initiative_id"] as? String
            isDeleted = i["is_deleted"] as? Int
        }
        
    }
}
