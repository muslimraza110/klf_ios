//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventPeopleDetailModel: Codable, NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = SummitEventPeopleDetailModel(id: id, name: name, designation: designation, short_description: short_description, long_description: long_description, facebook_link: facebook_link, twitter_link: twitter_link, user_image: user_image, linkedin_link: linkedin_link, is_speaker: is_speaker, is_delegate: is_delegate, meeting_slots: meeting_slots, speech_time: speech_time, speech_date: speech_date, summit_events_faculty_id: summit_events_faculty_id, isSelected: isSelected)
        return copy
    }


    enum CodingKeys: String, CodingKey {
        
        case id
        case name
        case designation
        case short_description
        case long_description
        case facebook_link
        case twitter_link
        case user_image
        case linkedin_link
        case is_speaker
        case is_delegate
        case meeting_slots
        case speech_time = "summit_event_program_time"
        case speech_date = "summit_event_program_date"
        case summit_events_faculty_id
    }
    
    
    var id, name, designation, short_description, long_description, facebook_link, twitter_link, user_image, linkedin_link, is_speaker, is_delegate, speech_time, speech_date, summit_events_faculty_id: String?
    var isSelected: Bool = false
    var meeting_slots: [SummitEventDayAvailabilityModel]?
    
    
    
    
    init ( id:String?, name:String?, designation:String?, short_description:String?, long_description:String?, facebook_link:String?, twitter_link:String?, user_image:String?, linkedin_link:String?, is_speaker:String?, is_delegate:String?, meeting_slots: [SummitEventDayAvailabilityModel]?, speech_time: String?, speech_date: String?, summit_events_faculty_id: String?, isSelected: Bool = false) {
        
        self.id = id
        self.name = name
        self.designation = designation
        self.short_description = short_description
        self.long_description = long_description
        self.facebook_link = facebook_link
        self.twitter_link = twitter_link
        self.user_image = user_image
        self.linkedin_link = linkedin_link
        self.is_speaker = is_speaker
        self.is_delegate = is_delegate
        self.meeting_slots = meeting_slots
        self.speech_time = speech_time
        self.speech_date = speech_date
        self.summit_events_faculty_id = summit_events_faculty_id
        self.isSelected = isSelected
    }
    
     required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decodeIfPresent(String.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        designation = try container.decodeIfPresent(String.self, forKey: .designation)
        short_description = try container.decodeIfPresent(String.self, forKey: .short_description)
        long_description = try container.decodeIfPresent(String.self, forKey: .long_description)
        facebook_link = try container.decodeIfPresent(String.self, forKey: .facebook_link)
        twitter_link = try container.decodeIfPresent(String.self, forKey: .twitter_link)
        user_image = try container.decodeIfPresent(String.self, forKey: .user_image)
        linkedin_link = try container.decodeIfPresent(String.self, forKey: .linkedin_link)
        is_speaker = try container.decodeIfPresent(String.self, forKey: .is_speaker)
        is_delegate = try container.decodeIfPresent(String.self, forKey: .is_delegate)
        meeting_slots = try container.decodeIfPresent([SummitEventDayAvailabilityModel].self, forKey: .meeting_slots)
        speech_time = try container.decodeIfPresent(String.self, forKey: .speech_time)
        speech_date = try container.decodeIfPresent(String.self, forKey: .speech_date)
        summit_events_faculty_id = try container.decodeIfPresent(String.self, forKey: .summit_events_faculty_id)
    }
    
}


class SummitEventDayAvailabilityModel: Codable {
    
    enum CodingKeys: String, CodingKey {
    
        case program_date
        case program_day
        case slots
    
    }
    

    var program_day: String?
    var program_date: String?
    var slots: [SummitEventTimeAvailabilityModel]?
    
    init(program_day: String?, program_date: String?, slots: [SummitEventTimeAvailabilityModel]?) {
        self.program_day = program_day
        self.program_date = program_date
        self.slots = slots
    }
    
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
        program_day = try container.decodeIfPresent(String.self, forKey: .program_day)
        program_date = try container.decodeIfPresent(String.self, forKey: .program_date)
        slots = try container.decodeIfPresent([SummitEventTimeAvailabilityModel].self, forKey: .slots)

          
    }
    
}


class SummitEventTimeAvailabilityModel: Codable {
    
    
    enum CodingKeys: String, CodingKey {
    
        case parentid
        case day
        case start_time
        case end_time
        case current_status
        case time = "times"
        case program_id
    
    }
    
    
    
    var parentid, day, start_time, end_time, current_status, time, program_id : String?
    
    init(parentid: String?,day: String?,start_time: String?,end_time: String?, current_status: String?,time: String?, program_id: String?) {
        self.parentid = parentid
        self.day = day
        self.start_time = start_time
        self.end_time = end_time
        self.current_status = current_status
        self.time = time
        self.program_id = program_id
    }
    
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
        parentid = try container.decodeIfPresent(String.self, forKey: .parentid)
        day = try container.decodeIfPresent(String.self, forKey: .day)
        start_time = try container.decodeIfPresent(String.self, forKey: .start_time)
        end_time = try container.decodeIfPresent(String.self, forKey: .end_time)
        current_status = try container.decodeIfPresent(String.self, forKey: .current_status)
        time = try container.decodeIfPresent(String.self, forKey: .time)
        program_id = try container.decodeIfPresent(String.self, forKey: .program_id)
          
    }
}



