//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventPeopleDetailBaseModel: Codable {

    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data = "data"
    }

    var status, message: String?
    var data: SummitEventPeopleDetailModel?


    
    init (status:String?, message:String?, data:SummitEventPeopleDetailModel?) {
        self.status = status
        self.message = message
        self.data = data
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    status = try container.decodeIfPresent(String.self, forKey: .status)
    message = try container.decodeIfPresent(String.self, forKey: .message)
    data = try container.decodeIfPresent(SummitEventPeopleDetailModel.self, forKey: .data)
    
    
  }

}
