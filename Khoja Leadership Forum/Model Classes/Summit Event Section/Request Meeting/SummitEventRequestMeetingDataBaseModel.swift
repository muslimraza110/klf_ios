//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventRequestMeetingDataBaseModel: Codable {

    enum CodingKeys: String, CodingKey {
        case eventDetails
        case dayList
        case peopleList
    }

    var eventDetails: SummitEventModel?
    var dayList: [SummitEventRequestMeetingDaysModel]?
    var peopleList: [SummitEventRequestMeetingPeoplesModel]?


    
    init (eventDetails:SummitEventModel?, dayList:[SummitEventRequestMeetingDaysModel]?, peopleList:[SummitEventRequestMeetingPeoplesModel]?) {
        self.eventDetails = eventDetails
        self.dayList = dayList
        self.peopleList = peopleList
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    eventDetails = try container.decodeIfPresent(SummitEventModel.self, forKey: .eventDetails)
    dayList = try container.decodeIfPresent([SummitEventRequestMeetingDaysModel].self, forKey: .dayList)
    peopleList = try container.decodeIfPresent([SummitEventRequestMeetingPeoplesModel].self, forKey: .peopleList)
    
    
  }

}
