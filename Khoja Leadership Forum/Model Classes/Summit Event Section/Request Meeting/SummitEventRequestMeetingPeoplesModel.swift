//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventRequestMeetingPeoplesModel: Codable {

    enum CodingKeys: String, CodingKey {

        case recently_added
        case all_country
        case all_industry
        case all_users_by_type

    }

  
//    var sortBy: String?
//    var selected:Bool?
//    var peopleList: [SummitEventPeopleDetailModel]?
    var recently_added: [SummitEventPeopleDetailModel]?
    var all_country: [SummitEventPeopleDetailModel]?
    var all_industry: [SummitEventPeopleDetailModel]?
    var all_users_by_type: [SummitEventPeopleDetailModel]?


    init(recently_added: [SummitEventPeopleDetailModel]?, all_country: [SummitEventPeopleDetailModel]?, all_industry: [SummitEventPeopleDetailModel]?, all_users_by_type: [SummitEventPeopleDetailModel]?) {
        self.recently_added = recently_added
        self.all_industry = all_industry
        self.all_country = all_country
        self.all_users_by_type = all_users_by_type
    }
    
//    init (sortBy:String?, selected:Bool?, peopleList: [SummitEventPeopleDetailModel]?) {

//        self.sortBy = sortBy
//        self.selected = selected
//        self.peopleList = peopleList
//    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    
//    sortBy = try container.decodeIfPresent(String.self, forKey: .sortBy)
//    selected = try container.decodeIfPresent(Bool.self, forKey: .selected)
//    peopleList = try container.decodeIfPresent([SummitEventPeopleDetailModel].self, forKey: .peopleList)
    
    recently_added = try container.decodeIfPresent([SummitEventPeopleDetailModel].self, forKey: .recently_added)
    all_country = try container.decodeIfPresent([SummitEventPeopleDetailModel].self, forKey: .all_country)
    all_industry = try container.decodeIfPresent([SummitEventPeopleDetailModel].self, forKey: .all_industry)
    all_users_by_type = try container.decodeIfPresent([SummitEventPeopleDetailModel].self, forKey: .all_users_by_type)
        
  }

}
