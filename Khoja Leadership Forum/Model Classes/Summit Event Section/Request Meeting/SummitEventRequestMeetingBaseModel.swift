//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventRequestMeetingBaseModel: Codable {

    enum CodingKeys: String, CodingKey {
        case status
        case message
        case listingData = "data"
    }

    var status, message: String?
    var listingData: SummitEventRequestMeetingDataBaseModel?


    
    init (status:String?, message:String?, listingData:SummitEventRequestMeetingDataBaseModel?) {
        self.status = status
        self.message = message
        self.listingData = listingData
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    status = try container.decodeIfPresent(String.self, forKey: .status)
    message = try container.decodeIfPresent(String.self, forKey: .message)
    listingData = try container.decodeIfPresent(SummitEventRequestMeetingDataBaseModel.self, forKey: .listingData)
    
    
  }

}
