//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventRequestMeetingDaysModel: Codable {

    enum CodingKeys: String, CodingKey {

        case day
        case time
    }

  
    var day: String?
    var time: [String]?
    
    
    init ( day:String?, time:[String]?) {

        self.day = day
        self.time = time
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    day = try container.decodeIfPresent(String.self, forKey: .day)
    time = try container.decodeIfPresent([String].self, forKey: .time)
    
        
  }

}
