//
//  CheckAvailabilityModel.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 9/14/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import Foundation

//
//class CheckAvailabilityModel: NSObject {
//
//    var dayId: String?
//    var checkAvailabilityItems: [CheckAvailabilityItemModel]
//
//    init(dayId: String?, checkAvailabilityItems: [CheckAvailabilityItemModel]) {
//        self.dayId = dayId
//        self.checkAvailabilityItems = checkAvailabilityItems
//    }
//
//}


class CheckAvailabilityItemModel: NSObject {
    
    var id: String?
    var day: String?
    var dayId: String?
    var slotId: String?
    var time: String?
    var status: String?
    var lastMeetingStatus: String?
    
    init(id: String?,day: String?, dayId: String?, slotId: String?, time: String?, status: String?,lastMeetingStatus: String?) {
        self.id = id
        self.day = day
        self.dayId = dayId
        self.slotId = slotId
        self.time = time
        self.status = status
        self.lastMeetingStatus = lastMeetingStatus

    }
}

enum CheckAvailabilityStatus: String {
    case YES = "yes"
    case NO = "no"
    case NOT_AVAILABLE = "not_available"
    case TOP_HEADER = "top_header_view"

}


enum StringForOneZero: String {
    case One = "1"
    case Zero = "0"
    
}

class CheckAvailabilitySetModel: Codable {
    
      enum CodingKeys: String, CodingKey {
          case status
          case message
          case data
      }

      var status, message: String?
      var data: CheckAvailabilityUpdatedModel?


      
      init (status:String?, message:String?, data:CheckAvailabilityUpdatedModel?) {
          self.status = status
          self.message = message
          self.data = data
      }

    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      status = try container.decodeIfPresent(String.self, forKey: .status)
      message = try container.decodeIfPresent(String.self, forKey: .message)
      data = try container.decodeIfPresent(CheckAvailabilityUpdatedModel.self, forKey: .data)
      
      
    }
}

class CheckAvailabilityUpdatedModel: Codable {
//      enum CodingKeys: String, CodingKey {
//          case status
//          case message
//      }
//
//      var status, message: String?
//
//
//
//      init (status:String?, message:String?) {
//          self.status = status
//          self.message = message
//      }
//
//    required init(from decoder: Decoder) throws {
//      let container = try decoder.container(keyedBy: CodingKeys.self)
//      status = try container.decodeIfPresent(String.self, forKey: .status)
//      message = try container.decodeIfPresent(String.self, forKey: .message)
//    }
}
