//
//  SummitEventProgram_MeetingRequestDetailsModel.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 12/7/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import Foundation

class SummitEventProgram_MeetingRequestDetailsModel: Codable {
    enum CodingKeys: String, CodingKey {
        
        case is_received
        case note
        case meeting_request_status
        case requested_by_id
        case requested_to_id
        case requested_to_details
        case requested_by_details
        
    }
    
    var is_received: Bool?
    var note: String?
    var meeting_request_status: [RequestMeetingStatus]?
    var requested_by_id, requested_to_id: String?
    var requested_to_details, requested_by_details : SummitEventPeopleDetailModel?

    
    init ( is_received:Bool?,note: String?, meeting_request_status: [RequestMeetingStatus]?, requested_by_id: String?, requested_to_id: String?, requested_to_details : SummitEventPeopleDetailModel, requested_by_details : SummitEventPeopleDetailModel) {
        
        self.is_received = is_received
        self.note = note
        self.meeting_request_status = meeting_request_status
        self.requested_by_id = requested_by_id
        self.requested_to_id = requested_to_id
        self.requested_by_details = requested_by_details
        self.requested_to_details = requested_to_details
        
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        meeting_request_status = try container.decodeIfPresent([RequestMeetingStatus].self, forKey: .meeting_request_status)
        note = try container.decodeIfPresent(String.self, forKey: .note)
        is_received = try container.decodeIfPresent(Bool.self, forKey: .is_received)
        requested_to_id = try container.decodeIfPresent(String.self, forKey: .requested_to_id)
        requested_by_id = try container.decodeIfPresent(String.self, forKey: .requested_by_id)
        requested_by_details = try container.decodeIfPresent(SummitEventPeopleDetailModel.self, forKey: .requested_by_details)
        requested_to_details = try container.decodeIfPresent(SummitEventPeopleDetailModel.self, forKey: .requested_to_details)
        
    }
    
}

class RequestMeetingStatus: Codable {
    
    enum CodingKeys: String, CodingKey {
        
        case status
        case created
        
    }
    
    var status, created : String?
    
    
    init (status: String?, created: String?) {
        
        self.status = status
        self.created = created
        
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        status = try container.decodeIfPresent(String.self, forKey: .status)
        created = try container.decodeIfPresent(String.self, forKey: .created)

        
    }
    
    
    
}

class RequestMeetingSetModel: Codable {
      enum CodingKeys: String, CodingKey {
          case status
          case message
          case data = "data"
      }

      var status, message: String?
      var data: RequestMeetingUpdatedModel?


      
      init (status:String?, message:String?, data:RequestMeetingUpdatedModel?) {
          self.status = status
          self.message = message
          self.data = data
      }

    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      status = try container.decodeIfPresent(String.self, forKey: .status)
      message = try container.decodeIfPresent(String.self, forKey: .message)
      data = try container.decodeIfPresent(RequestMeetingUpdatedModel.self, forKey: .data)
      
      
    }
}

class RequestMeetingUpdatedModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        
        case is_error
        case updated_status
        
    }
    
    var is_error: Bool?
    var updated_status: String?
    
    
    init (is_error: Bool?, updated_status: String?) {
        
        self.is_error = is_error
        self.updated_status = updated_status
        
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        updated_status = try container.decodeIfPresent(String.self, forKey: .updated_status)
        is_error = try container.decodeIfPresent(Bool.self, forKey: .is_error)
        
    }
    
}
