//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventMyPlannerTopMenusModel: Codable {

    enum CodingKeys: String, CodingKey {

        case heading
        case id
        case selected
    }

  
    var heading:String?
    var id: Int?
    var selected:Bool?
    
    
    init ( heading:String?, id:Int?, selected:Bool?) {

        self.heading = heading
        self.id = id
        self.selected = selected
        
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    heading = try container.decodeIfPresent(String.self, forKey: .heading)
    id = try container.decodeIfPresent(Int.self, forKey: .id)
    selected = try container.decodeIfPresent(Bool.self, forKey: .selected)
        
  }

}

enum RequestMeeting: String {
    case YES = "1"
    case NO = "0"
}
