//
//  TimeSlotModel.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 9/14/20.
//  Copyright © 2020 Adeel ilyas. All rights reserved.
//

import Foundation


class TimeSlotModel: NSObject {
    
    var slotId: String?
    var time: String?
    
    init(slotId: String?, time: String?) {
        self.slotId = slotId
        self.time = time
    }
}


class DayData: NSObject {

//    var dayId: String?
    var day: String?
    var date: String?
    
    init(day: String?, date: String?) {
        super.init()
//        self.dayId = dayId
        self.day = day
        self.date = date
    }
    
}
