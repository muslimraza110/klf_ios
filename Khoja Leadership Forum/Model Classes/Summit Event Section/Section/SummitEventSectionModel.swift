//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventSectionModel: Codable {

    enum CodingKeys: String, CodingKey {

        case id
        case summit_events_id
        case title
        case short_description
        case tag
    }

    var id: String?
    var summit_events_id: String?
    var title: String?
    var short_description: String?
    var tag: String?

    
    init (id: String?, summit_events_id:String?, title:String?, short_description:String?, tag:String?) {

        self.id = id
        self.summit_events_id = summit_events_id
        self.title = title
        self.short_description = short_description
        self.tag = tag
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = try container.decodeIfPresent(String.self, forKey: .id)
    summit_events_id = try container.decodeIfPresent(String.self, forKey: .summit_events_id)
    title = try container.decodeIfPresent(String.self, forKey: .title)
    short_description = try container.decodeIfPresent(String.self, forKey: .short_description)
    tag = try container.decodeIfPresent(String.self, forKey: .tag)
    
  }

}
