//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventSectionBaseModel: Codable {

    enum CodingKeys: String, CodingKey {
        case status
        case message
        case sections = "data"
    }

    var status, message: String?
    var sections: [SummitEventSectionModel]?


    
    init (status:String?, message:String?, /*event_id:Int?, */ sections:[SummitEventSectionModel]?) {
        self.status = status
        self.message = message
        self.sections = sections
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    status = try container.decodeIfPresent(String.self, forKey: .status)
    message = try container.decodeIfPresent(String.self, forKey: .message)
    sections = try container.decodeIfPresent([SummitEventSectionModel].self, forKey: .sections)
    
    
  }

}
