//
//  SummitEventNotificationsModel.swift
//  Khoja Leadership Forum
//
//  Created by koderlabs on 1/26/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class SummitEventNotificationsBaseModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
    
    var status, message: String?
    var data: [SummitEventNotificationModel]?
    
    
    
    init (status:String?, message:String?, data:[SummitEventNotificationModel]?) {
        self.status = status
        self.message = message
        self.data = data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decodeIfPresent(String.self, forKey: .status)
        message = try container.decodeIfPresent(String.self, forKey: .message)
        data = try container.decodeIfPresent([SummitEventNotificationModel].self, forKey: .data)
    }
    
}


class SummitEventNotificationModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case description
        case date_time
        case identifier
        case data
    }
    
    var id, description, date_time, identifier: String?
    
    var data: SummitEventNotificationDataModel?
    
    init (id: String?, description: String?, date_time: String?, identifier: String?, data: SummitEventNotificationDataModel?) {
        self.id = id
        self.description = description
        self.date_time = date_time
        self.identifier = identifier
        self.data = data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(String.self, forKey: .id)
        description = try container.decodeIfPresent(String.self, forKey: .description)
        date_time = try container.decodeIfPresent(String.self, forKey: .date_time)
        identifier = try container.decodeIfPresent(String.self, forKey: .identifier)
        data = try container.decodeIfPresent(SummitEventNotificationDataModel.self, forKey: .data)
    }
    
}

class SummitEventNotificationDataModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case event_day_index
    }
    
    var event_day_index: Int?
    
    init (event_day_index: Int?) {
        self.event_day_index = event_day_index

    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        event_day_index = try container.decodeIfPresent(Int.self, forKey: .event_day_index)
    }
    
}
