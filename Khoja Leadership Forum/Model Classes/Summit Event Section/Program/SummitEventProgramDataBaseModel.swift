//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventProgramDataBaseModel: Codable {

    enum CodingKeys: String, CodingKey {
        //case eventDetails
        case programData = "program_data"
       
    }

    //var eventDetails: SummitEventModel?
    var programData: [SummitEventProgramModel]?


    
    init (/*eventDetails:SummitEventModel?, */ programData:[SummitEventProgramModel]?) {
        //self.eventDetails = eventDetails
        self.programData = programData
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    //eventDetails = try container.decodeIfPresent(SummitEventModel.self, forKey: .eventDetails)
    programData = try container.decodeIfPresent([SummitEventProgramModel].self, forKey: .programData)
    
    
  }

}
