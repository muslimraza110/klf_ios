//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventProgramModel: Codable {

    enum CodingKeys: String, CodingKey {

        case program_date
        case program_day
        case selected
        case program_list = "program_details"
        case id
    }

  
    var program_date, program_day: String?
    var selected:Bool?
    var program_list: [SummitEventProgramDetailsModel]?
    var id: String?

    
    init (id:String?, program_date:String?, program_day:String?, selected:Bool?, program_list: [SummitEventProgramDetailsModel]?) {

        self.program_date = program_date
        self.program_day = program_day
        self.selected = selected
        self.program_list = program_list
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    id = try container.decodeIfPresent(String.self, forKey: .id)
    program_date = try container.decodeIfPresent(String.self, forKey: .program_date)
    program_day = try container.decodeIfPresent(String.self, forKey: .program_day)
    selected = try container.decodeIfPresent(Bool.self, forKey: .selected)
    program_list = try container.decodeIfPresent([SummitEventProgramDetailsModel].self, forKey: .program_list)
        
  }

}
