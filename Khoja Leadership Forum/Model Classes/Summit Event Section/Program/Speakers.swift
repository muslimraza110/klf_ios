//
//  Speakers.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 02, 2021
//
import Foundation

struct Speakers: Codable {

	let id: String?
	let summitEventsProgramDetailsSessionsId: String?
	let summitEventsFacultyId: String?
	let isSpeaker: String?
	let isDelegate: String?
	let created: String?
	let modified: String?
	let summitEventsSessionFacultiesDetail: SummitEventPeopleDetailModel?

	private enum CodingKeys: String, CodingKey {
		case id = "id"
		case summitEventsProgramDetailsSessionsId = "summit_events_program_details_sessions_id"
		case summitEventsFacultyId = "summit_events_faculty_id"
		case isSpeaker = "is_speaker"
		case isDelegate = "is_delegate"
		case created = "created"
		case modified = "modified"
		case summitEventsSessionFacultiesDetail = "summit_events_session_faculties_detail"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decode(String?.self, forKey: .id)
		summitEventsProgramDetailsSessionsId = try values.decode(String?.self, forKey: .summitEventsProgramDetailsSessionsId)
		summitEventsFacultyId = try values.decode(String?.self, forKey: .summitEventsFacultyId)
		isSpeaker = try values.decode(String?.self, forKey: .isSpeaker)
		isDelegate = try values.decode(String?.self, forKey: .isDelegate)
		created = try values.decode(String?.self, forKey: .created)
		modified = try values.decode(String?.self, forKey: .modified)
		summitEventsSessionFacultiesDetail = try values.decode(SummitEventPeopleDetailModel?.self, forKey: .summitEventsSessionFacultiesDetail)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(summitEventsProgramDetailsSessionsId, forKey: .summitEventsProgramDetailsSessionsId)
		try container.encode(summitEventsFacultyId, forKey: .summitEventsFacultyId)
		try container.encode(isSpeaker, forKey: .isSpeaker)
		try container.encode(isDelegate, forKey: .isDelegate)
		try container.encode(created, forKey: .created)
		try container.encode(modified, forKey: .modified)
		try container.encode(summitEventsSessionFacultiesDetail, forKey: .summitEventsSessionFacultiesDetail)
	}

}
