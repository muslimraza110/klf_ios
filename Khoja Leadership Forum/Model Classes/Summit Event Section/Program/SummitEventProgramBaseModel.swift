//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventProgramBaseModel: Codable {

    enum CodingKeys: String, CodingKey {
        case status
        case message
        case programList = "data"
    }

    var status, message: String?
    var programList: SummitEventProgramDataBaseModel?


    
    init (status:String?, message:String?, programList:SummitEventProgramDataBaseModel?) {
        self.status = status
        self.message = message
        self.programList = programList
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    status = try container.decodeIfPresent(String.self, forKey: .status)
    message = try container.decodeIfPresent(String.self, forKey: .message)
    programList = try container.decodeIfPresent(SummitEventProgramDataBaseModel.self, forKey: .programList)
    
    
  }

}
