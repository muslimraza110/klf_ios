//
//  SessionDetails.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 02, 2021
//
import Foundation

//class SessionDetails: Codable {
//
//    var id: String
//    var parentId: String
//    var title: String
//    var description: String
//    var roomLimit: Int
//    var roomOccupied: Int
//    var iAmEnrolled: Bool
//    var delegatesEnrolled: [DelegatesEnrolled]
//    var sessionFaculties: SessionFaculties
//    var zoomLink: String?
//
//	private enum CodingKeys: String, CodingKey {
//		case id = "id"
//		case parentId = "parent_id"
//		case title = "title"
//		case description = "description"
//		case roomLimit = "room_limit"
//		case roomOccupied = "room_occupied"
//		case iAmEnrolled = "i_am_enrolled"
//		case delegatesEnrolled = "delegates_enrolled"
//		case sessionFaculties = "session_faculties"
//        case zoomLink = "zoom_link"
//	}
//
//	init(from decoder: Decoder) throws {
//		let values = try decoder.container(keyedBy: CodingKeys.self)
//		id = try values.decode(String.self, forKey: .id)
//		parentId = try values.decode(String.self, forKey: .parentId)
//		title = try values.decode(String.self, forKey: .title)
//		description = try values.decode(String.self, forKey: .description)
//		roomLimit = try values.decode(Int.self, forKey: .roomLimit)
//		roomOccupied = try values.decode(Int.self, forKey: .roomOccupied)
//		iAmEnrolled = try values.decode(Bool.self, forKey: .iAmEnrolled)
//		delegatesEnrolled = try values.decode([DelegatesEnrolled].self, forKey: .delegatesEnrolled)
//		sessionFaculties = try values.decode(SessionFaculties.self, forKey: .sessionFaculties)
//        zoomLink = try values.decode(String.self, forKey: .zoomLink)
//	}
//
//	func encode(to encoder: Encoder) throws {
//		var container = encoder.container(keyedBy: CodingKeys.self)
//		try container.encode(id, forKey: .id)
//		try container.encode(parentId, forKey: .parentId)
//		try container.encode(title, forKey: .title)
//		try container.encode(description, forKey: .description)
//		try container.encode(roomLimit, forKey: .roomLimit)
//		try container.encode(roomOccupied, forKey: .roomOccupied)
//		try container.encode(iAmEnrolled, forKey: .iAmEnrolled)
//		try container.encode(delegatesEnrolled, forKey: .delegatesEnrolled)
//		try container.encode(sessionFaculties, forKey: .sessionFaculties)
//        try container.encode(zoomLink, forKey: .zoomLink)
//	}
//
//}

class SessionDetails: Codable {

    enum CodingKeys: String, CodingKey {
        case id
        case parent_id
        case title
        case description
        case room_limit
        case room_occupied
        case i_am_enrolled
        case delegates_enrolled
        case session_faculties
        case zoom_link
    }


    var id, parentid, title, description, zoomLink: String?
    var room_limit, room_occupied: Int?
    var i_am_enrolled: Bool?
    var delegatesEnrolled: [DelegatesEnrolled]?
    var sessionFaculties: SessionFaculties?

    init ( id: String?, parentid: String?, title: String?, description: String?, zoomLink: String?, room_limit: Int?, room_occupied: Int?, i_am_enrolled: Bool?, delegatesEnrolled: [DelegatesEnrolled]?, sessionFaculties: SessionFaculties?) {

        self.id = id
        self.parentid = parentid
        self.title = title
        self.description = description
        self.zoomLink = zoomLink
        self.i_am_enrolled = i_am_enrolled
        self.delegatesEnrolled = delegatesEnrolled
        self.sessionFaculties = sessionFaculties
        self.room_limit = room_limit
        self.room_occupied = room_occupied
    }

    required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)

     id = try container.decodeIfPresent(String.self, forKey: .id) ?? ""
    parentid = try container.decodeIfPresent(String.self, forKey: .parent_id)
    title = try container.decodeIfPresent(String.self, forKey: .title)
    description = try container.decodeIfPresent(String.self, forKey: .description)
    room_limit = try container.decodeIfPresent(Int.self, forKey: .room_limit)
    room_occupied = try container.decodeIfPresent(Int.self, forKey: .room_occupied)
    i_am_enrolled = try container.decodeIfPresent(Bool.self, forKey: .i_am_enrolled)
    delegatesEnrolled = try container.decodeIfPresent([DelegatesEnrolled].self, forKey: .delegates_enrolled)
    sessionFaculties = try container.decodeIfPresent(SessionFaculties.self, forKey: .session_faculties)
    zoomLink = try container.decodeIfPresent(String.self, forKey: .zoom_link)
  }

func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(id, forKey: .id)
    try container.encode(parentid, forKey: .parent_id)
    try container.encode(title, forKey: .title)
    try container.encode(description, forKey: .description)
    try container.encode(room_limit, forKey: .room_limit)
    try container.encode(room_occupied, forKey: .room_occupied)
    try container.encode(i_am_enrolled, forKey: .i_am_enrolled)
    try container.encode(delegatesEnrolled, forKey: .delegates_enrolled)
    try container.encode(sessionFaculties, forKey: .session_faculties)
    try container.encode(zoomLink, forKey: .zoom_link)
}

}
