//
//  SummitEventsFacultyDetail.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 02, 2021
//
import Foundation

struct SummitEventsFacultyDetail: Codable {

	let id: String?
	let summitEventsFacultyId: String?
	let name: String?
	let designation: String?
	let shortDescription: String?
	let longDescription: String?
	let facebookLink: String?
	let twitterLink: String?
	let userImage: String?
	let linkedinLink: String?
	let isSpeaker: String?
	let isDelegate: String?
	let created: String?
	let modified: String?
	let countryId: String?
	let industryId: String?
	let userId: String?
	let meetingSlots: [Any]?

	private enum CodingKeys: String, CodingKey {
		case id = "id"
		case summitEventsFacultyId = "summit_events_faculty_id"
		case name = "name"
		case designation = "designation"
		case shortDescription = "short_description"
		case longDescription = "long_description"
		case facebookLink = "facebook_link"
		case twitterLink = "twitter_link"
		case userImage = "user_image"
		case linkedinLink = "linkedin_link"
		case isSpeaker = "is_speaker"
		case isDelegate = "is_delegate"
		case created = "created"
		case modified = "modified"
		case countryId = "country_id"
		case industryId = "industry_id"
		case userId = "user_id"
		case meetingSlots = "meeting_slots"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decode(String?.self, forKey: .id)
		summitEventsFacultyId = try values.decode(String?.self, forKey: .summitEventsFacultyId)
		name = try values.decode(String?.self, forKey: .name)
		designation = try values.decode(String?.self, forKey: .designation)
		shortDescription = try values.decode(String?.self, forKey: .shortDescription)
		longDescription = try values.decode(String?.self, forKey: .longDescription)
		facebookLink = try values.decode(String?.self, forKey: .facebookLink)
		twitterLink = try values.decode(String?.self, forKey: .twitterLink)
		userImage = try values.decode(String?.self, forKey: .userImage)
		linkedinLink = try values.decode(String?.self, forKey: .linkedinLink)
		isSpeaker = try values.decode(String?.self, forKey: .isSpeaker)
		isDelegate = try values.decode(String?.self, forKey: .isDelegate)
		created = try values.decode(String?.self, forKey: .created)
		modified = try values.decode(String?.self, forKey: .modified)
		countryId = try values.decode(String?.self, forKey: .countryId)
		industryId = try values.decode(String?.self, forKey: .industryId)
		userId = try values.decode(String?.self, forKey: .userId)
		meetingSlots = [] // TODO: Add code for decoding `meetingSlots`, It was empty at the time of model creation.
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(summitEventsFacultyId, forKey: .summitEventsFacultyId)
		try container.encode(name, forKey: .name)
		try container.encode(designation, forKey: .designation)
		try container.encode(shortDescription, forKey: .shortDescription)
		try container.encode(longDescription, forKey: .longDescription)
		try container.encode(facebookLink, forKey: .facebookLink)
		try container.encode(twitterLink, forKey: .twitterLink)
		try container.encode(userImage, forKey: .userImage)
		try container.encode(linkedinLink, forKey: .linkedinLink)
		try container.encode(isSpeaker, forKey: .isSpeaker)
		try container.encode(isDelegate, forKey: .isDelegate)
		try container.encode(created, forKey: .created)
		try container.encode(modified, forKey: .modified)
		try container.encode(countryId, forKey: .countryId)
		try container.encode(industryId, forKey: .industryId)
		try container.encode(userId, forKey: .userId)
		// TODO: Add code for encoding `meetingSlots`, It was empty at the time of model creation.
	}

}
