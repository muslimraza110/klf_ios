//
//  SessionFaculties.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 02, 2021
//
import Foundation

struct SessionFaculties: Codable {

	let speakers: [Speakers]?
	let delegates: [Any]?

	private enum CodingKeys: String, CodingKey {
		case speakers = "speakers"
		case delegates = "delegates"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		speakers = try values.decode([Speakers].self, forKey: .speakers)
		delegates = [] // TODO: Add code for decoding `delegates`, It was empty at the time of model creation.
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(speakers, forKey: .speakers)
		// TODO: Add code for encoding `delegates`, It was empty at the time of model creation.
	}

}
