//
//  DelegatesEnrolled.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 02, 2021
//
import Foundation

struct DelegatesEnrolled: Codable {

	let id: String?
	let summitEventsFacultyId: String?
	let summitEventsProgramDetailsSessionsId: String?
	let created: String?
	let modified: String?
	let summitEventsFacultyDetail: SummitEventsFacultyDetail?

	private enum CodingKeys: String, CodingKey {
		case id = "id"
		case summitEventsFacultyId = "summit_events_faculty_id"
		case summitEventsProgramDetailsSessionsId = "summit_events_program_details_sessions_id"
		case created = "created"
		case modified = "modified"
		case summitEventsFacultyDetail = "summit_events_faculty_detail"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decode(String?.self, forKey: .id)
		summitEventsFacultyId = try values.decode(String?.self, forKey: .summitEventsFacultyId)
		summitEventsProgramDetailsSessionsId = try values.decode(String?.self, forKey: .summitEventsProgramDetailsSessionsId)
		created = try values.decode(String?.self, forKey: .created)
		modified = try values.decode(String?.self, forKey: .modified)
		summitEventsFacultyDetail = try values.decode(SummitEventsFacultyDetail?.self, forKey: .summitEventsFacultyDetail)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(summitEventsFacultyId, forKey: .summitEventsFacultyId)
		try container.encode(summitEventsProgramDetailsSessionsId, forKey: .summitEventsProgramDetailsSessionsId)
		try container.encode(created, forKey: .created)
		try container.encode(modified, forKey: .modified)
		try container.encode(summitEventsFacultyDetail, forKey: .summitEventsFacultyDetail)
	}

}
