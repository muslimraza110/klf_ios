//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventProgramDetailsModel: Codable {

    enum CodingKeys: String, CodingKey {

        case id
        case parentid
        case day
        case sort
        case start_time
        case end_time
        case title
        case description
        case can_request_a_meeting
        case check_my_availability_is_available
        case status
        case start_end_time
        case speakers
        case delegates
        case meeting_request_details
        case event_session_type
        case session_details
    }

  
    var id, parentid, day, sort, start_time, end_time, title, description, can_request_a_meeting, check_my_availability_is_available, status, start_end_time, event_session_type: String?
    var speakers, delegates: [SummitEventPeopleDetailModel]?
    
    var meeting_request_details: SummitEventProgram_MeetingRequestDetailsModel?
    var session_details: [SessionDetails]?
    // Meeting Request Status currently not getting from api
//    var meeting_status: String?


    
    init ( id:String?, parentid:String?,day:String?,sort:String?,start_time:String?,end_time:String?,title:String?,description:String?,can_request_a_meeting:String?, check_my_availability_is_available: String? ,status:String?,start_end_time:String?, event_session_type: String?, speakers:[SummitEventPeopleDetailModel]?, delegates: [SummitEventPeopleDetailModel]?,
           meeting_status: String?, meeting_request_details: SummitEventProgram_MeetingRequestDetailsModel?, session_details: [SessionDetails]?) {

        self.id = id
        self.parentid = parentid
        self.day = day
        self.sort = sort
        self.start_time = start_time
        self.end_time = end_time
        self.title = title
        self.description = description
        self.can_request_a_meeting = can_request_a_meeting
        self.check_my_availability_is_available = check_my_availability_is_available
        self.status = status
        self.start_end_time = start_end_time
        self.speakers = speakers
        self.delegates = delegates
//        self.meeting_status = meeting_status
        self.meeting_request_details = meeting_request_details
        self.event_session_type = event_session_type
        self.session_details = session_details
        
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    id = try container.decodeIfPresent(String.self, forKey: .id)
    parentid = try container.decodeIfPresent(String.self, forKey: .parentid)
    day = try container.decodeIfPresent(String.self, forKey: .day)
    sort = try container.decodeIfPresent(String.self, forKey: .sort)
    start_time = try container.decodeIfPresent(String.self, forKey: .start_time)
    end_time = try container.decodeIfPresent(String.self, forKey: .end_time)
    title = try container.decodeIfPresent(String.self, forKey: .title)
    description = try container.decodeIfPresent(String.self, forKey: .description)
    can_request_a_meeting = try container.decodeIfPresent(String.self, forKey: .can_request_a_meeting)
    check_my_availability_is_available = try container.decodeIfPresent(String.self, forKey: .check_my_availability_is_available)
    status = try container.decodeIfPresent(String.self, forKey: .status)
    start_end_time = try container.decodeIfPresent(String.self, forKey: .start_end_time)
    speakers = try container.decodeIfPresent([SummitEventPeopleDetailModel].self, forKey: .speakers)
    delegates = try container.decodeIfPresent([SummitEventPeopleDetailModel].self, forKey: .delegates)
    meeting_request_details = nil //try container.decode(SummitEventProgram_MeetingRequestDetailsModel.self, forKey: .meeting_request_details) ?? nil
    event_session_type =  try container.decodeIfPresent(String.self, forKey: .event_session_type)
    session_details =  try container.decodeIfPresent([SessionDetails].self, forKey: .session_details)
    
  }

}

enum MeetingStatus: String{
//    case SLOT_AVAILABLE = "0"
//    case REQUEST_RECEIVED = "2"
    case NOTES_UPDATED = "0"
    case MEETING_REQUESTED = "1"
    case MEETING_ACCEPTED = "2"
    case MEETING_REJECTED  = "3"
    case MEETING_HOST_CANCELED = "4"
    case MEETING_DELETED = "5"
    case MEETING_SELF_CANCELED = "6"


}
