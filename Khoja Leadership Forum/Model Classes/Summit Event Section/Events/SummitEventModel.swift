//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventModel: Codable {

    enum CodingKeys: String, CodingKey {

        case id
        case name
        case short_description
        case start_date
        case end_date
        case venue_address
        case venue_city
        case price_range_start
        case price_range_end
        case profile_link
        case event_images
        case is_user_registered = "is_userRegistered"
        case summit_events_faculty_id
        
        case registration_link
        
    }

    var id: String?
    var name: String?
    var short_description: String?
    var start_date: String?
    var end_date: String?
    var venue_address:String?
    var venue_city:String?
    var price_range_start:String?
    var price_range_end:String?
    var profile_link:String?
    var event_images:[String]?
    var is_user_registered: Bool?
    var summit_events_faculty_id: String?
    
    var registration_link:String?

    
    init (id: String?, name:String?, short_description:String?, start_date:String?, end_date:String?, venue_address:String?, venue_city:String?, price_range_start:String?, price_range_end:String?, profile_link:String?, event_images:[String]?,
          is_user_registered: Bool?, summit_events_faculty_id: String?, registration_link:String?) {

        self.id = id
        self.name = name
        self.short_description = short_description
        self.start_date = start_date
        self.end_date = end_date
        self.venue_address = venue_address
        
        self.venue_city = venue_city
        self.price_range_start = price_range_start
        self.price_range_end = price_range_end
        self.profile_link = profile_link
        self.event_images = event_images
        self.is_user_registered = is_user_registered
        self.summit_events_faculty_id = summit_events_faculty_id
        
        self.registration_link = registration_link

    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = try container.decodeIfPresent(String.self, forKey: .id)
    name = try container.decodeIfPresent(String.self, forKey: .name)
    short_description = try container.decodeIfPresent(String.self, forKey: .short_description)
    start_date = try container.decodeIfPresent(String.self, forKey: .start_date)
    end_date = try container.decodeIfPresent(String.self, forKey: .end_date)
    venue_address = try container.decodeIfPresent(String.self, forKey: .venue_address)
    
    venue_city = try container.decodeIfPresent(String.self, forKey: .venue_city)
    price_range_start = try container.decodeIfPresent(String.self, forKey: .price_range_start)
    price_range_end = try container.decodeIfPresent(String.self, forKey: .price_range_end)
    profile_link = try container.decodeIfPresent(String.self, forKey: .profile_link)
    event_images = try container.decodeIfPresent([String].self, forKey: .event_images)
    is_user_registered = try container.decodeIfPresent(Bool.self, forKey: .is_user_registered)
    summit_events_faculty_id = try container.decodeIfPresent(String.self, forKey: .summit_events_faculty_id)
    
    
    registration_link = try container.decodeIfPresent(String.self, forKey: .registration_link)
    
    
  }

}
