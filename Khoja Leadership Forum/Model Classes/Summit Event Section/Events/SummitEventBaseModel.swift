//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class SummitEventBaseModel: Codable {

    enum CodingKeys: String, CodingKey {
        case status
        case message
        case eventLists = "data"
    }

    var status, message: String?
    var eventLists: [SummitEventModel]?


    
    init (status:String?, message:String?, eventLists:[SummitEventModel]?) {
        self.status = status
        self.message = message
        self.eventLists = eventLists
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    status = try container.decodeIfPresent(String.self, forKey: .status)
    message = try container.decodeIfPresent(String.self, forKey: .message)
    eventLists = try container.decodeIfPresent([SummitEventModel].self, forKey: .eventLists)
    
    
  }

}
