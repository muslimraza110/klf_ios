//
//  SummitEventsFacultyBaseModel.swift
//  Khoja Leadership Forum
//
//  Created by Koderlabs - MS on 9/2/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation


class SummitEventsFacultyBaseModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
    
    var status, message: String?
    var data: SummitEventsFacultyModel?
    
    
    
    init (status:String?, message:String?, data:SummitEventsFacultyModel?) {
        self.status = status
        self.message = message
        self.data = data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decodeIfPresent(String.self, forKey: .status)
        message = try container.decodeIfPresent(String.self, forKey: .message)
        data = try container.decodeIfPresent(SummitEventsFacultyModel.self, forKey: .data)
    }
    
}


class SummitEventsFacultyModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case user_id
        case summit_event_id
        case is_paid
        
    }
    
    var id, user_id, summit_event_id, is_paid: String?
    
    
    init (id: String?, user_id: String?, summit_event_id: String?, is_paid: String? ) {
        self.id = id
        self.user_id = user_id
        self.summit_event_id = summit_event_id
        self.is_paid = is_paid
        
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(String.self, forKey: .id)
        user_id = try container.decodeIfPresent(String.self, forKey: .user_id)
        summit_event_id = try container.decodeIfPresent(String.self, forKey: .summit_event_id)
        is_paid = try container.decodeIfPresent(String.self, forKey: .is_paid)
        
    }
    
}
