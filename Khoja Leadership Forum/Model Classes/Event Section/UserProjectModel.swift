

import Foundation
class UserProjectModel {
    var projectID, groupID, projectsGroupsPercent, projectsID: String?
    var projectsCreatorID, projectsKhojaCareCategoryID, projectsName, projectsWebsite: String?
    var projectsDescription, projectsStartDate, projectsEstCompvarionDate: String?
    var projectsFundraisingTarget: String?
    var projectsCity: String?
    var projectsRegion: String?
    var projectsCountry, projectsApprovalStamp, projectsStatus, projectsCreated: String?
    var projectsModified: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.projectID = i["project_id"] as? String
            self.groupID = i["group_id"] as? String
            self.projectsGroupsPercent = i["ProjectsGroups__percent"] as? String
            self.projectsID = i["Projects__id"] as? String
            self.projectsCreatorID = i["Projects__creator_id"] as? String
            self.projectsKhojaCareCategoryID = i["Projects__khoja_care_category_id"] as? String
            self.projectsName = i["Projects__name"] as? String
            self.projectsWebsite = i["Projects__website"] as? String
            self.projectsDescription = i["Projects__description"] as? String
            self.projectsStartDate = i["Projects__start_date"] as? String
            self.projectsEstCompvarionDate = i["Projects__est_completion_date"] as? String
            self.projectsFundraisingTarget = i["Projects__fundraising_target"] as? String
            self.projectsCity = i["Projects__city"] as? String
            self.projectsRegion = i["Projects__region"] as? String
            self.projectsCountry = i["Projects__country"] as? String
            self.projectsApprovalStamp = i["Projects__approval_stamp"] as? String
            self.projectsStatus = i["Projects__status"] as? String
            self.projectsCreated = i["Projects__created"] as? String
            self.projectsModified = i["Projects__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

