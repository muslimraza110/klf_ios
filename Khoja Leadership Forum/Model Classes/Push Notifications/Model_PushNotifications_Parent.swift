//
//  Model_PushNotifications.swift
//  ClubChat
//
//  Created by Koderlabs - MS on 3/15/21.
//

import Foundation


class Model_PushNotifications_Parent: Codable {
    
    
    enum CodingKeys: String, CodingKey {
        case identifier
        case details = "details"
        
    }
    
    var identifier: Enum_PushNotifications_Identifiers?
    var details: Model_PushNotifications_Child?
    
    
    init (identifier: Enum_PushNotifications_Identifiers?, details: Model_PushNotifications_Child?) {
        self.identifier = identifier
        self.details = details
        
        
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        details = try container.decodeIfPresent(Model_PushNotifications_Child.self, forKey: .details)
        identifier = try container.decodeIfPresent(Enum_PushNotifications_Identifiers.self, forKey: .identifier)
        
        
    }
    
    
}

