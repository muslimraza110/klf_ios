//
//  Model_PushNotifications_Child.swift
//  ClubChat
//
//  Created by Koderlabs - MS on 3/15/21.
//

import Foundation



enum Enum_PushNotifications_Identifiers: String, Codable {
    
    case PushNotification_SummitEvents_MeetingRequest_Sent
    case PushNotification_SummitEvents_MeetingRequest_Updated
    case PushNotification_SummitEvents_MeetingRequest_Accepted
    case PushNotification_SummitEvents_MeetingRequest_Rejected
    case PushNotification_SummitEvents_MeetingRequest_Cancelled
    case PushNotification_SummitEvents_MeetingRequest_Deleted
    
}

class Model_PushNotifications_Child: Codable {
    
  
    enum CodingKeys: String, CodingKey {
        case user_id
        case event_id
        case summit_events_faculty_id
        case meeting_parent_id
        
        
        
    }
    
    var user_id, event_id, summit_events_faculty_id, meeting_parent_id: String?
    
    
    
    init (user_id: String?, event_id: String?, summit_events_faculty_id: String?, meeting_parent_id: String?) {
        self.user_id = user_id
        self.event_id = event_id
        self.summit_events_faculty_id = summit_events_faculty_id
        self.meeting_parent_id = meeting_parent_id
        
        
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        user_id = try container.decodeIfPresent(String.self, forKey: .user_id)
        event_id = try container.decodeIfPresent(String.self, forKey: .event_id)
        summit_events_faculty_id = try container.decodeIfPresent(String.self, forKey: .summit_events_faculty_id)
        meeting_parent_id = try container.decodeIfPresent(String.self, forKey: .meeting_parent_id)
        
    }
    
    
}

