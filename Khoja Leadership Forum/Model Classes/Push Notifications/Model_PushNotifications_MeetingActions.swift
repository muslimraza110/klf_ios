//
//  Model_PushNotifications.swift
//  ClubChat
//
//  Created by Koderlabs - MS on 3/15/21.
//

import Foundation


class Model_PushNotifications_MeetingActions: Codable {
    
    
    enum CodingKeys: String, CodingKey {
        case user_id
        case event_id
        case summit_events_faculty_id
        
    }
    
    var user_id, event_id, summit_events_faculty_id: Int?
    
    
    
    init (user_id: Int?, event_id: Int?, summit_events_faculty_id: Int?) {
        self.user_id = user_id
        self.event_id = event_id
        self.summit_events_faculty_id = summit_events_faculty_id
        
        
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        user_id = try container.decodeIfPresent(Int.self, forKey: .user_id)
        event_id = try container.decodeIfPresent(Int.self, forKey: .event_id)
        summit_events_faculty_id = try container.decodeIfPresent(Int.self, forKey: .summit_events_faculty_id)
        
    }
    
    
    
    
}

