//
//  MailData.swift
//
//  Created by Farooq Rasheed on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation

class BaseModelClass: Codable {

    enum CodingKeys: String, CodingKey {
        case status
        case message
        
    }

    var status, message: String?


    
    init (status:String?, message:String?) {
        self.status = status
        self.message = message
    }

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    status = try container.decodeIfPresent(String.self, forKey: .status)
    message = try container.decodeIfPresent(String.self, forKey: .message)
  }

}
