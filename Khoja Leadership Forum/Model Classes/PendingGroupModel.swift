//
//  PendingGroupModel.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 30/08/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import Foundation
class PendingGroupModel {
    var groupRequestsID, groupRequestsGroupID, groupRequestsUserID, groupRequestsStatus: String?
    var groupRequestsCreated, groupRequestsModified, groupsID, groupsCategoryID: String?
    var groupsName, groupsDescription, groupsEmail, groupsPhone: String?
    var groupsAddress, groupsAddress2, groupsWebsite, groupsCity: String?
    var groupsRegion, groupsCountry, groupsPostalCode, groupsBio: String?
    var groupsHidden, groupsPending, groupsVoteDecision, groupsVoteDecisionDate: String?
    var groupsCreated, groupsStatus, groupsDonations, groupsPassive: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.groupRequestsID = i["GroupRequests__id"] as? String
            self.groupRequestsGroupID = i["GroupRequests__group_id"] as? String
            self.groupRequestsUserID = i["GroupRequests__user_id"] as? String
            self.groupRequestsStatus = i["GroupRequests__status"] as? String
            self.groupRequestsCreated = i["GroupRequests__created"] as? String
            self.groupRequestsModified = i["GroupRequests__modified"] as? String
            self.groupsID = i["Groups__id"] as? String
            self.groupsCategoryID = i["Groups__category_id"] as? String
            self.groupsName = i["Groups__name"] as? String
            self.groupsDescription = i["Groups__description"] as? String
            self.groupsEmail = i["Groups__email"] as? String
            self.groupsPhone = i["Groups__phone"] as? String
            self.groupsAddress = i["Groups__address"] as? String
            self.groupsAddress2 = i["Groups__address2"] as? String
            self.groupsWebsite = i["Groups__website"] as? String
            self.groupsCity = i["Groups__city"] as? String
            self.groupsRegion = i["Groups__region"] as? String
            self.groupsCountry = i["Groups__country"] as? String
            self.groupsPostalCode = i["Groups__postal_code"] as? String
            self.groupsBio = i["Groups__bio"] as? String
            self.groupsHidden = i["Groups__hidden"] as? String
            self.groupsPending = i["Groups__pending"] as? String
            self.groupsVoteDecision = i["Groups__vote_decision"] as? String
            self.groupsVoteDecisionDate = i["Groups__vote_decision_date"] as? String
            self.groupsCreated = i["Groups__created"] as? String
            self.groupsStatus = i["Groups__status"] as? String
            self.groupsDonations = i["Groups__donations"] as? String
            self.groupsPassive = i["Groups__passive"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
