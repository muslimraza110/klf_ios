

import Foundation
class ListOfAllGroupsModel {
   var groupID: String?
   var categoryID: String?
   var name: String?
   var datumDescription: String?
   var email: String?
   var address: String?
   var website: String?
   var city: String?
   var region: String?
   var country: String?
   var postalCode: String?
   var bio: String?
   var hidden: String?
   var voteDecision: String?
   var status: String?
   var donations: String?
   var passive: String?
   var createdByUserID: String?
   var groupsImage: String?
    
    init(data:[[String:Any]]) {
        for i in data {
            self.groupID = i["group_id"] as? String
            self.categoryID = i["category_id"] as? String
            self.name = i["name"] as? String
            self.datumDescription = i["description"] as? String
            self.email = i["email"] as? String
            self.address = i["address"] as? String
            self.website = i["website"] as? String
            self.city = i["city"] as? String
            self.region = i["region"] as? String
            self.country = i["country"] as? String
            self.postalCode = i["postal_code"] as? String
            self.bio = i["bio"] as? String
            self.hidden = i["hidden"] as? String
            self.voteDecision = i["vote_decision"] as? String
            self.status = i["status"] as? String
            self.donations = i["donations"] as? String
            self.passive = i["passive"] as? String
            self.createdByUserID = i["CreatedByUserId"] as? String
            self.groupsImage = i["groups_image"] as? String
        }
        
    }
}
