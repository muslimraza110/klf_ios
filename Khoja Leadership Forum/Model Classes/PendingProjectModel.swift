//
//  PendingProjectModel.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 30/08/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import Foundation
class PendingProjectModel {
    var projectRequestsID, projectRequestsProjectID, projectRequestsUserID, projectRequestsStatus: String?
    var projectRequestsCreated, projectRequestsModified, projectsID, projectsCreatorID: String?
    var projectsKhojaCareCategoryID, projectsName, projectsWebsite, projectsDescription: String?
    var projectsStartDate, projectsEstCompvarionDate: String?
    var projectsFundraisingTarget: String?
    var projectsCity: String?
    var projectsRegion: String?
    var projectsCountry, projectsApprovalStamp, projectsStatus, projectsCreated: String?
    var projectsModified: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.projectRequestsID = i["ProjectRequests__id"] as? String
            self.projectRequestsProjectID = i["ProjectRequests__project_id"] as? String
            self.projectRequestsUserID = i["ProjectRequests__user_id"] as? String
            self.projectRequestsStatus = i["ProjectRequests__status"] as? String
            self.projectRequestsCreated = i["ProjectRequests__created"] as? String
            self.projectRequestsModified = i["ProjectRequests__modified"] as? String
            self.projectsID = i["Projects__id"] as? String
            self.projectsCreatorID = i["Projects__creator_id"] as? String
            self.projectsKhojaCareCategoryID = i["Projects__khoja_care_category_id"] as? String
            self.projectsName = i["Projects__name"] as? String
            self.projectsWebsite = i["Projects__website"] as? String
            self.projectsDescription = i["Projects__description"] as? String
            self.projectsStartDate = i["Projects__start_date"] as? String
            self.projectsEstCompvarionDate = i["Projects__est_completion_date"] as? String
            self.projectsFundraisingTarget = i["Projects__fundraising_target"] as? String
            self.projectsCity = i["Projects__city"] as? String
            self.projectsRegion = i["Projects__region"] as? String
            self.projectsCountry = i["Projects__country"] as? String
            self.projectsApprovalStamp = i["Projects__approval_stamp"] as? String
            self.projectsStatus = i["Projects__status"] as? String
            self.projectsCreated = i["Projects__created"] as? String
            self.projectsModified = i["Projects__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
