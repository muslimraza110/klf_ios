//
//  MeetingRequestBaseModel.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/25/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class MeetingRequestBaseModel: Codable {
    var status, message: String?
  //  var data: [AnyObject]?

    enum CodingKeys: String, CodingKey {
        case status, message
       // case data
    }

    init(status: String?, message: String?) {
        self.status = status
        self.message = message
     //   self.data = data
    }
}
