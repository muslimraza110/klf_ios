//
//  MeetingHeaderItem.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/25/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class MeetingHeaderItem {
    var meetingId: String?
    var iAmHost: Bool?
    var dateTime: String?
    var startTime: String?
    var endTime: String?
    var simpleDate: String?
    var meetingNotes: String?
    var isOpened: Bool?

    init(meetingId: String?, dateTime: String?, meetingNotes: String?, isOpened: Bool?, startTime: String?, endTime: String?, simpleDate: String?, iAmHost: Bool?) {
        self.meetingId = meetingId
        self.iAmHost = iAmHost
        self.dateTime = dateTime
        self.meetingNotes = meetingNotes
        self.isOpened = isOpened
        self.startTime = startTime
        self.endTime = endTime
        self.simpleDate = simpleDate
    }
}
