//
//  MeetingItemUIModel.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/25/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class MeetingItemUIModel {

    var meetingChildId,name, designation, requestedBy, requestedTo, meetingStatus, myFacultyId: String?
    var isLast, iAmHost: Bool?

    init(meetingChildId: String?, name: String?, designation: String?, requestedBy: String?,requestedTo: String?, meetingStatus: String?, myFacultyId: String?, isLast: Bool?, iAmHost: Bool?) {
        self.meetingChildId = meetingChildId
        self.name = name
        self.designation = designation
        self.requestedBy = requestedBy
        self.requestedTo = requestedTo
        self.meetingStatus = meetingStatus
        self.myFacultyId = myFacultyId
        self.isLast = isLast
        self.iAmHost = iAmHost
    }
}
