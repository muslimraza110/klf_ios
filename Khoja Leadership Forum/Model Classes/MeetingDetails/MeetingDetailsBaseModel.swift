//
//  MeetingDetailsBaseModel.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/24/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class MeetingDetailsBaseModel: Codable {
    var status, message: String?
    var data: [MeetingDetailsModel]?

    init(status: String?, message: String?, data: [MeetingDetailsModel]?) {
        self.status = status
        self.message = message
        self.data = data
    }
}
