//
//  MeetingDetailsModel.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/24/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class MeetingDetailsModel: Codable {
    var hostDetails: HostDetails?
    var meetingDetails: [MeetingDetail]?

    enum CodingKeys: String, CodingKey {
        case hostDetails = "host_details"
        case meetingDetails = "meeting_details"
    }

    init(hostDetails: HostDetails?, meetingDetails: [MeetingDetail]?) {
        self.hostDetails = hostDetails
        self.meetingDetails = meetingDetails
    }
}
