//
//  MeetingDetail.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/24/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class MeetingDetail: Codable {
    var id, parentID, requestedTo, status: String?
    var created, modified: Created?
    var requestedToDetails: RequestedDetails?

    enum CodingKeys: String, CodingKey {
        case id
        case parentID = "parent_id"
        case requestedTo = "requested_to"
        case status, created, modified
        case requestedToDetails = "requested_to_details"
    }

    init(id: String?, parentID: String?, requestedTo: String?, status: String?, created: Created?, modified: Created?, requestedToDetails: RequestedDetails?) {
        self.id = id
        self.parentID = parentID
        self.requestedTo = requestedTo
        self.status = status
        self.created = created
        self.modified = modified
        self.requestedToDetails = requestedToDetails
    }
}
