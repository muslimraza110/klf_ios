//
//  RequestedDetails.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/24/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class RequestedDetails: Codable {
    var id, summitEventsFacultyID, name, designation: String?
    var shortDescription, longDescription, facebookLink, twitterLink: String?
    var userImage: String?
    var linkedinLink, isSpeaker, isDelegate, created: String?
    var modified, countryID, industryID, userID: String?

    enum CodingKeys: String, CodingKey {
        case id
        case summitEventsFacultyID = "summit_events_faculty_id"
        case name, designation
        case shortDescription = "short_description"
        case longDescription = "long_description"
        case facebookLink = "facebook_link"
        case twitterLink = "twitter_link"
        case userImage = "user_image"
        case linkedinLink = "linkedin_link"
        case isSpeaker = "is_speaker"
        case isDelegate = "is_delegate"
        case created, modified
        case countryID = "country_id"
        case industryID = "industry_id"
        case userID = "user_id"
    }

    init(id: String?, summitEventsFacultyID: String?, name: String?, designation: String?, shortDescription: String?, longDescription: String?, facebookLink: String?, twitterLink: String?, userImage: String?, linkedinLink: String?, isSpeaker: String?, isDelegate: String?, created: String?, modified: String?, countryID: String?, industryID: String?, userID: String?) {
        self.id = id
        self.summitEventsFacultyID = summitEventsFacultyID
        self.name = name
        self.designation = designation
        self.shortDescription = shortDescription
        self.longDescription = longDescription
        self.facebookLink = facebookLink
        self.twitterLink = twitterLink
        self.userImage = userImage
        self.linkedinLink = linkedinLink
        self.isSpeaker = isSpeaker
        self.isDelegate = isDelegate
        self.created = created
        self.modified = modified
        self.countryID = countryID
        self.industryID = industryID
        self.userID = userID
    }
}
