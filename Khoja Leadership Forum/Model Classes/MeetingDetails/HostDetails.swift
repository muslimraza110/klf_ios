//
//  HostDetails.swift
//  Khoja Leadership Forum
//
//  Created by Aijaz Ali on 3/24/21.
//  Copyright © 2021 Adeel ilyas. All rights reserved.
//

import Foundation

class HostDetails: Codable {
    var id, summitEventsID, requestedBy, meetingDate: String?
    var meetingStartTime, meetingEndTime, meetingVenue, notes: String?
    var status: String?
    var created, modified: Created?
    var meetingTime: String?
    var iAmHost: Bool?
    var requestedByDetails: RequestedDetails?

    enum CodingKeys: String, CodingKey {
        case id
        case summitEventsID = "summit_events_id"
        case requestedBy = "requested_by"
        case meetingDate = "meeting_date"
        case meetingStartTime = "meeting_start_time"
        case meetingEndTime = "meeting_end_time"
        case meetingVenue = "meeting_venue"
        case notes, status, created, modified
        case meetingTime = "meeting_time"
        case iAmHost = "i_am_host"
        case requestedByDetails = "requested_by_details"
    }

    init(id: String?, summitEventsID: String?, requestedBy: String?, meetingDate: String?, meetingStartTime: String?, meetingEndTime: String?, meetingVenue: String?, notes: String?, status: String?, created: Created?, modified: Created?, meetingTime: String?, iAmHost: Bool?, requestedByDetails: RequestedDetails?) {
        self.id = id
        self.summitEventsID = summitEventsID
        self.requestedBy = requestedBy
        self.meetingDate = meetingDate
        self.meetingStartTime = meetingStartTime
        self.meetingEndTime = meetingEndTime
        self.meetingVenue = meetingVenue
        self.notes = notes
        self.status = status
        self.created = created
        self.modified = modified
        self.meetingTime = meetingTime
        self.iAmHost = iAmHost
        self.requestedByDetails = requestedByDetails
    }
}

enum Created: String, Codable {
    case the00000000000000 = "0000-00-00 00:00:00"
}
