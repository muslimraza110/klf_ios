
import Foundation
import UIKit

class ContactModel {
    var userID, userRole, userEmail, userPassword: String?
    var userTitle, userFirstName, userMiddileName, userLastName: String?
    var userGender, userStatus, userChairPerson, userChatStatus: String?
    var userLastOnline, userLastLogin, userExpirationDate, userPending: String?
    var userVoteDecision, userVoteDecisionDate, userVoteAdminDecision, userTerms: String?
    var userCreated, userModified, userResetKey, userDetailId: String?
    var userDetailUserId, userDetailTitle, userDetailDescription, userDetailBio: String?
    var userDetailPrivateBio, userDetailAddress1, userDetailAddress2, userDetailCity: String?
    var userDetailRegion, userDetailCountry, userDetailPostalCode, userDetailEmail2: String?
    var userDetailWebsite, userDetailFacebook, userDetailLinkedin, userDetailPhone1: String?
    var userDetailPhone2, userDetailCreated, userDetailModified, userDetailStatus: String?
    var profileImgID, profileImgModel, profileImgModelID, profileImgAssociation: String?
    var profileImgName, profileImgAlt, profileImgType, profileImgSize: String?
    var profileImgWidth, profileImgHeight, profileImgCropX, profileImgCropY: String?
    var profileImgCropWidth, profileImgCropHeight, profileImgCreated, profileImgModified: String?
    var categoriesID, categoriesModel, categoriesName, categoriesDescription, profileImg: String?
    
    init() {}
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.userID = i["Users__id"] as? String
            self.userRole = i["Users__role"] as? String
            self.userEmail = i["Users__email"] as? String
            self.userPassword = i["Users__password"] as? String
            self.userTitle = i["Users__title"] as? String
            self.userFirstName = i["Users__first_name"] as? String
            self.userMiddileName = i["Users__middle_name"] as? String
            self.userLastName = i["Users__last_name"] as? String
            self.userGender = i["Users__gender"] as? String
            self.userStatus = i["Users__status"] as? String
            self.userChairPerson = i["Users__chair_person"] as? String
            self.userChatStatus = i["Users__chat_status"] as? String
            self.userLastOnline = i["Users__last_online"] as? String
            self.userLastLogin = i["Users__last_login"] as? String
            self.userExpirationDate = i["Users__expiration_date"] as? String
            self.userPending = i["Users__pending"] as? String
            self.userVoteDecision = i["Users__vote_decision"] as? String
            self.userVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.userVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.userTerms = i["Users__terms"] as? String
            self.userCreated = i["Users__created"] as? String
            self.userModified = i["Users__modified"] as? String
            self.userResetKey = i["Users__reset_key"] as? String
            self.userDetailId = i["UserDetails__id"] as? String
            self.userDetailUserId = i["UserDetails__user_id"] as? String
            self.userDetailTitle = i["UserDetails__title"] as? String
            self.userDetailDescription = i["UserDetails__description"] as? String
            self.userDetailBio = i["UserDetails__bio"] as? String
            self.userDetailPrivateBio = i["UserDetails__private_bio"] as? String
            self.userDetailAddress1 = i["UserDetails__address"] as? String
            self.userDetailAddress2 = i["UserDetails__address2"] as? String
            self.userDetailCity = i["UserDetails__city"] as? String
            self.userDetailRegion = i["UserDetails__region"] as? String
            self.userDetailCountry = i["UserDetails__country"] as? String
            self.userDetailPostalCode = i["UserDetails__postal_code"] as? String
            self.userDetailEmail2 = i["UserDetails__email2"] as? String
            self.userDetailWebsite = i["UserDetails__website"] as? String
            self.userDetailFacebook = i["UserDetails__facebook"] as? String
            self.userDetailLinkedin = i["UserDetails__linkedin"] as? String
            self.userDetailPhone1 = i["UserDetails__phone"] as? String
            self.userDetailPhone2 = i["UserDetails__phone2"] as? String
            self.userDetailCreated = i["UserDetails__created"] as? String
            self.userDetailModified = i["UserDetails__modified"] as? String
            self.userDetailStatus = i["UserDetails__status"] as? String
            self.profileImgID = i["ProfileImg__id"] as? String
            self.profileImgModel = i["ProfileImg__model"] as? String
            self.profileImgModelID = i["ProfileImg__model_id"] as? String
            self.profileImgAssociation = i["ProfileImg__association"] as? String
            self.profileImgName = i["ProfileImg__name"] as? String
            self.profileImgAlt = i["ProfileImg__alt"] as? String
            self.profileImgType = i["ProfileImg__type"] as? String
            self.profileImgSize = i["ProfileImg__size"] as? String
            self.profileImgWidth = i["ProfileImg__width"] as? String
            self.profileImgHeight = i["ProfileImg__height"] as? String
            self.profileImgCropX = i["ProfileImg__crop_x"] as? String
            self.profileImgCropY = i["ProfileImg__crop_y"] as? String
            self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
            self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
            self.profileImgCreated = i["ProfileImg__created"] as? String
            self.profileImgModified = i["ProfileImg__modified"] as? String
            self.profileImg = i["profileimg"] as? String

        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

