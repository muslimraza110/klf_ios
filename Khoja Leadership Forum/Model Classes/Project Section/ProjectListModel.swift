
import Foundation
class ProjectListModel {
    var projectsID: String?
    var projectsCreatorID: String?
    var projectsKhojaCareCategoryID: String?
    var projectsName: String?
    var projectsWebsite: String?
    var projectsDescription: String?
    var projectsStartDate: String?
    var projectsEstCompletionDate: String?
    var projectsFundraisingTarget: String?
    var projectsCity: String?
    var projectsRegion: String?
    var projectsCountry: String?
    var projectsApprovalStamp: String?
    var projectsStatus: String?
    var projectsCreated: String?
    var projectsModified: String?
    var creatorsID: String?
    var creatorsRole: String?
    var creatorsEmail: String?
    var creatorsPassword: String?
    var creatorsTitle: String?
    var creatorsFirstName: String?
    var creatorsMiddleName: String?
    var creatorsLastName: String?
    var creatorsGender: String?
    var creatorsStatus: String?
    var creatorsChairPerson: String?
    var creatorsChatStatus: String?
    var creatorsLastOnline: String?
    var creatorsLastLogin: String?
    var creatorsExpirationDate: String?
    var creatorsPending: String?
    var creatorsVoteDecision: String?
    var creatorsVoteDecisionDate: String?
    var creatorsVoteAdminDecision: String?
    var creatorsTerms: String?
    var creatorsCreated: String?
    var creatorsModified: String?
    var creatorsResetKey: String?
    var charityType = CharityType()
    var targetAudience = CharityType()
    var targetAnnualSpend = CharityType()
    
    init() {
    }
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.projectsID = i["Projects__id"] as? String
            self.projectsCreatorID = i["Projects__creator_id"] as? String
            self.projectsKhojaCareCategoryID = i["Projects__khoja_care_category_id"] as? String
            self.projectsName = i["Projects__name"] as? String
            self.projectsWebsite = i["Projects__website"] as? String
            self.projectsDescription = i["Projects__description"] as? String
            self.projectsStartDate = i["Projects__start_date"] as? String
            self.projectsEstCompletionDate = i["Projects__est_completion_date"] as? String
            self.projectsFundraisingTarget = i["Projects__fundraising_target"] as? String
            self.projectsCity = i["Projects__city"] as? String
            self.projectsRegion = i["Projects__region"] as? String
            self.projectsCountry = i["Projects__country"] as? String
            self.projectsApprovalStamp = i["Projects__approval_stamp"] as? String
            self.projectsStatus = i["Projects__status"] as? String
            self.projectsCreated = i["Projects__created"] as? String
            self.projectsModified = i["Projects__modified"] as? String
            self.creatorsID = i["Creators__id"] as? String
            self.creatorsRole = i["Creators__role"] as? String
            self.creatorsEmail = i["Creators__email"] as? String
            self.creatorsPassword = i["Creators__password"] as? String
            self.creatorsTitle = i["Creators__title"] as? String
            self.creatorsFirstName = i["Creators__first_name"] as? String
            self.creatorsMiddleName = i["Creators__middle_name"] as? String
            self.creatorsLastName = i["Creators__last_name"] as? String
            self.creatorsGender = i["Creators__gender"] as? String
            self.creatorsStatus = i["Creators__status"] as? String
            self.creatorsChairPerson = i["Creators__chair_person"] as? String
            self.creatorsChatStatus = i["Creators__chat_status"] as? String
            self.creatorsLastOnline = i["Creators__last_online"] as? String
            self.creatorsLastLogin = i["Creators__last_login"] as? String
            self.creatorsExpirationDate = i["Creators__expiration_date"] as? String
            self.creatorsPending = i["Creators__pending"] as? String
            self.creatorsVoteDecision = i["Creators__vote_decision"] as? String
            self.creatorsVoteDecisionDate = i["Creators__vote_decision_date"] as? String
            self.creatorsVoteAdminDecision = i["Creators__vote_admin_decision"] as? String
            self.creatorsTerms = i["Creators__terms"] as? String
            self.creatorsCreated = i["Creators__created"] as? String
            self.creatorsModified = i["Creators__modified"] as? String
            self.creatorsResetKey = i["Creators__reset_key"] as? String
            let charityData = i["charity_type"] as? NSDictionary
            charityType.id = charityData?["id"] as? String
            charityType.name = charityData?["name"] as? String
            charityType.projectID = charityData?["project_id"] as? String
            let targetAudianceData = i["target_audience"] as? NSDictionary
            targetAudience.id = targetAudianceData?["id"] as? String
            targetAudience.name = targetAudianceData?["name"] as? String
            targetAudience.projectID = targetAudianceData?["project_id"] as? String
            let targetAnnualData = i["target_annual_spend"] as? NSDictionary
            targetAnnualSpend.id = targetAnnualData?["id"] as? String
            targetAnnualSpend.name = targetAnnualData?["name"] as? String
            targetAnnualSpend.projectID = targetAnnualData?["project_id"] as? String

        }
    }
/*-----------------------------------------------------------------------------------------------------*/    
}

class CharityType {
    var id: String?
    var name: String?
    var projectID: String?
    
    init(){
    }
/*-----------------------------------------------------------------------------------------------------*/
    init(data:NSDictionary) {
        self.id = data["id"] as? String
        self.name = data["name"] as? String
        self.projectID = data["project_id"] as? String
    }
/*-----------------------------------------------------------------------------------------------------*/
}
