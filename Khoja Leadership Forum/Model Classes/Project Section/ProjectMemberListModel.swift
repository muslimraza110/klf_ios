
import Foundation
class ProjectMemberListModel {
    var role, title, firstName, middleName: String?
    var lastName, gender, chairPerson, id: String?
    var email: String?
    var projectID: String?
    var ismember: Int?
    var profileImage: String?
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.role = i["role"] as? String
            self.title = i["title"] as? String
            self.firstName = i["first_name"] as? String
            self.middleName = i["middle_name"] as? String
            self.lastName = i["last_name"] as? String
            self.gender = i["gender"] as? String
            self.chairPerson = i["chair_person"] as? String
            self.id = i["id"] as? String
            self.email = i["email"] as? String
            self.projectID = i["project_id"] as? String
            self.ismember = i["Ismember"] as? Int
            self.profileImage = i["profile_image"] as? String
            
        }
        
    }
}


/*-----------------------------------------------------------------------------------------------------*/

// MARK: - ProfileImage
class ProfileImage {
    var profileImgID, profileImgAssociation, profileImgName, profileImgAlt: String?
    var profileImgType, profileImgSize, profileImgWidth, profileImgHeight: String?
    var profileImgCropX, profileImgCropY, profileImgCropWidth, profileImgCropHeight: String?
    var profileImgCreated, profileImgModified: String?
    
    init(data:[[String:Any]]) {
        for i in data {
            self.profileImgID = i["ProfileImg__id"] as? String
            self.profileImgAssociation = i["ProfileImg__association"] as? String
            self.profileImgName = i["ProfileImg__name"] as? String
            self.profileImgAlt = i["ProfileImg__alt"] as? String
            self.profileImgType = i["ProfileImg__type"] as? String
            self.profileImgSize = i["ProfileImg__size"] as? String
            self.profileImgWidth = i["ProfileImg__width"] as? String
            self.profileImgHeight = i["ProfileImg__height"] as? String
            self.profileImgCropX = i["ProfileImg__crop_x"] as? String
            self.profileImgCropY = i["ProfileImg__crop_y"] as? String
            self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
            self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
            self.profileImgCreated = i["ProfileImg__created"] as? String
            self.profileImgModified = i["ProfileImg__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

