
import Foundation
class ProjectNotesModel {
    var projectNotesID: String?
    var projectNotesProjectID: String?
    var projectNotesUserID: String?
    var projectNotesNote: String?
    var projectNotesStatus: String?
    var projectNotesCreated: String?
    var usersID: String?
    var usersRole: String?
    var usersEmail: String?
    var usersPassword: String?
    var usersTitle: String?
    var usersFirstName: String?
    var usersMiddleName: String?
    var usersLastName: String?
    var usersGender: String?
    var usersStatus: String?
    var usersChairPerson: String?
    var usersChatStatus: String?
    var usersLastOnline: String?
    var usersLastLogin: String?
    var usersExpirationDate: String?
    var usersPending: String?
    var usersVoteDecision: String?
    var usersVoteDecisionDate: String?
    var usersVoteAdminDecision: String?
    var usersTerms: String?
    var usersCreated: String?
    var usersModified: String?
    var usersResetKey: String?
    var profileImgID: String?
    var profileImgModel: String?
    var profileImgModelID: String?
    var profileImgAssociation: String?
    var profileImgName: String?
    var profileImgAlt: String?
    var profileImgType: String?
    var profileImgSize: String?
    var profileImgWidth: String?
    var profileImgHeight: String?
    var profileImgCropX: String?
    var profileImgCropY: String?
    var profileImgCropWidth: String?
    var profileImgCropHeight: String?
    var profileImgCreated: String?
    var profileImgModified: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.projectNotesID = i["ProjectNotes__id"] as? String
            self.projectNotesProjectID = i["ProjectNotes__project_id"] as? String
            self.projectNotesUserID = i["ProjectNotes__user_id"] as? String
            self.projectNotesNote = i["ProjectNotes__note"] as? String
            self.projectNotesStatus = i["ProjectNotes__status"] as? String
            self.projectNotesCreated = i["ProjectNotes__created"] as? String
            self.usersID = i["Users__id"] as? String
            self.usersRole = i["Users__role"] as? String
            self.usersEmail = i["Users__email"] as? String
            self.usersPassword = i["Users__password"] as? String
            self.usersTitle = i["Users__title"] as? String
            self.usersFirstName = i["Users__first_name"] as? String
            self.usersMiddleName = i["Users__middle_name"] as? String
            self.usersLastName = i["Users__last_name"] as? String
            self.usersGender = i["Users__gender"] as? String
            self.usersStatus = i["Users__status"] as? String
            self.usersChairPerson = i["Users__chair_person"] as? String
            self.usersChatStatus = i["Users__chat_status"] as? String
            self.usersLastOnline = i["Users__last_online"] as? String
            self.usersLastLogin = i["Users__last_login"] as? String
            self.usersExpirationDate = i["Users__expiration_date"] as? String
            self.usersPending = i["Users__pending"] as? String
            self.usersVoteDecision = i["Users__vote_decision"] as? String
            self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.usersTerms = i["Users__terms"] as? String
            self.usersCreated = i["Users__created"] as? String
            self.usersModified = i["Users__modified"] as? String
            self.usersResetKey = i["Users__reset_key"] as? String
            self.profileImgID = i["ProfileImg__id"] as? String
            self.profileImgModel = i["ProfileImg__model"] as? String
            self.profileImgModelID = i["ProfileImg__model_id"] as? String
            self.profileImgAssociation = i["ProfileImg__association"] as? String
            self.profileImgName = i["ProfileImg__name"] as? String
            self.profileImgAlt = i["ProfileImg__alt"] as? String
            self.profileImgType = i["ProfileImg__type"] as? String
            self.profileImgSize = i["ProfileImg__size"] as? String
            self.profileImgWidth = i["ProfileImg__width"] as? String
            self.profileImgHeight = i["ProfileImg__height"] as? String
            self.profileImgCropX = i["ProfileImg__crop_x"] as? String
            self.profileImgCropY = i["ProfileImg__crop_y"] as? String
            self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
            self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
            self.profileImgCreated = i["ProfileImg__created"] as? String
            self.profileImgModified = i["ProfileImg__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
