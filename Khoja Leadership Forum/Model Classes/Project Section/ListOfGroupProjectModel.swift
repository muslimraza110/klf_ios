
import Foundation
class ListOfGroupProjectModel {
    var id, name: String?
    var projectID, groupID, profileImg: String?
    var isSelected: Int?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
        self.id = i["id"] as? String
        self.name = i["name"] as? String
        self.groupID = i["group_id"] as? String
        self.isSelected = i["IsSelected"] as? Int
        self.profileImg = i["profileimg"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
