
import Foundation
class ProjectInfoModel {
    var projectsID: String?
    var projectsCreatorID: String?
    var projectsKhojaCareCategoryID: String?
    var projectsName: String?
    var projectsWebsite: String?
    var projectsDescription: String?
    var projectsStartDate: String?
    var projectsEstCompletionDate: String?
    var projectsFundraisingTarget: String?
    var projectsCity: String?
    var projectsRegion: String?
    var projectsCountry: String?
    var projectsApprovalStamp: String?
    var projectsStatus: String?
    var projectsCreated: String?
    var projectsModified: String?
    init() {
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:NSDictionary) {
            self.projectsID = data["Projects__id"] as? String
            self.projectsCreatorID = data["Projects__creator_id"] as? String
            self.projectsKhojaCareCategoryID = data["Projects__khoja_care_category_id"] as? String
            self.projectsName = data["Projects__name"] as? String
            self.projectsWebsite = data["Projects__website"] as? String
            self.projectsDescription = data["Projects__description"] as? String
            self.projectsStartDate = data["Projects__start_date"] as? String
            self.projectsEstCompletionDate = data["Projects__est_completion_date"] as? String
            self.projectsFundraisingTarget = data["Projects__fundraising_target"] as? String
            self.projectsCity = data["Projects__city"] as? String
            self.projectsRegion = data["Projects__region"] as? String
            self.projectsCountry = data["Projects__country"] as? String
            self.projectsApprovalStamp = data["Projects__approval_stamp"] as? String
            self.projectsStatus = data["Projects__status"] as? String
            self.projectsCreated = data["Projects__created"] as? String
            self.projectsModified = data["Projects__modified"] as? String
      
        
    }
/*-----------------------------------------------------------------------------------------------------*/
}
