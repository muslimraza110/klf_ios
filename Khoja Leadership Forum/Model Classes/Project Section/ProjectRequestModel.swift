
import Foundation
class ProjectRequestModel {
    var projectRequestsID: String?
    var projectRequestsProjectID: String?
    var projectRequestsUserID: String?
    var projectRequestsStatus: String?
    var projectRequestsCreated: String?
    var projectRequestsModified: String?

/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.projectRequestsID = i["ProjectRequests__id"] as? String
            self.projectRequestsProjectID = i["ProjectRequests__project_id"] as? String
            self.projectRequestsUserID = i["ProjectRequests__user_id"] as? String
            self.projectRequestsStatus = i["ProjectRequests__status"] as? String
            self.projectRequestsCreated = i["ProjectRequests__created"] as? String
            self.projectRequestsModified = i["ProjectRequests__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

