
import Foundation
class CharityModel {
    
    var tagsID, tagsParentID, tagsLft, tagsRght: String?
    var tagsLevel, tagsType, tagsName, tagsOrderNum: String?
    var tagsMandatory, tagsStatus, tagsCreated, tagsModified: String?

/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
        self.tagsID = i["Tags__id"] as? String
        self.tagsParentID = i["Tags__parent_id"] as? String
        self.tagsLft = i["Tags__lft"] as? String
        self.tagsRght = i["Tags__rght"] as? String
        self.tagsLevel = i["Tags__level"] as? String
        self.tagsType = i["Tags__type"] as? String
        self.tagsName = i["Tags__name"] as? String
        self.tagsOrderNum = i["Tags__order_num"] as? String
        self.tagsMandatory = i["Tags__mandatory"] as? String
        self.tagsStatus = i["Tags__status"] as? String
        self.tagsCreated = i["Tags__created"] as? String
        self.tagsModified = i["Tags__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

