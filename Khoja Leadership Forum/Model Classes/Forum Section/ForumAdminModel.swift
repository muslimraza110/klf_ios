
import Foundation
class MainScreenData {
    var subforumsID, subforumsUserID, subforumsModel, subforumsTitle: String?
    var subforumsDescription, subforumsStatus, subforumsCreated, subforumsModified: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
        self.subforumsID = i["Subforums__id"] as? String
        self.subforumsUserID = i["Subforums__user_id"] as? String
        self.subforumsModel = i["Subforums__model"] as? String
        self.subforumsTitle = i["Subforums__title"] as? String
        self.subforumsDescription = i["Subforums__description"] as? String
        self.subforumsStatus = i["Subforums__status"] as? String
        self.subforumsCreated = i["Subforums__created"] as? String
        self.subforumsModified = i["Subforums__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

