
import Foundation
class ModeratorList {
    var email, id, firstName, lastName: String?
    var isModerator: Int?
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data{
        self.email = i["email"] as? String
        self.id = i["id"] as? String
        self.isModerator = i["is_moderator"] as? Int
        self.firstName = i["first_name"] as? String
        self.lastName = i["last_name"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
