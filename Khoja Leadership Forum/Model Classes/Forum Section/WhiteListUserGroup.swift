
import Foundation
class WhiteListUserGroup {
    var id, firstName: String?
    var iswhitelisted:Int?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.id = i["id"] as? String
            self.iswhitelisted = i["iswhitelisted"] as? Int
            self.firstName = i["name"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

