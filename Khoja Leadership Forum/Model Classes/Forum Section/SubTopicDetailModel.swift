
import Foundation

// MARK: - Datum
class SubTopicDetailModel{
    var comments, pendingcomments, likes, dislike, content: String?
    var forumPostsModified, id: String?
    var particptatinguser: Int?
    var forumPostsID, forumPostsUserID: String?
    var forumPostsParentID: NSNull?
    var forumPostsSubforumID, forumPostsLft, forumPostsRght, forumPostsTopic: String?
    var forumPostsPending, forumPostsModerated, forumPostsPinned, forumPostsSod: String?
    var forumPostsNotifyUsers: String?
    var forumPostsVisibility: String?
    var forumPostsPostVisibility, forumPostsStatus, forumPostsEblast, forumPostsCreated: String?
    var usersID, usersRole, usersEmail, usersPassword: String?
    var usersTitle, usersFirstName, usersMiddleName, usersLastName: String?
    var usersGender, usersStatus, usersChairPerson, usersChatStatus: String?
    var usersLastOnline: NSNull?
    var usersLastLogin: String?
    var usersExpirationDate: NSNull?
    var usersPending, usersVoteDecision, usersVoteDecisionDate, usersVoteAdminDecision: String?
    var usersTerms, usersCreated, usersModified, usersResetKey: String?
    var profileImgID, profileImgModel, profileImgModelID, profileImgAssociation: String?
    var profileImgName, profileImgAlt, profileImgType, profileImgSize: String?
    var profileImgWidth, profileImgHeight, profileImgCropX, profileImgCropY: String?
    var profileImgCropWidth, profileImgCropHeight, profileImgCreated, profileImgModified,postApprove: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.comments = i["comments"] as? String
            self.pendingcomments = i["pendingcomments"] as? String
            self.likes = i["likes"] as? String
            self.dislike = i["dislike"] as? String
            self.content = i["content"] as? String
            self.forumPostsModified = i["ForumPosts__modified"] as? String
            self.id = i["id"] as? String
            self.particptatinguser = i["particptatinguser"] as? Int
            self.forumPostsID = i["ForumPosts__id"] as? String
            self.forumPostsUserID = i["ForumPosts__user_id"] as? String
            self.forumPostsParentID = i["ForumPosts__parent_id"] as? NSNull
            self.forumPostsSubforumID = i["ForumPosts__subforum_id"] as? String
            self.forumPostsLft = i["ForumPosts__lft"] as? String
            self.forumPostsRght = i["ForumPosts__rght"] as? String
            self.forumPostsTopic = i["ForumPosts__topic"] as? String
            self.forumPostsPending = i["ForumPosts__pending"] as? String
            self.forumPostsModerated = i["ForumPosts__moderated"] as? String
            self.forumPostsPinned = i["ForumPosts__pinned"] as? String
            self.forumPostsSod = i["ForumPosts__sod"] as? String
            self.forumPostsNotifyUsers = i["ForumPosts__notify_users"] as? String
            self.forumPostsVisibility = i["ForumPosts__visibility"] as? String
            self.forumPostsPostVisibility = i["ForumPosts__post_visibility"] as? String
            self.forumPostsStatus = i["ForumPosts__status"] as? String
            self.forumPostsEblast = i["ForumPosts__eblast"] as? String
            self.forumPostsCreated = i["ForumPosts__created"] as? String
            self.usersID = i["Users__id"] as? String
            self.usersRole = i["Users__role"] as? String
            self.usersEmail = i["Users__email"] as? String
            self.usersPassword = i["Users__password"] as? String
            self.usersTitle = i["Users__title"] as? String
            self.usersFirstName = i["Users__first_name"] as? String
            self.usersMiddleName = i["Users__middle_name"] as? String
            self.usersLastName = i["Users__last_name"] as? String
            self.usersGender = i["Users__gender"] as? String
            self.usersStatus = i["Users__status"] as? String
            self.usersChairPerson = i["Users__chair_person"] as? String
            self.usersChatStatus = i["Users__chat_status"] as? String
            self.usersLastOnline = i["Users__last_online"]as? NSNull
            self.usersLastLogin = i["Users__last_login"] as? String
            self.usersExpirationDate = i["Users__expiration_date"]as? NSNull
            self.usersPending = i["Users__pending"] as? String
            self.usersVoteDecision = i["Users__vote_decision"] as? String
            self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.usersTerms = i["Users__terms"] as? String
            self.usersCreated = i["Users__created"] as? String
            self.usersModified = i["Users__modified"] as? String
            self.usersResetKey = i["Users__reset_key"] as? String
            self.profileImgID = i["ProfileImg__id"] as? String
            self.profileImgModel = i["ProfileImg__model"] as? String
            self.profileImgModelID = i["ProfileImg__model_id"] as? String
            self.profileImgAssociation = i["ProfileImg__association"] as? String
            self.profileImgName = i["ProfileImg__name"] as? String
            self.profileImgAlt = i["ProfileImg__alt"] as? String
            self.profileImgType = i["ProfileImg__type"] as? String
            self.profileImgSize = i["ProfileImg__size"] as? String
            self.profileImgWidth = i["ProfileImg__width"] as? String
            self.profileImgHeight = i["ProfileImg__height"] as? String
            self.profileImgCropX = i["ProfileImg__crop_x"] as? String
            self.profileImgCropY = i["ProfileImg__crop_y"] as? String
            self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
            self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
            self.profileImgCreated = i["ProfileImg__created"] as? String
            self.profileImgModified = i["ProfileImg__modified"] as? String
            self.postApprove = i["post_approved_post"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

