
import Foundation
class whiteList {
    var role, id, title, firstName: String?
    var middleName, lastName, email, modelID: String?
    var model, forumPostID: String?
    var iswhitelisted: Int?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.role = i["role"] as? String
            self.id = i["id"] as? String
            self.title = i["title"] as? String
            self.firstName = i["first_name"] as? String
            self.middleName = i["middle_name"] as? String
            self.lastName = i["last_name"] as? String
            self.email = i["email"] as? String
            self.modelID = i["model_id"] as? String
            self.model = i["model"] as? String
            self.forumPostID = i["forum_post_id"] as? String
            self.iswhitelisted = i["iswhitelisted"] as? Int
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}

