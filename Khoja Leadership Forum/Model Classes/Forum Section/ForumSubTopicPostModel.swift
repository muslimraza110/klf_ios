
import Foundation

class ForumSubTopicPostModel {
    
    var forumPostsID, forumPostsUserID: String?
    var forumPostsParentID: String?
    var forumPostsSubforumID, forumPostsLft, forumPostsRght, forumPostsTopic: String?
    var forumPostsContent, forumPostsPending, forumPostsModerated, forumPostsPinned: String?
    var forumPostsSod, forumPostsNotifyUsers: String?
    var forumPostsVisibility: String?
    var forumPostsPostVisibility, forumPostsStatus, forumPostsEblast, forumPostsCreated: String?
    var forumPostsModified, usersID, usersRole, usersEmail: String?
    var usersPassword, usersTitle, usersFirstName, usersMiddleName: String?
    var usersLastName, usersGender, usersStatus, usersChairPerson: String?
    var usersChatStatus: String?
    var usersLastOnline: NSNull?
    var usersLastLogin: String?
    var usersExpirationDate: NSNull?
    var usersPending, usersVoteDecision, usersVoteDecisionDate, usersVoteAdminDecision: String?
    var usersTerms, usersCreated, usersModified, usersResetKey: String?
    var profileImgID, profileImgModel, profileImgModelID, profileImgAssociation: String?
    var profileImgName: String?
    var profileImgAlt, profileImgType, profileImgSize, profileImgWidth: String?
    var profileImgHeight, profileImgCropX, profileImgCropY, profileImgCropWidth: String?
    var profileImgCropHeight, profileImgCreated, profileImgModified, posts: String?
    var likescount, dislikescount, islike, isdislike: Int?
    var frequency: NSNull?
    var comments = [Comments]()
    var isModerator, pageNo, totalNoOfPages : Int?
    
/*-----------------------------------------------------------------------------------------------------*/
    init() {
        
    }
    
    init(i:NSDictionary) {
        self.forumPostsID = i["ForumPosts__id"] as? String
        self.forumPostsUserID = i["ForumPosts__user_id"] as? String
        self.forumPostsParentID = i["ForumPosts__parent_id"] as? String
        self.forumPostsSubforumID = i["ForumPosts__subforum_id"] as? String
        self.forumPostsLft = i["ForumPosts__lft"] as? String
        self.forumPostsRght = i["ForumPosts__rght"] as? String
        self.forumPostsTopic = i["ForumPosts__topic"] as? String
        self.forumPostsContent = i["ForumPosts__content"] as? String
        self.forumPostsPending = i["ForumPosts__pending"] as? String
        self.forumPostsModerated = i["ForumPosts__moderated"] as? String
        self.forumPostsPinned = i["ForumPosts__pinned"] as? String
        self.forumPostsSod = i["ForumPosts__sod"] as? String
        self.forumPostsNotifyUsers = i["ForumPosts__notify_users"] as? String
        self.forumPostsVisibility = i["ForumPosts__visibility"] as? String
        self.forumPostsPostVisibility = i["ForumPosts__post_visibility"] as? String
        self.forumPostsStatus = i["ForumPosts__status"] as? String
        self.forumPostsEblast = i["ForumPosts__eblast"] as? String
        self.forumPostsCreated = i["ForumPosts__created"] as? String
        self.forumPostsModified = i["ForumPosts__modified"] as? String
        self.usersID = i["Users__id"] as? String
        self.usersRole = i["Users__role"] as? String
        self.usersEmail = i["Users__email"] as? String
        self.usersPassword = i["Users__password"] as? String
        self.usersTitle = i["Users__title"] as? String
        self.usersFirstName = i["Users__first_name"] as? String
        self.usersMiddleName = i["Users__middle_name"] as? String
        self.usersLastName = i["Users__last_name"] as? String
        self.usersGender = i["Users__gender"] as? String
        self.usersStatus = i["Users__status"] as? String
        self.usersChairPerson = i["Users__chair_person"] as? String
        self.usersChatStatus = i["Users__chat_status"] as? String
        self.usersLastOnline = i["Users__last_online"] as? NSNull
        self.usersLastLogin = i["Users__last_login"] as? String
        self.usersExpirationDate = i["Users__expiration_date"] as? NSNull
        self.usersPending = i["Users__pending"] as? String
        self.usersVoteDecision = i["Users__vote_decision"] as? String
        self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
        self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
        self.usersTerms = i["Users__terms"] as? String
        self.usersCreated = i["Users__created"] as? String
        self.usersModified = i["Users__modified"] as? String
        self.usersResetKey = i["Users__reset_key"] as? String
        self.profileImgID = i["ProfileImg__id"] as? String
        self.profileImgModel = i["ProfileImg__model"] as? String
        self.profileImgModelID = i["ProfileImg__model_id"] as? String
        self.profileImgAssociation = i["ProfileImg__association"] as? String
        self.profileImgName = i["ProfileImg__name"] as? String
        self.profileImgAlt = i["ProfileImg__alt"] as? String
        self.profileImgType = i["ProfileImg__type"] as? String
        self.profileImgSize = i["ProfileImg__size"] as? String
        self.profileImgWidth = i["ProfileImg__width"] as? String
        self.profileImgHeight = i["ProfileImg__height"] as? String
        self.profileImgCropX = i["ProfileImg__crop_x"] as? String
        self.profileImgCropY = i["ProfileImg__crop_y"] as? String
        self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
        self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
        self.profileImgCreated = i["ProfileImg__created"] as? String
        self.profileImgModified = i["ProfileImg__modified"] as? String
        self.posts = i["Posts"] as? String
        self.likescount = i["likescount"] as? Int
        self.dislikescount = i["dislikescount"] as? Int
        self.islike = i["islike"] as? Int
        self.isdislike = i["isdislike"] as? Int
        self.frequency = i["frequency"] as? NSNull
        self.isModerator = i["is_moderator"] as? Int
        self.pageNo = i["page_no"] as? Int
        self.totalNoOfPages = i["Total_NoOfPages"] as? Int
        if let comment = i["comments"] as? [[String:Any]] {
            for i in comment {
                comments.append(Comments(data: [i]))
            }
            
        }
    }
}


class Comments {
    
    var forumPostsID, forumPostsUserID: String?
    var forumPostsParentID: String?
    var forumPostsSubforumID, forumPostsLft, forumPostsRght, forumPostsTopic: String?
    var forumPostsContent, forumPostsPending, forumPostsModerated, forumPostsPinned: String?
    var forumPostsSod, forumPostsNotifyUsers: String?
    var forumPostsVisibility: NSNull?
    var forumPostsPostVisibility, forumPostsStatus, forumPostsEblast, forumPostsCreated: String?
    var forumPostsModified, usersID, usersRole, usersEmail: String?
    var usersPassword, usersTitle, usersFirstName, usersMiddleName: String?
    var usersLastName, usersGender, usersStatus, usersChairPerson: String?
    var usersChatStatus: String?
    var usersLastOnline: NSNull?
    var usersLastLogin: String?
    var usersExpirationDate: NSNull?
    var usersPending, usersVoteDecision, usersVoteDecisionDate, usersVoteAdminDecision: String?
    var usersTerms, usersCreated, usersModified, usersResetKey: String?
    var profileImgID, profileImgModel, profileImgModelID, profileImgAssociation: String?
    var profileImgName: String?
    var profileImgAlt, profileImgType, profileImgSize, profileImgWidth: String?
    var profileImgHeight, profileImgCropX, profileImgCropY, profileImgCropWidth: String?
    var profileImgCropHeight, profileImgCreated, profileImgModified, posts: String?
    var likescount, dislikescount, islike, isdislike: Int?
    var frequency: NSNull?
    
    /*-----------------------------------------------------------------------------------------------------*/
    
    init(data:[[String:Any]]) {
        for i in data {
            self.forumPostsID = i["ForumPosts__id"] as? String
            self.forumPostsUserID = i["ForumPosts__user_id"] as? String
            self.forumPostsParentID = i["ForumPosts__parent_id"] as? String
            self.forumPostsSubforumID = i["ForumPosts__subforum_id"] as? String
            self.forumPostsLft = i["ForumPosts__lft"] as? String
            self.forumPostsRght = i["ForumPosts__rght"] as? String
            self.forumPostsTopic = i["ForumPosts__topic"] as? String
            self.forumPostsContent = i["ForumPosts__content"] as? String
            self.forumPostsPending = i["ForumPosts__pending"] as? String
            self.forumPostsModerated = i["ForumPosts__moderated"] as? String
            self.forumPostsPinned = i["ForumPosts__pinned"] as? String
            self.forumPostsSod = i["ForumPosts__sod"] as? String
            self.forumPostsNotifyUsers = i["ForumPosts__notify_users"] as? String
            self.forumPostsVisibility = i["ForumPosts__visibility"] as? NSNull
            self.forumPostsPostVisibility = i["ForumPosts__post_visibility"] as? String
            self.forumPostsStatus = i["ForumPosts__status"] as? String
            self.forumPostsEblast = i["ForumPosts__eblast"] as? String
            self.forumPostsCreated = i["ForumPosts__created"] as? String
            self.forumPostsModified = i["ForumPosts__modified"] as? String
            self.usersID = i["Users__id"] as? String
            self.usersRole = i["Users__role"] as? String
            self.usersEmail = i["Users__email"] as? String
            self.usersPassword = i["Users__password"] as? String
            self.usersTitle = i["Users__title"] as? String
            self.usersFirstName = i["Users__first_name"] as? String
            self.usersMiddleName = i["Users__middle_name"] as? String
            self.usersLastName = i["Users__last_name"] as? String
            self.usersGender = i["Users__gender"] as? String
            self.usersStatus = i["Users__status"] as? String
            self.usersChairPerson = i["Users__chair_person"] as? String
            self.usersChatStatus = i["Users__chat_status"] as? String
            self.usersLastOnline = i["Users__last_online"] as? NSNull
            self.usersLastLogin = i["Users__last_login"] as? String
            self.usersExpirationDate = i["Users__expiration_date"] as? NSNull
            self.usersPending = i["Users__pending"] as? String
            self.usersVoteDecision = i["Users__vote_decision"] as? String
            self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.usersTerms = i["Users__terms"] as? String
            self.usersCreated = i["Users__created"] as? String
            self.usersModified = i["Users__modified"] as? String
            self.usersResetKey = i["Users__reset_key"] as? String
            self.profileImgID = i["ProfileImg__id"] as? String
            self.profileImgModel = i["ProfileImg__model"] as? String
            self.profileImgModelID = i["ProfileImg__model_id"] as? String
            self.profileImgAssociation = i["ProfileImg__association"] as? String
            self.profileImgName = i["ProfileImg__name"] as? String
            self.profileImgAlt = i["ProfileImg__alt"] as? String
            self.profileImgType = i["ProfileImg__type"] as? String
            self.profileImgSize = i["ProfileImg__size"] as? String
            self.profileImgWidth = i["ProfileImg__width"] as? String
            self.profileImgHeight = i["ProfileImg__height"] as? String
            self.profileImgCropX = i["ProfileImg__crop_x"] as? String
            self.profileImgCropY = i["ProfileImg__crop_y"] as? String
            self.profileImgCropWidth = i["ProfileImg__crop_width"] as? String
            self.profileImgCropHeight = i["ProfileImg__crop_height"] as? String
            self.profileImgCreated = i["ProfileImg__created"] as? String
            self.profileImgModified = i["ProfileImg__modified"] as? String
            self.posts = i["Posts"] as? String
            self.likescount = i["likescount"] as? Int
            self.dislikescount = i["dislikescount"] as? Int
            self.islike = i["islike"] as? Int
            self.isdislike = i["isdislike"] as? Int
            self.frequency = i["frequency"] as? NSNull
            
        }
    }
    
}
