

import Foundation
class RecipientDetailModel {
    var email: String?
    var title: String?
    var userID: String?
    var firstName: String?
    var lastName: String?
    var profileURL: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.email = i["Email"] as? String
            self.title = i["title"] as? String
            self.userID = i["UserID"] as? String
            self.firstName = i["FirstName"] as? String
            self.lastName = i["LastName"] as? String
            self.profileURL = i["ProfileURL"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
