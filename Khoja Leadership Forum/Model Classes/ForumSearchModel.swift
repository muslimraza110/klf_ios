//
//  ForumSearchModel.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 17/10/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import Foundation
class ForumSearchModel {
    
    var forumPostId, forumTopic, forumContent, postApprovePost : String?
    
    init(data:[[String:Any]]) {
        for i in data {
            forumPostId = i["Forum__id"] as? String
            forumTopic = i["Forum__topic"] as? String
            forumContent = i["Forum__content"] as? String
            postApprovePost = i["post_approved_post"] as? String
        }
    }
    
    
}
