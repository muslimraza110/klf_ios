

import Foundation
class InitiativeListModel {
    var initiativesID, initiativesUserID, initiativesName, initiativesDescription: String?
    var initiativesShowOnDashboard, initiativesBroadcast: String?
    var initiativesEmailSubject: String?
    var initiativesEmailBroadcast: String?
    var initiativesEmailBody: String?
    var initiativesStatus, initiativesCreated, initiativesModified, usersID: String?
    var usersRole, usersEmail, usersPassword, usersTitle: String?
    var usersFirstName, usersMiddleName, usersLastName, usersGender: String?
    var usersStatus, usersChairPerson, usersChatStatus: String?
    var usersLastOnline: String?
    var usersLastLogin: String?
    var usersExpirationDate: String?
    var usersPending, usersVoteDecision, usersVoteDecisionDate, usersVoteAdminDecision: String?
    var usersTerms, usersCreated, usersModified, usersResetKey: String?
    var blocksHaveBeenPledged, blocksNeededToReachFundraisingTarget: Int?
    var maximumBlocksYouCanPledgeIs, pledge1_Block: String?
    var ammount, fundraisingsAmmount, amountCollected: String?
    
    init() {
    }
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.initiativesID = i["Initiatives__id"] as? String
            self.initiativesUserID = i["Initiatives__user_id"] as? String
            self.initiativesName = i["Initiatives__name"] as? String
            self.initiativesDescription = i["Initiatives__description"] as? String
            self.initiativesShowOnDashboard = i["Initiatives__show_on_dashboard"] as? String
            self.initiativesBroadcast = i["Initiatives__broadcast"] as? String
            self.initiativesEmailSubject = i["Initiatives__email_subject"] as? String
            self.initiativesEmailBroadcast = i["Initiatives__email_broadcast"] as? String
            self.initiativesEmailBody = i["Initiatives__email_body"] as? String
            self.initiativesStatus = i["Initiatives__status"] as? String
            self.initiativesCreated = i["Initiatives__created"] as? String
            self.initiativesModified = i["Initiatives__modified"] as? String
            self.usersID = i["Users__id"] as? String
            self.usersRole = i["Users__role"] as? String
            self.usersEmail = i["Users__email"] as? String
            self.usersPassword = i["Users__password"] as? String
            self.usersTitle = i["Users__title"] as? String
            self.usersFirstName = i["Users__first_name"] as? String
            self.usersMiddleName = i["Users__middle_name"] as? String
            self.usersLastName = i["Users__last_name"] as? String
            self.usersGender = i["Users__gender"] as? String
            self.usersStatus = i["Users__status"] as? String
            self.usersChairPerson = i["Users__chair_person"] as? String
            self.usersChatStatus = i["Users__chat_status"] as? String
            self.usersLastOnline = i["Users__last_online"] as? String
            self.usersLastLogin = i["Users__last_login"] as? String
            self.usersExpirationDate = i["Users__expiration_date"] as? String
            self.usersPending = i["Users__pending"] as? String
            self.usersVoteDecision = i["Users__vote_decision"] as? String
            self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.usersTerms = i["Users__terms"] as? String
            self.usersCreated = i["Users__created"] as? String
            self.usersModified = i["Users__modified"] as? String
            self.usersResetKey = i["Users__reset_key"] as? String
            self.ammount = i["Amount"] as? String
            self.fundraisingsAmmount = i["Fundraisings__amount"] as? String
            self.amountCollected = i["amount_collected"] as? String

            
        }
       
    }
}

/*-----------------------------------------------------------------------------------------------------*/

// MARK: - AmountCollected
class AmountCollected {
    var fundraisingPledgesAmount, fundraisingPledgesFromAmount, fundraisingPledgesAmountPending, fundraisingPledgesAdminResponse: String?
    var fundraisingPledgesAnonymous, fundraisingPledgesHideAmount, fundraisingPledgesCreated, fundraisingPledgesModified: String?
    
    init(data:[[String:Any]]) {
        for i in data {
            self.fundraisingPledgesAmount = i["FundraisingPledges__amount"] as? String
            self.fundraisingPledgesFromAmount = i["FundraisingPledges__from_amount"] as? String
            self.fundraisingPledgesAmountPending = i["FundraisingPledges__amount_pending"] as? String
            self.fundraisingPledgesAdminResponse = i["FundraisingPledges__admin_response"] as? String
            self.fundraisingPledgesAnonymous = i["FundraisingPledges__anonymous"] as? String
            self.fundraisingPledgesHideAmount = i["FundraisingPledges__hide_amount"] as? String
            self.fundraisingPledgesCreated = i["FundraisingPledges__created"] as? String
            self.fundraisingPledgesModified = i["FundraisingPledges__modified"] as? String
        }
       
    }
}
/*-----------------------------------------------------------------------------------------------------*/

// MARK: - Project
class Project {
    var initiativesProjectsProjectID, initiativesProjectsID, initiativesProjectsInitiativeID, initiativesProjectsPercentage: String?
    var initiativesProjectsCreated, initiativesProjectsModified, projectsID, projectsCreatorID: String?
    var projectsKhojaCareCategoryID, projectsName, projectsWebsite, projectsDescription: String?
    var projectsStartDate, projectsEstCompvarionDate: String?
    var projectsFundraisingTarget: String?
    var projectsCity: String?
    var projectsRegion: String?
    var projectsCountry, projectsApprovalStamp, projectsStatus, projectsCreated: String?
    var projectsModified: String?
   // var projectGroups:[ProjectGroups]?
    var groupDetails: String?
    var projectFundraisingsAmount: String?
    
    init(data:[[String:Any]]) {
        for i in data {
            self.initiativesProjectsProjectID = i["InitiativesProjects__project_id"] as? String
            self.initiativesProjectsID = i["InitiativesProjects__id"] as? String
            self.initiativesProjectsInitiativeID = i["InitiativesProjects__initiative_id"] as? String
            self.initiativesProjectsPercentage = i["InitiativesProjects__percentage"] as? String
            self.initiativesProjectsCreated = i["InitiativesProjects__created"] as? String
            self.initiativesProjectsModified = i["InitiativesProjects__modified"] as? String
            self.projectsID = i["Projects__id"] as? String
            self.projectsCreatorID = i["Projects__creator_id"] as? String
            self.projectsKhojaCareCategoryID = i["Projects__khoja_care_category_id"] as? String
            self.projectsName = i["Projects__name"] as? String
            self.projectsWebsite = i["Projects__website"] as? String
            self.projectsDescription = i["Projects__description"] as? String
            self.projectsStartDate = i["Projects__start_date"] as? String
            self.projectsEstCompvarionDate = i["Projects__est_compvarion_date"] as? String
            self.projectsFundraisingTarget = i["Projects__fundraising_target"] as? String
            self.projectsCity = i["Projects__city"] as? String
            self.projectsRegion = i["Projects__region"] as? String
            self.projectsCountry = i["Projects__country"] as? String
            self.projectsApprovalStamp = i["Projects__approval_stamp"] as? String
            self.projectsStatus = i["Projects__status"] as? String
            self.projectsCreated = i["Projects__created"] as? String
            self.projectsModified = i["Projects__modified"] as? String
            self.groupDetails = i["Groups_details"] as? String
            self.projectFundraisingsAmount = "\(i["ProjectFundraisings__amount"] as? NSNumber ?? 0.0)"
          //  projectGroups = i["groups"] as? [ProjectGroups]

           
        }
      
    }
}

class ProjectGroups {
    var projectsGroupsProjectId, groupsName, projectsGroupsId, projectsGroupsGroupId, initiativesProjectsPercentage, groupsId: String?



    init(data:[[String:Any]]) {
        for i in data {
            self.projectsGroupsProjectId = i["ProjectsGroups__project_id"] as? String
            self.projectsGroupsId = i["ProjectsGroups__id"] as? String
            self.projectsGroupsGroupId = i["ProjectsGroups__group_id"] as? String
            self.initiativesProjectsPercentage = i["InitiativesProjects__percentage"] as? String
            self.groupsId = i["Groups__id"] as? String
            self.groupsName = i["Groups__name"] as? String
        }

    }
}


/*-----------------------------------------------------------------------------------------------------*/
// MARK: - Pledger
class Pledger {
    var fundraisingPledgesID, fundraisingPledgesFundraisingID, fundraisingPledgesUserID, fundraisingPledgesGroupID: String?
    var fundraisingPledgesAmount, fundraisingPledgesFromAmount, fundraisingPledgesAmountPending: String?
    var fundraisingPledgesAdminResponse: String?
    var fundraisingPledgesAnonymous, fundraisingPledgesHideAmount, fundraisingPledgesCreated, fundraisingPledgesModified: String?
    var usersID, usersRole, usersEmail, usersPassword: String?
    var usersTitle, usersFirstName, usersMiddleName, usersLastName: String?
    var usersGender, usersStatus, usersChairPerson, usersChatStatus: String?
    var usersLastOnline, usersLastLogin, usersExpirationDate: String?
    var usersPending, usersVoteDecision, usersVoteDecisionDate, usersVoteAdminDecision: String?
    var usersTerms, usersCreated, usersModified, usersResetKey, fundaraisingName, charityName: String?
    
    init(data:[[String:Any]]) {
        for i in data {
            self.fundraisingPledgesID = i["FundraisingPledges__id"] as? String
            self.fundraisingPledgesFundraisingID = i["FundraisingPledges__fundraising_id"] as? String
            self.fundraisingPledgesUserID = i["FundraisingPledges__user_id"] as? String
            self.fundraisingPledgesGroupID = i["FundraisingPledges__group_id"] as? String
            self.fundraisingPledgesAmount = i["FundraisingPledges__amount"] as? String
            self.fundraisingPledgesFromAmount = i["FundraisingPledges__from_amount"] as? String
            self.fundraisingPledgesAmountPending = i["FundraisingPledges__amount_pending"] as? String
            self.fundraisingPledgesAdminResponse = i["FundraisingPledges__admin_response"] as? String
            self.fundraisingPledgesAnonymous = i["FundraisingPledges__anonymous"] as? String
            self.fundraisingPledgesHideAmount = i["FundraisingPledges__hide_amount"] as? String
            self.fundraisingPledgesCreated = i["FundraisingPledges__created"] as? String
            self.fundraisingPledgesModified = i["FundraisingPledges__modified"] as? String
            self.usersID = i["Users__id"] as? String
            self.usersRole = i["Users__role"] as? String
            self.usersEmail = i["Users__email"] as? String
            self.usersPassword = i["Users__password"] as? String
            self.usersTitle = i["Users__title"] as? String
            self.usersFirstName = i["Users__first_name"] as? String
            self.usersMiddleName = i["Users__middle_name"] as? String
            self.usersLastName = i["Users__last_name"] as? String
            self.usersGender = i["Users__gender"] as? String
            self.usersStatus = i["Users__status"] as? String
            self.usersChairPerson = i["Users__chair_person"] as? String
            self.usersChatStatus = i["Users__chat_status"] as? String
            self.usersLastOnline = i["Users__last_online"] as? String
            self.usersLastLogin = i["Users__last_login"] as? String
            self.usersExpirationDate = i["Users__expiration_date"] as? String
            self.usersPending = i["Users__pending"] as? String
            self.usersVoteDecision = i["Users__vote_decision"] as? String
            self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.usersTerms = i["Users__terms"] as? String
            self.usersCreated = i["Users__created"] as? String
            self.usersModified = i["Users__modified"] as? String
            self.usersResetKey = i["Users__reset_key"] as? String
            self.fundaraisingName = i["fundraising"] as? String
            self.charityName = i["charity"] as? String

        }
       
    }
}

/*-----------------------------------------------------------------------------------------------------*/

// MARK: - Fundraising
class Fundraising {
    var fundraisingsID, fundraisingsUserID, fundraisingsInitiativeID, fundraisingsName: String?
    var fundraisingsDescription, fundraisingsAmount, fundraisingsType, fundraisingsBlocks: String?
    var fundraisingsMaxBlocks, fundraisingsAllowHiddenAmount, fundraisingsAllowAnonymous, fundraisingsBroadcast: String?
    var fundraisingsEmailSubject: String?
    var fundraisingsEmailBroadcast: String?
    var fundraisingsEmailBody: String?
    var fundraisingsFundraisingStatus, fundraisingsCreated, fundraisingsModified, fundraisingsStatus: String?
    var amount, totalFundraising, fundraisingCollected: String?
    var isPledge: Int?
    var pledgeBlockAmount: Double?
    var matcher = [Matcher]()
    
    init(data:[[String:Any]]) {
        for i in data {
            self.fundraisingsID = i["Fundraisings__id"] as? String
            self.fundraisingsUserID = i["Fundraisings__user_id"] as? String
            self.fundraisingsInitiativeID = i["Fundraisings__initiative_id"] as? String
            self.fundraisingsName = i["Fundraisings__name"] as? String
            self.fundraisingsDescription = i["Fundraisings__description"] as? String
            self.fundraisingsAmount = i["Fundraisings__amount"] as? String
            self.fundraisingsType = i["Fundraisings__type"] as? String
            self.fundraisingsBlocks = i["Fundraisings__blocks"] as? String
            self.fundraisingsMaxBlocks = i["Fundraisings__max_blocks"] as? String
            self.fundraisingsAllowHiddenAmount = i["Fundraisings__allow_hidden_amount"] as? String
            self.fundraisingsAllowAnonymous = i["Fundraisings__allow_anonymous"] as? String
            self.fundraisingsBroadcast = i["Fundraisings__broadcast"] as? String
            self.fundraisingsEmailSubject = i["Fundraisings__email_subject"] as? String
            self.fundraisingsEmailBroadcast = i["Fundraisings__email_broadcast"] as? String
            self.fundraisingsEmailBody = i["Fundraisings__email_body"] as? String
            self.fundraisingsFundraisingStatus = i["Fundraisings__fundraising_status"] as? String
            self.fundraisingsCreated = i["Fundraisings__created"] as? String
            self.fundraisingsModified = i["Fundraisings__modified"] as? String
            self.fundraisingsStatus = i["Fundraisings__status"] as? String
            self.amount = i["Amount"] as? String
            self.totalFundraising = i["totalfundraising"] as? String
            self.fundraisingCollected = i["fundrasingcollectedamount"] as? String
            self.pledgeBlockAmount = i["Pledge_1_Block"] as? Double
            self.isPledge = i["is_plege"] as? Int
            if let matcherData = i["matchers"] as? [[String:Any]] {
                for a in matcherData {
                    matcher.append(Matcher(data: [a]))
                }
            }
            
            
        }
       
    }
}

/*-----------------------------------------------------------------------------------------------------*/
// MARK: - Pledger
class Groups {
    var initiativesGroupsGroupId, initiativesGroupsId,
        initiativesGroups__initiative_id, groupsId,
        groups__category_id, groupsName, groups__description,
        groups__email, groups__phone, groups__address,
        groups__address2, groups__website, groups__city,
        groups__region, groups__country, groups__postal_code,
        groups__bio, groups__hidden, groups__pending,
        groups__vote_decision,
        groups__vote_decision_date, groups__created, groups__status,
        groups__donations, groups__passive: String?


    init(data:[[String:Any]]) {
        for i in data {
            self.initiativesGroupsGroupId = i["InitiativesGroups__group_id"] as? String
            self.initiativesGroupsId = i["InitiativesGroups__id"] as? String
            self.initiativesGroups__initiative_id = i["InitiativesGroups__initiative_id"] as? String
            self.groupsId = i["Groups__id"] as? String
            self.groups__category_id = i["Groups__category_id"] as? String
            self.groupsName = i["Groups__name"] as? String
            self.groups__description = i["Groups__description"] as? String
            self.groups__email = i["Groups__email"] as? String
            self.groups__phone = i["Groups__phone"] as? String
            self.groups__address = i["Groups__address"] as? String
            self.groups__address2 = i["Groups__address2"] as? String
            self.groups__website = i["Groups__website"] as? String
            self.groups__city = i["Groups__city"] as? String
            self.groups__region = i["Groups__region"] as? String
            self.groups__country = i["Groups__country"] as? String
            self.groups__postal_code = i["Groups__postal_code"] as? String
            self.groups__bio = i["Groups__bio"] as? String
            self.groups__hidden = i["Groups__hidden"] as? String
            self.groups__pending = i["Groups__pending"] as? String
            self.groups__vote_decision = i["Groups__vote_decision"] as? String
            self.groups__vote_decision_date = i["Groups__vote_decision_date"] as? String
            self.groups__created = i["Groups__created"] as? String
            self.groups__status = i["Groups__status"] as? String
            self.groups__donations = i["Groups__donations"] as? String
            self.groups__passive = i["Groups__passive"] as? String
        }

    }
}

/*-----------------------------------------------------------------------------------------------------*/

class InitiativeGroupModel {
    var groupId, groupName: String?
    
    init(data:[[String:Any]]) {
        for i in data {
            groupId = i["Groups__id"] as? String
            groupName = i["Groups__name"] as? String
        }
        
    }
}

class Matcher {
    var matcherName, matcherFullAmount, matcherAmount, hideAmount, anonymous : String?
    init(data:[[String:Any]]) {
        for i in data {
            matcherName = i["Matcher__name"] as? String
            matcherAmount = i["Matcher__amount"] as? String
            matcherFullAmount = i["Matcher__match_full_amount"] as? String
            hideAmount = i["Matcher__hide_amount"] as? String
            anonymous = i["Matcher__anonymous"] as? String
        }
    }
}
