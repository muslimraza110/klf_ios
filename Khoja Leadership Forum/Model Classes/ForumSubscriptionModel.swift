//
//  ForumSubscriptionModel.swift
//  Khoja Leadership Forum


import Foundation
class ForumSubscriptionlModel {
    var forumPostSubscriptionsID, forumPostSubscriptionsForumPostID, forumPostSubscriptionsUserID, forumPostSubscriptionsFrequency: String?
    var forumPostSubscriptionsCreated, forumPostSubscriptionsModified, forumPostsID, forumPostsUserID: String?
    var forumPostsParentID: String?
    var forumPostsSubforumID, forumPostsLft, forumPostsRght, forumPostsTopic: String?
    var forumPostsContent, forumPostsPending, forumPostsModerated, forumPostsPinned: String?
    var forumPostsSod, forumPostsNotifyUsers: String?
    var forumPostsVisibility: String?
    var forumPostsPostVisibility, forumPostsStatus, forumPostsEblast, forumPostsCreated: String?
    var forumPostsModified, postApprove: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.forumPostSubscriptionsID = i["ForumPostSubscriptions__id"] as? String
            self.forumPostSubscriptionsForumPostID = i["ForumPostSubscriptions__forum_post_id"] as? String
            self.forumPostSubscriptionsUserID = i["ForumPostSubscriptions__user_id"] as? String
            self.forumPostSubscriptionsFrequency = i["ForumPostSubscriptions__frequency"] as? String
            self.forumPostSubscriptionsCreated = i["ForumPostSubscriptions__created"] as? String
            self.forumPostSubscriptionsModified = i["ForumPostSubscriptions__modified"] as? String
            self.forumPostsID = i["ForumPosts__id"] as? String
            self.forumPostsUserID = i["ForumPosts__user_id"] as? String
            self.forumPostsParentID = i["ForumPosts__parent_id"] as? String
            self.forumPostsSubforumID = i["ForumPosts__subforum_id"] as? String
            self.forumPostsLft = i["ForumPosts__lft"] as? String
            self.forumPostsRght = i["ForumPosts__rght"] as? String
            self.forumPostsTopic = i["ForumPosts__topic"] as? String
            self.forumPostsContent = i["ForumPosts__content"] as? String
            self.forumPostsPending = i["ForumPosts__pending"] as? String
            self.forumPostsModerated = i["ForumPosts__moderated"] as? String
            self.forumPostsPinned = i["ForumPosts__pinned"] as? String
            self.forumPostsSod = i["ForumPosts__sod"] as? String
            self.forumPostsNotifyUsers = i["ForumPosts__notify_users"] as? String
            self.forumPostsVisibility = i["ForumPosts__visibility"] as? String
            self.forumPostsPostVisibility = i["ForumPosts__post_visibility"] as? String
            self.forumPostsStatus = i["ForumPosts__status"] as? String
            self.forumPostsEblast = i["ForumPosts__eblast"] as? String
            self.forumPostsCreated = i["ForumPosts__created"] as? String
            self.forumPostsModified = i["ForumPosts__modified"] as? String
            self.postApprove = i["postaprovepost"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
