
import Foundation
class NewsListModel {
    var postsID: String?
    var postsUserID: String?
    var postsCategoryID: String?
    var postsPostThemeID: String?
    var postsTopic: String?
    var postsName: String?
    var postsContent: String?
    var postsRelatedPosts: String?
    var postsActiveSlide: String?
    var postsPinned: String?
    var postsCreated: String?
    var postsModified: String?
    var postsPublished: String?
    var postsArchived: String?
    var postsNotificationEmailsSent: String?
    var postsStatus: String?
    var usersID: String?
    var usersRole: String?
    var usersEmail: String?
    var usersPassword: String?
    var usersTitle: String?
    var usersFirstName: String?
    var usersMiddleName: String?
    var usersLastName: String?
    var usersGender: String?
    var usersStatus: String?
    var usersChairPerson: String?
    var usersChatStatus: String?
    var usersLastOnline: String?
    var usersLastLogin: String?
    var usersExpirationDate: String?
    var usersPending: String?
    var usersVoteDecision: String?
    var usersVoteDecisionDate: String?
    var usersVoteAdminDecision: String?
    var usersTerms: String?
    var usersCreated: String?
    var usersModified: String?
    var usersResetKey: String?
    var isPublished : String?
    var profileImage : String?
    
    init() {}
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.postsID = i["Posts__id"] as? String
            self.postsUserID = i["Posts__user_id"] as? String
            self.postsCategoryID = i["Posts__category_id"] as? String
            self.postsPostThemeID = i["Posts__post_theme_id"] as? String
            self.postsTopic = i["Posts__topic"] as? String
            self.postsName = i["Posts__name"] as? String
            self.postsContent = i["Posts__content"] as? String
            self.postsRelatedPosts = i["Posts__related_posts"] as? String
            self.postsActiveSlide = i["Posts__active_slide"] as? String
            self.postsPinned = i["Posts__pinned"] as? String
            self.postsCreated = i["Posts__created"] as? String
            self.postsModified = i["Posts__modified"] as? String
            self.postsPublished = i["Posts__published"] as? String
            self.postsArchived = i["Posts__archived"] as? String
            self.postsNotificationEmailsSent = i["Posts__notification_emails_sent"] as? String
            self.postsStatus = i["Posts__status"] as? String
            self.usersID = i["Users__id"] as? String
            self.usersRole = i["Users__role"] as? String
            self.usersEmail = i["Users__email"] as? String
            self.usersPassword = i["Users__password"] as? String
            self.usersTitle = i["Users__title"] as? String
            self.usersFirstName = i["Users__first_name"] as? String
            self.usersMiddleName = i["Users__middle_name"] as? String
            self.usersLastName = i["Users__last_name"] as? String
            self.usersGender = i["Users__gender"] as? String
            self.usersStatus = i["Users__status"] as? String
            self.usersChairPerson = i["Users__chair_person"] as? String
            self.usersChatStatus = i["Users__chat_status"] as? String
            self.usersLastOnline = i["Users__last_online"] as? String
            self.usersLastLogin = i["Users__last_login"] as? String
            self.usersExpirationDate = i["Users__expiration_date"] as? String
            self.usersPending = i["Users__pending"] as? String
            self.usersVoteDecision = i["Users__vote_decision"] as? String
            self.usersVoteDecisionDate = i["Users__vote_decision_date"] as? String
            self.usersVoteAdminDecision = i["Users__vote_admin_decision"] as? String
            self.usersTerms = i["Users__terms"] as? String
            self.usersCreated = i["Users__created"] as? String
            self.usersModified = i["Users__modified"] as? String
            self.usersResetKey = i["Users__reset_key"] as? String
            self.isPublished = i["is_published"] as? String
            self.profileImage = i["profileimage"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
