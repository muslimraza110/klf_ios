//
//  InitiativesModel.swift
//  Khoja Leadership Forum
//
//  Created by Hamza Saeed on 30/08/2019.
//  Copyright © 2019 Adeel ilyas. All rights reserved.
//

import Foundation
// MARK: - Initiative
class InitiativeModel {
    var initiativesID, initiativesUserID, initiativesName, initiativesDescription: String?
    var initiativesShowOnDashboard, initiativesBroadcast: String?
    var initiativesEmailSubject: String?
    var initiativesEmailBroadcast: String?
    var initiativesEmailBody: String?
    var initiativesStatus, initiativesCreated, initiativesModified: String?
    
/*-----------------------------------------------------------------------------------------------------*/
    init(data:[[String:Any]]) {
        for i in data {
            self.initiativesID = i["Initiatives__id"] as? String
            self.initiativesUserID = i["Initiatives__user_id"] as? String
            self.initiativesName = i["Initiatives__name"] as? String
            self.initiativesDescription = i["Initiatives__description"] as? String
            self.initiativesShowOnDashboard = i["Initiatives__show_on_dashboard"] as? String
            self.initiativesBroadcast = i["Initiatives__broadcast"] as? String
            self.initiativesEmailSubject = i["Initiatives__email_subject"] as? String
            self.initiativesEmailBroadcast = i["Initiatives__email_broadcast"] as? String
            self.initiativesEmailBody = i["Initiatives__email_body"] as? String
            self.initiativesStatus = i["Initiatives__status"] as? String
            self.initiativesCreated = i["Initiatives__created"] as? String
            self.initiativesModified = i["Initiatives__modified"] as? String
        }
    }
/*-----------------------------------------------------------------------------------------------------*/
}
