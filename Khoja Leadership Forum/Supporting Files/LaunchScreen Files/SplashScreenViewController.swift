
import UIKit



class SplashScreenViewController: UIViewController {

    //Outlets
    @IBOutlet weak var imageView: UIImageView!
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animate_images()
        self.imageView.image = UIImage (imageLiteralResourceName: "10.png")
         let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5000)) {
            // Code
            if (UserDefaults.standard.value(forKey: "islogin") as? String == nil) {
                UserDefaults.standard.set("False", forKey: "islogin")
            }
            
            let uid = UserDefaults.standard.value(forKey: "islogin") as! String
            print(UserDefaults.standard.value(forKey: "islogin") as! String)
            if(uid == "True"){
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "ContainerViewController") as! ContainerViewController
                appDelegate!.window?.rootViewController = initialViewController
            }
            else{
                let redViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                appDelegate!.window?.rootViewController = redViewController
            }
        } /// if condition for crednetials saved, set the rootviewcontroller as dashboard instead login .. needs to be implemented

    
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
// Gif image for launchScreen Function
// MARK -  animation images
    func animate_images()
    {
        let imageArr = ["0.png","1.png","2.png","3.png","4.png","5.png","6.png","7.png","8.png","9.png","10.jpg"]
        var images = [UIImage]()
        
        for i in 0..<imageArr.count
        {
            images.append(UIImage(named: imageArr[i])!)
        }
        
        self.imageView.animationImages = images
        self.imageView.animationDuration = 2.5  //animation duration can be set from here
        self.imageView.animationRepeatCount = 1 //if repeation is required
        self.imageView.startAnimating()
    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    func animate_usingGif(){

    }
/*-----------------------------------------------------------------------------------------------------------------------------------*/
    //MAARK - Hide status Bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

/*-----------------------------------------------------------------------------------------------------------------------------------*/
