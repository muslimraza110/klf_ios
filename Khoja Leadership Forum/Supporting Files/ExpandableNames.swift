
import Foundation

struct ExpandableNames {
    
    var isExpanded : Bool
    let names: [String]
}

struct ExpandableNamesKhoja {
    
    var isExpanded : Bool
    let names: [KhojaCareListModel]
    
}

struct ExpandableDashBoard {
    
    var isExpanded : Bool
    let names: [AnyObject]
}

struct ExpandableViewProfile {
    
    var isExpanded : Bool
    let names: [UserProjectModel]
}

struct ExpandableGroupProjectMember {
    
    var isExpanded : Bool
    let names: [GroupProjectListMemberModel]
}
